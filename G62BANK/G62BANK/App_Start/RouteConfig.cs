﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace G62BANK
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // xem thông tin tài khoản internetbanking
            routes.MapRoute(
                "DetailsCustomerParent", // Route name
                "BankCustomer/DetailsCustomerParent", // URL with parameters
                new { controller = "BankCustomer", action = "DetailsCustomerParent" } // Parameter defaults
            );

            // xem chi tiết tài khoản sổ tiết kiệm
            routes.MapRoute(
                "DetailsSavingBook", // Route name
                "SavingBook/DetailsSavingBook/{code}", // URL with parameters
                new { controller = "SavingBook", action = "DetailsSavingBook", code = UrlParameter.Optional } // Parameter defaults
            );


            // xem chi tiết tài khoản thẻ
            routes.MapRoute(
                "DetailsATMAccount", // Route name
                "ATMAccount/DetailsATMAccount/{code}", // URL with parameters
                new { controller = "ATMAccount", action = "DetailsATMAccount", code = UrlParameter.Optional} // Parameter defaults
            );
            // tin tức
            routes.MapRoute(
                "News", // Route name
                "Home/News", // URL with parameters
                new { controller = "Home", action = "News"} // Parameter defaults
            );

            // tin tức
            routes.MapRoute(
                "DetailsNews", // Route name
                "Home/DetailsNews/{Url}", // URL with parameters
                new { controller = "Home", action = "DetailsNews", Url = UrlParameter.Optional } // Parameter defaults
            );

            //------------------ ADMIN --------------------//
            // login admin manager
            routes.MapRoute(
                "Login AdminManager", // Route name
                "BankManager/Login", // URL with parameters
                new { controller = "BankManager", action = "Login"} // Parameter defaults
            );
            // login admin manager
            routes.MapRoute(
                "Manager", // Route name
                "BankManager", // URL with parameters
                new { controller = "BankManager", action = "Index" } // Parameter defaults
            );
            // login admin manager
            routes.MapRoute(
                "ManagerLink", // Route name
                "BankManager/Index/{link}", // URL with parameters
                new { controller = "BankManager", action = "Index", link = UrlParameter.Optional } // Parameter defaults
            );
            //// Reset Password
            //routes.MapRoute(
            //    "ResetPassWord", // Route name
            //    "BankCustomer/ResetPassWord", // URL with parameters
            //    new { controller = "BankCustomer", action = "ResetPassWord"} // Parameter defaults
            //);
            // mặc định
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}