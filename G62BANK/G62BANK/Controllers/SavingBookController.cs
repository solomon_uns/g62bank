﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using G62BANK.Models;
using Recaptcha;
namespace G62BANK.Controllers
{
    public class SavingBookController : Controller
    {
        //
        // GET: /SavingBok/
        private BankRepository bankRepository;
        public static SavingBook CurrentSavingBook;

        //
        // GET: /BankCustomer/
        public SavingBookController()
            : this(new BankRepository())
        { }

        public SavingBookController(BankRepository bankRepository)
        {
            this.bankRepository = bankRepository;
        }
        
        [HttpGet]
        public ActionResult CreateSavingBook()
        {
            if (User.Identity.Name != null && User.Identity.Name != "")
                return View();
            else
                return RedirectToAction("Login","BankCustomer");
        }

        [HttpPost]
        [RecaptchaControlMvc.CaptchaValidatorAttribute]
        public ActionResult CreateSavingBook(string type, string money)
        {
            if (type == "") // chưa chọn loại sổ tiết kiệm
                return Json(0, JsonRequestBehavior.AllowGet);
            else
            {
                double _money = bankRepository.GetSavingBookTypeByCode(int.Parse(type)).MinimumMoney;
                try
                {
                    double sotien = double.Parse(money);
                    if (sotien >= _money && sotien % 50000 == 0)
                    {
                        Customer customer = bankRepository.GetCustomerbyUserName(User.Identity.Name);
                        SavingBook savingbook = bankRepository.CreateSavingBook(customer, int.Parse(type), double.Parse(money));
                        return Json(savingbook.SavingBookCode, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(1, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                
            }
        }

        [HttpGet]
        public ActionResult ViewListSavingBookType()
        {
            return View(bankRepository.GetListSavingBookType());
        }

        [HttpGet]
        public ActionResult SelectedSavingBookType(string code)
        {
            return View(bankRepository.GetSavingBookTypeByCode(int.Parse(code)));
        }

        [HttpGet]
        public ActionResult ViewSavingBook()
        {
            if (User.Identity.Name != null && User.Identity.Name != "")
                return View();
            else
                return RedirectToAction("Login", "BankCustomer");
        }

        [HttpGet]
        public ActionResult DetailsSavingBook(string code)
        {
            SavingBook _savingbook;
            if (code != null)
                _savingbook = bankRepository.GetSavingBookByCode(code);
            else
                _savingbook = G62BANK.Controllers.SavingBookController.CurrentSavingBook;
            SavingBookType _savingbooktype = bankRepository.GetSavingBookTypeByCode(_savingbook.SavingBookTypeCode);
            Account acc = bankRepository.GetAccountByCode(_savingbook.AccountCode);
            return View(new G62BANK.ViewModels.SavingBookModel.DetailsSavingBookModel {account = acc, savingbook = _savingbook, savingbooktype = _savingbooktype});
        }
        [HttpGet]
        public ActionResult CheckExistsSavingBook(string code)
        {
            SavingBook _savingbook = bankRepository.GetSavingBookByCode(code);
            if (_savingbook != null)
                return Json(true,JsonRequestBehavior.AllowGet);
            return Json(false, JsonRequestBehavior.AllowGet);
        }

    }
}
