﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Web.Helpers;
using System.Web.Security;
using Recaptcha;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using G62BANK.Models;
using System.Web.Services;
using System.Configuration;
namespace G62BANK.Controllers
{
    public class ATMAccountController : Controller
    {
        //
        // GET: /ATMAccount/
        private BankRepository bankRepository;
        public static ATMAccount CurrentATMAccount;

        public ATMAccountController()
            : this(new BankRepository())
        { }

        public ATMAccountController(BankRepository bankRepository)
        {
            this.bankRepository = bankRepository;
        }
        public ActionResult ViewBankCustomer()
        {
            Customer cus = bankRepository.GetCustomerbyUserName(User.Identity.Name);
            if (cus != null)
                return PartialView(cus);
            else
                return View();
        }

        [HttpGet]
        public ActionResult OpenATMAccount()
        {
            if (User.Identity.Name != null && User.Identity.Name != "")
                return View();
            else
                return RedirectToAction("Login","BankCustomer");
        }

        [HttpGet]
        public ActionResult ValidateCaptcha(string challengeValue, string responseValue)
        {
            Recaptcha.RecaptchaValidator captchaValidtor = new Recaptcha.RecaptchaValidator
            {
                PrivateKey = "6LdkXNsSAAAAAF4HW1ZCfq58bfB5uO-kVrm2Vn1G", // Get Private key of the CAPTCHA from Web.config file.
                RemoteIP = System.Web.HttpContext.Current.Request.UserHostAddress,
                Challenge = challengeValue,
                Response = responseValue
            };

            Recaptcha.RecaptchaResponse recaptchaResponse = captchaValidtor.Validate(); // Send data about captcha validation to reCAPTCHA site.
            if (recaptchaResponse.IsValid) // Get boolean value about Captcha success / failure.
                return Json(true,JsonRequestBehavior.AllowGet);
            else
                return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult OpenATMAccount(string code)
        {
            try
            {
                ATMAccount atm = bankRepository.CreateATMAccount(bankRepository.GetCustomerbyUserName(User.Identity.Name), int.Parse(code));
                return Json(atm.ATMAcountCode, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DetailsATMAccount(string code)
        {
            ATMAccount atm;
            if (code != null)
            {
                atm = bankRepository.GetATMAccountByCode(code);
                G62BANK.Controllers.ATMAccountController.CurrentATMAccount = atm;
                Account acc = bankRepository.GetAccountByCode(atm.AccountCode);
                G62BANK.Controllers.AccountController.CurrentAccount = acc;
                if (atm.ATMAccountTypeCode == 1)// debitcard
                    return RedirectToAction("DetailsATMAccount_DebitCard", "ATMAccount", new { code = atm.ATMAcountCode });
                else // creditcard
                    return RedirectToAction("DetailsATMAccount_CreditCard", "ATMAccount", new { code = atm.ATMAcountCode });
            }
            else
            {
                return RedirectToAction("OpenATMAccount", "ATMAccount");
            }
        }

        public ActionResult DetailsATMAccount_DebitCard(string code)
        {
            ATMAccount atm;
            if (code != null)
                atm = bankRepository.GetATMAccountByCode(code);
            else
                atm = G62BANK.Controllers.ATMAccountController.CurrentATMAccount;

            G62BANK.ViewModels.ATMModel.DebitCardModel view = new ViewModels.ATMModel.DebitCardModel();

            Account acc = new Account();
            acc = bankRepository.GetAccountByCode(atm.AccountCode);

            ATMAccountType acctype = bankRepository.GetAtmAccountTypeByCode(atm.ATMAccountTypeCode.ToString());

            // hien tai chi lam 1 thẻ có trạng thái enable cho 1 tài khoản
            DebitCard debitcard = bankRepository.FindDebitCardByATMAccountCode(atm.ATMAcountCode);

            view.account = acc;
            view.atmaccount = atm;
            view.atmaccounttype = acctype;
            view.debitcard = debitcard;

            return View(view);
        }

        public ActionResult DetailsATMAccount_CreditCard(string code)
        {
            ATMAccount atm;
            if (code != null)
                atm = bankRepository.GetATMAccountByCode(code);
            else
                atm = G62BANK.Controllers.ATMAccountController.CurrentATMAccount;

            G62BANK.ViewModels.ATMModel.CreditCardModel view = new ViewModels.ATMModel.CreditCardModel();

            Account acc = new Account();
            acc = bankRepository.GetAccountByCode(atm.AccountCode);

            ATMAccountType acctype = bankRepository.GetAtmAccountTypeByCode(atm.ATMAccountTypeCode.ToString());

            // hien tai chi lam 1 thẻ có trạng thái enable cho 1 tài khoản
            CreditCard creditcard = bankRepository.FindCreditCardByATMAccountCode(atm.ATMAcountCode);

            view.account = acc;
            view.atmaccount = atm;
            view.atmaccounttype = acctype;
            view.creditcard = creditcard;

            return View(view);
        }

        public ActionResult DetailsATMAccount_CreditCard_2(string code)
        {
            try
            {
                ATMAccount atm = bankRepository.GetATMAccountByAccountCode(code);

                return RedirectToAction("DetailsATMAccount_CreditCard", "ATMAccount", new { code = atm.ATMAcountCode });
            }
            catch (Exception)
            {
                return null;
            }
            
        }

        [HttpGet]
        public ActionResult ViewListATMAccount()
        {
            if (User.Identity.Name != null && User.Identity.Name != "")
            {
                //bankRepository.GetCustomerbyUserName(User.Identity.Name)
                return View();
            }
            else
                return RedirectToAction("Login", "BankCustomer");
        }
        
    }
}
