﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace G62BANK.Controllers
{
    public class OtherController : Controller
    {
        //
        // GET: /Other/

        [HttpGet]
        public ActionResult SlideShow()
        {
            List<string> list = new List<string>();
            list.Add("../../Content/slider/1.jpg");
            list.Add("../../Content/slider/2.jpg");
            list.Add("../../Content/slider/3.jpg");
            list.Add("../../Content/slider/4.jpg");
            return View(list);
        }

        [HttpGet]
        public ActionResult EmptyView(string v)
        {
            return View(v);
        }


    }
}
