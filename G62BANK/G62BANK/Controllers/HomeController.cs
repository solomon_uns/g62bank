﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Data;
using System.Net;
using System.IO;
using System.Text;

namespace G62BANK.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult abc(string mes)
        {
            return View();
        }

        public ActionResult News()
        {
            List<G62BANK.ViewModels.HomeModel.News> list = new List<ViewModels.HomeModel.News>();
            try
            {
                string url = "http://vnexpress.net/rss/gl/trang-chu.rss";
                XmlTextReader reader = new XmlTextReader(url);

                DataSet ds = new DataSet();
                ds.ReadXml(reader);


                for (int i = 0; i < ds.Tables["item"].Rows.Count; i++)
                {
                    G62BANK.ViewModels.HomeModel.News n = new ViewModels.HomeModel.News();
                    n.title = ds.Tables["item"].Rows[i]["title"].ToString();
                    n.link = ds.Tables["item"].Rows[i]["link"].ToString();
                    n.description = ds.Tables["item"].Rows[i]["description"].ToString();
                    n.pubdate = ds.Tables["item"].Rows[i]["pubdate"].ToString();
                    list.Add(n);
                }
            }
            catch (Exception)
            {
            }
            
            return View(list);
        }

        public ActionResult DetailsNews(string Url)
        {
            if (string.IsNullOrEmpty(Url))
            {
                ViewData["content"] = "Không tìm thấy tin này!";
                return View();
            }
            try
            {
                string html = "";
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(Url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                html = reader.ReadToEnd();
                response.Close();
                reader.Close();

                int t = 0;
                int b = 0;
                t = html.IndexOf("<div class=\"content\"");
                if (t > 0)
                    html = html.Substring(t + 21, html.Length - t - 22);

                b = html.LastIndexOf("class=\"tag-parent\"");
                html = html.Substring(0, b - 5);

                html = html.Replace("http://vnexpress.net", "DetailsNews?url=http://vnexpress.net");
                html = html.Replace("\"/gl/", "DetailsNews?url=http://vnexpress.net/gl/");
                html = html.Replace("/Files", "http://vnexpress.net/Files");
                html = html.Replace("/Library", "http://vnexpress.net/Library");
                html = html.Replace("/Images", "http://vnexpress.net/Images");
                html = html.Replace("/Service", "http://vnexpress.net/Service");
                ViewData["content"] = html;
                return View();
            }
            catch (Exception)
            {
                ViewData["content"] = "Không tìm thấy tin này!";
                return View();
            }
            
        }

        public ActionResult TopNews()
        {
            List<G62BANK.ViewModels.HomeModel.News> list = new List<ViewModels.HomeModel.News>();
            try
            {
                string url = "http://vnexpress.net/rss/gl/trang-chu.rss";
                XmlTextReader reader = new XmlTextReader(url);

                DataSet ds = new DataSet();
                ds.ReadXml(reader);


                for (int i = 0; i < 20; i++)
                {
                    try
                    {
                        G62BANK.ViewModels.HomeModel.News n = new ViewModels.HomeModel.News();
                        n.title = ds.Tables["item"].Rows[i]["title"].ToString();
                        n.link = ds.Tables["item"].Rows[i]["link"].ToString();
                        n.description = ds.Tables["item"].Rows[i]["description"].ToString();
                        n.pubdate = ds.Tables["item"].Rows[i]["pubdate"].ToString();
                        list.Add(n);
                    }
                    catch (Exception)
                    {
                    }

                }
            }
            catch (Exception)
            {
            }
            
            return View(list);
        }

        public ActionResult Forum()
        {
            return View();
        }
    }
}
