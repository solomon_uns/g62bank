﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using G62BANK.Models;
using System.Web.Security;
using System.IO;
using System.Web.Helpers;

namespace G62BANK.Controllers
{
    public class BankManagerController : Controller
    {
        //
        // GET: /BankManager/
         private BankRepository bankRepository;

        //
        // GET: /BankCustomer/
        public BankManagerController()
            : this(new BankRepository())
        { }

        public BankManagerController(BankRepository bankRepository)
        {
            this.bankRepository = bankRepository;
        }


        #region Đăng nhập
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginManager model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("LoginError", "Login Failed!");
                return View();
            }
            else
            {
                if (bankRepository.LoginManager(model.UserName, model.Password))
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, createPersistentCookie: false);
                    return RedirectToAction("Index", "BankManager");
                }
                else
                {
                    ModelState.AddModelError("LoginError", "Login Failed!");
                    return View();
                }
            }
        }
        #endregion

        #region Hiện Thông Báo
        [HttpPost]
        public ActionResult Messages(Meassages mes)
        {
            List<string> list = new List<string>();
            if (mes != null){
                if( mes._mes != null){
                    string[] temp = mes._mes.Split('|');
                    foreach (string str in temp)
                        list.Add(str);
                }
            }
            else{
                list.Add("Không Thể Thực Hiện Thao Tác");
            }
            return View(list);
        }
        #endregion

        #region Trang Chủ
        public ActionResult Index(string link)
        {
            if (link == null)
            {
                if (User.Identity.Name != null && User.Identity.Name != "")
                {
                    if (bankRepository.CheckExistsUser(User.Identity.Name))
                        return View();
                    else
                        return RedirectToAction("Login", "BankManager");
                }

                return RedirectToAction("Login", "BankManager");
            }
            else
            {
                return View();
            }
        }
        #endregion

        #region Đăng Xuất
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "BankManager");
        }
        #endregion

        #region Profile
        [HttpGet]
        public ActionResult Profile()
        {
            ModelState.Clear();
            if (User.Identity.Name != null && User.Identity.Name != "")
            {
                return View(bankRepository.GetUserManagerByUserName(User.Identity.Name));
            }
            else
                return RedirectToAction("Login", "BankManager");

        }

        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Profile(UserManager user)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("Error", "Có lỗi, vui lòng kiểm tra lại!");
                return View(bankRepository.GetUserManagerByUserName(User.Identity.Name));
            }
            else
            {
                if (User.Identity.Name != null && User.Identity.Name != "")
                {
                    // change 
                    if (Request.Files.Count > 0)
                        ChangeAvatar("",Request.Files[0]);
                    bankRepository.ChangeProfile(user, User.Identity.Name);
                    return View(bankRepository.GetUserManagerByUserName(User.Identity.Name));
                }
                else
                    return RedirectToAction("Login", "BankManager");
            }
        }

        public bool ChangeAvatar(string usercode, HttpPostedFileBase file)
        {
            try
            {
                string physicalPath = HttpContext.Server.MapPath("../") + "Content\\Manager\\UploadAvatar" + "\\";
                file.SaveAs(physicalPath + System.IO.Path.GetFileName(file.FileName));
                if(usercode == null || usercode == "")
                    bankRepository.ChangeAvatar(bankRepository.GetUserManagerByUserCode(int.Parse(usercode)).UserName, file.FileName);
                else
                    bankRepository.ChangeAvatar(bankRepository.GetUserManagerByUserCode(int.Parse(usercode)).UserName, file.FileName);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region Role
        public ActionResult CheckRole()
        {
            return View(bankRepository.GetUserManagerByUserName(User.Identity.Name).RoleCode);
        }
        public ActionResult ListRole()
        {
            return View(bankRepository.GetListRoleManager());
        }
        #endregion

        #region Quản Lý Nhân Viên
        public ActionResult StaffManager()
        {
            return View(bankRepository.GetListUserManager());
        }

        [HttpGet]
        public ActionResult EditUserManager(string username)
        {
            try
            {
                if (username != null)
                    return View(bankRepository.GetUserManagerByUserName(username));
                else
                    return View("StaffManager", "BankManager");
            }
            catch (Exception)
            {
                return View("StaffManager", "BankManager");
            }
            
        }

        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditUserManager(UserManager user)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Json(false, JsonRequestBehavior.AllowGet);
                else
                    return Json(bankRepository.EditUser(user), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            
        }

        [HttpGet]
        public ActionResult AddUserManager()
        {
            return View();
        }

        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddUserManager(UserManager user)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Json(false, JsonRequestBehavior.AllowGet);
                else
                {
                    if (user.UserName != null && user.UserName != "")
                        return Json(bankRepository.CreateUserManager(user), JsonRequestBehavior.AllowGet);
                    else
                        return Json(false, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            
        }

        [OutputCache(Duration = 0)]
        [HttpGet]
        public ActionResult BeforeDelete(string username)
        {
            return PartialView(bankRepository.GetUserManagerByUserName(username));
        }

        [HttpGet]
        public ActionResult AcceptDelete(string username)
        {
            try
            {
                //bankRepository.DeleteUserManager(User.Identity.Name, username);
                return Json(bankRepository.DeleteUserManager(User.Identity.Name, username), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [OutputCache(Duration = 0)]
        [HttpGet]
        public ActionResult BeforeResetPassword(string username)
        {
            return PartialView(bankRepository.GetUserManagerByUserName(username));
        }

        [HttpGet]
        public ActionResult AcceptResetPassword(string username)
        {
            try
            {
                return Json(bankRepository.RestPasswordUserManager(username), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ShowAvatar()
        {
            return View(bankRepository.GetUserManagerByUserName(User.Identity.Name));
        }
        #endregion

        #region Quản Lý Khách Hàng
        [HttpGet]
        public ActionResult CustomerManager()
        {
            return View();
        }


        [HttpGet]
        public ActionResult CustomerReview(string CustomerID)
        {
            return PartialView(bankRepository.GetCustomerbyCMND(CustomerID));
        }

        [HttpPost]
        public ActionResult CustomerReview(Customer customer)
        {
            if (ModelState.IsValid)
                bankRepository.EditCustomer(customer);

            return RedirectToAction("Index", "BankManager", new { @link = "AccountManager" });

        }

        [HttpGet]
        public ActionResult CreateCustomer()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateCustomer(Customer cus)
        {
            Customer customer = bankRepository.MCreateCustomer(cus);
            if(customer != null)
                return Json(customer,JsonRequestBehavior.AllowGet);
            return Json(false,JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ListCustomer()
        {
            return View(bankRepository.GetListCustomer());
        }

        [OutputCache(Duration = 0)]
        [HttpGet]
        public ActionResult BeforeDeleteCustomer(string username)
        {
            return PartialView(bankRepository.GetCustomerbyUserName(username));
        }

        [HttpGet]
        public ActionResult AcceptDeleteCustomer(string username)
        {
            try
            {
                return Json(bankRepository.DeleteCustomer(username), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [OutputCache(Duration = 0)]
        [HttpGet]
        public ActionResult BeforeResetPasswordCustomer(string username)
        {
            return PartialView(bankRepository.GetCustomerbyUserName(username));
        }

        [HttpGet]
        public ActionResult AcceptResetPasswordCustomer(string username)
        {
            try
            {
                return Json(bankRepository.RestPasswordCustomer(username), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Quản lý Account
        public ActionResult AccountManager()
        {
            AccountManager am = new AccountManager();
            am.NewDebitCard = bankRepository.Count_ATM_Debit_Resgis();
            am.NewCreditCard = bankRepository.Count_ATM_Credit_Resgis();
            return View(am);
        }

        public bool CheckAccount(string code)
        {
            Account acc = bankRepository.FindAccount(code);
            if (acc != null)
                return true;
            return false;
        }

        [HttpGet]
        public ActionResult CheckAccountExists(string code)
        {
            Account acc = bankRepository.FindAccount(code);
            if (acc != null)
                return Json(acc.FullName,JsonRequestBehavior.AllowGet);
            else
                return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult SearchDebitCard() 
        {
            return View();
        }
        [HttpGet]
        public ActionResult SearchCreditCard() 
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetAtmAccount(string code)
        {
            try
            {
                return Json(bankRepository.GetATMAccountByAccountCode(code).ATMAcountCode, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false,JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DetailsATMAccount(string code)
        {
            ATMAccount atm;
            if (code != null)
                atm = bankRepository.GetATMAccountByCode(code);
            else
                atm = G62BANK.Controllers.ATMAccountController.CurrentATMAccount;
            if (atm.ATMAccountTypeCode == 1)// debitcard
                return RedirectToAction("DetailsATMAccount_DebitCard", "BankManager", new { code = atm.ATMAcountCode });
            else // creditcard
                return RedirectToAction("DetailsATMAccount_CreditCard", "BankManager", new { code = atm.ATMAcountCode });
        }

        public ActionResult DetailsATMAccount_DebitCard(string code)
        {
            ATMAccount atm;
            if (code != null)
                atm = bankRepository.GetATMAccountByCode(code);
            else
                atm = G62BANK.Controllers.ATMAccountController.CurrentATMAccount;

            G62BANK.ViewModels.ATMModel.DebitCardModel view = new ViewModels.ATMModel.DebitCardModel();

            Account acc = new Account();
            acc = bankRepository.GetAccountByCode(atm.AccountCode);

            ATMAccountType acctype = bankRepository.GetAtmAccountTypeByCode(atm.ATMAccountTypeCode.ToString());

            // hien tai chi lam 1 thẻ có trạng thái enable cho 1 tài khoản
            DebitCard debitcard = bankRepository.FindDebitCardByATMAccountCode(atm.ATMAcountCode);

            view.account = acc;
            view.atmaccount = atm;
            view.atmaccounttype = acctype;
            view.debitcard = debitcard;

            return View(view);
        }

        public ActionResult DetailsATMAccount_CreditCard(string code)
        {
            ATMAccount atm;
            if (code != null)
                atm = bankRepository.GetATMAccountByCode(code);
            else
                atm = G62BANK.Controllers.ATMAccountController.CurrentATMAccount;

            G62BANK.ViewModels.ATMModel.CreditCardModel view = new ViewModels.ATMModel.CreditCardModel();

            Account acc = new Account();
            acc = bankRepository.GetAccountByCode(atm.AccountCode);

            ATMAccountType acctype = bankRepository.GetAtmAccountTypeByCode(atm.ATMAccountTypeCode.ToString());

            // hien tai chi lam 1 thẻ có trạng thái enable cho 1 tài khoản
            CreditCard creditcard = bankRepository.FindCreditCardByATMAccountCode(atm.ATMAcountCode);

            view.account = acc;
            view.atmaccount = atm;
            view.atmaccounttype = acctype;
            view.creditcard = creditcard;

            return View(view);
        }

        [HttpGet]
        public ActionResult SearchCreditCardResult(SearchCreditCard sear)
        {
            return View(bankRepository.SearchCreditCard(sear));
        }

        [HttpGet]
        public ActionResult SearchDebitCardResult(SearchDebitCard sear)
        {
            return View(bankRepository.SearchDebitCard(sear));
        }

        [HttpGet]
        public ActionResult CheckDebitCardRegister()
        {
            return View(bankRepository.GetListAccount_ATM_Debit_Resgis());
        }

        [HttpPost]
        public ActionResult CheckDebitCardRegister(string data)
        {
            List<string> list = new List<string>();
            string[] temp = data.Split('-');
            for (int i = 1; i < temp.Count(); i++)
                list.Add(temp[i]);
            if (data.Contains("Accept"))
                return Json(bankRepository.ProcessAcceptRegisDebitCard(list), JsonRequestBehavior.AllowGet);
            else
                return Json(bankRepository.ProcessDeleteRegisDebitCard(list), JsonRequestBehavior.AllowGet);            
        }

        [HttpGet]
        public ActionResult CheckCreditCardRegister()
        {
            return View(bankRepository.GetListAccount_ATM_Credit_Resgis());
        }

        [HttpPost]
        public ActionResult CheckCreditCardRegister(string data)
        {
            List<string> list = new List<string>();
            string[] temp = data.Split('-');
            for (int i = 1; i < temp.Count(); i++)
                list.Add(temp[i]);
            if (data.Contains("Accept"))
                return Json(bankRepository.ProcessAcceptRegisCreditCard(list), JsonRequestBehavior.AllowGet);
            else
                return Json(bankRepository.ProcessDeleteRegisCreditCard(list), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult LoanCreditCard()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult LoanCreditCard_Post(string code, string money)
        {
            try
            {
                return Json(bankRepository.LoanCreditCard(code, double.Parse(money)), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                return Json(false, JsonRequestBehavior.AllowGet);
            }
            
        }
        #endregion

        #region Quản Lý SavingBook

        public ActionResult SavingBookManager()
        {
            return View();
        }

        public bool CheckSavingBook(string code)
        {
            if (bankRepository.GetSavingBookByCode(code) != null)
                return true;
            return false;
        }

        [HttpGet]
        public ActionResult OpenSavingBook()
        {
            return View();
        }

        [HttpPost]
        public ActionResult OpenSavingBook(CreateSavingBook createsavingbook)
        {
            try
            {
                SavingBook sav = bankRepository.CreateSavingBook(bankRepository.GetCustomerbyCMND(createsavingbook.CustomerID), int.Parse(createsavingbook.SavingBookTypeCode), createsavingbook.Savingbook.Money);
                if(sav != null)
                    return Json(sav.SavingBookCode, JsonRequestBehavior.AllowGet);
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ListSavingBookType()
        {
            return View(bankRepository.GetListSavingBookType());
        }

        [HttpPost]
        public ActionResult DetailsSavingBook(string code)
        {
            SavingBook _savingbook;
            if (code != null)
                _savingbook = bankRepository.GetSavingBookByCode(code);
            else
                _savingbook = G62BANK.Controllers.SavingBookController.CurrentSavingBook;
            SavingBookType _savingbooktype = bankRepository.GetSavingBookTypeByCode(_savingbook.SavingBookTypeCode);
            Account acc = bankRepository.GetAccountByCode(_savingbook.AccountCode);
            return View(new G62BANK.ViewModels.SavingBookModel.DetailsSavingBookModel { account = acc, savingbook = _savingbook, savingbooktype = _savingbooktype });
        }

        [HttpGet]
        public ActionResult SearchSavingBook()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SearchSavingBookResult(SearchSavingBook sear)
        {
            return View(bankRepository.SearchSavingBook(sear));
        }

        #region Rút tiền trước hạn
        [HttpPost]
        public ActionResult GetMoneySavingBook(string code)
        {
            return View(bankRepository.GetSavingBookByCode(code));
        }

        [HttpPost]
        public ActionResult GetCostGetMoney()
        {
            try
            {
                return Json(bankRepository.GetCostGetMoney(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetMoneySavingBook_Submit(string code, string money)
        {
            double res = bankRepository.GetMoneySavingBook(code, money);
            if (res != -1)
                return Json(true, JsonRequestBehavior.AllowGet);
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CheckGetMoneySavingBook(string code, string money)
        {
            try
            {
                List<string> result = bankRepository.CheckGetMoney(code, money);
                if (result.Count() == 1)
                    return Json(false, JsonRequestBehavior.AllowGet);
                else
                    return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetStringGetMoneySavingBook(string code, string money)
        {
            try
            {
                List<string> result = bankRepository.CheckGetMoney(code, money);
                if (result.Count() == 1)
                    return Json(false, JsonRequestBehavior.AllowGet);
                else
                    return Json(new Meassages { _mes = result[1] }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Đáo hạn - tất toán sổ đúng hạn
        [HttpPost]
        public ActionResult MaturitySavingBook(string code)
        {
            return View(bankRepository.GetSavingBookByCode(code));
        }

        [HttpPost]
        public ActionResult GetCostMaturity()
        {
            try
            {
                return Json(bankRepository.GetCostMaturity(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult MaturitySavingBook_Submit(string code)
        {
            double res = bankRepository.MaturitySavingBook(code);
            if (res != -1)
                return Json(true, JsonRequestBehavior.AllowGet);
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CheckMaturitySavingBook(string code) // tính lai thoi
        {
            try
            {
                List<string> result = bankRepository.CheckMaturity(code,true);
                if (result.Count() == 1)
                    return Json(false, JsonRequestBehavior.AllowGet);
                else
                    return Json(result[0], JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetStringMaturitySavingBook(string code)
        {
            try
            {
                List<string> result = bankRepository.CheckMaturity(code,false);
                if (result.Count() == 1)
                    return Json(false, JsonRequestBehavior.AllowGet);
                else
                    return Json(new Meassages { _mes = result[1] }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Quá hạn -tất toán sổ quá hạn
        [HttpPost]
        public ActionResult TimeOutSavingBook(string code)
        {
            return View(bankRepository.GetSavingBookByCode(code));
        }

        [HttpPost]
        public ActionResult GetCostTimeOut()
        {
            try
            {
                return Json(bankRepository.GetCostTimeOut(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult TimeOutSavingBook_Submit(string code)
        {
            double res = bankRepository.TimeOutSavingBook(code);
            if (res != -1)
                return Json(true, JsonRequestBehavior.AllowGet);
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CheckTimeOutSavingBook(string code) // tính lai thoi
        {
            try
            {
                List<string> result = bankRepository.CheckTimeOut(code, true);
                if(result.Count > 0 && result[0] == "false")
                    return Json(false, JsonRequestBehavior.AllowGet);
                else
                    return Json(result[1]+"|"+result[2]+"|"+result[3], JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetStringTimeOutSavingBook(string code)
        {
            try
            {
                List<string> result = bankRepository.CheckTimeOut(code, false);
                if (result.Count > 0 && result[0] == "false")
                    return Json(false, JsonRequestBehavior.AllowGet);
                else
                    return Json(new Meassages { _mes = result[1] }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #endregion

        #region Quản Lý Phí Giao Dịch
        [HttpGet]
        public ActionResult CostManager()
        {
            return View(bankRepository.GetListTransactionType());
        }

        [HttpPost]
        public ActionResult EditTransaction_Get(string code)
        {
            return View(bankRepository.GetTransactionTypeByCode(int.Parse(code)));
        }

        [HttpPost]
        public ActionResult EditTransaction_Post(TransactionType tran)
        {
            return Json(bankRepository.EditTransactionType(tran),JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Quản Lý Lãi Suất
        [HttpGet]
        public ActionResult RateManager()
        {
            return View();
        }

        #region Lãi Suất Thẻ ATM
        [HttpPost]
        public ActionResult RateATM_Get()
        {
            return View(bankRepository.GetListAtmAccountType());
        }

        [HttpPost]
        public ActionResult EditRateATM_Get(string code)
        {
            return View(bankRepository.GetAtmAccountTypeByCode(code));
        }

        [HttpPost]
        public ActionResult EditRateATM_Post(ATMAccountType atm)
        {
            return Json(bankRepository.EditATMAccountType(atm),JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Lãi Suất Sổ Tiết Kiệm
        [HttpPost]
        public ActionResult RateSavingBookType_Get()
        {
            return View(bankRepository.GetListSavingBookType());
        }

        [HttpPost]
        public ActionResult EditRateSavingBookType_Get(string code)
        {
            return View(bankRepository.GetSavingBookTypeByCode(int.Parse(code)));
        }

        [HttpPost]
        public ActionResult EditRateSavingBookType_Post(SavingBookType sav)
        {
            return Json(bankRepository.EditSavingBookType(sav), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteRateSavingBookType(string code)
        {
            try
            {
                return Json(bankRepository.DeleteSavingBookType(int.Parse(code)), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult CreateNewSavingBookType()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateNewSavingBookType_Post(SavingBookType sav)
        {
            try
            {
                return Json(bankRepository.CreateSavingBookType(sav), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return View();
        }
        #endregion

        #endregion

        #region Nạp Tiền

        [HttpGet]
        public ActionResult PlusMoneyDebitCardIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult PlusMoneyDebitCardIn(PlusMoneyDebitCardIn model)
        {
            if (!ModelState.IsValid)
            {
                return Json("Thông tin không chính xác", JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (bankRepository.PlusMoneyDebitCardIn(model))
                    return Json(true, JsonRequestBehavior.AllowGet);
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            
        }
        [HttpPost]
        public ActionResult GetStringPlusMoneyDebitCardIn(PlusMoneyDebitCardIn model)
        {
            try
            {
                Meassages mes = new Meassages();
                mes = bankRepository.GetStringPlusMoneyDebitCardIn(model);
                if(mes != null)
                    return Json(mes, JsonRequestBehavior.AllowGet);
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false,JsonRequestBehavior.AllowGet);
            }
        }
        
        #endregion

        #region Chuyển Tiền

        /// <summary>
        /// ATM to ATM
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Tranfers_ATM_to_ATM()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Tranfers_ATM_to_ATM(TranferATMtoATM model)
        {
            if (!ModelState.IsValid)
            {
                return Json("Thông tin không chính xác", JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (bankRepository.TranfersMoneyATMtoATM(model))
                    return Json(true, JsonRequestBehavior.AllowGet);
                return Json("Thông tin không chính xác", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetStringTranfersMoneyATMtoATM(TranferATMtoATM model)
        {
            try
            {
                Meassages mes = bankRepository.GetStringTranfersMoneyATMtoATM(model);
                if (mes != null)
                    return Json(mes, JsonRequestBehavior.AllowGet);
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetCostTranfersATMtoATM(string code)
        {
            try
            {
                TransactionType tra = bankRepository.GetTransactionTypeByCode(17);
                if (tra != null)
                {
                    return Json((double)((tra.Cost * double.Parse(code)) / 100), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// ATM to Person
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Tranfers_ATM_to_Person()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Tranfers_ATM_to_Person(TranferATMtoPerson model)
        {
            if (!ModelState.IsValid)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            else
            {
                TranferATMtoPerson tra = bankRepository.TranfersMoneyATMtoPerson(model);

                if (tra != null)
                    return Json(tra.TransactionTranferCode, JsonRequestBehavior.AllowGet);
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetStringTranfersMoneyATMtoPerson(TranferATMtoPerson model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Meassages mes = bankRepository.GetStringTranfersMoneyATMtoPerson(model);
                    if (mes != null)
                        return Json(mes, JsonRequestBehavior.AllowGet);
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetCostTranfersATMtoPerson(string code)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    TransactionType tra = bankRepository.GetTransactionTypeByCode(13);
                    if (tra != null)
                    {
                        return Json((double)((tra.Cost * double.Parse(code)) / 100), JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// Person to Person
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Tranfers_Person_to_Person()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Tranfers_Person_to_Person(TranferPersontoPerson model)
        {
            if (!ModelState.IsValid)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            else
            {
                TranferPersontoPerson tra = bankRepository.TranfersMoneyPersontoPerson(model);

                if (tra != null)
                    return Json(tra.TransactionTranferCode, JsonRequestBehavior.AllowGet);
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetStringTranfersMoneyPersontoPerson(TranferPersontoPerson model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Meassages mes = bankRepository.GetStringTranfersMoneyPersontoPerson(model);
                    if (mes != null)
                        return Json(mes, JsonRequestBehavior.AllowGet);
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetCostTranfersPersontoPerson(string code)
        {
            try
            {
                TransactionType tra = bankRepository.GetTransactionTypeByCode(14);
                if (tra != null)
                {
                    return Json((double)((tra.Cost * double.Parse(code)) / 100), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Person Get Money Tranfers From ATMAccount
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult PersonGetMoney_Tranfers_ATM_to_Person()
        {
            return View();
        }

        [HttpPost]
        public ActionResult PersonGetMoney_Tranfers_ATM_to_Person(TranferATMtoPerson model)
        {
            if (!ModelState.IsValid)
                return Json(false, JsonRequestBehavior.AllowGet);
            else
                return Json(bankRepository.PersonGetMoney_TranfersMoneyATMtoPerson(model), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Person Get Money Tranfers From Person
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult PersonGetMoney_Tranfers_Person_to_Person()
        {
            return View();
        }

        [HttpPost]
        public ActionResult PersonGetMoney_Tranfers_Person_to_Person(TranferPersontoPerson model)
        {
            if (!ModelState.IsValid)
                return Json(false, JsonRequestBehavior.AllowGet);
            else
                return Json(bankRepository.PersonGetMoney_TranfersMoneyPersontoPerson(model), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Trả lãi Credit Card
        [HttpPost]
        public ActionResult ReturnRateCreditCard()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SearchReturnRateCreditCard_Result(SearchReturnRateCreditCard sear)
        {
            return View(bankRepository.SearchLoanCreditCard(sear));
        }

        [HttpPost]
        public ActionResult ReturnRateCreditCard_Post(string code)
        {
            return View(bankRepository.GetCreditCardLoan(code));
        }
        
        [HttpPost]
        public ActionResult CheckOverCreditCard(string code)
        {
            try
            {
                return Json(bankRepository.CheckOverCreditCard(code), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false,JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ReturnRateCreditCard_Path_Over(string code, string money)
        {
            try 
	        {
                return Json(bankRepository.ReturnRateCreditCard_Path_Over(code,double.Parse(money)), JsonRequestBehavior.AllowGet);
	        }
	        catch (Exception)
	        {
                return Json(false, JsonRequestBehavior.AllowGet);
	        }
        }
        #endregion

        #region Thống kê

        [HttpGet]
        public ActionResult ChartProfit()
        {
            Bank bank = bankRepository.GetBank();
            var chart = new Chart(width: 600, height: 400).AddSeries(
                            chartType: "pie",
                            xValue: new[] { "CreditCard", "DebitCard", "SavingBook", "Customer", "Other" },
                            yValues: new[] { bank.Profit_ATM_CreditCard, bank.Profit_ATM_DebitCard, bank.Profit_SavingBook, bank.Profit_Customer, bank.Profit_Other })
                            .GetBytes("png"
                            );
            return File(chart, "image/bytes");
        }

        [HttpGet]
        public ActionResult ChartTotalProceeds()
        {
            Bank bank = bankRepository.GetBank();
            var chart = new Chart(width: 600, height: 400).AddSeries(
                            chartType: "line",
                            xValue: new[] { "CreditCard", "DebitCard", "SavingBook", "Customer", "Other" },
                            yValues: new[] { bank.TotalProceedsCreditCard, bank.TotalProceedsDebitCard, bank.TotalProceedsSavingBook, bank.TotalProceedsCustomer, bank.TotalProceedsOther })
                            .GetBytes("png"
                            );
            return File(chart, "image/bytes");
        }
        
        #endregion

    }
}
