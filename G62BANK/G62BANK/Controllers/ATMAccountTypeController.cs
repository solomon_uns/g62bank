﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using G62BANK.Models;

namespace G62BANK.Controllers
{
    public class ATMAccountTypeController : Controller
    {
        //
        // GET: /ATMAccountType/
        private BankRepository bankRepository;

        //
        // GET: /BankCustomer/
        public ATMAccountTypeController()
            : this(new BankRepository())
        { }

        public ATMAccountTypeController(BankRepository bankRepository)
        {
            this.bankRepository = bankRepository;
        }

        [HttpGet]
        public ActionResult ViewListATMAccountType()
        {
            return View(bankRepository.GetListAtmAccountType());
        }

        [HttpGet]
        public ActionResult DetailsAnATMAccountType(string code)
        {
            return RedirectToAction("DetailsATMAccountType","ATMAccountType",bankRepository.GetAtmAccountTypeByCode(code));
        }
        [HttpGet]
        public ActionResult DetailsATMAccountType(ATMAccountType atmaccounttype)
        {
            return View(atmaccounttype);
        }
       
    }
}
