﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using G62BANK.Models;

namespace G62BANK.Controllers
{
    public class ForumController : Controller
    {
        //
        // GET: /Forum/

        public ActionResult Index()
        {

            //ForumModel.DeleteAll();
            //ForumModel.CreateTopic(new ForumModel()
            //{
            //    Owner = "G62 Bank",
            //    Title = "Đăng Ký Dịch Vụ Ngân Hàng Trực Tuyến",
            //    Content = "Để đăng ký Dịch Vụ Ngân Hàng Trực Tuyến, Quý khách phải có một trong những thông tin sau..",
            //    Comments = new List<ForumModel.Comment>(),
            //    CreationDate = DateTime.Now
            //});
            //ForumModel.CreateTopic(new ForumModel()
            //{
            //    Owner = "G62 Bank",
            //    Title = "Quên Mật Khẩu Ngân Hàng Trực Tuyến",
            //    Content = "Có 2 cách để đăng ký lại mật khẩu Ngân Hàng Trực Tuyến như sau..",
            //    Comments = new List<ForumModel.Comment>(),
            //    CreationDate = DateTime.Now
            //});
            //ForumModel.CreateTopic(new ForumModel()
            //{
            //    Owner = "G62 Bank",
            //    Title = "Kiểm Tra Số Dư Thẻ Tín Dụng",
            //    Content = "Quý khách có thể kiểm tra số dư thẻ tín dụng khi sử dụng một trong hai dịch vụ sau..",
            //    Comments = new List<ForumModel.Comment>(),
            //    CreationDate = DateTime.Now
            //});
            //ForumModel.CreateTopic(new ForumModel()
            //{
            //    Owner = "G62 Bank",
            //    Title = "Khi Nhận Được 2 Số Pin, Làm Sao Biết Số Pin Nào Của Thẻ Tín Dụng?",
            //    Content = "Quý khách sẽ nhận được mã số PIN sau khi đăng ký sử dụng thẻ thanh toán quốc tế hoặc thẻ tín dụng hoặc Dịch Vụ Ngân Hàng qua Điện Thoại. Trong những mã số PIN trên, Quý khách vui lòng sử dụng mã PIN có in chữ Credit Card (Thẻ Tín Dụng) trên phong bì.",
            //    Comments = new List<ForumModel.Comment>(),
            //    CreationDate = DateTime.Now
            //});

            this.ViewBag.Topics = ForumModel.GetAll();

            return View();
        }

        //
        // GET: /Forum/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Forum/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Forum/Create

        [HttpPost]
        public string AddComment(ForumModel.Comment comment)
        {
            ForumModel.PostComment(comment);

            return "Success";
            //try
            //{
            //    // TODO: Add insert logic here

            //    return RedirectToAction("Index");
            //}
            //catch
            //{
            //    return View();
            //}
        }

        [HttpPost]
        public ActionResult AddTopic(ForumModel topic)
        {
            topic.Comments = new List<ForumModel.Comment>();
            topic.CreationDate = DateTime.Now;
            ForumModel.CreateTopic(topic);

            return RedirectToAction("Index");
        }

        //
        // GET: /Forum/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Forum/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Forum/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Forum/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

    }
}
