﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using G62BANK.Models;

namespace G62BANK.Controllers
{
    public class TransactionTypeController : Controller
    {
        //
        // GET: /TransactionType/
        private BankRepository bankRepository;
        public ActionResult ViewTransactionType()
        {
            return View(bankRepository.GetListTransactionType());
        }
        public TransactionTypeController()
            : this(new BankRepository())
        { }

        public TransactionTypeController(BankRepository bankRepository)
        {
            this.bankRepository = bankRepository;
        }
        
    }
}
