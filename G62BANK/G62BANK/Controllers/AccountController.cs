﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using G62BANK.Models;
using Microsoft.Web.Helpers;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using Recaptcha;
using System.Web.Helpers;
namespace G62BANK.Controllers
{
    public class AccountController : Controller
    {
        //
        // GET: /BankAccount/
        private BankRepository bankRepository;

        public static Account CurrentAccount;


        public AccountController()
            : this(new BankRepository())
        { }

        public AccountController(BankRepository bankRepository)
        {
            this.bankRepository = bankRepository;
        }

        [HttpGet]
        public ActionResult CreateBankAccount()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateBankAccount(BankCustomer cus)
        {
            return View();
        }

        public ActionResult DetailsAccount(Account acc)
        {
            BankAccount bacc = new BankAccount();
            bacc.AccountCode = acc.AccountCode;
            bacc.AccountTypeCode = acc.AccountTypeCode;
            bacc.CustomerID = acc.CustomerID;
            bacc.Status = acc.Status;
            bacc.FullName = acc.FullName;
            bacc.PinCode = acc.PinCode;

            return PartialView(bacc);
        }

        [HttpGet]
        public ActionResult ChangePasswordAccount()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePasswordAccount(string current, string pass)
        {
            try 
	        {
                if (current == CurrentAccount.PinCode && pass != "")
                {
                    bankRepository.ChangePasswordAccount(CurrentAccount.AccountCode, pass);
                    // doi lai password cua current account
                    CurrentAccount.PinCode = pass;
                }
                else
                    return Json(false, JsonRequestBehavior.AllowGet);
	        }
	        catch (Exception)
	        { 
	        }
            return Json(true, JsonRequestBehavior.AllowGet);
                
        }

        [HttpGet]
        public JsonResult CheckPassword(string CurrentPassword)
        {
            try
            {
                if (CurrentPassword != CurrentAccount.PinCode)
                    return Json("Mật khẩu không chính xác!", JsonRequestBehavior.AllowGet);
                else
                    return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            
        }

        [HttpGet]
        public ActionResult PutMoneyAccountG62()
        {
            G62BANK.ViewModels.AccountModel.PutMoneyOnAccountModel model = new ViewModels.AccountModel.PutMoneyOnAccountModel();
            model.day = DateTime.Now.Date;
            model.listbank = new List<BankSystem>();
            model.listbank.Add(bankRepository.GetBankByCode("G62"));
            model.bankcode = bankRepository.GetBankByCode("G62").BankCode;
            return View(model);
        }

        [HttpPost]
        [RecaptchaControlMvc.CaptchaValidatorAttribute]
        public ActionResult PutMoneyAccountG62(G62BANK.ViewModels.AccountModel.PutMoneyOnAccountModel model, bool captchaValid)
        {
            model.listbank = new List<BankSystem>();
            model.listbank.Add(bankRepository.GetBankByCode("G62"));
            if (!captchaValid)
                return View(model);
            else
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult CheckBankCode(string code)
        {
            if (bankRepository.GetBankByCode(code) != null)
                return Json(true,JsonRequestBehavior.AllowGet);
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ShowOwnerOfAccount(string code)
        {
            Account acc = bankRepository.GetAccountByCode(code);
            if(acc != null)
                return View(acc);
            else
                return Json("false", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult SumMoneyPutMoneyAccountG62(string money)
        {
            TransactionType transactiontype = bankRepository.GetTransactionTypeByCode(2);
            double _money = double.Parse(money);

            if (transactiontype.MinimumMoney <= _money && transactiontype.MaximumMoney >= _money)
            {
                double cost = (transactiontype.Cost * 100) / _money;
                return Json(_money + cost,JsonRequestBehavior.AllowGet);
            }
            else
                return Json(false, JsonRequestBehavior.AllowGet);

        }


        [HttpGet]
        public ActionResult ChangeFullName()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangeFullName(string fullname)
        {
            if (fullname != null && fullname != "")
            {
                bankRepository.ChangeFullNameCustomer(User.Identity.Name, fullname);
                
                return Json(bankRepository.GetCustomerbyUserName(User.Identity.Name).FullName, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ChangeAddress()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangeAddress(string address)
        {
            if (address != null && address != "")
            {
                bankRepository.ChangeAddressCustomer(User.Identity.Name, address);

                return Json(bankRepository.GetCustomerbyUserName(User.Identity.Name).Address, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ChangePhone()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePhone(string phone)
        {
            if (phone != null && phone != "")
            {
                bankRepository.ChangePhoneCustomer(User.Identity.Name, phone);

                return Json(bankRepository.GetCustomerbyUserName(User.Identity.Name).Phone, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ChangeBirthday()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangeBirthday(string date)
        {
            try
            {
                if (date != null && date != "")
                {
                    string[] temp = date.Split('/');
                    DateTime day = new DateTime(int.Parse(temp[2]), int.Parse(temp[0]), int.Parse(temp[1]));
                    bankRepository.ChangeBirthdayCustomer(User.Identity.Name, day);

                    return Json(bankRepository.GetCustomerbyUserName(User.Identity.Name).Birthday.ToString(), JsonRequestBehavior.AllowGet);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            
        }

        [HttpGet]
        public ActionResult ViewAccount()
        {
            if (User.Identity.Name != null && User.Identity.Name != "")
                return View();
            else
                return RedirectToAction("Login", "BankCustomer");
        }

        [HttpPost]
        public ActionResult ViewATMAccount(string code, string pass)
        {
            try
            {
                ATMAccount atm = bankRepository.GetATMAccountByAccountCodeAndPass(code, pass);
                if (atm != null)
                {
                    //return RedirectToAction("DetailsATMAccount", "ATMAccount", new { code = atm.ATMAcountCode });
                    return Json(atm.ATMAcountCode, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(false, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false,JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult TranferMoneyToCustomer()
        {
            if (User.Identity.Name != null && User.Identity.Name != "")
                return View();
            else
                return RedirectToAction("Login", "BankCustomer");
        }

        [HttpPost]
        public ActionResult TranferMoneyToCustomer_Service(string code, string pass, string id, string name, double money)
        {
            G62Service.Service1 ser = new G62Service.Service1();
            return Json(ser.TranfersMoneyToCustomer(code, pass, id, name , money),JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult TranferMoneyToATM()
        {
            if (User.Identity.Name != null && User.Identity.Name != "")
                return View();
            else
                return RedirectToAction("Login", "BankCustomer");
        }

        [HttpPost]
        public ActionResult TranferMoneyToATM_Service(string code, string pass, string code2, double money)
        {
            G62Service.Service1 ser = new G62Service.Service1();
            return Json(ser.TranfersMoneyToAccount(code, pass, code2, money), JsonRequestBehavior.AllowGet);
        }
    }
}
