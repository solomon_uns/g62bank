﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using G62BANK.Models;
using Microsoft.Web.Helpers;
using System.Web.Security;
using Recaptcha;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using System.Net.Mail;
using System.Net;
namespace G62BANK.Controllers
{
    public class BankCustomerController : Controller
    {
        private BankRepository bankRepository;

        //
        // GET: /BankCustomer/
        public BankCustomerController()
            : this(new BankRepository())
        { }

        public BankCustomerController(BankRepository bankRepository)
        {
            this.bankRepository = bankRepository;
        }

        [HttpGet]
        public ActionResult CreateBankCustomer()
        {
            return View();
        }

        [HttpPost]
        [RecaptchaControlMvc.CaptchaValidatorAttribute]
        public ActionResult CreateBankCustomer(BankCustomer cus, bool captchaValid)
        {
            if(!captchaValid)
                return View();
            if (!ModelState.IsValid)
                return View();
            FormsAuthentication.SetAuthCookie(cus.UserName, createPersistentCookie: false);
            bankRepository.CreateCustomer(cus);
            return RedirectToAction("Index", "Home",cus.FullName);
        }
        public ActionResult CountCustomer()
        {
            ViewData["count"] = bankRepository.CountCustomer();
            return PartialView("CountCustomer");
        }
        [HttpGet]
        public ActionResult DetailsCustomerParent()
        {
            if (User.Identity.Name != null && User.Identity.Name != "")
            {
                Customer user = bankRepository.GetCustomerbyUserName(User.Identity.Name);
                if(user != null)
                    return View(bankRepository.GetCustomerbyUserName(User.Identity.Name));
                return RedirectToAction("Login", "BankCustomer");
            }
            else
                return RedirectToAction("Login", "BankCustomer");
        }

        public ActionResult GetListAcountTypeCard()
        {
            if (User.Identity.Name != null && User.Identity.Name != "")
                return View(bankRepository.GetListAccount_CardByUserName(User.Identity.Name));
            else
                return RedirectToAction("Login", "BankCustomer");
        }

        public ActionResult DetailsCusTomer(string username)
        {
            if(username!= null)
                return View(bankRepository.GetCustomerbyUserName(username));
            else
                return View(bankRepository.GetCustomerbyUserName(User.Identity.Name));
        }
        [HttpPost]
        public ActionResult DetailsCusTomer2(string username)
        {
            if (username != null)
            {
                return PartialView(bankRepository.GetCustomerbyUserName(username));
            }
            else
                return PartialView(bankRepository.GetCustomerbyUserName(User.Identity.Name));
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public JsonResult CheckCMND(string Cmnd)
        {
            Customer user = bankRepository.GetCustomerbyCMND(Cmnd);
            if (user != null)
                return Json("Số CMND đã tồn tại, vui lòng chọn số CMND khác!", JsonRequestBehavior.AllowGet);
            else
                return Json(true, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult CheckUserName(string username)
        {
            Customer user = bankRepository.GetCustomerbyUserName(username);
            if (user != null)
                return Json("UserName đã tồn tại, vui lòng chọn số UserName khác!", JsonRequestBehavior.AllowGet);
            else
                return Json(true, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult CheckUserName2(string id)
        {
            Customer user = bankRepository.GetCustomerbyCMND(id);
            if (user != null)
                return Json(user.FullName, JsonRequestBehavior.AllowGet);
            else
                return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            ModelState.Clear();
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("LoginError", "Username hoặc Password không chính xác!");
                return View();
            }
            else
            {
                if (bankRepository.Login(model.UserName, model.Password))
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, createPersistentCookie: false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("LoginError", "Username hoặc Password không chính xác!");
                    return View(model);
                }
            }
        }

        //
        // POST: /Account/ExternalLogin

        //[HttpPost]
        [AllowAnonymous]
        public ActionResult ExternalLogin_google(string provider, string returnUrl)
        {
            return new ExternalLoginResult("google", Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
        }

        [AllowAnonymous]
        public ActionResult ExternalLogin_facebook(string provider, string returnUrl)
        {
            return new ExternalLoginResult("facebook", Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
        }

        [AllowAnonymous]
        public ActionResult ExternalLogin_twitter(string provider, string returnUrl)
        {
            return new ExternalLoginResult("twitter", Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback

        [AllowAnonymous]
        public ActionResult ExternalLoginCallback(string returnUrl)
        {
            try
            {
                AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
                if (!result.IsSuccessful)
                {
                    return RedirectToAction("Error", "Shared");
                }
                // User is new, ask for their desired membership name
                string loginData = OAuthWebSecurity.SerializeProviderUserId(result.Provider, result.ProviderUserId);
                return ExternalLoginConfirmation(new RegisterExternalLoginModel { UserName = result.UserName, ExternalLoginData = loginData });
            }
            catch (Exception)
            {
                return RedirectToAction("Error","Shared");
            } 
        }

        [HttpPost]
        public ActionResult ExternalLoginConfirmation(RegisterExternalLoginModel model)
        {
            if (!ModelState.IsValid)
                return View();
            else
            {
                if (bankRepository.Login(model.UserName))
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, createPersistentCookie: false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("1", "Bạn chưa tạo tài khoản!");
                    return RedirectToAction("CreateBankCustomer", "BankCustomer");
                }
            }
        }


        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpGet]
        public ActionResult ResetPassWord()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ResetPassWord(string email)
        {
            try
            {
                string pass = bankRepository.GetPassWordbyEmail(email);
                if(pass != null && pass != "")
                {
                    MailAddress mailfrom = new MailAddress("g62bank@gmail.com");
                    MailAddress mailto = new MailAddress(email);
                    MailMessage newmsg = new MailMessage(mailfrom, mailto);

                    newmsg.Subject = "Password Reset G62Bank";
                    newmsg.Body = "This is a Password your account: "+ pass;

                    ////For File Attachment, more file can also be attached
                    SmtpClient smtps = new SmtpClient("smtp.gmail.com", 587);
                    smtps.UseDefaultCredentials = false;
                    smtps.Credentials = new NetworkCredential("g62bank@gmail.com", "g62banking");
                    smtps.EnableSsl = true;
                    smtps.Send(newmsg);
                    return Json(true,JsonRequestBehavior.AllowGet);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false,JsonRequestBehavior.AllowGet);
            }
        }

    }
}
