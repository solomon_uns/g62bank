﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using G62BANK.Models;

namespace G62BANK.Controllers
{
    public class SavingBookTypeController : Controller
    {
        //
        // GET: /SavingBookType/
        private BankRepository bankRepository;

        //
        // GET: /BankCustomer/
        public SavingBookTypeController()
            : this(new BankRepository())
        { }

        public SavingBookTypeController(BankRepository bankRepository)
        {
            this.bankRepository = bankRepository;
        }

        public ActionResult ShowListSavingBook()
        {
            return View(bankRepository.GetListSavingBookType());
        }
    }
}
