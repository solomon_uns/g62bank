﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using G62BANK.Models;
namespace G62BANK.ViewModels
{
    public class AccountModel
    {
        public class PassworModel
        {
            [Required]
            [Display(Name = "CurrentPassword")]
            [Remote("CheckPassword", "Account")]
            public string CurrentPassword { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            [StringLength(6, ErrorMessage = "Password phải là một số có 6 chữ số!", MinimumLength = 6)]
            [RegularExpression("^[0-9]+$",ErrorMessage = "Mật khẩu phải là chữ số!")]
            public string Password { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "2 ô mật khẩu không chính xác!")]
            public string ConfirmPassword { get; set; }
        }

        public class PutMoneyOnAccountModel {

            [Required]
            public List<BankSystem> listbank { get; set; }

            [Required]
            public string bankcode { get; set; }

            [Required]
            public string accountcode { get; set; }
            
            [Required]
            public double money { get; set; }
            
            [Required]
            public double cost { get; set; }

            [Required]
            public DateTime day { get; set; }

            [Required]
            public string customerName { get; set; }

            [Required]
            public string customerID { get; set; }

            [Required]
            public string customerAddress { get; set; }

            [Required]
            public string content { get; set; }
        }

        public class BirthdayModel
        {
            [Required]
            public string Bbirthday { get; set; }
        }

    }
}