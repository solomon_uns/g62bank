﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using G62BANK.Models;

namespace G62BANK.ViewModels
{
    public class SavingBookModel
    {
        public class DetailsSavingBookModel
        {
            public Account account { get; set; }
            public SavingBook savingbook { get; set; }
            public SavingBookType savingbooktype { get; set;}
        }
    }
}