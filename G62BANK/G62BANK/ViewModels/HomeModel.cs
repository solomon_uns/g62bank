﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G62BANK.ViewModels
{
    public class HomeModel
    {
        public class News {
            public string title { get; set; }
            public string description { get; set; }
            public string link { get; set; }
            public string pubdate { get; set; }
        }
    }
}