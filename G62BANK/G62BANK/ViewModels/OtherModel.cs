﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using G62BANK.Models;

namespace G62BANK.ViewModels
{
    public class OtherModel
    {
        public class EmptyModel
        {
            public Account account { get; set; }
            public AccountType accounttype { get; set; }
            public ATMAccount atmaccount { get; set; }
            public ATMAccountType atmaccounttype { get; set; }
            public Bank bank { get; set; }
            public BankSystem banksytem { get; set; }
            public CreditCard creditcard { get; set; }
            //public CreditCardLoan creditcardloan { get; set; }
            public CreditCardTransaction creditcardtransaction { get; set; }
            public Customer customer { get; set; }
            //public CustomerTransaction customertransaction { get; set; }
            public DebitCard debitcard { get; set; }
            public DebitCardTransaction debitcardtransaction { get; set; }
            //public Parameter parameter { get; set; }
            public SavingBook savingbook { get; set; }
            public SavingBookType savingbooktype { get; set; }
            public SavingBookTransaction savingbooktransaction { get; set; }
            public TransactionType transactiontype { get; set; }
        }
    }
}