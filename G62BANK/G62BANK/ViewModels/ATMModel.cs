﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using G62BANK.Models;
namespace G62BANK.ViewModels
{
    public class ATMModel
    {
        public class DebitCardModel
        {
            public Account account { get; set; }
            public ATMAccountType atmaccounttype { get; set; }
            public ATMAccount atmaccount { get; set; }
            public DebitCard debitcard { get; set; }
        }
        public class CreditCardModel
        {
            public Account account { get; set; }
            public ATMAccountType atmaccounttype { get; set; }
            public ATMAccount atmaccount { get; set; }
            public CreditCard creditcard { get; set; }
        }
        public class ListATMAccount
        {
            public List<DebitCardModel> ListDebitCard { get; set; }
            public List<CreditCardModel> ListCreditCard { get; set; }
            // danh sach sổ tiết kiệm
            // public List<SavingBook> ListSavingBook { get; set;}

        }
    }
}