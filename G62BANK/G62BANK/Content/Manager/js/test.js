﻿    $(function () {
        $("#NoteDialog").dialog({
            autoOpen: false, width: 400, height: 250, modal: true,
            buttons: {
                "Save": function () {
                    // goi thực hiện
                    $se = "Accept" + GetSelected();
                    $.ajax({
                        url: '<%= Url.Action("CheckCreditCardRegister","BankManager") %>',
                        type: "POST",
                        data: { data: $se },
                        success: function (data) {
                            if (data.toString() == "true") {
                                $("#NoteDialog").dialog("close");
                                $.ajax({
                                    url: '<%= Url.Action("CheckCreditCardRegister","BankManager") %>',
                                    type: "GET",
                                    success: function (data) {
                                        $("#main-content").html(data);
                                    }
                                });
                            }
                            else
                            { alert('Error !'); }
                        }
                    });
                },
                Cancel: function () { $("#NoteDialog").dialog("close"); }
            }
        });
    });

    function AcceptSelected() {
        $("#NoteDialog").html("Chấp nhận kích hoạt các tài khoản thẻ CreditCard này?").dialog("option", "title", "Accept Selected");
        $("#NoteDialog").dialog("open");
    }

    $(function () {
        $("#NoteDialog2").dialog({
            autoOpen: false, width: 400, height: 250, modal: true,
            buttons: {
                "Save": function () {
                    // goi thực hiện
                    $se2 = "Delete" + GetSelected();
                    $.ajax({
                        url: '<%= Url.Action("CheckCreditCardRegister","BankManager") %>',
                        type: "POST",
                        data: { data: $se2 },
                        success: function (data) {
                            if (data.toString() == "true") {
                                $("#NoteDialog2").dialog("close");
                                $.ajax({
                                    url: '<%= Url.Action("CheckCreditCardRegister","BankManager") %>',
                                    type: "GET",
                                    success: function (data) {
                                        $("#main-content").html(data);
                                    }
                                });
                            }
                            else
                            { alert('Error !'); }
                        }
                    });
                },
                Cancel: function () { $("#NoteDialog2").dialog("close"); }
            }
        });
    });


    function DeleteSelected() {
        $("#NoteDialog2").html("Hủy các tài khoản thẻ CreditCard này?").dialog("option", "title", "Delete Selected")
        $("#NoteDialog2").dialog("open");
    }

    $(function () {
        $("#NoteDialog3").dialog({
            autoOpen: false, width: 800, height: 600, modal: true,
            buttons: {
                Cancel: function () { $("NoteDialog3").dialog("close"); }
            }
        });
    });

    function CustomerReview(id) {
        $("#NoteDialog3").html("")
        .dialog("option", "title", "CustomerReview")
        .load("/BankManager/CustomerReview?CustomerID=" + id, function () { $("#NoteDialog3").dialog("open"); });
    }
