﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace G62BANK.Models
{
    public class BankAccount
    {
        public string AccountCode { get; set; }

        [Required]
        [MaxLength(6)]
        public string PinCode { get; set; }

        public string FullName { get; set; }

        public string CustomerID { get; set; }

        public int AccountTypeCode { get; set; }

        public int Status { get; set; }
    }
}