﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G62BANK.Models
{
    public class ProcessDay
    {
        public struct Ngay
        {
            public int ng { get; set; }
            public int th { get; set; }
            public int nam { get; set; }
        };
        public  bool CheckNamNhuan(Ngay ngay)
        {
            if (ngay.nam % 4 == 0 && ngay.nam % 100 != 0)
                return true;
            else
            {
                if(ngay.nam % 400 == 0)
                    return true;
                else
                return false;
            }
        }

        public  int SoNgayTrongThang(Ngay x)
        {
            switch (x.th)
            {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    return 31;
                case 4:
                case 6:
                case 9:
                case 11:
                    return 30;
                case 2:
                    if (CheckNamNhuan(x))
                        return 29;
                    else
                        return 28;
                default:
                    return 0;
            }
        }

        public  int STTTrongNam(Ngay x)
        {
            int stt = 0;
            for (int i = 1; i <= x.th; i++)
                stt = stt + SoNgayTrongThang(new Ngay { nam = x.nam, th = i, ng = x.ng });
            return stt;
        }

        public  long STT(Ngay x)
        {
            long stt = 0;
            for (int i = 1; i < x.nam - 1; i++)
            {
                stt += 365;
                Ngay temp = new Ngay();
                temp.ng = 1;
                temp.th = 1;
                temp.nam = i;
                if (CheckNamNhuan(x))
                    stt += 1;
            }
            return stt + STTTrongNam(x);
        }

        // tim ngay khi biet stt cua ngay trong nam va nam
        Ngay TimNgay(int nam, int stt)
        {
            Ngay temp = new Ngay();
            temp.ng = 1;
            temp.th = 1;
            temp.nam = nam;
            while (stt - SoNgayTrongThang(temp) > 0)
            {
                stt -= SoNgayTrongThang(temp);
                temp.th++;
            }
            temp.ng = stt;
            return temp;
        }

        // tim ngay khi biet stt cua ngay ke tu ngay 1/1/1
        Ngay TimNgay(long stt)
        {
            int nam = 1;
            int sn = 365;
            while (stt - sn > 0)
            {
                stt -= sn;
                nam++;
                sn = 365;
                Ngay temp = new Ngay();
                temp.ng = 1;
                temp.th = 1;
                temp.nam = nam;
                if(CheckNamNhuan(temp))
                    sn = 366;
            }
            return TimNgay(nam, (int)stt);
        }


        public DateTime CongNgay(DateTime day1, int songay)
        {
            // tim stt ngay ke tu ngay 1/1/1
            long stt = STT(new Ngay { ng = day1.Day, th = day1.Month, nam = day1.Year });
            stt += songay;
            Ngay temp = TimNgay(stt);
            DateTime day = new DateTime(temp.nam,temp.th,temp.ng);
            return day;
        }

    }
}