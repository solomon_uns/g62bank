﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace G62BANK.Models
{
    public class Money
    {
        [Required(ErrorMessage = "(Chưa nhập số tiền)")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "(Số tiền phải là 1 số)")]
        public double money { get; set; }
    }
}