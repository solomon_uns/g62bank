﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace G62BANK.Models
{
    public class BankRepository
    {
        
        public G62DBDataContext ServerLinq = new G62DBDataContext();

        #region BankCustomer

        public Bank GetBank()
        {
            ServerLinq = new G62DBDataContext();
            return ServerLinq.Banks.SingleOrDefault(p => p.BankCode == "62");
        }

        public void CreateCustomer(BankCustomer cus)
        {
            Customer customer = new Customer();
            customer.UserName = cus.UserName;
            customer.CustomerID = cus.Cmnd;
            customer.FullName = cus.FullName;
            customer.Address = cus.Address;
            string[] da = cus.Birthday.Split('/');
            DateTime date = new DateTime(int.Parse(da[2]), int.Parse(da[0]), int.Parse(da[1]));
            customer.Birthday = date;
            customer.Email = cus.Email;
            customer.Phone = cus.Phone;
            customer.Password = cus.Password;
            customer.DayCreated = DateTime.Now;
            customer.Status = 1;
            ServerLinq.Customers.InsertOnSubmit(customer);
            ServerLinq.SubmitChanges();
        }
        public string CountCustomer()
        {
            return ServerLinq.Customers.Count().ToString();
        }
        public Customer GetCustomerbyCMND(string cmnd)
        {
            Customer us = ServerLinq.Customers.SingleOrDefault(p => p.CustomerID == cmnd);
            return us;
        }
        public Customer GetCustomerbyUserName(string username)
        {
            ServerLinq = new G62DBDataContext();
            Customer us = ServerLinq.Customers.SingleOrDefault(p => p.UserName == username);
            return us;
        }
        public bool Login(string username, string password)
        {
            try
            {
                Customer us = ServerLinq.Customers.SingleOrDefault(p => p.UserName == username && p.Password == password);
                if (us == null)
                    return false;
                else
                    return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public bool Login(string username)
        {
            try
            {
                Customer us = ServerLinq.Customers.SingleOrDefault(p => p.UserName == username);
                if (us == null)
                    return false;
                else
                    return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public Account GetAccountByCode(string code)
        {
            ServerLinq = new G62DBDataContext();
            try
            {
                return ServerLinq.Accounts.SingleOrDefault(p => p.AccountCode == code);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool EditCustomer(Customer cus)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                Customer customer = ServerLinq.Customers.SingleOrDefault(p => p.UserName == cus.UserName);
                customer.Address = cus.Address;
                customer.Birthday = cus.Birthday;
                customer.CustomerID = cus.CustomerID;
                customer.Email = cus.Email;
                customer.FullName = cus.FullName;
                customer.Phone = cus.Phone;
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool DeleteCustomer(string cus)
        {
            ServerLinq = new G62DBDataContext();
            try
            {

                Customer customer =  ServerLinq.Customers.SingleOrDefault(p=>p.UserName == cus);
                customer.Status = 0;
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public bool RestPasswordCustomer(string cus)
        {
            ServerLinq = new G62DBDataContext();
            try
            {
                Customer us = ServerLinq.Customers.SingleOrDefault(p => p.UserName == cus);
                us.Password = us.UserName;
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public string GetPassWordbyEmail(string email)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                return ServerLinq.Customers.SingleOrDefault(p =>p.Email == email).Password;
            }
            catch (Exception)
            {
                return null;
            }
            

        }
        #endregion

        #region Account
        public List<Account> GetListAccount_CardByUserName(string username)
        {
            ServerLinq = new G62DBDataContext();
            List<Account> list = new List<Account>();
            Customer cus = ServerLinq.Customers.SingleOrDefault(p=>p.UserName == username);
            var table = from account in ServerLinq.Accounts
                        where account.CustomerID == cus.CustomerID && account.AccountTypeCode == 1
                        select account;

            foreach (var row in table)
                list.Add(row);
            return list;
        }
        public Account FindAccount(string acccode)
        {
            ServerLinq = new G62DBDataContext();
            return ServerLinq.Accounts.SingleOrDefault(p => p.AccountCode == acccode);
        }

        
        #endregion

        #region ATMAccount

        public ATMAccount CreateATMAccount(Customer customer, int atmaccounttypecode)
        {
            try
            {
                // Mở thẻ = Tạo 1 tài khoản + Tạo 1 Thẻ cho khách hàng
                ServerLinq = new G62DBDataContext();
                // tạo tài khoản ngân hàng
                Account acc = new Account();
                acc.AccountCode = CreateAccountCode();
                acc.AccountTypeCode = 1;
                acc.CustomerID = customer.CustomerID;
                acc.Status = 0;
                acc.FullName = customer.FullName;
                acc.PinCode = CreatePin();

                // tạo thẻ ATM
                ATMAccount atm = new ATMAccount();
                atm.AccountCode = acc.AccountCode;
                atm.ATMAccountTypeCode = atmaccounttypecode;
                atm.ATMAcountCode = CreateCode();
                atm.Balance = 0;
                atm.DayCreated = DateTime.Now.Date;
                atm.Status = 0;

                ATMAccountType atmaccounttype = ServerLinq.ATMAccountTypes.SingleOrDefault(p => p.ATMAccountTypeCode == atmaccounttypecode);

                if (atmaccounttypecode == 1) // Debitcard
                {
                    DebitCard debitcard = new DebitCard();
                    debitcard.ATMAccountCode = atm.ATMAcountCode;
                    debitcard.DateCreated = DateTime.Now.Date;

                    int day = (int)atmaccounttype.NumberDayExpired;

                    DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    debitcard.DateExpired = now.Add(new TimeSpan(day,0,0,0));
                    debitcard.DebitCardCode = CreateCode();
                    debitcard.Status = 0;
                    ServerLinq.DebitCards.InsertOnSubmit(debitcard);
                    ServerLinq.SubmitChanges();
                }
                else
                {
                    if (atmaccounttypecode == 2) // Creditcard
                    {
                        CreditCard creditcard = new CreditCard();

                        creditcard.ATMAccountCode = atm.ATMAcountCode;
                        creditcard.CreditCardCode = CreateCode();

                        int day = (int)atmaccounttype.NumberDayExpired;
                        DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                        creditcard.DayCreated = DateTime.Now.Date;
                        creditcard.DayExpired = now.Add(new TimeSpan(day,0,0,0,0));
                        creditcard.Loan = 0;
                        creditcard.Status = 0;
                        ServerLinq.CreditCards.InsertOnSubmit(creditcard);
                    }
                }
                ServerLinq.ATMAccounts.InsertOnSubmit(atm);
                ServerLinq.Accounts.InsertOnSubmit(acc);

                ServerLinq.SubmitChanges();

                G62BANK.Controllers.ATMAccountController.CurrentATMAccount = atm;
                G62BANK.Controllers.AccountController.CurrentAccount = acc;

                return atm;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public bool ChangePasswordAccount(string code, string pass)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                Account acc = ServerLinq.Accounts.SingleOrDefault(p => p.AccountCode == code);
                acc.PinCode = pass;
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public ATMAccount GetATMAccountByCode(string code)
        {
            ServerLinq = new G62DBDataContext();
            try
            {
                return ServerLinq.ATMAccounts.SingleOrDefault(p => p.ATMAcountCode == code);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public ATMAccount GetATMAccountByAccountCode(string code)
        {
            ServerLinq = new G62DBDataContext();
            try
            {
                return ServerLinq.ATMAccounts.SingleOrDefault(p => p.AccountCode == code);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public ATMAccount GetATMAccountByAccountCodeAndPass(string code, string pass)
        {
            ServerLinq = new G62DBDataContext();
            try
            {
                Account acc = ServerLinq.Accounts.SingleOrDefault(p => p.AccountCode == code && p.PinCode == pass);
                return ServerLinq.ATMAccounts.SingleOrDefault(p => p.AccountCode == acc.AccountCode);
            }
            catch (Exception)
            {
                return null;
            }
        }
        #endregion

        #region ATMAccountType
        public List<ATMAccountType> GetListAtmAccountType()
        {
            ServerLinq = new G62DBDataContext();
            List<ATMAccountType> list = new List<ATMAccountType>();
            var table = from atmacctype in ServerLinq.ATMAccountTypes
                        select atmacctype;

            foreach (var row in table)
                list.Add(row);
            return list;
        }

        public ATMAccountType GetAtmAccountTypeByCode(string code)
        {
            return ServerLinq.ATMAccountTypes.SingleOrDefault(p => p.ATMAccountTypeCode == int.Parse(code.Trim()));
        }

        public bool EditATMAccountType(ATMAccountType type)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                ATMAccountType ty = ServerLinq.ATMAccountTypes.SingleOrDefault(p => p.ATMAccountTypeCode == type.ATMAccountTypeCode);
                ty.ATMAccountTypeName = type.ATMAccountTypeName;
                ty.Description = type.Description;
                ty.NumberDayExpired = type.NumberDayExpired;
                ty.Rate = type.Rate;
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region Count Rate for ATMAccount DebitCard Before Process Transaction
        public bool CountRate(string code)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                // B1: Xác định loại lãi suất đang áp dụng cho tài khoản này
                ATMAccount atm = ServerLinq.ATMAccounts.SingleOrDefault(p => p.AccountCode == code);
                ATMAccountType atmtype = ServerLinq.ATMAccountTypes.SingleOrDefault(p => p.ATMAccountTypeCode == atm.ATMAccountTypeCode);
                double b1 = atmtype.Rate;
                // B2: Xác định số ngày được hưởng lãi =  now - ngày bắt đầu hưởng lãi
                DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                if (atm.DayForRate != null)
                {
                    DateTime HL = new DateTime(atm.DayForRate.Value.Year, atm.DayForRate.Value.Month, atm.DayForRate.Value.Day);
                    double b2 = now.Subtract(HL).Days;
                    // B3: Số tiền lãi 1 ngày được hưởng
                    // B3.1: Số tiền lãi được hưởng nếu dủ 360 ngày
                    double b31 = (double)((b2 * atm.Balance) / 100);
                    // B3.2: Số tiền lãi 1 ngày dc hưởng
                    double b3 = (double)(b31 / 360);
                    // B4: Số tiền lãi được hưởng tính tới hiện tại
                    double b4 = b3 * b2;
                    // B5: Cộng lãi suất nhận được ở B3 vào số dư tài khoản
                    double b5 = (double)atm.Balance + b4;
                    atm.Balance = b5;
                }
                // B6: Cập nhật lại ngày bắt đầu hưởng lãi
                atm.DayForRate = now;
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public Meassages GetStringCountRate(string code)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                string str = "";
                // B1: Xác định loại lãi suất đang áp dụng cho tài khoản này
                ATMAccount atm = ServerLinq.ATMAccounts.SingleOrDefault(p => p.AccountCode == code);
                str += "Mã Tài Khoản: " + atm.AccountCode + "|";
                ATMAccountType atmtype = ServerLinq.ATMAccountTypes.SingleOrDefault(p => p.ATMAccountTypeCode == atm.ATMAccountTypeCode);
                double b1 = atmtype.Rate;
                str += "Lãi suất được hưởng: " + b1 + " %/năm|";
                // B2: Xác định số ngày được hưởng lãi =  now - ngày bắt đầu hưởng lãi
                DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                if (atm.DayForRate != null)
                {
                    DateTime HL = new DateTime(atm.DayForRate.Value.Year, atm.DayForRate.Value.Month, atm.DayForRate.Value.Day);
                    double b2 = now.Subtract(HL).Days;
                    // B3: Số tiền lãi 1 ngày được hưởng
                    // B3.1: Số tiền lãi được hưởng nếu dủ 360 ngày
                    double b31 = (double)((b1 * atm.Balance) / 100);
                    str += "Số tiền lãi nếu được hưởng đúng 360 ngày: (" + b1 + " * " + atm.Balance + ")/100 = " + b31 + "|";
                    // B3.2: Số tiền lãi 1 ngày dc hưởng
                    double b3 = (double)(b31 / 360);
                    str += "Số tiền lãi được hưởng trong 1 ngày: " + b31 + "/360 = " + b3 + "|";
                    // B4: Số tiền lãi được hưởng tính tới hiện tại
                    str += "Ngày bắt đầu tính lãi: " + HL + "|";
                    str += "Số Ngày được hưởng lãi: " + now + "-" + HL + " = " + b2 + "|";
                    double b4 = b3 * b2;
                    str += "Số tiền lãi được hưởng: " + b3 + " * " + b2 + " = " + b4 + "|";
                    // B5: Cộng lãi suất nhận được ở B3 vào số dư tài khoản
                    double b5 = (double)atm.Balance + b4;
                    str += "Số dư tài khoản sau khi cộng lãi: " + atm.Balance + "+" + b4 + " = " + b5 + "|";
                    atm.Balance = b5;
                }
                // B6: Cập nhật lại ngày bắt đầu hưởng lãi
                atm.DayForRate = now;
                str += "Cập nhật lại ngày bắt đầu tính lãi tiếp theo: " + now + "|";
                Meassages mes = new Meassages();
                mes._mes = str;
                return mes;
            }
            catch (Exception)
            {
                return null;
            }
        }
        #endregion

        #region DebitCard
        public List<Account> SearchDebitCard(SearchDebitCard sear)
        {
            List<Account> list = new List<Account>();
            try
            {
                string query = "SELECT Account.AccountCode, Account.FullName, Account.CustomerID, Account.AccountTypeCode, Account.status FROM ATMAccount, DebitCard, Account WHERE Account.AccountCode = ATMAccount.AccountCode AND ATMAccount.ATMAcountCode = DebitCard.ATMAccountCode ";
                if (sear.AccountCode != null && sear.AccountCode != "")
                    query += "AND Account.AccountCode LIKE '%" + sear.AccountCode + "%' ";
                if (sear.DebitCardCode != null && sear.DebitCardCode != "")
                    query += "AND DebitCard.DebitCardCode LIKE '%" + sear.DebitCardCode + "%' ";
                if (sear.CustomerID != null && sear.CustomerID != "")
                    query += "AND Account.CustomerID LIKE '%" + sear.CustomerID + "%' ";
                if (sear.CustomerName != null && sear.CustomerName != "")
                    query += "AND Account.CustomerName LIKE '%" + sear.CustomerName + "%' ";
                DataTable table = SQL.GetTable(query);
                foreach (DataRow row in table.Rows)
                {
                    Account de = new Account();
                    de.AccountCode = row["AccountCode"].ToString();
                    de.FullName = row["FullName"].ToString();
                    de.CustomerID = row["CustomerID"].ToString();
                    de.AccountTypeCode = int.Parse(row["AccountTypeCode"].ToString());
                    de.Status = int.Parse(row["Status"].ToString());
                    list.Add(de);

                }
            }
            catch (Exception)
            {
            }
            
            return list;
        }

        public DebitCard FindDebitCardByATMAccountCode(string code)
        {
            ServerLinq = new G62DBDataContext();
            return ServerLinq.DebitCards.SingleOrDefault(p => p.ATMAccountCode == code);
        }

        // Nap tien vao tai khoan
        public bool PlusMoneyDebitCardIn(PlusMoneyDebitCardIn model)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                if (model.Money >= 50000)
                {
                    // B1: Xác định loại lãi suất đang áp dụng cho tài khoản này
                    ATMAccount atm = ServerLinq.ATMAccounts.SingleOrDefault(p => p.AccountCode == model.AccountCode);
                    ATMAccountType atmtype = ServerLinq.ATMAccountTypes.SingleOrDefault(p => p.ATMAccountTypeCode == atm.ATMAccountTypeCode);
                    double b1 = atmtype.Rate;
                    // B2: Xác định số ngày được hưởng lãi =  now - ngày bắt đầu hưởng lãi
                    DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    if (atm.DayForRate != null)
                    {
                        DateTime HL = new DateTime(atm.DayForRate.Value.Year, atm.DayForRate.Value.Month, atm.DayForRate.Value.Day);
                        double b2 = now.Subtract(HL).Days;
                        // B3: Số tiền lãi 1 ngày được hưởng
                        // B3.1: Số tiền lãi được hưởng nếu dủ 360 ngày
                        double b31 = (double)((2 * atm.Balance) / 100);
                        // B3.2: Số tiền lãi 1 ngày dc hưởng
                        double b3 = (double)(b31 / 360);
                        // B4: Số tiền lãi được hưởng tính tới hiện tại
                        double b4 = b3 * b2;
                        // B5: Cộng lãi suất nhận được ở B3 vào số dư tài khoản
                        double b5 = (double)atm.Balance + b4;
                        atm.Balance = b5;
                    }
                    // B5: Cộng số tiền gửi vào số dư
                    atm.Balance += model.Money;
                    // B6: Cập nhật lại ngày bắt đầu hưởng lãi
                    atm.DayForRate = now;

                    // Lưu nhật ký
                    DebitCardTransaction tra = new DebitCardTransaction();
                    tra.Balance = atm.Balance;
                    tra.Day = now;
                    tra.DebitCardTransactionCode = CreateCode();
                    tra.Description = model.Description;
                    tra.Money = model.Money;
                    TransactionType tratype = ServerLinq.TransactionTypes.SingleOrDefault(p => p.TransactionTypeCode == 2);
                    tra.TransactionTypeCode = tratype.TransactionTypeCode;
                    tra.TransactionTypeName = tratype.TransactionTypeName;
                    tra.SumCost = tratype.Cost;
                    tra.SumMoney = model.Money + tra.SumCost;
                    tra.ToAccountCode = model.AccountCode;
                    tra.ToATMAccountCode = atm.ATMAcountCode;
                    tra.Status = 1;
                    ServerLinq.DebitCardTransactions.InsertOnSubmit(tra);

                    // cập nhật
                    ServerLinq.SubmitChanges();

                    return true;
                }
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public Meassages GetStringPlusMoneyDebitCardIn(PlusMoneyDebitCardIn model)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                string str = "";
                // B1: Xác định loại lãi suất đang áp dụng cho tài khoản này
                ATMAccount atm = ServerLinq.ATMAccounts.SingleOrDefault(p => p.AccountCode == model.AccountCode);
                str += "Mã Tài Khoản: " + atm.AccountCode + "|";
                ATMAccountType atmtype = ServerLinq.ATMAccountTypes.SingleOrDefault(p => p.ATMAccountTypeCode == atm.ATMAccountTypeCode);
                double b1 = atmtype.Rate;
                str += "Lãi suất được hưởng: " + b1 + " %/năm|";
                // B2: Xác định số ngày được hưởng lãi =  now - ngày bắt đầu hưởng lãi
                DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                if (atm.DayForRate != null)
                {
                    DateTime HL = new DateTime(atm.DayForRate.Value.Year, atm.DayForRate.Value.Month, atm.DayForRate.Value.Day);
                    double b2 = now.Subtract(HL).Days;
                    // B3: Số tiền lãi 1 ngày được hưởng
                    // B3.1: Số tiền lãi được hưởng nếu dủ 360 ngày
                    double b31 = (double)((b1 * atm.Balance) / 100);
                    str += "Số tiền lãi nếu được hưởng đúng 360 ngày: (" + b1 + " * " + atm.Balance + ")/100 = " + b31 + "|";
                    // B3.2: Số tiền lãi 1 ngày dc hưởng
                    double b3 = (double)(b31 / 360);
                    str += "Số tiền lãi được hưởng trong 1 ngày: " + b31 + "/360 = " + b3 + "|";
                    // B4: Số tiền lãi được hưởng tính tới hiện tại
                    str += "Ngày bắt đầu tính lãi: " + HL + "|";
                    str += "Số Ngày được hưởng lãi: " + now + "-" + HL + " = " + b2 + "|";
                    double b4 = b3 * b2;
                    str += "Số tiền lãi được hưởng: "+b3+" * "+b2+" = " + b4 + "|";
                    // B5: Cộng lãi suất nhận được ở B3 vào số dư tài khoản
                    double b5 = (double)atm.Balance + b4;
                    str += "Số dư tài khoản sau khi cộng lãi: "+atm.Balance +"+"+b4+" = " + b5 + "|";
                    atm.Balance = b5;
                }
                // B5: Cộng số tiền gửi vào số dư
                atm.Balance += model.Money;
                str += "Số dư tài khoản sau khi nạp tiền: " + atm.Balance + "|";
                // B6: Cập nhật lại ngày bắt đầu hưởng lãi
                atm.DayForRate = now;
                str += "Cập nhật lại ngày bắt đầu tính lãi tiếp theo: " + now + "|";
                Meassages mes = new Meassages();
                mes._mes = str;
                return mes;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // Chuyen tien giua cac tai khoan
        public bool TranfersMoneyATMtoATM(TranferATMtoATM model)
        {
            try
            {
                if (model.Money >= 50000)
                {
                    // 4 bước tính lãi cho tài khoản chuyển
                    CountRate(model.FromAccountCode);
                    CountRate(model.ToAccountCode);
                    ServerLinq = new G62DBDataContext();
                    // B1: Xác định loại lãi suất đang áp dụng cho tài khoản này
                    ATMAccount atm = ServerLinq.ATMAccounts.SingleOrDefault(p => p.AccountCode == model.FromAccountCode);
                    DebitCard Fdb = ServerLinq.DebitCards.SingleOrDefault(p => p.ATMAccountCode == atm.ATMAcountCode);

                    ATMAccountType atmtype = ServerLinq.ATMAccountTypes.SingleOrDefault(p => p.ATMAccountTypeCode == atm.ATMAccountTypeCode);
                    DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    //B5: Kiểm tra số dư tk có thỏa số tiền cần chuyển không

                    if ((atm.Balance - 50000) >= model.SumMoney)
                    {
                        // B5: Cộng số tiền gửi vào số dư
                        atm.Balance -= model.SumMoney;
                        // B6: Cập nhật lại ngày bắt đầu hưởng lãi
                        atm.DayForRate = now;
                        // Tài khoản nhận
                        // 4 bước tính lãi cho tài khoản nhận
                        //CountRate(model.ToAccountCode);

                        ATMAccount atm2 = ServerLinq.ATMAccounts.SingleOrDefault(p => p.AccountCode == model.ToAccountCode);
                        DebitCard Tdb = ServerLinq.DebitCards.SingleOrDefault(p => p.ATMAccountCode == atm2.ATMAcountCode);
                        ATMAccountType atmtype2 = ServerLinq.ATMAccountTypes.SingleOrDefault(p => p.ATMAccountTypeCode == atm2.ATMAccountTypeCode);
                        atm2.Balance += model.Money;
                        atm2.DayForRate = now;

                        // Lưu nhật ký
                        DebitCardTransaction tra = new DebitCardTransaction();
                        tra.Balance = atm.Balance;
                        tra.Day = now;
                        tra.DebitCardTransactionCode = CreateCode();
                        tra.Description = model.Description;
                        tra.FromAccountCode = model.FromAccountCode;
                        tra.FromATMAccountCode = atm.ATMAcountCode;
                        tra.FromDebitCardCode = Fdb.DebitCardCode;
                        tra.Money = model.Money;
                        tra.SumCost = model.Cost;
                        tra.SumMoney = model.SumMoney;

                        TransactionType tratype = ServerLinq.TransactionTypes.SingleOrDefault(p => p.TransactionTypeCode == 17);
                        tra.TransactionTypeCode = tratype.TransactionTypeCode;
                        tra.TransactionTypeName = tratype.TransactionTypeName;
                        tra.Status = 1;
                        ServerLinq.DebitCardTransactions.InsertOnSubmit(tra);

                        // Cộng lãi cho Ngân Hàng
                        Bank bank = ServerLinq.Banks.SingleOrDefault(p => p.BankCode == "62");
                        bank.Profit_ATM_CreditCard += model.Cost;
                        bank.SumProfit += model.Cost;
                        bank.TotalProceedsDebitCard += model.Cost;

                        ServerLinq.SubmitChanges();
                        return true;
                    }
                    else
                        return false;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public Meassages GetStringTranfersMoneyATMtoATM(TranferATMtoATM model)
        {
            try
            {
                if (model.Money >= 50000)
                {
                    String str = "";
                    // 4 bước tính lãi cho tài khoản chuyển
                    str += GetStringCountRate(model.FromAccountCode)._mes;

                    ServerLinq = new G62DBDataContext();
                    // B1: Xác định loại lãi suất đang áp dụng cho tài khoản này
                    ATMAccount atm = ServerLinq.ATMAccounts.SingleOrDefault(p => p.AccountCode == model.FromAccountCode);
                    DebitCard Fdb = ServerLinq.DebitCards.SingleOrDefault(p => p.ATMAccountCode == atm.ATMAcountCode);

                    ATMAccountType atmtype = ServerLinq.ATMAccountTypes.SingleOrDefault(p => p.ATMAccountTypeCode == atm.ATMAccountTypeCode);
                    DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    //B5: Kiểm tra số dư tk có thỏa số tiền cần chuyển không
                    if ((atm.Balance - 50000) >= model.SumMoney)
                    {
                        CountRate(model.ToAccountCode);

                        // B5: Cộng số tiền gửi vào số dư
                        atm.Balance -= model.SumMoney;
                        str += "Trừ đi số tiền phải trả khi chuyển :" + model.SumMoney + " , Số dư còn lại = " + atm.Balance + "|";
                        // B6: Cập nhật lại ngày bắt đầu hưởng lãi
                        atm.DayForRate = now;

                        // Tài khoản nhận
                        // 4 bước tính lãi cho tài khoản nhận
                        //CountRate(model.ToAccountCode);
                        str += GetStringCountRate(model.ToAccountCode)._mes;

                        ATMAccount atm2 = ServerLinq.ATMAccounts.SingleOrDefault(p => p.AccountCode == model.ToAccountCode);
                        DebitCard Tdb = ServerLinq.DebitCards.SingleOrDefault(p => p.ATMAccountCode == atm2.ATMAcountCode);
                        ATMAccountType atmtype2 = ServerLinq.ATMAccountTypes.SingleOrDefault(p => p.ATMAccountTypeCode == atm2.ATMAccountTypeCode);

                        atm2.Balance += model.Money;
                        str += "Cộng vào số tiền được chuyển :" + model.Money + " , Số dư còn lại = " + atm2.Balance + "|";

                        Meassages mes = new Meassages();
                        mes._mes = str;
                        return mes;
                    }
                    else
                        return null;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // Chuyen tien từ tài khoản cho cá nhân
        public TranferATMtoPerson TranfersMoneyATMtoPerson(TranferATMtoPerson model)
        {
            try
            {
                if (model.Money >= 50000)
                {
                    // 4 bước tính lãi cho tài khoản chuyển
                    CountRate(model.FromAccountCode);
                    ServerLinq = new G62DBDataContext();
                    // B1: Xác định loại lãi suất đang áp dụng cho tài khoản này
                    ATMAccount atm = ServerLinq.ATMAccounts.SingleOrDefault(p => p.AccountCode == model.FromAccountCode);
                    DebitCard Fdb = ServerLinq.DebitCards.SingleOrDefault(p => p.ATMAccountCode == atm.ATMAcountCode);

                    ATMAccountType atmtype = ServerLinq.ATMAccountTypes.SingleOrDefault(p => p.ATMAccountTypeCode == atm.ATMAccountTypeCode);
                    DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    //B5: Kiểm tra số dư tk có thỏa số tiền cần chuyển không
                    if ((atm.Balance - 50000) >= model.SumMoney)
                    {

                        // B5: Trừ số tiền chuyển vào số dư
                        atm.Balance -= model.SumMoney;
                        // B6: Cập nhật lại ngày bắt đầu hưởng lãi
                        atm.DayForRate = now;
                        // Lưu nhật ký
                        DebitCardTransaction tra = new DebitCardTransaction();
                        tra.Balance = atm.Balance;
                        tra.Day = now;
                        tra.DebitCardTransactionCode = CreateCode();
                        tra.Description = model.Description;
                        tra.FromAccountCode = model.FromAccountCode;
                        tra.FromATMAccountCode = atm.ATMAcountCode;
                        tra.FromDebitCardCode = Fdb.DebitCardCode;
                        tra.Money = model.Money;
                        tra.SumCost = model.Cost;
                        tra.SumMoney = model.SumMoney;

                        // nguoi nhan
                        tra.ToCustomerID = model.ToCustomerID;
                        tra.ToCustomerName = model.ToCustomerName;

                        TransactionType tratype = ServerLinq.TransactionTypes.SingleOrDefault(p => p.TransactionTypeCode == 13);
                        tra.TransactionTypeCode = tratype.TransactionTypeCode;
                        tra.TransactionTypeName = tratype.TransactionTypeName;
                        tra.Status = 0;
                        ServerLinq.DebitCardTransactions.InsertOnSubmit(tra);

                        // Cộng lãi cho Ngân Hàng
                        Bank bank = ServerLinq.Banks.SingleOrDefault(p => p.BankCode == "62");
                        bank.Profit_ATM_CreditCard += model.Cost;
                        bank.SumProfit += model.Cost;
                        bank.TotalProceedsDebitCard += model.Cost;

                        ServerLinq.SubmitChanges();

                        model.TransactionTranferCode = tra.DebitCardTransactionCode;
                        return model;
                    }
                    else
                        return null;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public Meassages GetStringTranfersMoneyATMtoPerson(TranferATMtoPerson model)
        {
            try
            {
                String str = "";
                // 4 bước tính lãi cho tài khoản chuyển
                str += GetStringCountRate(model.FromAccountCode)._mes;

                ServerLinq = new G62DBDataContext();
                // B1: Xác định loại lãi suất đang áp dụng cho tài khoản này
                ATMAccount atm = ServerLinq.ATMAccounts.SingleOrDefault(p => p.AccountCode == model.FromAccountCode);
                DebitCard Fdb = ServerLinq.DebitCards.SingleOrDefault(p => p.ATMAccountCode == atm.ATMAcountCode);

                ATMAccountType atmtype = ServerLinq.ATMAccountTypes.SingleOrDefault(p => p.ATMAccountTypeCode == atm.ATMAccountTypeCode);
                DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                //B5: Kiểm tra số dư tk có thỏa số tiền cần chuyển không
                if ((atm.Balance - 50000) >= model.SumMoney)
                {
                    // B5: Cộng số tiền gửi vào số dư
                    atm.Balance -= model.SumMoney;
                    str += "Trừ đi số tiền phải trả khi chuyển :" + model.SumMoney + " , Số dư còn lại = " + atm.Balance + "|";
                    // B6: Cập nhật lại ngày bắt đầu hưởng lãi
                    atm.DayForRate = now;


                    str += "CMND người nhận :" + model.ToCustomerID + "| Họ Tên Người Nhận :" + model.ToCustomerName + "|";
                    Meassages mes = new Meassages();
                    mes._mes = str;
                    return mes;
                }
                else
                    return null;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool PersonGetMoney_TranfersMoneyATMtoPerson(TranferATMtoPerson model)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                DebitCardTransaction tran = ServerLinq.DebitCardTransactions.SingleOrDefault(p => p.DebitCardTransactionCode == model.TransactionTranferCode
                                && p.FromAccountCode == model.FromAccountCode
                                && p.Money == model.Money
                                && p.ToCustomerID == model.ToCustomerID
                                && p.ToCustomerName == model.ToCustomerName
                                && p.Status == 0);
                tran.Status = 1;
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        // Chuyen tien giữa các cá nhân
        public TranferPersontoPerson TranfersMoneyPersontoPerson(TranferPersontoPerson model)
        {
            try
            {
                if (model.Money >= 50000)
                {
                    ServerLinq = new G62DBDataContext();
                    DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    // Lưu nhật ký
                    CustomerTransaction tra = new CustomerTransaction();
                    tra.CustomerTransactionCode = CreateCode();
                    tra.Day = now;

                    tra.Description = model.Description;
                    tra.FromCustomerID = model.FromCustomerID;
                    tra.FromCustomnerName = model.FromCustomerName;
                    tra.ToCustomerID = model.ToCustomerID;
                    tra.ToCustomerName = model.ToCustomerName;

                    tra.Money = model.Money;
                    tra.SumCost = model.Cost;
                    tra.SumMoney = model.SumMoney;

                    TransactionType tratype = ServerLinq.TransactionTypes.SingleOrDefault(p => p.TransactionTypeCode == 14);
                    tra.TransactionTypeCode = tratype.TransactionTypeCode;
                    tra.TransactionTypeName = tratype.TransactionTypeName;
                    tra.Status = 0;
                    ServerLinq.CustomerTransactions.InsertOnSubmit(tra);

                    // Cộng lãi cho Ngân Hàng
                    Bank bank = ServerLinq.Banks.SingleOrDefault(p => p.BankCode == "62");
                    bank.Profit_Customer += model.Cost;
                    bank.SumProfit += model.Cost;
                    bank.TotalProceedsCustomer += model.Cost;

                    ServerLinq.SubmitChanges();

                    model.TransactionTranferCode = tra.CustomerTransactionCode;
                    return model;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public Meassages GetStringTranfersMoneyPersontoPerson(TranferPersontoPerson model)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                String str = "";
                DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                // Lưu nhật ký
                CustomerTransaction tra = new CustomerTransaction();
                tra.CustomerTransactionCode = CreateCode();
                tra.Day = now;
                str += "Ngày Chuyển: " + now + "|";
                tra.Description = model.Description;
                str += "Nội dung: " + model.Description + "|";
                tra.FromCustomerID = model.FromCustomerID;
                tra.FromCustomnerName = model.FromCustomerName;
                str += "Người Chuyển: " + model.FromCustomerName + "|";

                tra.ToCustomerID = model.ToCustomerID;
                tra.ToCustomerName = model.ToCustomerName;
                str += "Người Nhận: " + model.ToCustomerName + "|";

                tra.Money = model.Money;
                str += "Số Tiền: " + model.Money + "|";
                tra.SumCost = model.Cost;
                str += "Phí dịch vụ: " + model.Cost + "|";
                tra.SumMoney = model.SumMoney;
                str += "Tồng số tiền: " + model.SumMoney + "|";

                // cho nay cong phi vao ngan sach ngan hang

                TransactionType tratype = ServerLinq.TransactionTypes.SingleOrDefault(p => p.TransactionTypeCode == 14);
                tra.TransactionTypeCode = tratype.TransactionTypeCode;
                tra.TransactionTypeName = tratype.TransactionTypeName;

                Meassages mes = new Meassages();
                mes._mes = str;
                return mes;

            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool PersonGetMoney_TranfersMoneyPersontoPerson(TranferPersontoPerson model)
        {
            try
            {
                if (model.Money >= 50000)
                {
                    ServerLinq = new G62DBDataContext();
                    CustomerTransaction tran = ServerLinq.CustomerTransactions.SingleOrDefault(p => p.CustomerTransactionCode == model.TransactionTranferCode
                                    && p.FromCustomerID == model.FromCustomerID
                                    && p.FromCustomnerName == model.FromCustomerName
                                    && p.Money == model.Money
                                    && p.ToCustomerID == model.ToCustomerID
                                    && p.ToCustomerName == model.ToCustomerName
                                    && p.Status == 0);
                    tran.Status = 1;
                    ServerLinq.SubmitChanges();
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region CreditCard
        public List<Account> SearchCreditCard(SearchCreditCard sear)
        {
            List<Account> list = new List<Account>();
            try
            {
                string query = "SELECT Account.AccountCode, Account.FullName, Account.CustomerID, Account.AccountTypeCode, Account.status FROM ATMAccount, CreditCard, Account WHERE Account.AccountCode = ATMAccount.AccountCode AND ATMAccount.ATMAcountCode = CreditCard.ATMAccountCode ";
                if (sear.AccountCode != null && sear.AccountCode != "")
                    query += "AND Account.AccountCode LIKE '%" + sear.AccountCode + "%' ";
                if (sear.CreditCardCode != null && sear.CreditCardCode != "")
                    query += "AND DebitCard.CreditCardCode LIKE '%" + sear.CreditCardCode + "%' ";
                if (sear.CustomerID != null && sear.CustomerID != "")
                    query += "AND Account.CustomerID LIKE '%" + sear.CustomerID + "%' ";
                if (sear.CustomerName != null && sear.CustomerName != "")
                    query += "AND Account.CustomerName LIKE '%" + sear.CustomerName + "%' ";
                DataTable table = SQL.GetTable(query);
                foreach (DataRow row in table.Rows)
                {
                    Account de = new Account();
                    de.AccountCode = row["AccountCode"].ToString();
                    de.FullName = row["FullName"].ToString();
                    de.CustomerID = row["CustomerID"].ToString();
                    de.AccountTypeCode = int.Parse(row["AccountTypeCode"].ToString());
                    de.Status = int.Parse(row["Status"].ToString());
                    list.Add(de);
                }
            }
            catch (Exception)
            {
            }

            return list;
        }

        public CreditCard FindCreditCardByATMAccountCode(string code)
        {
            ServerLinq = new G62DBDataContext();
            return ServerLinq.CreditCards.SingleOrDefault(p => p.ATMAccountCode == code);
        }

        public bool LoanCreditCard(string code, double money)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                ATMAccount atm = ServerLinq.ATMAccounts.SingleOrDefault(p => p.AccountCode == code);
                CreditCard cre = ServerLinq.CreditCards.SingleOrDefault(p => p.ATMAccountCode == atm.ATMAcountCode);
                CreditCardLoan loan = new CreditCardLoan();
                if (cre.Loan != null || cre.Loan == 0)
                {
                    TransactionType tran = ServerLinq.TransactionTypes.SingleOrDefault(p => p.TransactionTypeCode == 19);
                    if ((tran.MaximumMoney != null || tran.MaximumMoney != 0) && cre.Loan + money <= tran.MaximumMoney && cre.Loan + money >= tran.MinimumMoney)
                    {
                        ATMAccountType atmtype = ServerLinq.ATMAccountTypes.SingleOrDefault(p => p.ATMAccountTypeCode == atm.ATMAccountTypeCode);
                        loan.ATMAccountCode = atm.ATMAcountCode;
                        loan.Day = DateTime.Now;
                        loan.DayLimit = DateTime.Now.Date.Add(new TimeSpan(45, 0, 0, 0));
                        loan.LoanCode = CreateCode();
                        loan.Money = money;
                        loan.NumberDayLimt = 45;
                        loan.NumberDayRemaining = 45;
                        loan.Pay = 0;
                        loan.PayRemaining = money;
                        loan.Rate = atmtype.Rate;
                        loan.Status = 0;

                        if (cre.Loan == null || cre.Loan == 0)
                            cre.Loan = money;
                        else
                            cre.Loan += money;

                        ServerLinq.CreditCardLoans.InsertOnSubmit(loan);
                        ServerLinq.SubmitChanges();
                        return true;
                    }
                    else
                        return false;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
            
        }

        public CreditCardLoan GetCreditCardLoan(string code)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                return ServerLinq.CreditCardLoans.SingleOrDefault(p => p.LoanCode == code);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public double CheckOverCreditCard(string code)
        {
            ServerLinq = new G62DBDataContext();
            CreditCardLoan loan = ServerLinq.CreditCardLoans.SingleOrDefault(p => p.LoanCode == code);
            TimeSpan span = DateTime.Now.Date.Subtract(loan.DayLimit.Value);
            if (span.Days > 0)
            {
                int songay = DateTime.Now.Date.Subtract(loan.DayLimit.Value).Days;
                double lai = (double)((loan.Rate * loan.Money * songay) / 3600);
                return (double) loan.PayRemaining + lai;
            }
            else
                return (double) loan.PayRemaining;
        }

        public List<CreditCardLoan> SearchLoanCreditCard(SearchReturnRateCreditCard sear)
        {
            ServerLinq = new G62DBDataContext();
            List<CreditCardLoan> list = new List<CreditCardLoan>();
            try
            {
                if (sear.AccountCode != null && sear.AccountCode != "")
                {
                    Account acc = ServerLinq.Accounts.SingleOrDefault(p => p.AccountCode == sear.AccountCode);
                    ATMAccount atm = ServerLinq.ATMAccounts.SingleOrDefault(p => p.AccountCode == acc.AccountCode);

                    var loan = from p in ServerLinq.CreditCardLoans
                               where p.ATMAccountCode == atm.ATMAcountCode && (p.Status == 0 || p.Status == 1)
                               select p;
                    foreach (var va in loan)
                        list.Add(va);
                }
                else
                {
                    if (sear.ATMAccountCode != null && sear.ATMAccountCode != "")
                    {
                        ATMAccount atm = ServerLinq.ATMAccounts.SingleOrDefault(p => p.ATMAcountCode == sear.ATMAccountCode);

                        var loan = from p in ServerLinq.CreditCardLoans
                                   where p.ATMAccountCode == atm.ATMAcountCode && (p.Status == 0 || p.Status == 1)
                                   select p;
                        foreach (var va in loan)
                            list.Add(va);
                    }
                    else {
                        if (sear.CreditCardCode != null && sear.CreditCardCode != "")
                        {
                            CreditCard cre = ServerLinq.CreditCards.SingleOrDefault(p => p.CreditCardCode == sear.CreditCardCode);

                            var loan = from p in ServerLinq.CreditCardLoans
                                       where p.ATMAccountCode == cre.ATMAccountCode && (p.Status == 0 || p.Status == 1)
                                       select p;
                            foreach (var va in loan)
                                list.Add(va);
                        }
                        else
                        {
                            var loan = from p in ServerLinq.CreditCardLoans
                                       where p.Status == 0 || p.Status == 1
                                       select p;
                            foreach (var va in loan)
                                list.Add(va);
                        }
                    }
                }

            }
            catch (Exception)
            {
                var loan = from p in ServerLinq.CreditCardLoans
                            select p;
                foreach (var va in loan)
                    list.Add(va);
            }

            return list;
        }

        public bool ReturnRateCreditCard_Path_Over(string code, double money)
        {
            // code = ma LoadCreditCardCode
            try
            {
                ServerLinq = new G62DBDataContext();
                CreditCardLoan loan = ServerLinq.CreditCardLoans.SingleOrDefault(p => p.LoanCode == code);
                // kiem tra co qua han hong
                TimeSpan span = DateTime.Now.Date.Subtract(loan.DayLimit.Value);
                if (span.Days > 0)
                    return ReturnRateCreditCard_Over(code, money);
                else
                    return ReturnRateCreditCard_Path(code, money);
            }
            catch (Exception)
            {
                return false;
            }
        }

        // trước hạn - không tính lãi
        public bool ReturnRateCreditCard_Path(string code, double money)
        {
            // code = ma LoadCreditCardCode
            try
            {
                ServerLinq = new G62DBDataContext();
                CreditCardLoan loan = ServerLinq.CreditCardLoans.SingleOrDefault(p => p.LoanCode == code);
                loan.Pay += money;
                
                if (loan.Pay >= loan.PayRemaining) // tra het
                {
                    loan.PayRemaining -= money;
                    loan.NumberDayRemaining = 0;
                    loan.Status = 2;
                }
                else
                {
                    loan.PayRemaining -= money;
                    DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    loan.DayLimit =  loan.DayLimit.Value.Add(new TimeSpan(45, 0, 0, 0));
                    loan.NumberDayRemaining = 45;
                    loan.Status = 1;
                }
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        // quá hạn - tính lãi
        public bool ReturnRateCreditCard_Over(string code, double money)
        {
            // code = ma LoadCreditCardCode
            try
            {
                ServerLinq = new G62DBDataContext();
                CreditCardLoan loan = ServerLinq.CreditCardLoans.SingleOrDefault(p => p.LoanCode == code);
                // số ngày quá hạn
                int songay = DateTime.Now.Date.Subtract(loan.DayLimit.Value).Days;
                double lai = (double)((loan.Rate * loan.Money * songay) / 3600);

                loan.PayRemaining += lai; 

                loan.Pay += money;
                if (loan.Pay >= loan.PayRemaining) // tra het
                {
                    loan.PayRemaining -= money;
                    loan.NumberDayRemaining = 0;
                    loan.Status = 2;
                }
                else
                {
                    loan.PayRemaining -= money;
                    loan.Day = DateTime.Now.Date;
                    DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    loan.DayLimit = loan.DayLimit.Value.Add(new TimeSpan(45, 0, 0, 0));
                    loan.NumberDayRemaining = 45;
                    loan.Status = 1;
                }
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region SavingBook

        public List<SavingBookType> GetListSavingBookType()
        {
            List<SavingBookType> list = new List<SavingBookType>();
            ServerLinq = new G62DBDataContext();
            var table = from savt in ServerLinq.SavingBookTypes
                        select savt;

            foreach (var row in table)
                list.Add(row);
            return list;
        }

        public SavingBookType GetSavingBookTypeByCode(int code)
        {
            ServerLinq = new G62DBDataContext();
            return ServerLinq.SavingBookTypes.SingleOrDefault(p => p.SavingBookTypeCode == code);
        }

        public SavingBook CreateSavingBook(Customer customer, int code, double money)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                SavingBook savingbook = new SavingBook();

                // Mở sổ tiết kiệm = Tạo 1 tài khoản + Tạo 1 sổ tiết kiệm
                ServerLinq = new G62DBDataContext();
                // tạo tài khoản ngân hàng
                Account acc = new Account();
                acc.AccountCode = CreateAccountCode();
                acc.AccountTypeCode = 2;
                acc.CustomerID = customer.CustomerID;
                acc.Status = 0;
                acc.FullName = customer.FullName;
                acc.PinCode = CreatePin();

                SavingBookType savingbooktype = ServerLinq.SavingBookTypes.SingleOrDefault(p => p.SavingBookTypeCode == code);

                if (money < savingbooktype.MinimumMoney)
                    return null;
                savingbook.AccountCode = acc.AccountCode;
                savingbook.CustomerID = customer.CustomerID;
                savingbook.CustomerName = customer.FullName;
                savingbook.CustomerAddress = customer.Address;
                // ngay tao
                savingbook.DayCreated = DateTime.Now.Date;
                // ngay bat dau tinh lai
                savingbook.DayforRate = DateTime.Now.Date;
                // ngay đáo hạn

                int songay = 0;
                try
                {
                    songay = int.Parse(savingbooktype.SavingBookTime.ToString());
                    DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    savingbook.DayExpired = now.Add(new TimeSpan(songay, 0, 0, 0, 0));
                }
                catch (Exception)
                {
                }
                savingbook.Status = 0;
                savingbook.Money = money;
                savingbook.RateNow = savingbooktype.Rate;
                savingbook.RateBeforeTerm = savingbooktype.RateBeforeTerm;
                savingbook.SavingBookCode = CreateCode();
                savingbook.SavingBookTypeCode = code;

                ServerLinq.Accounts.InsertOnSubmit(acc);
                ServerLinq.SavingBooks.InsertOnSubmit(savingbook);
                Bank bank = ServerLinq.Banks.SingleOrDefault(p => p.BankCode == "62");
                bank.TotalProceedsSavingBook += savingbook.Money;
                bank.TotalProceeds += savingbook.Money;

                ServerLinq.SubmitChanges();
                return savingbook;
            }
            catch (Exception)
            {
                return null;
            }
            
        }

        public SavingBook GetSavingBookByCode(string code)
        {
            ServerLinq = new G62DBDataContext();
            return ServerLinq.SavingBooks.SingleOrDefault(p=>p.SavingBookCode == code);
        }

        public List<SavingBook> SearchSavingBook(SearchSavingBook sear)
        {
            DataTable table = new DataTable();
            string query = "Select * from SavingBook ";
            if (sear.AccountCode != null && sear.AccountCode != "")
            {
                if (!query.Contains("Where"))
                    query += "Where AccountCode LIKE '%" + sear.AccountCode + "%'";
                else
                    query += "AND AccountCode LIKE '%" + sear.AccountCode + "%'";
            }
            if (sear.CustomerID != null && sear.CustomerID != "")
            {
                if (!query.Contains("Where"))
                    query += "Where CustomerID LIKE '%" + sear.CustomerID + "%'";
                else
                    query += "AND CustomerID LIKE '%" + sear.CustomerID + "%'";
            }
            if (sear.CustomerName != null && sear.CustomerName != "")
            {
                if (!query.Contains("Where"))
                    query += "Where CustomerName LIKE '%" + sear.CustomerName + "%'";
                else
                    query += "AND CustomerName LIKE '%" + sear.CustomerName + "%'";
            }
            if (sear.CustomerAddress != null && sear.CustomerAddress != "")
            {
                if (!query.Contains("Where"))
                    query += "Where CustomerAddress LIKE '%" + sear.CustomerAddress + "%'";
                else
                    query += "AND CustomerAddress LIKE '%" + sear.CustomerAddress + "%'";
            }

            if (sear.SavingBookCode != null && sear.SavingBookCode != "")
            {
                if (!query.Contains("Where"))
                    query += "Where SavingBookCode LIKE '%" + sear.SavingBookCode + "%'";
                else
                    query += "AND SavingBookCode LIKE '%" + sear.SavingBookCode + "%'";
            }

            if (sear.Type)
            {
                if (!query.Contains("Where"))
                    query += "Where SavingBookTypeCode = " + sear.SavingBookTypeCode;
                else
                    query += "AND SavingBookTypeCode = " + sear.SavingBookTypeCode;
            }

            if (sear.Money != null && sear.Money != "")
            {
                if (!query.Contains("Where"))
                    query += "Where Money = " + sear.Money;
                else
                    query += "AND Money = " + sear.Money;
            }
            if (!query.Contains("Where"))
                query += "Where Status = '0'";
            else
                query += "And Status = '0'";

            table = SQL.GetTable(query);
            List<SavingBook> list = new List<SavingBook>();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                SavingBook sav = new SavingBook();
                sav.SavingBookCode = table.Rows[i]["SavingBookCode"].ToString();
                sav.AccountCode = table.Rows[i]["AccountCode"].ToString();
                sav.SavingBookTypeCode = int.Parse(table.Rows[i]["SavingBookTypeCode"].ToString());
                try{
                    string[] da = table.Rows[i]["DayCreated"].ToString().Split(' ')[0].Split('/');
                    int y = int.Parse(da[2]);
                    int m = int.Parse(da[0]);
                    int d = int.Parse(da[1]);
                    sav.DayCreated = new DateTime(y,m,d);
                }
                catch (Exception){}

                try{
                    string[] da2 = table.Rows[i]["DayExpired"].ToString().Split(' ')[0].Split('/');
                    int y = int.Parse(da2[2]);
                    int m = int.Parse(da2[0]);
                    int d = int.Parse(da2[1]);
                    sav.DayExpired = new DateTime(y, m, d);
                }
                catch (Exception){}

                sav.Money = double.Parse(table.Rows[i]["Money"].ToString());
                sav.Status = int.Parse(table.Rows[i]["Status"].ToString());
                try{
                    string[] da2 = table.Rows[i]["DayWithDraw"].ToString().Split(' ')[0].Split('/');
                    int y = int.Parse(da2[2]);
                    int m = int.Parse(da2[0]);
                    int d = int.Parse(da2[1]);
                    sav.DayWithDraw = new DateTime(y, m, d);
                }
                catch (Exception){}

                try { sav.Payment = double.Parse(table.Rows[i]["Payment"].ToString()); }
                catch (Exception){ }

                try { sav.RateNow = int.Parse(table.Rows[i]["RateNow"].ToString()); }
                catch (Exception) { }

                try { sav.RateBeforeTerm = int.Parse(table.Rows[i]["RateBeforeTerm"].ToString()); }
                catch (Exception) { }

                try { sav.CustomerID = table.Rows[i]["CustomerID"].ToString(); }
                catch (Exception) { }

                try { sav.CustomerName = table.Rows[i]["CustomerName"].ToString(); }
                catch (Exception) { }

                try { sav.CustomerAddress = table.Rows[i]["CustomerAddress"].ToString(); }
                catch (Exception) { }

                list.Add(sav);
            }
            
            return list;
        }

        public bool EditSavingBookType(SavingBookType sav)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                SavingBookType sa = ServerLinq.SavingBookTypes.SingleOrDefault(p => p.SavingBookTypeCode == sav.SavingBookTypeCode);
                sa.DayApply = sav.DayApply;
                sa.MaximumMoney = sav.MaximumMoney;
                sa.MinimumMoney = sav.MinimumMoney;
                sa.PaymentType = sav.PaymentType;
                sa.Rate = sav.Rate;
                sa.RateBeforeTerm = sav.RateBeforeTerm;
                sa.SavingBookTime = sav.SavingBookTime;
                sa.SavingBookTypeName = sav.SavingBookTypeName;
                sa.TypeOfDeposit = sav.TypeOfDeposit;
                sa.Other = sav.Other;
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteSavingBookType(int code)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                SavingBookType sav = ServerLinq.SavingBookTypes.SingleOrDefault(p => p.SavingBookTypeCode == code);
                var va = from p in ServerLinq.SavingBooks
                         where p.SavingBookTypeCode == sav.SavingBookTypeCode
                         select p;
                if (va == null || va.Count() == 0 )
                {
                    ServerLinq.SavingBookTypes.DeleteOnSubmit(sav);
                    ServerLinq.SubmitChanges();
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CreateSavingBookType(SavingBookType sav)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                SavingBookType sa = new SavingBookType();
                sa.DayApply = DateTime.Now;
                sa.MaximumMoney = sav.MaximumMoney;
                sa.MinimumMoney = sav.MinimumMoney;
                sa.Other = sav.Other;
                sa.PaymentType = sav.PaymentType;
                sa.Rate = sav.Rate;
                sa.RateBeforeTerm = sav.RateBeforeTerm;
                sa.SavingBookTime = sav.SavingBookTime;
                sa.SavingBookTypeName = sav.SavingBookTypeName;
                sa.TypeOfDeposit = sav.TypeOfDeposit;
                ServerLinq.SavingBookTypes.InsertOnSubmit(sa);
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #region Rút tiến trước hạn
        public List<string> CheckGetMoney(string code, string money)
        {
            List<string> list = new List<string>();
            string str = "";
            ServerLinq = new G62DBDataContext();
            SavingBook sav = ServerLinq.SavingBooks.SingleOrDefault(p => p.SavingBookCode == code);
            if (sav != null)
            {
                double mg = double.Parse(money);
                str += "Số tiền rút: "+ mg +" |";
                // tính lãi
                DateTime darate = new DateTime(sav.DayforRate.Value.Year, sav.DayforRate.Value.Month, sav.DayforRate.Value.Day);
                str += "Ngày bắt đầu tính lãi: " + darate + " |";
                DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                DateTime daex = new DateTime(sav.DayExpired.Value.Year, sav.DayExpired.Value.Month, sav.DayExpired.Value.Day);
                str += "Ngày đáo hạn: " + daex + " |";
                // kiem tra là rút tiền trước hạn hay đúng hạn
                TimeSpan tsp = daex.Subtract(now);
                str += "Số ngày được hưởng lãi Không kỳ hạn: " + now.Subtract(darate).Days + " |";
                SavingBookType savtype = ServerLinq.SavingBookTypes.SingleOrDefault(p => p.SavingBookTypeCode == sav.SavingBookTypeCode);
                str += "Lãi suất trước hạn cho số tiền rút: " + sav.RateBeforeTerm + "% |";
                if (tsp.Days > 0)  // trước hạn
                {
                    double nganhangtralai = CountRateGetMoney_SavingBook(sav.RateBeforeTerm, mg, now.Subtract(darate).Days);
                    str += "Tồng lãi tính dc: " + nganhangtralai + " |";
                    // trừ cái này vào tổng doanh thu
                    double MoneyEnd = mg + nganhangtralai;
                    str += "Số tiền Thanh toán: " + MoneyEnd + " |";
                    // tính phí cho ngân hàng
                    TransactionType tran = ServerLinq.TransactionTypes.SingleOrDefault(p=>p.TransactionTypeCode == 11);
                    double cost = (tran.Cost * mg)/100;
                    str += "Số tiền Phí dịch vụ: " + cost + " |";
                    if (sav.Money - mg - cost < savtype.MinimumMoney)
                    {
                        list.Clear();
                        list.Add("false");
                        return list;
                    }

                    str += "Số dư còn lại: " + (sav.Money - mg - cost) + " |";
                    list.Clear();
                    list.Add(MoneyEnd.ToString());
                    list.Add(str);
                    return list;
                }
                else
                {
                    list.Clear();
                    list.Add("false");
                    return list;
                }
            }
            list.Clear();
            list.Add("false");
            return list;
        }

        public double CountRateGetMoney_SavingBook(double rate_truochan, double sotien_rut, int songay)
        {
            // B1: Số tiền lãi được hưởng nếu dủ 360 ngày
            double b1 = (double)((rate_truochan * sotien_rut) / 100);
            // B2: Số tiền lãi được hưởng trong 1 ngày
            double b2 = b1 / 360;

            return b2 * songay;
        }

        public double GetMoneySavingBook(string code, string money)
        {
            ServerLinq = new G62DBDataContext();
            SavingBook sav = ServerLinq.SavingBooks.SingleOrDefault(p => p.SavingBookCode == code);
            if (sav != null)
            {
                double mg = double.Parse(money);
                // tính lãi
                DateTime darate = new DateTime(sav.DayforRate.Value.Year, sav.DayforRate.Value.Month, sav.DayforRate.Value.Day);
                DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                DateTime daex = new DateTime(sav.DayExpired.Value.Year, sav.DayExpired.Value.Month, sav.DayExpired.Value.Day);

                // kiem tra là rút tiền trước hạn hay đúng hạn
                TimeSpan tsp = daex.Subtract(now);
                SavingBookType savtype = ServerLinq.SavingBookTypes.SingleOrDefault(p => p.SavingBookTypeCode == sav.SavingBookTypeCode);

                if (tsp.Days > 0)  // trước hạn
                {
                    double nganhangtralai = CountRateGetMoney_SavingBook(sav.RateBeforeTerm, mg, now.Subtract(darate).Days);
                    // trừ cái này vào tổng doanh thu
                    double MoneyEnd = mg + nganhangtralai;
                    Bank bank = ServerLinq.Banks.SingleOrDefault(p => p.BankCode == "62");
                    bank.TotalProceedsSavingBook -= MoneyEnd;
                    bank.TotalProceeds -= MoneyEnd;

                    // tính phí cho ngân hàng
                    TransactionType tran = ServerLinq.TransactionTypes.SingleOrDefault(p => p.TransactionTypeCode == 11);
                    double cost = (tran.Cost * mg) / 100;
                    bank.Profit_SavingBook += cost;
                    bank.SumProfit += cost;

                    if (sav.Money - mg - cost < savtype.MinimumMoney)
                        return -1;

                    sav.Money -= mg - cost;
                    sav.DayWithDraw = now;
                    sav.Payment = MoneyEnd;
                    ServerLinq.SubmitChanges();
                    return MoneyEnd;
                }
                else
                {
                    return -1;
                }
            }
            return -1;
        }

        public string GetCostGetMoney()
        {
            ServerLinq = new G62DBDataContext();
            TransactionType tran = ServerLinq.TransactionTypes.SingleOrDefault(p => p.TransactionTypeCode == 11);
            return tran.Cost.ToString();
        }
        #endregion


        #region Đáo hạn - rút tiền đúng hạn
        public List<string> CheckMaturity(string code, bool check)
        {
            List<string> list = new List<string>();
            string str = "";
            ServerLinq = new G62DBDataContext();
            SavingBook sav = ServerLinq.SavingBooks.SingleOrDefault(p => p.SavingBookCode == code);
            if (sav != null)
            {
                double mg = sav.Money;
                str += "Số tiền rút: " + mg + " |";
                // tính lãi
                DateTime darate = new DateTime(sav.DayforRate.Value.Year, sav.DayforRate.Value.Month, sav.DayforRate.Value.Day);
                str += "Ngày bắt đầu tính lãi: " + darate + " |";
                DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                DateTime daex = new DateTime(sav.DayExpired.Value.Year, sav.DayExpired.Value.Month, sav.DayExpired.Value.Day);
                str += "Ngày đáo hạn: " + daex + " |";
                // kiem tra là rút tiền trước hạn hay đúng hạn
                TimeSpan tsp = daex.Subtract(now);
                str += "Lãi suất: " + sav.RateNow + "% |";

                SavingBookType savtype = ServerLinq.SavingBookTypes.SingleOrDefault(p => p.SavingBookTypeCode == sav.SavingBookTypeCode);
                if (tsp.Days == 0)  // đúng hạn
                {
                    double nganhangtralai = 0;
                    if (sav.SavingBookTypeCode == 1) //khong ki han
                        nganhangtralai = CountRateGetMoney_SavingBook(sav.RateNow, mg, now.Subtract(darate).Days);
                    else
                        nganhangtralai = CountRateMaturity_SavingBook(sav.RateNow, mg);

                    if (check) // tính lai thoi
                    {
                        list.Clear();
                        list.Add(nganhangtralai.ToString());
                        list.Add(str);
                        return list;
                    }
                    else
                    {
                        str += "Tồng lãi tính dc: " + nganhangtralai + " |";
                        // trừ cái này vào tổng doanh thu
                        double MoneyEnd = mg + nganhangtralai;
                        str += "Số tiền Thanh toán: " + MoneyEnd + " |";
                        // tính phí cho ngân hàng
                        TransactionType tran = ServerLinq.TransactionTypes.SingleOrDefault(p => p.TransactionTypeCode == 10);
                        double cost = (tran.Cost * mg) / 100;
                        str += "Số tiền Phí dịch vụ: " + cost + " |";

                        list.Clear();
                        list.Add(MoneyEnd.ToString());
                        list.Add(str);
                        return list;
                    }
                }
                else
                {
                    list.Clear();
                    list.Add("false");
                    return list;
                }
            }
            list.Clear();
            list.Add("false");
            return list;
        }

        public double CountRateMaturity_SavingBook(double rate, double sotien)
        {
            // B1: Số tiền lãi được hưởng nếu dủ 360 ngày
            double b1 = (double)((rate * sotien) / 100);
            return b1;
        }

        public double MaturitySavingBook(string code)
        {
            List<string> list = new List<string>();
            string str = "";
            ServerLinq = new G62DBDataContext();
            SavingBook sav = ServerLinq.SavingBooks.SingleOrDefault(p => p.SavingBookCode == code);
            if (sav != null)
            {
                double mg = sav.Money;
                // tính lãi
                DateTime darate = new DateTime(sav.DayforRate.Value.Year, sav.DayforRate.Value.Month, sav.DayforRate.Value.Day);
                DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                DateTime daex = new DateTime(sav.DayExpired.Value.Year, sav.DayExpired.Value.Month, sav.DayExpired.Value.Day);
                // kiem tra là rút tiền trước hạn hay đúng hạn
                TimeSpan tsp = daex.Subtract(now);

                SavingBookType savtype = ServerLinq.SavingBookTypes.SingleOrDefault(p => p.SavingBookTypeCode == sav.SavingBookTypeCode);
                if (tsp.Days == 0)  // đúng hạn
                {
                    double nganhangtralai = 0;
                    if (sav.SavingBookTypeCode == 1) //khong ki han
                        nganhangtralai = CountRateGetMoney_SavingBook(sav.RateNow, mg, now.Subtract(darate).Days);
                    else
                        nganhangtralai = CountRateMaturity_SavingBook(sav.RateNow, mg);
                    // trừ cái này vào tổng doanh thu
                    double MoneyEnd = mg + nganhangtralai;

                    // tính phí cho ngân hàng - ở đây = 0
                    TransactionType tran = ServerLinq.TransactionTypes.SingleOrDefault(p => p.TransactionTypeCode == 10);
                    double cost = (tran.Cost * mg) / 100;
                    
                    Bank bank = ServerLinq.Banks.SingleOrDefault(p => p.BankCode == "62");
                    bank.Profit_SavingBook -= MoneyEnd;
                    bank.SumProfit -= MoneyEnd;
                    bank.TotalProceedsSavingBook -= MoneyEnd;
                    bank.TotalProceeds -= MoneyEnd;

                    // cộng phí cho ngân hàng - ở đây = 0
                    bank.Profit_SavingBook += cost;
                    bank.SumProfit += cost;

                    // cập nhật lại cho sav
                    sav.DayWithDraw = now;
                    sav.Payment = MoneyEnd;
                    sav.Status = 1;
                    ServerLinq.SubmitChanges();
                    return MoneyEnd;
                }
                else
                {
                    return -1;
                }
            }
            return -1;
        }

        public string GetCostMaturity()
        {
            ServerLinq = new G62DBDataContext();
            TransactionType tran = ServerLinq.TransactionTypes.SingleOrDefault(p => p.TransactionTypeCode == 10);
            return tran.Cost.ToString();
        }
        #endregion


        #region Quá Hạn - rút tiền quá hạn
        public List<string> CheckTimeOut(string code, bool check)
        {
            List<string> list = new List<string>();
            string str = "";
            ServerLinq = new G62DBDataContext();
            SavingBook sav = ServerLinq.SavingBooks.SingleOrDefault(p => p.SavingBookCode == code);
            if (sav != null)
            {
                double mg = sav.Money;
                str += "Số tiền rút: " + mg + "|";
                // tính lãi
                DateTime darate = new DateTime(sav.DayforRate.Value.Year, sav.DayforRate.Value.Month, sav.DayforRate.Value.Day);
                str += "Ngày bắt đầu tính lại đúng hạn: " + darate + "|";
                DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                DateTime daex = new DateTime(sav.DayExpired.Value.Year, sav.DayExpired.Value.Month, sav.DayExpired.Value.Day);
                str += "Ngày quá hạn: " + daex + "|";
                str += "Ngày bắt đầu tính lãi quá hạn: " + daex + "|";
                // ngay dung de kiem tra tinh trang cua so tiet kiem
                int ngaykt = daex.Subtract(now).Days;
                // so ngay qua han
                int songayquahan = now.Subtract(daex).Days;
                str += "Số ngày quá hạn: " + songayquahan + "|";
                SavingBookType savtype = ServerLinq.SavingBookTypes.SingleOrDefault(p => p.SavingBookTypeCode == sav.SavingBookTypeCode);
                if (ngaykt < 0)  // quá hạn
                {

                    // lãi đúng hạn
                    double laidunghan = TinhLaiDungHan_SavingBook(sav.RateNow, mg);
                    str += "Tiền lãi đúng hạn: " + laidunghan + "|";
                    // tổng tiền vốn + lãi đúng hạn
                    double Sum1 = mg + laidunghan;
                    str += "Tồng số tiền phải trả đúng hạn: " + Sum1 + "|";
                    // lãi quá hạn
                    SavingBookType sav_khongkyhan = ServerLinq.SavingBookTypes.SingleOrDefault(p => p.SavingBookTypeCode == 1);
                    double laiquahan = TinhLaiQuaHan_SavingBook(sav_khongkyhan.Rate, Sum1, songayquahan);
                    str += "Số tiền lãi quá hạn trên tổng số tiền phải trả đúng hạn theo số ngày quá hạn: " + laiquahan + "|";
                    if (check) // chỉ show phẩn lãi thôi
                    {
                        list.Clear();
                        list.Add("true");
                        list.Add(laidunghan.ToString());
                        list.Add(Sum1.ToString());
                        list.Add(laiquahan.ToString());
                        return list;
                    }
                    else
                    {
                        // tổng số tiền phải thanh toán
                        double MoneyEnd = Sum1 + laiquahan;
                        str += "Số tiền thu được sau cùng: " + MoneyEnd + "|";
                        // tính phí cho ngân hàng - ở hiện tại phí này =0
                        TransactionType tran = ServerLinq.TransactionTypes.SingleOrDefault(p => p.TransactionTypeCode == 18);
                        double cost = (tran.Cost * mg) / 100;
                        str += "Phí dịch vụ: " + cost + "|";
                        //Bank bank = ServerLinq.Banks.SingleOrDefault(p => p.BankCode == "62");
                        //bank.Profit_SavingBook -= MoneyEnd;
                        //bank.SumProfit -= MoneyEnd;
                        //bank.TotalProceedsSavingBook -= MoneyEnd;
                        //bank.TotalProceeds -= MoneyEnd;

                        //// cộng phí cho ngân hàng - ở hiện tại phí này =0
                        //bank.Profit_SavingBook += cost;
                        //bank.SumProfit += cost;
                        list.Clear();
                        list.Add("true");
                        list.Add(str);
                        return list;
                    }

                }
                else
                {
                    list.Clear();
                    list.Add("false");
                    return list;
                }
            }
            list.Clear();
            list.Add("false");
            return list;
        }

        // lãi suất đúng hạn
        public double TinhLaiDungHan_SavingBook(double rate, double sotien)
        {
            // B1: Số tiền lãi được hưởng nếu dủ 360 ngày
            double b1 = (double)((rate * sotien) / 100);
            return b1;
        }
        // lãi suất phần quá hạn cho phần (vốn + lãi) trên thời gian quá hạn
        public double TinhLaiQuaHan_SavingBook(double rate, double sotien, int songay)
        {
            // B1: Số tiền lãi được hưởng nếu dủ 360 ngày
            double b1 = (double)((rate * sotien) / 100);
            double b2 = (b1 / 360) * songay;
            return b2;
        }

        public double TimeOutSavingBook(string code)
        {
            ServerLinq = new G62DBDataContext();
            SavingBook sav = ServerLinq.SavingBooks.SingleOrDefault(p => p.SavingBookCode == code);
            if (sav != null)
            {
                double mg = sav.Money;
                // tính lãi
                DateTime darate = new DateTime(sav.DayforRate.Value.Year, sav.DayforRate.Value.Month, sav.DayforRate.Value.Day);
                DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                DateTime daex = new DateTime(sav.DayExpired.Value.Year, sav.DayExpired.Value.Month, sav.DayExpired.Value.Day);
                // ngay dung de kiem tra tinh trang cua so tiet kiem
                int ngaykt = daex.Subtract(now).Days;
                // so ngay qua han
                int songayquahan = now.Subtract(daex).Days;
                SavingBookType savtype = ServerLinq.SavingBookTypes.SingleOrDefault(p => p.SavingBookTypeCode == sav.SavingBookTypeCode);
                if (ngaykt < 0)  // quá hạn
                {
                    // lãi đúng hạn
                    double laidunghan = TinhLaiDungHan_SavingBook(sav.RateNow, mg);

                    // tổng tiền vốn + lãi đúng hạn
                    double Sum1 = mg + laidunghan;
                    
                    // lãi quá hạn
                    SavingBookType sav_khongkyhan = ServerLinq.SavingBookTypes.SingleOrDefault(p=>p.SavingBookTypeCode == 1);
                    double laiquahan = TinhLaiQuaHan_SavingBook(sav_khongkyhan.Rate, Sum1, songayquahan);

                    // tổng số tiền phải thanh toán
                    double MoneyEnd = Sum1 + laiquahan;

                    // tính phí cho ngân hàng - ở hiện tại phí này =0
                    TransactionType tran = ServerLinq.TransactionTypes.SingleOrDefault(p => p.TransactionTypeCode == 18);
                    double cost = (tran.Cost * mg) / 100;

                    Bank bank = ServerLinq.Banks.SingleOrDefault(p => p.BankCode == "62");
                    bank.Profit_SavingBook -= MoneyEnd;
                    bank.SumProfit -= MoneyEnd;
                    bank.TotalProceedsSavingBook -= MoneyEnd;
                    bank.TotalProceeds -= MoneyEnd;

                    // cộng phí cho ngân hàng - ở hiện tại phí này =0
                    bank.Profit_SavingBook += cost;
                    bank.SumProfit += cost;

                    // cập nhật lại cho sav
                    sav.DayWithDraw = now;
                    sav.Payment = MoneyEnd;
                    sav.Status = 1;
                    ServerLinq.SubmitChanges();
                    return MoneyEnd;
                }
                else
                {
                    return -1;
                }
            }
            return -1;
        }

        public string GetCostTimeOut()
        {
            ServerLinq = new G62DBDataContext();
            TransactionType tran = ServerLinq.TransactionTypes.SingleOrDefault(p => p.TransactionTypeCode == 18);
            return tran.Cost.ToString();
        }
        #endregion
        #endregion

        #region BankSystem
        public BankSystem GetBankByCode(string code)
        {
            ServerLinq = new G62DBDataContext();
            return ServerLinq.BankSystems.SingleOrDefault(p => p.BankCode == code);
        }
        #endregion

        #region TransactionType
        public List<TransactionType> GetListTransactionType()
        {
            ServerLinq = new G62DBDataContext();
            List<TransactionType> list = new List<TransactionType>();
            var va = from p in ServerLinq.TransactionTypes
                     select p;
            foreach (var v in va)
                list.Add(v);
            return list;
        }
        public TransactionType GetTransactionTypeByCode(int code)
        {
            ServerLinq = new G62DBDataContext();
            return ServerLinq.TransactionTypes.SingleOrDefault(p => p.TransactionTypeCode == code);
        }

        public bool EditTransactionType(TransactionType tra)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                TransactionType tran = ServerLinq.TransactionTypes.SingleOrDefault(p => p.TransactionTypeCode == tra.TransactionTypeCode);
                tran.Cost = tra.Cost;
                tran.DayApply = tra.DayApply;
                tran.Description = tra.Description;
                tran.MaximumMoney = tra.MaximumMoney;
                tran.MinimumMoney = tra.MinimumMoney;
                tran.TransactionTypeName = tra.TransactionTypeName;
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region Customer
        public bool ChangeFullNameCustomer(string username, string fullname)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                Customer customer = ServerLinq.Customers.SingleOrDefault(p => p.UserName == username);
                customer.FullName = fullname;
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            
        }
        public bool ChangeAddressCustomer(string username, string address)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                Customer customer = ServerLinq.Customers.SingleOrDefault(p => p.UserName == username);
                customer.Address = address;
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public bool ChangePhoneCustomer(string username, string phone)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                Customer customer = ServerLinq.Customers.SingleOrDefault(p => p.UserName == username);
                customer.Phone = phone;
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public bool ChangeBirthdayCustomer(string username, DateTime date)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                Customer customer = ServerLinq.Customers.SingleOrDefault(p => p.UserName == username);
                customer.Birthday = date;
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }


        public Customer MCreateCustomer(Customer customer)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                Customer cus = ServerLinq.Customers.SingleOrDefault(p => p.CustomerID == customer.CustomerID);
                if (cus == null)
                {
                    customer.DayCreated = DateTime.Now;
                    customer.Password = customer.UserName;
                    ServerLinq.Customers.InsertOnSubmit(customer);
                    ServerLinq.SubmitChanges();
                    return customer;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
            
            
        }

        public List<Customer> GetListCustomer()
        {
            List<Customer> list = new List<Customer>();
            var cus = from p in ServerLinq.Customers
                      where p.Status == 1
                      select p;
            foreach (var cu in cus)
            {
                list.Add(cu);
            }
            return list;
        }
        #endregion

        #region CreateCode

        public string CreatePin()
        {
            return DateTime.Now.ToString("mmssff");
        }

        public string CreateCode()
        {
            return DateTime.Now.ToString("yyyyddMMHHmmssff");
        }

        public string CreateAccountCode()
        {
            ServerLinq = new G62DBDataContext();
            string bankcode = ServerLinq.Banks.SingleOrDefault(p => p.BankCode != "").BankCode;
            return bankcode + "" + DateTime.Now.ToString("yyyyddMMHHmmss");
        }
        #endregion


        //======================= MANAGER =============================//

        #region UserManager
        public bool LoginManager(string username, string password)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                UserManager user = ServerLinq.UserManagers.SingleOrDefault(p => p.UserName == username && p.Password == password);
                if (user != null)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool CheckExistsUser(string _user)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                UserManager user = ServerLinq.UserManagers.SingleOrDefault(p => p.UserName == _user);
                if (user != null)
                    return true;
                return false;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public UserManager GetUserManagerByUserName(string username)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                UserManager user = ServerLinq.UserManagers.SingleOrDefault(p => p.UserName == username);
                return user;
            }
            catch (Exception)
            {
                return null;
            }
            
        }
        public UserManager GetUserManagerByUserCode(int code)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                UserManager user = ServerLinq.UserManagers.SingleOrDefault(p => p.UserCode == code);
                return user;
            }
            catch (Exception)
            {
                return null;
            }

        }
        public bool ChangeAvatar(string username, string filename)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                UserManager user = ServerLinq.UserManagers.SingleOrDefault(p => p.UserName == username);
                user.Avatar = filename;
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool ChangeProfile(UserManager _user, string desuser)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                UserManager user = ServerLinq.UserManagers.SingleOrDefault(p => p.UserName == desuser);
                user.Address = _user.Address;
                user.Birthday = _user.Birthday;
                user.Email = _user.Email;
                user.FullName = _user.FullName;
                user.Phone = _user.Phone;
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool EditUser(UserManager _user)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                UserManager user = ServerLinq.UserManagers.SingleOrDefault(p => p.UserName == _user.UserName);
                user.Address = _user.Address;
                user.Birthday = _user.Birthday;
                user.Email = _user.Email;
                user.FullName = _user.FullName;
                user.Phone = _user.Phone;
                if (_user.RoleCode != 0)
                    user.RoleCode = _user.RoleCode;
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<UserManager> GetListUserManager()
        {
            ServerLinq = new G62DBDataContext();
            List<UserManager> list = new List<UserManager>();
            var table = from p in ServerLinq.UserManagers
                        select p;

            foreach (var row in table)
                list.Add(row);
            return list;
        }

        public List<RoleManager> GetListRoleManager()
        {
            ServerLinq = new G62DBDataContext();
            List<RoleManager> list = new List<RoleManager>();
            var table = from p in ServerLinq.RoleManagers
                        select p;

            foreach (var row in table)
                list.Add(row);
            return list;
        }

        public bool CreateUserManager(UserManager user)
        {
            try
            {
                ServerLinq = new G62DBDataContext();

                UserManager usc = ServerLinq.UserManagers.SingleOrDefault(p => p.UserName == user.UserName);
                if (usc == null)
                {
                    UserManager us = new UserManager();
                    us.Address = user.Address;
                    us.Birthday = user.Birthday;
                    us.DayCreated = DateTime.Now;
                    us.Email = user.Email;
                    us.FullName = user.FullName;
                    us.Password = user.UserName;
                    us.Phone = user.Phone;
                    us.RoleCode = user.RoleCode;
                    RoleManager rol = ServerLinq.RoleManagers.SingleOrDefault(p => p.RoleCode == user.RoleCode);
                    us.RoleName = rol.RoleName;
                    us.Sex = user.Sex;
                    us.UserName = user.UserName;
                    ServerLinq.UserManagers.InsertOnSubmit(us);
                    ServerLinq.SubmitChanges();

                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public bool DeleteUserManager(string user1, string user)
        {
            ServerLinq = new G62DBDataContext();
            try
            {
                if (user1 != user)
                {
                    ServerLinq.UserManagers.DeleteOnSubmit(ServerLinq.UserManagers.SingleOrDefault(p => p.UserName == user));
                    ServerLinq.SubmitChanges();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public bool RestPasswordUserManager(string user)
        {
            ServerLinq = new G62DBDataContext();
            try
            {
                UserManager us = ServerLinq.UserManagers.SingleOrDefault(p => p.UserName == user);
                us.Password = us.UserName;
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        #endregion

        #region Account
        public List<Account> GetListAccount_ATM_Debit_Resgis()
        {
            ServerLinq = new G62DBDataContext();
            List<Account> list = new List<Account>();
            var table = from deb in ServerLinq.DebitCards
                        join atm in ServerLinq.ATMAccounts on deb.ATMAccountCode equals atm.ATMAcountCode
                        join acc in ServerLinq.Accounts on atm.AccountCode equals acc.AccountCode
                        where acc.Status == 0
                        select acc;

            foreach (var row in table)
                list.Add(row);
            return list;
        }
        public List<Account> GetListAccount_ATM_Credit_Resgis()
        {
            ServerLinq = new G62DBDataContext();
            List<Account> list = new List<Account>();
            var table = from Cre in ServerLinq.CreditCards
                        join atm in ServerLinq.ATMAccounts on Cre.ATMAccountCode equals atm.ATMAcountCode
                        join acc in ServerLinq.Accounts on atm.AccountCode equals acc.AccountCode
                        where acc.Status == 0
                        select acc;

            foreach (var row in table)
                list.Add(row);
            return list;
        }
        public int Count_ATM_Debit_Resgis()
        {
            ServerLinq = new G62DBDataContext();
            List<Account> list = new List<Account>();
            var table = from deb in ServerLinq.DebitCards
                        join atm in ServerLinq.ATMAccounts on deb.ATMAccountCode equals atm.ATMAcountCode
                        join acc in ServerLinq.Accounts on atm.AccountCode equals acc.AccountCode
                        where acc.Status == 0
                        select acc;

            return table.Count();
        }

        public int Count_ATM_Credit_Resgis()
        {
            ServerLinq = new G62DBDataContext();
            List<Account> list = new List<Account>();
            var table = from cre in ServerLinq.CreditCards
                        join atm in ServerLinq.ATMAccounts on cre.ATMAccountCode equals atm.ATMAcountCode
                        join acc in ServerLinq.Accounts on atm.AccountCode equals acc.AccountCode
                        where acc.Status == 0
                        select acc;

            return table.Count();
        }

        public bool ProcessAcceptRegisDebitCard(List<string> list)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                foreach (string str in list)
                {
                    Account acc = ServerLinq.Accounts.SingleOrDefault(p => p.AccountCode == str);
                    ATMAccount atm = ServerLinq.ATMAccounts.SingleOrDefault(p => p.AccountCode == str);
                    DebitCard deb = ServerLinq.DebitCards.SingleOrDefault(p => p.ATMAccountCode == atm.ATMAcountCode);
                    acc.Status = 1;
                    atm.Status = 1;
                    deb.Status = 1; 
                }
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public bool ProcessDeleteRegisDebitCard(List<string> list)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                foreach (string str in list)
                {
                    Account acc = ServerLinq.Accounts.SingleOrDefault(p => p.AccountCode == str);
                    ATMAccount atm = ServerLinq.ATMAccounts.SingleOrDefault(p => p.AccountCode == str);
                    DebitCard deb = ServerLinq.DebitCards.SingleOrDefault(p => p.ATMAccountCode == atm.ATMAcountCode);
                    acc.Status = 2;
                    atm.Status = 2;
                    deb.Status = 2;
                }
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public bool ProcessAcceptRegisCreditCard(List<string> list)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                foreach (string str in list)
                {
                    Account acc = ServerLinq.Accounts.SingleOrDefault(p => p.AccountCode == str);
                    ATMAccount atm = ServerLinq.ATMAccounts.SingleOrDefault(p => p.AccountCode == str);
                    CreditCard deb = ServerLinq.CreditCards.SingleOrDefault(p => p.ATMAccountCode == atm.ATMAcountCode);
                    acc.Status = 1;
                    atm.Status = 1;
                    deb.Status = 1;
                }
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public bool ProcessDeleteRegisCreditCard(List<string> list)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                foreach (string str in list)
                {
                    Account acc = ServerLinq.Accounts.SingleOrDefault(p => p.AccountCode == str);
                    ATMAccount atm = ServerLinq.ATMAccounts.SingleOrDefault(p => p.AccountCode == str);
                    CreditCard deb = ServerLinq.CreditCards.SingleOrDefault(p => p.ATMAccountCode == atm.ATMAcountCode);
                    acc.Status = 2;
                    atm.Status = 2;
                    deb.Status = 2;
                }
                ServerLinq.SubmitChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
        #endregion
    }
}