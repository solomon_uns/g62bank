﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;

namespace G62BANK.Models
{
    public class ForumModel
    {
        public ObjectId Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Owner { get; set; }
        public DateTime CreationDate { get; set; }
        public List<Comment> Comments { get; set; }

        public class Comment
        {
            public string TopicId { get; set; }
            public string Owner { get; set; }
            public string Content { get; set; }
            public DateTime CreationDate { get; set; }
        };
      

        public static bool CreateTopic(ForumModel model)
        {
            var collection = new MongoClient("mongodb://localhost").GetServer().GetDatabase("G62BankForum").GetCollection<ForumModel>("Forum");
            collection.Insert(model);
            
            return false;
        }

        public static bool PostComment(Comment comment)
        {
            var query = Query.EQ("_id", ObjectId.Parse(comment.TopicId));
            var collection = new MongoClient("mongodb://localhost").GetServer().GetDatabase("G62BankForum").GetCollection<ForumModel>("Forum");
            var model = collection.FindOne(query);

            comment.CreationDate = DateTime.Now;
            model.Comments.Add(comment);
            collection.Save(model);
            
            
            return false;
        }

        public static List<ForumModel> GetAll()
        {
            var collection = new MongoClient("mongodb://localhost").GetServer().GetDatabase("G62BankForum").GetCollection<ForumModel>("Forum");
            return collection.FindAll().OrderBy(p => p.CreationDate).ToList();
        }

        public static bool DeleteAll()
        {
            var collection = new MongoClient("mongodb://localhost").GetServer().GetDatabase("G62BankForum").GetCollection<ForumModel>("Forum");
            collection.RemoveAll();

            return true;
        }
    }
}