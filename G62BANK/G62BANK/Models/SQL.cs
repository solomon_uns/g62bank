﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace G62BANK.Models
{
    public class SQL
    {
        public static int ExecuteNonQuery(string query)
        {
            using (SqlConnection connect = new SqlConnection(ConfigurationManager.ConnectionStrings["G62BankConnectionString"].ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connect))
                {
                    connect.Open();
                    return command.ExecuteNonQuery();
                    connect.Close();
                }
            }
        }
        public static object ExecuteScalar(string query)
        {
            using (SqlConnection connect = new SqlConnection(ConfigurationManager.ConnectionStrings["G62BankConnectionString"].ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connect))
                {
                    connect.Open();
                    return command.ExecuteScalar();
                    connect.Close();
                }
            }
        }
        public static DataTable GetTable(string query)
        {
            try
            {
                using (SqlConnection connect = new SqlConnection(ConfigurationManager.ConnectionStrings["G62BankConnectionString"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(query, connect))
                    {
                        connect.Open();
                        SqlDataAdapter adap = new SqlDataAdapter(command);
                        DataTable tb = new DataTable { TableName = "TMP" };
                        adap.Fill(tb);
                        connect.Close();
                        return tb;
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }

        }
    }
}