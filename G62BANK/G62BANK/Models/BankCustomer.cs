﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace G62BANK.Models
{
    public class BankCustomer
    {
        [Required(ErrorMessage = "(*)")]
        [Display(Name = "user name")]
        [Remote("CheckUserName", "BankCustomer")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "(*)")]
        [Display(Name = "cmnd")]
        [Remote("CheckCMND", "BankCustomer")]
        public string Cmnd { get; set; }

        [Display(Name = "fullname")]
        [Required(ErrorMessage = "(*)")]
        public string FullName { get; set; }

        [Display(Name = "address")]
        [Required(ErrorMessage = "(*)")]
        public string Address { get; set; }

        [Display(Name = "phone")]
        [Required(ErrorMessage = "(*)")]
        [RegularExpression("^[0-9]+$")]
        public string Phone { get; set; }

        [Display(Name = "email")]
        public string Email { get; set; }

        [Display(Name = "password")]
        [Required(ErrorMessage = "(*)")]
        [Compare("PasswordRetry")]
        [StringLength(6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "passwordretry")]
        [Required(ErrorMessage = "(*)")]
        [Compare("Password")]
        [DataType(DataType.Password)]
        public string PasswordRetry { get; set; }

        [Display(Name = "birthday")]
        [Required(ErrorMessage = "(*)")]
        public string Birthday { get; set; }

        public DateTime DateCreated { get; set; }
    }
}