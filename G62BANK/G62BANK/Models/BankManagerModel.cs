﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using System.Web.Mvc;

namespace G62BANK.Models
{
    public class BankManagerModel
    {
    }
    public class LoginManager
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }

    public class AccountManager
    {
        public int NewDebitCard { get; set; }
        public int NewCreditCard { get; set; }
    }

    public class PlusMoneyDebitCardIn
    {
        // thong tin tai khoan
        [Required]
        [Remote("CheckAccountExists", "BankCustomer")]
        public string AccountCode { get; set; }

        [Required]
        public string CustomerName { get; set; }
       
        // thong tin nguoi nop tien
        [Required]
        public string Action_FullName { get; set;}
        
        [Required]
        public string Action_ID { get; set; }
        
        [Required]
        public string Description { get; set; }

        // ngay nop tien
        [Required]
        public DateTime Day { get; set; }

        // so tien
        [Required]
        public double Money { get; set; }
    }

    public class TranferATMtoATM
    {
        // thong tin tai khoan chuyển
        [Required]
        [Remote("CheckAccountExists", "BankCustomer")]
        public string FromAccountCode { get; set; }

        [Required]
        public string FromCustomerName { get; set; }
       
        // thong tin tai khoan nhan
        [Required]
        [Remote("CheckAccountExists", "BankCustomer")]
        public string ToAccountCode { get; set; }

        [Required]
        public string ToCustomerName { get; set; }
        
        [Required]
        public string Description { get; set; }

        // ngay chuyen
        [Required]
        public DateTime Day { get; set; }

        // phi dich vu
        public double Cost { get; set; }

        // so tien chuyen
        [Required]
        public double Money { get; set; }

        // tổng số tiền (tien chuyen  + phí )
        [Required]
        public double SumMoney { get; set; }
    }

    public class TranferATMtoPerson
    {
        public string TransactionTranferCode { get; set; }

        // thong tin tai khoan chuyển
        [Required]
        [Remote("CheckAccountExists", "BankCustomer")]
        public string FromAccountCode { get; set; }

        [Required]
        public string FromCustomerName { get; set; }

        // thong tin tai khoan nhan
        [Required]
        public string ToCustomerID { get; set; }

        [Required]
        public string ToCustomerName { get; set; }

        [Required]
        public string Description { get; set; }

        // ngay chuyen
        [Required]
        public DateTime Day { get; set; }

        // phi dich vu
        public double Cost { get; set; }

        // so tien chuyen
        [Required]
        public double Money { get; set; }

        // tổng số tiền (tien chuyen  + phí )
        [Required]
        public double SumMoney { get; set; }
    }

    public class TranferPersontoPerson
    {
        public string TransactionTranferCode { get; set; }

        // thong tin tai khoan chuyển
        [Required]
        public string FromCustomerID { get; set; }

        [Required]
        public string FromCustomerName { get; set; }

        // thong tin tai khoan nhan
        [Required]
        public string ToCustomerID { get; set; }

        [Required]
        public string ToCustomerName { get; set; }

        [Required]
        public string Description { get; set; }

        // ngay chuyen
        [Required]
        public DateTime Day { get; set; }

        // phi dich vu
        public double Cost { get; set; }

        // so tien chuyen
        [Required]
        public double Money { get; set; }

        // tổng số tiền (tien chuyen  + phí )
        [Required]
        public double SumMoney { get; set; }
    }

    public class CreateSavingBook {
        public string CustomerID { get; set; }
        public string FullName { get; set; }
        public SavingBook Savingbook { get; set; }
        public string SavingBookTypeCode { get; set; }

    }

    public class SearchSavingBook {
        public string CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string SavingBookCode { get; set; }
        public string AccountCode { get; set; }
        public bool Type { get; set; }
        public string SavingBookTypeCode { get; set; }
        public string Money { get; set; }
    }

    public class SearchDebitCard {
        public string AccountCode { get; set; }
        public string CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string DebitCardCode { get; set; }
    }
    public class SearchCreditCard
    {
        public string AccountCode { get; set; }
        public string CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string CreditCardCode { get; set; }
    }
    public class SearchReturnRateCreditCard
    {
        public string AccountCode { get; set; }
        public string CreditCardCode { get; set; }
        public string ATMAccountCode { get; set; }
    }
    public class Meassages{
        public string _mes { get; set; } 
    }

}