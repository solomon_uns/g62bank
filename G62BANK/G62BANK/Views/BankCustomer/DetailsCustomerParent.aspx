﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<G62BANK.Models.Customer>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    DetailsCusTomerParent
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="stylesheet" href="../../Content/Css/detailsAccount.css">
    <div id="SettingsPage_Content">
        <ul class="uiList fbSettingsList _4kg  _4ks">
            <div class="content">
            </div>
                <% using (Html.BeginForm())
                   { %>
                    <%Html.RenderAction("DetailsCustomer", "BankCustomer", Model.UserName);%>
                    <%} %>
        </ul>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
