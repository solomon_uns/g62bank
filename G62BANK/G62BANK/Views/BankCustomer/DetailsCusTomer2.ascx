﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.Customer>" %>
<div id="colunmLeft">
    <link rel="stylesheet" href="../../Content/Css/detailsAccount.css">
    <div id="SettingsPage_Content">
        <ul class="uiList fbSettingsList _4kg  _4ks">
            <div class="content">
            </div>
                
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink clearfix" style="background-color:#3b5998; text-align:center; font-size:13px; font-family:Tahoma; font-weight:bold;">
    THÔNG TIN KHÁCH HÀNG
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>Tên Đăng Nhập :</strong></span>
        <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p => p.UserName)%></strong></span>
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>CMND :</strong></span>
        <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p => p.CustomerID)%></strong></span>
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>Họ Tên :</strong></span>
        <span style="padding-left: 23px; cursor:pointer;" class="uiIconText fbSettingsListItemEdit" id="editfullname">
            Edit
            </span>
        <span class="fbSettingsListItemContent fcg" id="fullname"><strong>&nbsp;<%=Html.DisplayFor(p => p.FullName)%></strong></span>
        <script type="text/javascript">
            $(function () {
                $("#editfullname").click(function () {

                    $("#changeaddress").html("");
                    $("#changephone").html("");
                    $("#changebirthday").html("");

                    $("#editaddress").html("Edit");
                    $("#editphone").html("Edit");
                    $("#editbirthday").html("Edit");
                    $.ajax({
                        url: '<%= Url.Action("ChangeFullName","Account") %>',
                        type: "GET",
                        success: function (data) {
                            $("#changefullname").html(data);
                            $("#editfullname").html("");
                        }
                    });
                });
            });
        </script>
    </a>
    <div class="content" id ="changefullname">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>Địa Chỉ :</strong></span>
        <span style="padding-left: 23px;cursor:pointer;" class="uiIconText fbSettingsListItemEdit" id="editaddress">
            Edit
            </span>
        <span class="fbSettingsListItemContent fcg" id="address"><strong>&nbsp;<%=Html.DisplayFor(p => p.Address)%></strong></span>
        <script type="text/javascript">
            $(function () {
                $("#editaddress").click(function () {

                    $("#changefullname").html("");
                    $("#changephone").html("");
                    $("#changebirthday").html("");

                    $("#editfullname").html("Edit");
                    $("#editphone").html("Edit");
                    $("#editbirthday").html("Edit");
                    $.ajax({
                        url: '<%= Url.Action("ChangeAddress","Account") %>',
                        type: "GET",
                        success: function (data) {
                            $("#changeaddress").html(data);
                            $("#editaddress").html("");
                        }
                    });

                });
            });
        </script>
    </a>
    <div class="content" id="changeaddress">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>Điện Thoại :</strong></span>
        <span style="padding-left: 23px;cursor:pointer;" class="uiIconText fbSettingsListItemEdit" id = "editphone">
            Edit
            </span>
        <span class="fbSettingsListItemContent fcg" id= "phone"><strong>&nbsp;<%=Html.DisplayFor(p => p.Phone)%></strong></span>
        <script type="text/javascript">
            $(function () {
                $("#editphone").click(function () {

                    $("#changefullname").html("");
                    $("#changeaddress").html("");
                    $("#changebirthday").html("");

                    $("#editfullname").html("Edit");
                    $("#editaddress").html("Edit");
                    $("#editbirthday").html("Edit");
                    $.ajax({
                        url: '<%= Url.Action("ChangePhone","Account") %>',
                        type: "GET",
                        success: function (data) {
                            $("#changephone").html(data);
                            $("#editphone").html("");
                        }
                    });

                });
            });
        </script>
    </a>
    <div class="content" id="changephone">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>Ngày Sinh :</strong></span>
        <span style="padding-left: 23px;cursor:pointer;" class="uiIconText fbSettingsListItemEdit" id = "editbirthday">
            Edit
            </span>
        <span class="fbSettingsListItemContent fcg" id= "birthday"><strong>&nbsp;<%=Html.DisplayFor(p => p.Birthday)%></strong></span>
        <script type="text/javascript">
            $(function () {
                $("#editbirthday").click(function () {

                    $("#changefullname").html("");
                    $("#changeaddress").html("");
                    $("#changephone").html("");

                    $("#editfullname").html("Edit");
                    $("#editaddress").html("Edit");
                    $("#editphone").html("Edit");
                    $.ajax({
                        url: '<%= Url.Action("ChangeBirthday","Account") %>',
                        type: "GET",
                        success: function (data) {
                            $("#editbirthday").html("");
                            $("#changebirthday").html(data);

                        }
                    });
                });
            });
        </script>
    </a>
    <div class="content" id="changebirthday">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>Email :</strong></span>
        <span style="padding-left: 23px; cursor:pointer;" class="uiIconText fbSettingsListItemEdit"></span>
        <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p => p.Email)%></strong></span>
    </a>
    <div class="content">
    </div>
</li>
        </ul>
    </div>

            </div>
