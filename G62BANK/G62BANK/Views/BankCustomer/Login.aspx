﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<G62BANK.Models.LoginModel>" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <link rel="Stylesheet" href="../../Content/Css/login.css">
    <%-- hien dialog --%>
<link href="../../Content/themes/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-ui-1.8.20.min.js" type="text/javascript"></script>
    <script src="../../Scripts/modernizr-2.5.3.js" type="text/javascript"></script>
<%-- goi ajax --%>
    <script src="../../Scripts/MicrosoftMvcAjax.js" type="text/javascript"></script>
    <title>Login</title>
</head>
<body>
<% using (Html.BeginForm())
   { %>
    <div id="container">	
        <div class="hxt46">
    		<div id="page_effect" class="hxt29">
        		<div class="hxt1">
            		<div class="hxt8">
            		</div>
                        <div class="hxt13">
                            <table cellpadding="0px" cellspacing="0px">
                                <tbody>
                                    <tr>
                                        <td class="hxt14">
                                        </td>
                                        <td class="hxt15">
                                            <a href="#" target="_parent">Trang chủ</a><span>&nbsp;|&nbsp;</span>
                                            <a href="#" target="_parent">Giới thiệu<span>&nbsp;|&nbsp;</span></a>
                                            <a href="#" target="_parent">Máy ATM</a><span>&nbsp;|&nbsp;</span>
                                            <a href="#">Easy Account</a><span>&nbsp;|&nbsp;</span>
                                            <a href="#" target="_parent">Easy Transfer</a><span>&nbsp;|&nbsp;</span>
                                            <a href="#" target="_parent">Easy Saving</a>
                                        </td>
                                        <td class="hxt16">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
            			<div class="hxt11">
                			<table cellpadding="0px" cellspacing="0px">
                    			<tbody>
                                	<tr>
                                        <td class="hxt12">
                                        </td>
                                        <td class="hxt9">
                                            <div class="hxt17">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tbody>
                                                    	<tr>
                                                        	<td class="hxt24">
                                                        	</td>
                                                            <td>
                                                                <a href="#" target="_self">Đăng nhập</a>
                                                            </td>
                                                    	</tr>
                                                        <tr>
                                                            <td class="hxt24">
                                                            </td>
                                                            <td>
                                                                <a href="#" target="_blank">Phí dịch vụ</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="hxt24">
                                                            </td>
                                                            <td>
                                                                <a href="#" target="_blank">Hướng dẫn sử dụng</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="hxt24">
                                                            </td>
                                                            <td>
                                                                <a href="#" target="_blank">Phản hồi từ khách hàng</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="hxt24">
                                                            </td>
                                                            <td>
                                                                <a href="#" target="_blank">Lãi suất</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="hxt24">
                                                            </td>
                                                            <td>
                                                                <a href="#" target="_blank">Thanh toán quốc tế</a>
                                                            </td>
                                                        </tr>
                                                	</tbody>
                                               	</table>
                                            </div>
                                        </td>
                                        <td class="hxt10">
                                        </td>
                    				</tr>
                				</tbody>
                         	</table>
            			</div>
                        <div class="hxt18">
                        </div>
                        <table class="hxt21">
                            <tbody>
                            	<tr>
                                    <td>
                                        <table class="hxt19">
                                            <tbody>
                                            	<tr>
                                                    <td>
                                                        <p class="hxt27">
                                                        Easy Corporate Banking là dịch vụ vượt trội ra đời dựa trên nền tảng công nghệ Hiện đại, An toàn, Bảo mật mà OceanBank cung cấp riêng cho Khách hàng Doanh nghiệp. Đây là dịch vụ Ngân hàng trực tuyến giúp ... 
                                                        </p>
                                                    </td>
                                            	</tr>
                                                <tr>
                                                    <td class="hxt28">
                                                    </td>
                                                </tr>
                                        	</tbody>
                                     	</table>
                                    </td>
                                    <td>
                                        <table class="hxt20">
                                            <tbody>
                                            	<tr>
                                                    <td>
                                                        <p class="hxt27">
                                                            Easy Mobile Banking là dịch vụ hiện đại mà OceanBank cung cấp giúp Khách hàng có thể giao dịch với Ngân hàng mọi lúc, mọi nơi qua tin nhắn SMS. Khách hàng sử dụng dịch vụ bằng một trong hai hình thức... 
                                                        </p>
                                                    </td>
                                            	</tr>
                                                <tr>
                                                    <td class="hxt28">
                                                    </td>
                                                </tr>
                                        	</tbody>
                                      	</table>
                                    </td>
                                    <td>
                                        <div class="hxt22">
                                            <div class="hxt25">
                                                <span>Liên kết ngân hàng</span>
                                          	</div>
                                            <table cellpadding="0" cellspacing="0">
                                                <tbody>
                                                    <tr>
                                                        <td class="hxt26">
                                                        </td>
                                                        <td>
                                                            <a href="#" target="_blank">G58 Bank</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="hxt26">
                                                        </td>
                                                        <td>
                                                            <a href="#" target="_blank">G59 Bank</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="hxt26">
                                                        </td>
                                                        <td>
                                                            <a href="#" target="_blank">G60 Bank</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="hxt26">
                                                        </td>
                                                        <td>
                                                            <a href="#" target="_blank">G61 Bank</a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                            	</tr>
                        	</tbody>
                     	</table>
            			<div class="hxt23">
                			<table class="hxt30">
                    			<tbody>
                                	<tr>
                        				<td class="hxt31">
                            				<div id="img1">
                        					</div>
                                     	</td>
                                        <td>
                                            <a href="" target="_blank" class="style1">HOTLINE: 1800 588 815</a>
                                        </td>
                    				</tr>
                                    <tr>
                                        <td colspan="2" class="hxt33">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="hxt31">
                                            <div id="img2">
                                        </div></td>
                                        <td>
                                            <a href="mailto:ebanking@oceanbank.vn" target="_blank">GỬI EMAIL</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="hxt33">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="hxt31">
                                            <div id="img5">
                                        </div></td>
                                        <td>
                                            <a href="" target="_blank">ĐẶT LỊCH HẸN</a>
                                        </td>
                                    </tr>
                				</tbody>
                      		</table>
                    		<div class="hxt32">
                        		<table style="margin-top:60px;" width="100%">
                            		<tbody>
                                   		<tr>
                                			<td>
                                    			<div class="block_top">ĐĂNG NHẬP ĐỂ SỬ DỤNG DỊCH VỤ
                                                </div>
                                    			<div class="block_middle" align="center" id = "contentlogin">
                                        			<table cellspacing="2px" style="height:100%;">
                                            			<tbody>
                                                            
                                                            <tr>
                                                				<td align="center" style="width:100%;" colspan="3"><span style="color:Red"><%=Html.ValidationSummary()%></span></td>
                                            				</tr>
                                                        	<tr>
                                                				<td align="right" style="width:100%;">Tên đăng nhập &nbsp;&nbsp;&nbsp</td>
                                                				<td><%=Html.TextBoxFor(p=>p.UserName,new{@id="fldLoginUserId"}) %></td>
                                                                <td></td>
                                            				</tr>
                                            				<tr>
                                                                <td align="right">Mật khẩu &nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                                <td><%=Html.PasswordFor(p => p.Password, new { @id = "fldPassword", @type = "password" })%></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td align="left">
                                                                    <input type="submit" class="btnlogin" value="Đăng Nhập"/>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                            </tr>
                                                            <tr>
                                                               <td style="width:auto;">
                                                                    <input style="float:right" type="button"class="btngmail" title="Đăng nhập từ tài khoản Google" onclick="location.href='<%: Url.Action("ExternalLogin_google", "BankCustomer") %>'" />
                                                               </td>
                                                               <td style="width:auto;float:left;">
                                                                    <input type="button"class="btntwitter" title="Đăng nhập từ tài khoản Twitter" onclick="location.href='<%: Url.Action("ExternalLogin_twitter", "BankCustomer") %>'" />
                                                               </td>
                                                               <td style="width:50%;float:left;">
                                                                    <input type="button"class="btnfacebook" title="Đăng nhập từ tài khoản Facebook" onclick="location.href='<%: Url.Action("ExternalLogin_facebook", "BankCustomer") %>'" />
                                                                </td>  
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3" align=center>
                                                                    <span style="cursor:pointer;" onclick="RestPassWord();"><a class="hxt41" target="_blank">Quên mật khẩu?</a></span> | 
                                                                    <span><a class="hxt41" href='<%: Url.Action("CreateBankCustomer", "BankCustomer") %>'">Đăng ký</a></span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                            </tr>
                                        				</tbody>
                                                 	</table>
                                    			</div>
                                    			<div class="block_bottom">
                                                </div>
                                			</td>
                            			</tr>
                        			</tbody>
                             	</table>
                    		</div>
            			</div>
        			</div>
        			<div class="hxt2">
        			</div>
        			<div class="hxt3">
                        <table cellpadding="0px" cellspacing="0px">
                            <tbody><tr>
                                <td class="hxt4">
                                </td>
                                <td class="hxt5">
                                    <span class="hxt7">Copyright by G62 Bank @ 2012. Ngân hàng TMCP G62: Số 79 - Bàu Cát4 - P.14 - Q. Tân Bình - HCM</span>
                                </td>
                                <td class="hxt6">
                                </td>
                            </tr>
                        </tbody>
                 	</table>
        		</div>
			</div>
		</div>    
	</div>
<%} %>
<div id="NoteDialog" title="" class="Hidden"></div>
<script type="text/javascript">
    $(function () {
        $("#NoteDialog").dialog({
            autoOpen: false, width: 300, height: 180, modal: true,
            buttons: {
                "Reset": function () {
                    $.ajax({
                        url: '<%= Url.Action("ResetPassWord","BankCustomer") %>',
                        type: "Post",
                        data: { email: $("#resetpassword").val() },
                        success: function (_data) {
                            if (_data.toString() == "true") {
                                alert("Đã gửi email xác nhận!");
                                $("#NoteDialog").dialog("close");
                            }
                            else {
                                alert("Email Không hợp lệ!");
                            }
                        }
                    });
                },
                Cancel: function () { $(this).dialog("close"); }
            }
        });
    });
</script>
<script type="text/javascript">
    function RestPassWord() {
        $("#NoteDialog").html("")
        .dialog("option", "title", "RestPassword")
        .load("/BankCustomer/ResetPassWord", function () { $("#NoteDialog").dialog("open"); });
    }
</script>
</body>

</html>
