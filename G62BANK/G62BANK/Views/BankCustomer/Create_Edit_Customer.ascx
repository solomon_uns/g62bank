﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.BankCustomer>" %>
<form id="form1" runat="server">
    <div id="menu" align="center">
		<div id="mnu" class="sontn" style="margin-bottom:5px; vertical-align:middle; text-align: center; font-weight: bold;">
			<br />
				ĐĂNG KÝ SỬ DỤNG DỊCH VỤ NGÂN HÀNG ĐIỆN TỬ TẠI G62BANK 
			<br />
				EASY ACCOUNT, EASY TRANSFER, EASY SAVING							
        </div>
    </div>       
    <div id="divPersonalInfo" margin-left :20px">
        <br/>
		Bạn muốn quản lý tài khoản của mình và muốn giao dịch mọi lúc mọi nơi mà không cần phải đến Ngân hàng? Bạn muốn giao dịch của bạn thực hiện nhanh nhất và tốn ít công sức, chi phí nhất? Dịch vụ ngân hàng điện tử tại G62bank sẽ giúp cho bạn thực hiện được việc đó với các ứng dụng tuyệt vời.	
		<br/>
        <br/>
		Hãy đăng ký dịch vụ Ngân hàng điện tử ngay tại G62Bank để bạn có thể tiết kiệm tối đa thời gian, chi phí và nhận được
        nhiều ưu đãi từ phía G62Bank											
		<br/>
        <br/>
        <span style="font-weight:bold; font-style:italic">Chú ý</span>: Các phần có dấu (<span style="color:Red">*</span>) là những thông tin <span style="color:Red">bắt buộc</span>											
        <br/>						
    </div>
    <div id="Div3" class="sontn" style="margin-bottom:5px ; font-weight: bold; vertical-align:middle ">
         
		THÔNG TIN CÁ NHÂN NGƯỜI ĐĂNG KÝ 
        <br />
    </div>	
	<div id="divPersonalInfo" style="height: 81%; margin-left :20px">
	    <table style="width: 100%;">
		    <tbody>
            <tr>
		        <td width="33%" valign="baseline" style="width: 50%; text-align:right">
		                Họ và tên <span style="color:Red">*</span>
		        </td>
		        <td width="5px">&nbsp;</td>
		        <td width="66%" style="width: 50%">
                    <asp:TextBox ID="TextBox1" runat="server" Width="265px"></asp:TextBox>
                    <span id="RequiredFieldValidator1" style="color:Red;visibility:hidden;">(*)</span></td>
		    </tr>
            <tr>
                <td style="width: 50%; height: 25px; text-align:right" valign="baseline">
                    Email <span style="color:Red">*</span></td>
                <td width="5px">&nbsp;</td>
                <td style="width: 50%; height: 25px;">
                    <asp:TextBox ID="TextBox2" runat="server" Width="265px"></asp:TextBox>
                    </td>
            </tr>
            <tr>
                <td style="width: 50%; height: 25px; text-align:right" valign="baseline">
                    Điện thoại di động <span style="color: #ff0000">*</span></td>
                <td width="5px">&nbsp;</td>
                <td style="width: 50%; height: 25px;">
                    <asp:TextBox ID="TextBox3" runat="server" Width="265px"></asp:TextBox>
                    </td>
            </tr>
            <tr>
                <td style="width: 50%; height: 25px; text-align:right" valign="baseline">
                    Ngày sinh (dd/mm/yyyy)<span style="color: #ff0000">*</span></td>
                <td width="5px">&nbsp;</td>
                <td style="width: 50%; height: 25px;">
                    <asp:TextBox ID="TextBox4" runat="server" Width="265px"></asp:TextBox>
                    </td>
            </tr>
            <tr>
                <td style="width: 50%; height: 25px; text-align:right" valign="baseline">
                    Số CMND <span style="color: #ff0000">*</span></td>
                <td width="5px">&nbsp;</td>
                <td style="width: 50%; height: 25px;">
                    <asp:TextBox ID="TextBox5" runat="server" Width="265px"></asp:TextBox>
                    </td>
            </tr>
            <tr>
                <td style="width: 50%; height: 25px; text-align:right" valign="baseline">
                    Địa chỉ liên hệ <span style="color: #ff0000">*</span></td>
                <td width="5px">&nbsp;</td>
                <td style="width: 50%; height: 25px;">
                    <asp:TextBox ID="TextBox6" runat="server" Width="265px"></asp:TextBox>
                    </td>
            </tr>
            <tr>
                <td style="width: 50%; height: 25px;" valign="baseline">&nbsp;</td>
                <td width="5px">&nbsp;</td>
                <td style="width: 50%; height: 25px;">&nbsp;</td>
            </tr>
	    </tbody>
	</table>
</div>
<div id="divEsayOnlineContent" style="height: 37%; margin-left :20px"></div>
	<div id="divEasyMobileContent" style="height: 26%; margin-left :20px"></div>
<div id="divMplusTitle" class="sontn" style="font-weight: bold">
        		CHỨNG THỰC</div>	
	<div id="divMplusContent" style="height: 26%; margin-left :20px"></div> 
        <div id="div1" style="height: 26%; margin-left :20px">
<table width="100%">
                <tbody><tr>
            <td width="50%" valign="middle" style="width: 50%; height: 25px;text-align: right;">
                Nhập dãy số sau <span style="color: #ff0000">*</span></td>
            <td width="50%" style="height: 25px; width: 50%;">&nbsp;</td>
        </tr>
            <tr>
                    <td style="width: 50%; height: 21px; vertical-align: baseline; text-align: left;" valign="baseline">
                </td>
                <td colspan="1" style="height: 21px; width: 50%;">
                    &nbsp;<span id="lblError"></span>
                </td>
            </tr>
            <tr align="left">
            <td style="width: 50%; height: 25px; text-decoration: none;" valign="baseline">
                </td>
                <td align="left" style="width: 50%">
                    &nbsp;
                <input type="submit" name="btbSend" value="Gửi tới G62Bank" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;btbSend&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, false))" id="btbSend">
                </td>
            </tr>
        </tbody></table>
        <br>
    </div>
</form>   
    </form>