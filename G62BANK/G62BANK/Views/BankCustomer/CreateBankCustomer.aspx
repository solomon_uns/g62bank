﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<G62BANK.Models.BankCustomer>" %>  

<% Html.ValidationSummary(); %>
<% Html.EnableClientValidation();%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript" src="../../Scripts/jquery-1.7.1.js" ></script>
<script type="text/javascript" src="../../Scripts/jquery-1.7.1.min.js" ></script>
<script type="text/javascript" src="../../Scripts/jquery.validate.min.js" ></script>
<script type="text/javascript" src="../../Scripts/jquery.validate.unobtrusive.min.js" ></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
  <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>G62Bank - Đăng ký tài khoản</title>
<link href="../../Content/Css/Register.css" rel="stylesheet" type="text/css"/>
<script>
    $(function () {
        $(".date").datepicker();
        //$(".date").datepicker({ dateFormat: 'dd/mm/yy' });
    });
  </script>
</head>

<body>
<% using (Html.BeginForm())
   { %>
        <div id="menu" align="center">
			<div id="mnu" class="sontn" style="margin-bottom:5px; vertical-align:middle; text-align: center; font-weight: bold;">
			    	<br />
						ĐĂNG KÝ SỬ DỤNG DỊCH VỤ NGÂN HÀNG ĐIỆN TỬ TẠI G62BANK 
					<br />
						EASY ACCOUNT, EASY TRANSFER, EASY SAVING							
            </div>
        </div>       
        <div style="height: 81%; margin-left :20px">
            <br/>
			Bạn muốn quản lý tài khoản của mình và muốn giao dịch mọi lúc mọi nơi mà không cần phải đến Ngân hàng? Bạn muốn giao dịch của bạn thực hiện nhanh nhất và tốn ít công sức, chi phí nhất? Dịch vụ ngân hàng điện tử tại G62bank sẽ giúp cho bạn thực hiện được việc đó với các ứng dụng tuyệt vời.	
			<br/>
            <br/>
			Hãy đăng ký dịch vụ Ngân hàng điện tử ngay tại G62Bank để bạn có thể tiết kiệm tối đa thời gian, chi phí và nhận được
            nhiều ưu đãi từ phía G62Bank											
			<br/>
            <br/>
            <span style="font-weight:bold; font-style:italic">Chú ý</span>: Các phần có dấu (<span style="color:Red">*</span>) là những thông tin <span style="color:Red">bắt buộc</span>											
            <br/>
            <br/>
            <span style="font-style:italic">
                <span id="Label1" style="font-weight:bold;font-style:normal;"></span><br/>
            </span>							
        </div>
        <div id="div3" class="sontn" style="margin-bottom:5px ; font-weight: bold; vertical-align:middle ">
			  THÔNG TIN CÁ NHÂN NGƯỜI ĐĂNG KÝ 
            <br />
	    </div>	
		<div id="divPersonalInfo" style="height: 81%; margin-left :20px">
		    <table style="width: 100%;">
		        <tbody>
                <tr>
		            <td width="20%" valign="baseline" style="width: 20%; text-align:right">
		            </td>
		            <td width="5px">&nbsp;</td>
		            <td width="100%"><span style="color:Red"></span></td>	         
		        </tr>
                <tr>
		            <td width="20%" valign="baseline" style="width: 20%; text-align:right">
		            </td>
		            <td width="5px">&nbsp;</td>
		            <td width="100%"><span style="color:Red">Nhập tên đăng nhập tài khoản Facebook, Gmail, Twitter của bạn vào ô tên đăng nhập nếu bạn muốn login bằng chính các tài khoản này!</span></td>	         
		        </tr>
                <tr>
		            <td width="20%" valign="baseline" style="width: 20%; text-align:right">
		            </td>
		            <td width="5px">&nbsp;</td>
		            <td width="100%"><span style="color:Red">vd: Tôi có tài khoản Twitter: G62bank => Tên đăng nhập cần điền là: G62bank </span></td>	         
		        </tr>
                <tr>
		            <td width="20%" valign="baseline" style="width: 20%; text-align:right">
		            </td>
		            <td width="5px">&nbsp;</td>
		            <td width="100%"><span style="color:Red">vd: Tên hiển thị của tôi trên facebook: g62bank => Tên đăng nhập cần điền là: g62bank </span></td>	         
		        </tr>
                <tr>
		            <td width="20%" valign="baseline" style="width: 20%; text-align:right">
		                 Tên đăng nhập <span style="color:Red">*</span>
		            </td>
		            <td width="5px">&nbsp;</td>
		            <td width="100%">
                        <%=Html.TextBoxFor(p=>p.UserName)%> <span style="color:Red"><%=Html.ValidationMessageFor(p=>p.UserName) %></span></td>	         
		        </tr>
                <tr>
		            <td width="20%" valign="baseline" style="width: 20%; text-align:right">
		                 Họ và tên <span style="color:Red">*</span>
		            </td>
		            <td width="5px">&nbsp;</td>
		            <td width="100%">
                        <%=Html.TextBoxFor(p=>p.FullName)%> <span style="color:Red"><%=Html.ValidationMessageFor(p=>p.FullName) %></span></td>	         
		        </tr>
                <tr>
                    <td style="width: 20%; height: 25px; text-align:right" valign="baseline">
                        Email&nbsp;&nbsp;&nbsp;</td>
                    <td width="5px">&nbsp;</td>
                    <td>
                        <%=Html.TextBoxFor(p => p.Email)%> <span style="color:Red"><%=Html.ValidationMessageFor(p=>p.Email) %></span></td>
                </tr>
                <tr>
                    <td style="width: 20%; height: 25px; text-align:right" valign="baseline">
                        Điện thoại di động <span style="color: #ff0000">*</span></td>
                    <td width="5px">&nbsp;</td>
                    <td>
                        <%=Html.TextBoxFor(p=>p.Phone)%> <span style="color:Red"><%=Html.ValidationMessageFor(p=>p.Phone) %></span></td>
                </tr>
                <tr>
                    <td style="width: 20%; height: 25px; text-align:right" valign="baseline">
                        Ngày sinh (mm/dd/yyyy)<span style="color: #ff0000">*</span></td>
                    <td width="5px">&nbsp</td>
                    <td>                    
                        <%=Html.TextBoxFor(p => p.Birthday, new { @class = "date"})%> <span style="color:Red"><%=Html.ValidationMessageFor(p=>p.Birthday) %></span>
                    
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%; height: 25px; text-align:right" valign="baseline">
                        Số CMND <span style="color: #ff0000">*</span></td>
                    <td width="5px">&nbsp;</td>
                    <td>
                        <%=Html.TextBoxFor(p=>p.Cmnd)%> <span style="color:Red"><%=Html.ValidationMessageFor(p=>p.Cmnd) %></span></td>
                </tr>
                <tr>
                    <td style="width: 20%; height: 25px; text-align:right" valign="baseline">
                        Địa chỉ liên hệ <span style="color: #ff0000">*</span></td>
                    <td width="5px">&nbsp;</td>
                    <td>
                        <%=Html.TextAreaFor(p=>p.Address)%> <span style="color:Red"><%=Html.ValidationMessageFor(p=>p.Address) %></span></td>
                </tr>
                <tr>
                  <td style="width: 20%; height: 25px;" valign="baseline">&nbsp;</td>
                  <td width="5px">&nbsp;</td>
                  <td style="width: 50%; height: 25px;">&nbsp;</td>
                </tr>
	          </tbody>
		    </table>
	    </div>
        <div id="div3" class="sontn" style="font-weight: bold">QUẢN LÝ TÀI KHOẢN</div>
        <div id="divPersonalInfo" style="height: 81%; margin-left :20px">
		    <table style="width: 100%;">
		        <tbody><tr>
		            <td width="20%" valign="baseline" style="width: 20%; text-align:right">
		                 Mật khẩu <span style="color:Red">*</span>
		            </td>
		            <td width="5px">&nbsp;</td>
		            <td>
                        <%=Html.PasswordFor(p=>p.Password)%> <span style="color:Red"><%=Html.ValidationMessageFor(p=>p.Password) %></span></td>
		        </tr>
                <tr>
                    <td style="width: 20%; height: 25px; text-align:right" valign="baseline">
                        Nhắc Lại Mật Khẩu <span style="color:Red">*</span></td>
                    <td width="5px">&nbsp;</td>
                    <td >
                        <%=Html.PasswordFor(p => p.PasswordRetry)%> <span style="color:Red"><%=Html.ValidationMessageFor(p=>p.PasswordRetry) %></span></td>
                </tr>
	          </tbody>
		    </table>
	    </div>
            <div id="div3" class="sontn" style="font-weight: bold">
        			 CHỨNG THỰC</div>	
		    <div id="divPersonalInfo" style="height: 26%; margin-left :20px"> 
                <table style="width: 100%; table-layout:auto">
                     <tbody>
                     <tr>
                     <td style="width: 20%; height: 25px; text-align:right" valign="baseline">
                        Nhập chuỗi xác thực <span style="color:Red">*</span></td>
                    <td class="captcha">
                        <%=Html.Raw(Html.GenerateCaptcha()) %>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="width: 20%; height: 21px; vertical-align: baseline; text-align: left;" valign="baseline"></td>
                        <td colspan="1" style="height: 21px; width: 50%;">
                            &nbsp;<span id="lblError"></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 20%; height: 21px; vertical-align: baseline; text-align: left;" valign="baseline"></td>

                        <td colspan="1" style="height: 21px; width: 50%;">
                            <input type="submit" value="Tạo Tài Khoản" /><span id="Span1"></span>
                        </td>
                    </tr>
                </tbody>
                </table>
            </div>
<%} %>   
</body>
</html>
