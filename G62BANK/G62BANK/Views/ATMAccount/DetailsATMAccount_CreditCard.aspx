﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<G62BANK.ViewModels.ATMModel.CreditCardModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    DetailsATMAccount_CreditCard
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<link rel="stylesheet" href="../../Content/Css/detailsAccount.css">
<div id="SettingsPage_Content">
    <ul class="uiList fbSettingsList _4kg  _4ks">
        <div class="content">
        </div>
            <%using(Html.BeginForm()){ %>
                <%Html.RenderAction("DetailsAccount", "Account", Model.account); %>
            <%} %>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink clearfix">
                    <span class="pls fbSettingsListItemLabel"><strong>Mã Tài Khoản</strong></span>
                    <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
                        <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
                        Edit</span>--%>
                    <span class="fbSettingsListItemContent fcg"><strong id="Strong1">&nbsp;<%=Html.DisplayFor(p=>p.account.AccountCode) %></strong></span>
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink clearfix">
                    <span class="pls fbSettingsListItemLabel"><strong>Số Dư Tài Khoản</strong></span>
                    <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
                        <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
                        Edit</span>--%>
                    <span class="fbSettingsListItemContent fcg"><strong id="acc">&nbsp;<%=Html.DisplayFor(p=>p.atmaccount.Balance) %></strong></span>
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink clearfix">
                    <span class="pls fbSettingsListItemLabel"><strong>Trạng Thái:</strong></span>
                    <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
                        <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
                        Edit</span>--%>
                    <span class="fbSettingsListItemContent fcg"><strong id="Strong2">&nbsp;<%if (Model.account.Status == 0)
                        { 
                            %>Chưa kích hoạt <br/><span style="color:Red;">Vui lòng liên hệ Ngân hàng G62BANK để xác nhận thông tin và kích hoạt tài khoản</span><%                                                                      
                        }
                        else {
                            if (Model.account.Status == 1)
                            {  %>Đang hoạt động<%}
                            else {
                                %>Tài khoản đã bị hủy<br/><span style="color:Red;">Tài khoản này đã ngừng hoạt động theo qui định của Ngân hàng. vui lòng liên hệ G62BANK để biết thêm chi tiết</span><% 
                            } 
                        } %></strong></span>
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink clearfix" style="background-color:#3b5998; text-align:center; font-size:13px; font-family:Tahoma; font-weight:bold;">
                THÔNG TIN THẺ ATM
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink">
                    <span class="pls fbSettingsListItemLabel"><strong>Mã Thẻ</strong></span>
                    <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
                        <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
                        Edit
                        </span>--%>
                    <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.creditcard.CreditCardCode) %></strong></span>
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink">
                    <span class="pls fbSettingsListItemLabel"><strong>Ngày Mở</strong></span>
                    <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
                        <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
                        Edit
                        </span>--%>
                    <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.creditcard.DayCreated) %></strong></span>
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink">
                    <span class="pls fbSettingsListItemLabel"><strong>Ngày Hết Hạn</strong></span>
                    <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
                        <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
                        Edit
                        </span>--%>
                    <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.creditcard.DayExpired) %></strong></span>
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink">
                    <span class="pls fbSettingsListItemLabel"><strong>Số Tiền Đang Vay (nếu có)</strong></span>
                    <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
                        <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
                        Edit
                        </span>--%>
                    <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.creditcard.Loan) %></strong></span>
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink clearfix" style="background-color:#3b5998; text-align:center; font-size:13px; font-family:Tahoma; font-weight:bold;">
                THÔNG TIN LOẠI THẺ
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink">
                    <span class="pls fbSettingsListItemLabel"><strong>Mã Loại</strong></span>
                    <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
                        <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
                        Edit
                        </span>--%>
                    <span class="fbSettingsListItemContent fcg"><strong><%=Html.DisplayFor(p=>p.atmaccounttype.ATMAccountTypeCode) %></strong></span>
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink">
                    <span class="pls fbSettingsListItemLabel"><strong>Tên Loại</strong></span>
                    <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
                        <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
                        Edit
                        </span>--%>
                    <span class="fbSettingsListItemContent fcg"><strong><%=Html.DisplayFor(p=>p.atmaccounttype.ATMAccountTypeName) %></strong></span>
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink">
                    <span class="pls fbSettingsListItemLabel"><strong>Lãi Suất</strong></span>
                    <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
                        <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
                        Edit
                        </span>--%>
                    <span class="fbSettingsListItemContent fcg"><strong><%=Html.DisplayFor(p=>p.atmaccounttype.Rate)%> (%)</strong></span>
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink">
                    <span class="pls fbSettingsListItemLabel"><strong>Số Ngày Hết Hạn Từ Ngày Mở</strong></span>
                    <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
                        <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
                        Edit
                        </span>--%>
                    <span class="fbSettingsListItemContent fcg"><strong><%=Html.DisplayFor(p=>p.atmaccounttype.NumberDayExpired)%></strong></span>
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink">
                    <span class="pls fbSettingsListItemLabel"><strong>Mô Tả</strong></span>
                    <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
                        <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
                        Edit
                        </span>--%>
                    <span class="fbSettingsListItemContent fcg"><strong><%=Html.DisplayFor(p=>p.atmaccounttype.Description)%></strong></span>
                </a>
                <div class="content">
                </div>
            </li>

    </ul>
</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
