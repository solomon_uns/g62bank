﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<string>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    OpenATMAccount
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<link rel="stylesheet" href="../../Content/Css/detailsAccount.css">
<div id="SettingsPage_Content">
    <ul class="uiList fbSettingsList _4kg  _4ks">
        <div class="content">
        </div>
            <% using (Html.BeginForm())
               { %>
               <%Html.RenderAction("DetailsCustomer", "BankCustomer",null); %>
               <%Html.RenderAction("ViewListATMAccountType", "ATMAccountType"); %>
               <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink clearfix" style="background-color:#3b5998; text-align:center; font-size:13px; font-family:Tahoma; font-weight:bold;">
                    CHỨNG THỰC
                    </a>
                    <div class="content">
                    </div>
                </li>
               <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink">
                    <span class="pls fbSettingsListItemLabel"><strong></strong></span>
                    <span class="fbSettingsListItemContent fcg" style="text-align:center"><strong>&nbsp;<%=Html.Raw(Html.GenerateCaptcha()) %></strong></span>
                    <span class="fbSettingsListItemContent fcg" style="text-align:center"><strong id="val-recaptcha"></strong></span>
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink clearfix" style="background-color:#3b5998; text-align:center; font-size:13px; font-family:Tahoma; font-weight:bold;">
                    TẠO TÀI KHOẢN
                    </a>
                    <div class="content">
                    </div>
                </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink">
                    <span class="pls fbSettingsListItemLabel"><strong></strong></span>
                    <span class="fbSettingsListItemContent fcg" style="text-align:center"><strong>&nbsp;<input type="button" class="btnsend" id="btnsend" value="Send"/></strong></span>
                    <script type="text/javascript">
                          $(function () {
                            $("#btnsend").click(function () {

                                  $.ajax({
                                      url: '<%= Url.Action("ValidateCaptcha","ATMAccount") %>',
                                      type: "GET",
                                      data: {challengeValue: Recaptcha.get_challenge(),responseValue: Recaptcha.get_response()},
                                    success: function (data) 
                                    {
                                       if(data.toString() == "true")
                                       {
                                           $('#val-recaptcha').html("");
                                           $.ajax({
                                              url: '<%= Url.Action("OpenATMAccount","ATMAccount") %>',
                                              type: "POST",
                                              data: { code: $('#mathe').text()},
                                                success: function (data) 
                                                {
                                                    if(data.toString() == "false")
                                                    {
                                                        Recaptcha.reload();
                                                        $('#error_des').html("<span style='color:Red'>Chưa chọn loại thẻ!</span>");
                                                    }
                                                    else
                                                        window.location.href = "DetailsATMAccount/"+data;
                                                },
                                          });
                                       }
                                       else
                                       {
                                            Recaptcha.reload();
                                            $('#error_des').html("");
                                            $('#val-recaptcha').html("<span style='color:Red'>Chuỗi xác thực không chính xác!</span>");
                                        }
                                    },
                                  });
                              });
                          });
                    </script>
                </a>
                <div class="content">
                </div>
            </li>
              
            <%} %>
    </ul>
</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>