﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<G62BANK.Models.Money>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    CreateSavingBook
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<link rel="stylesheet" href="../../Content/Css/detailsAccount.css">
<div id="SettingsPage_Content">
    <ul class="uiList fbSettingsList _4kg  _4ks">
        <div class="content">
        </div>
            <% using (Html.BeginForm())
               { %>
               <%Html.RenderAction("DetailsCustomer", "BankCustomer",null); %>
               <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink clearfix" style="background-color:#3b5998; text-align:center; font-size:13px; font-family:Tahoma; font-weight:bold;">
                    CHỌN LOẠI TIẾT KIỆM
                    </a>
                    <div class="content">
                    </div>
                </li>
                <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled" id="tablesavingbook">
                        <%Html.RenderAction("ViewListSavingBookType", "SavingBook"); %>
                <div class="content">
                </div>
                </li>
                <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink">
                        <span class="fbSettingsListItemContent fcg" style="text-align:center"><strong id="des">&nbsp;</strong></span>
                        <span class="fbSettingsListItemContent fcg" style="text-align:center"><strong id="error_des">&nbsp;</strong></span>
                    </a>
                    <div class="content" >
                    </div>
               </li>
               <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink clearfix" style="background-color:#3b5998; text-align:center; font-size:13px; font-family:Tahoma; font-weight:bold;">
                    SỐ TIỀN GỬI
                    </a>
                    <div class="content">
                    </div>
                </li>
               <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink">
                        <span class="pls fbSettingsListItemLabel"><strong>Nhập Số Tiền Gửi :</strong></span>
                        <span class="fbSettingsListItemContent fcg" style="text-align:left;"><strong>&nbsp;<%=Html.TextBoxFor(p => p.money, new { id="money"})%></strong></span>
                        <span class="fbSettingsListItemContent fcg" style="text-align:center;color:Red;" ><strong id="error_mes"><%=Html.ValidationMessageFor(p=>p.money) %></strong></span>
                    </a>
                    <div class="content" >
                    </div>
               </li>
               <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink clearfix" style="background-color:#3b5998; text-align:center; font-size:13px; font-family:Tahoma; font-weight:bold;">
                    CHỨNG THỰC
                    </a>
                    <div class="content">
                    </div>
                </li>
               <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink">
                    <span class="pls fbSettingsListItemLabel">&nbsp;<strong></strong></span>
                    <span class="fbSettingsListItemContent fcg" style="text-align:center"><strong id="captcha">&nbsp;<%=Html.Raw(Html.GenerateCaptcha()) %></strong></span>
                    <span class="fbSettingsListItemContent fcg" style="text-align:center"><strong id="error_captcha"></strong></span>
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink clearfix" style="background-color:#3b5998; text-align:center; font-size:13px; font-family:Tahoma; font-weight:bold;">
                    MỞ SỔ TIẾT KIỆM
                    </a>
                    <div class="content">
                    </div>
                </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink">
                    <span class="pls fbSettingsListItemLabel"><strong></strong></span>
                    <span class="fbSettingsListItemContent fcg" style="text-align:center"><strong>&nbsp;<input type="button" class="btnsend" id="btnsend" value="Send"/></strong></span>
                    <script type="text/javascript">
                          $(function () {
                            $("#btnsend").click(function () {

                                  $.ajax({
                                      url: '<%= Url.Action("ValidateCaptcha","ATMAccount") %>',
                                      type: "GET",
                                      data: {challengeValue: Recaptcha.get_challenge(),responseValue: Recaptcha.get_response()},
                                    success: function (data) 
                                    {
                                       if(data.toString() == "true")
                                       {
                                           $('#error_captcha').html("");
                                           $.ajax({
                                              url: '<%= Url.Action("CreateSavingBook","SavingBook") %>',
                                              type: "POST",
                                              data: { type: $('#savingbooktype').text(),money: $('#money').val()},
                                                success: function (data) 
                                                {
                                                    if(data.toString() == "0")
                                                    {
                                                        Recaptcha.reload();
                                                        $('#error_des').html("<span style='color:Red'>Bạn chưa chọn loại tiết kiệm!</span>");
                                                        $('#error_mes').html();
                                                    }
                                                    else
                                                    {
                                                        if(data.toString() == "1")
                                                        {
                                                            Recaptcha.reload();
                                                            $('#error_des').html("" );
                                                            $('#error_mes').html("<span style='color:Red'>Số tiền phải lớn hơn số tiền tối thiểu và phải là bội của 50.000</span>");
                                                            
                                                        }
                                                        else
                                                            window.location.href = "DetailsSavingbook/"+data;
                                                    }
                                                },
                                          });
                                       }
                                       else
                                       {
                                            Recaptcha.reload();
                                            $('#error_des').html("");
                                            $('#error_mes').html("");
                                            $('#error_captcha').html("<span style='color:Red'>Chuỗi xác thực không chính xác! Vui Lòng Nhập Lại</span>");
                                       }
                                    },
                                  });
                              });

                          });
                    </script>
                </a>
                <div class="content">
                </div>
            </li>
              
            <%} %>
    </ul>
</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
