﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    ViewSavingBook
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div id="SettingsPage_Content">
    <ul class="uiList fbSettingsList _4kg  _4ks">
        <div class="content">
        </div>
        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink clearfix" style="background-color:#3b5998; text-align:center; font-size:13px; font-family:Tahoma; font-weight:bold;">
            Xác nhận sổ tiết kiệm
            </a>
            <div class="content">
            </div>
        </li>
        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink">
                <span class="pls fbSettingsListItemLabel"><strong>Mã Sổ:</strong></span>

                <span class="fbSettingsListItemContent fcg"><strong><input type="text" class="span4 text" id="AccountCode" /></strong></span>
            </a>
            <div class="content">
            </div>
        </li>
        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink clearfix" style="background-color:#3b5998; text-align:center; font-size:13px; font-family:Tahoma; font-weight:bold;">
            CHỨNG THỰC
            </a>
            <div class="content">
            </div>
        </li>
        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink">
                <span class="pls fbSettingsListItemLabel"><strong></strong></span>
                <span class="fbSettingsListItemContent fcg" style="text-align:center"><strong>&nbsp;<input type="button" class="btnsend" id="btnsend" value="Send"/></strong></span>
            </a>
            <div class="content">
            </div>
        </li>
    </ul>
</div>
<script type="text/javascript">
    $(function () {
        $("#btnsend").click(function () {
            $.ajax({
                url: '<%= Url.Action("CheckExistsSavingBook","SavingBook") %>',
                type: "Get",
                data: { code: $("#AccountCode").val()},
                success: function (_data) {
                    if (_data.toString() != "false") {
                        window.location.href = "/SavingBook/DetailsSavingBook/" + $("#AccountCode").val();
                    }
                    else {
                        alert("Không thể thực hiện thao tác!");
                    }
                }
            });
        });
    });
</script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
