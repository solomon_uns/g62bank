﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<G62BANK.Models.SavingBookType>>" %>
  
<table summary="Submitted table designs">
    <thead>
        <tr>
            <th scope="col">Mã Loại</th>
            <th scope="col">Tên Loại</th>
            <th scope="col">Thời Gian Đáo Hạn (Ngày)</th>
            <th scope="col">Lãi Suất</th>
            <th scope="col">Lãi Suất Trước Hạn</th>
            <th scope="col">Ngày Áp Dụng</th>
            <th scope="col">Click Chọn</th>
        </tr>
    </thead>

    <tbody>
        <%for(int i = 0 ; i < Model.Count(); i++) 
        {%>
            <tr>
                <td><%=Html.DisplayFor(p=>p[i].SavingBookTypeCode)%></td>
                <td><%=Html.DisplayFor(p=>p[i].SavingBookTypeName)%></td>
                <td><%=Html.DisplayFor(p=>p[i].SavingBookTime)%></td>
                <td><%=Html.DisplayFor(p=>p[i].Rate)%></td>
                <td><%=Html.DisplayFor(p=>p[i].RateBeforeTerm)%></td>
                <td><%=Html.DisplayFor(p=>p[i].DayApply)%></td>
                <td style="cursor:pointer; font-weight:bold;" onclick="DoNav('<%=Model[i].SavingBookTypeCode%>');">Chọn</td>
                <script type="text/javascript">
                    function DoNav(theUrl) {
                        $.ajax({
                            url: '<%= Url.Action("SelectedSavingBookType","SavingBook") %>',
                            type: "GET",
                            data: { code: theUrl},
                            success: function (data) 
                            {
                                $('#des').html(data);
                            }
                        });
                    }
                    </script>
            </tr>
        <%}%>
    </tbody>
</table>
<style>
#tablesavingbook table {
  border-collapse: collapse;
  border: 1px solid #38160C;
  font: normal 11px verdana, arial, helvetica, sans-serif;
  color: #F6ECF0;
  background-color:#641B35;
  width:100%;
  }
#tablesavingbook table caption 
{
  float:left;
  text-align: left;
  font: normal 11px verdana, arial, helvetica, sans-serif;
  background: transparent;
  }
#tablesavingbook table td
{
  border: 1px dashed #B85A7C;
  padding: .8em;
  color: #F6ECF0;
  width:16%;
  }
 #tablesavingbook table th
  {
  	  border: 1px dashed #B85A7C;
      padding: .8em;
      color: #F6ECF0;
  	}
#tablesavingbook table thead th, tfoot th 
{
  font: bold 11px verdana, arial, helvetica, sans-serif;
  border: 1px solid #A85070;
  text-align: center;
  background: #38160C;
  color: #F6ECF0;
  padding-top:6px;
  }
#tablesavingbook table tbody td a {
  background: transparent;
  text-decoration: none;
  color: #F6ECF0;
  }
#tablesavingbook table tbody td a:hover {
  background: transparent;
  color: #FFFFFF;
  }
#tablesavingbook table tbody th a {
  font: normal 11px verdana, arial, helvetica, sans-serif;
  background: transparent;
  text-decoration: none;
  font-weight:normal;
  color: #F6ECF0;
  }
#tablesavingbook table tbody th a:hover {
  background: transparent;
  color: #FFFFFF;
  }
#tablesavingbook table tbody th, tbody td {
  vertical-align: top;
  text-align: left;
  }
#tablesavingbook table tfoot td {
  border: 1px solid #38160C;
  background: #38160C;
  padding-top:6px;
  }
#tablesavingbook table .odd {
  background: #7B2342;
  }
#tablesavingbook table tbody tr:hover {
  background: #51152A;
  }
#tablesavingbook table tbody tr:hover th,
#tablesavingbook table tbody tr.odd:hover th {
  background: #51152A;
  }</style>