﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.SavingBookType>" %>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span style="color:Red; font-size:large;">Bạn đã chọn loại tiền gửi tiết kiệm <%=Html.DisplayFor(p=>p.SavingBookTypeName) %></span>
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>Mã Loại :</strong></span>
        <span class="fbSettingsListItemContent fcg"><strong id="savingbooktype"><%=Html.DisplayFor(p => p.SavingBookTypeCode)%></strong></span>
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>Tên Loại :</strong></span>
        <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.SavingBookTypeName) %></strong></span>
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>Thời Hạn :</strong></span>
        <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.SavingBookTime) %></strong></span>
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>Lãi Suất :</strong></span>
        <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.Rate) %> (%)</strong></span>
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>Lãi Suất Trước Hạn :</strong></span>
        <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.RateBeforeTerm) %>(%)</strong></span>
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>Loại Tiền Gửi :</strong></span>
        <span class="fbSettingsListItemContent fcg">&nbsp;<%=Html.DisplayFor(p=>p.TypeOfDeposit) %></span>
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>Số Tiền Gửi Tối Thiểu :</strong></span>
        <span class="fbSettingsListItemContent fcg">&nbsp;<%=Html.DisplayFor(p=>p.MinimumMoney) %></span>
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>Số Tiền Gửi Tối Đa :</strong></span>
        <span class="fbSettingsListItemContent fcg">&nbsp;<%=Html.DisplayFor(p=>p.MaximumMoney) %></span>
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>Hình Thức Thanh Toán:</strong></span>
        <span class="fbSettingsListItemContent fcg">&nbsp;<%=Html.DisplayFor(p=>p.PaymentType) %></span>
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>Khác :</strong></span>
        <span class="fbSettingsListItemContent fcg">&nbsp;<%=Html.DisplayFor(p=>p.Other) %></span>
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong></strong></span>
        <span class="fbSettingsListItemContent fcg"><strong>&nbsp;
        <input value="Hủy Chọn" type="button" id="btnsave"/>
        <script type="text/javascript">
            $(function () {
                $("#btnsave").click(function () {
                    $("#des").html("");
                });
            });
            </script></strong></span>
    </a>
    <div class="content">
    </div>
</li>
