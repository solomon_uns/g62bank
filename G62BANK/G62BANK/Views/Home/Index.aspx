﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="indexTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Home Page - My ASP.NET MVC Application
</asp:Content>
<asp:Content ID="indexFeatured" ContentPlaceHolderID="FeaturedContent" runat="server">
    
   </asp:Content>

<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server"> 
<%Html.RenderAction("SlideShow", "Other");%>
<div class="bderBtom">
    
            <div class="blockItemDv1">
                <div class="paddingBt6" style="height: 93px;">
                    
                    <a href="#">
                        <img alt="" style="width:100%;" src="../../Content/images/new_1.jpg">
                    </a>
                </div>
                
                <div class="bold" style="height:50px;padding-top:10px;">
                    <a style="text-decoration:none;color: #193a56;" href="#">
                        Nhân dịp đón chào năm mới 2013 xuân Quý Tỵ, kể từ ngày 17/12/2012 đến ngày 2/3/2013, Ngân hàng TMCP..
                        
                    </a>
                </div>
                <div class="textRight paddingR14 fontsize11">
                    <a style="text-decoration:none;" class="color2" href="#">Xem tiếp&gt;&gt;</a>
                </div>
            </div>
        
            <div class="blockItemDv2">
                <div class="paddingBt6" style="height: 93px;">
                    
                    <a style="text-decoration:none;" href="#">
                        <img alt="" style="width:100%;" src="../../Content/images/new_2.jpg">
                    </a>
                </div>
               
                <div class="bold" style="height:50px;padding-top:10px;">
                    <a style="text-decoration:none;color: #193a56;" href="#">
                        Nhằm chia sẻ khó khăn và hỗ trợ khách hàng doanh nghiệp có nhu cầu vay vốn, thúc đẩy sản xuất kinh..
                        
                    </a>
                </div>
                <div class="textRight paddingR14 fontsize11">
                    <a style="text-decoration:none;" class="color2" href="#">
                        Xem tiếp&gt;&gt;
                    </a>
                </div>
            </div>
        
            <div class="blockItemDv1">
                <div class="paddingBt6" style="height: 93px;">
                    
                    <a style="text-decoration:none;" href="#">
                        <img alt="" style="width:100%;" src="../../Content/images/new_3.jpg">
                    </a>
                </div>
                
                <div class="bold" style="height:50px;padding-top:10px;">
                    <a style="text-decoration:none;color: #193a56;" href="#">
                        Từ ngày 11/9/2012, Ngân hàng TMCP Đại Dương (OceanBank) triển khai đồng loạt 5 sản phẩm tín dụng..
                        
                    </a>
                </div>
                <div class="textRight paddingR14 fontsize11">
                    <a style="text-decoration:none;" class="color2" href="#">Xem tiếp&gt;&gt;</a>
                </div>
            </div>
        
    <div class="clearfix"></div>
</div>

<div class="boxGrModule">
    <div class="itemModule">
        <div class="paddingR10">
            <div class="paddingBt10">
                <div class="paddingleft14 textTrans">
                    <h1>
                        <span class="bold"><a class="color2" href="#">Ngân hàng doanh nghiệp</a> </span>
                    </h1>
                </div>
                <div class="paddingLr10">
                    <div class="mnuShareItemMdule">
                        <p class="menu_head">
                            <span>Dịch vụ tài khoản</span>
                        </p>
                        <p class="menu_head">
                            <span>Sản phẩm cho vay</span>
                        </p>
                        <p class="menu_head">
                            <span>Thanh toán quốc tế</span>
                        </p>
                    </div>
                </div>
            </div>
                        
        </div>
    </div>
    <div class="itemModule">
        <div class="paddingR10">
            <div class="paddingBt10">
                <div class="paddingleft14 textTrans">
                    <h1>
                        <span class="bold"><a class="color2" href="#">Ngân hàng doanh nghiệp</a> </span>
                    </h1>
                </div>
                <div class="paddingLr10">
                    <div class="mnuShareItemMdule">
                        <p class="menu_head">
                            <span>Dịch vụ tài khoản</span>
                        </p>
                        <p class="menu_head">
                            <span>Sản phẩm cho vay</span>
                        </p>
                        <p class="menu_head">
                            <span>Thanh toán quốc tế</span>
                        </p>
                    </div>
                </div>
            </div>
                        
        </div>
    </div>
    <div class="itemModule">
        <div class="paddingR10">
            <div class="paddingBt10">
                <div class="paddingleft14 textTrans">
                    <h1>
                        <span class="bold">
                            <a class="color2" href="#">Ngân hàng điện tử</a>
                        </span>
            
                    </h1>
                </div>
                <div class=" paddingLr10">
                    <div class="mnuShareItemMdule">
                        <p class="menu_head">
                            <span>
                                <a href="#">
                                    Easy Online Banking
                                </a>
                            </span>
                        </p>
                        <p class="menu_head">
                            <span>
                                <a href="#">
                                    Easy Mobile Banking
                                </a>
                            </span>
                        </p>
                        <p class="menu_head">
                            <span>
                                <a href="#">
                                    Easy M-Plus Banking
                                </a>
                            </span>
                        </p>
                        <p class="menu_head">
                            <span>
                                <a href="#">
                                    Easy Corporate Banking
                                </a>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="paddingBt10">
                <div class="paddingleft14">
                    <div class="boxTabShare">
                        <div class="boxTabShareLeft">
                            <div class="boxTabShareRight">
                                <h1>
                                    <a href="#" class="color2">Mạng lưới</a></h1>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" paddingLr10 fontsize11">
                    <ul class="mnuItemShareOther">
                        <li><a class="clorHex" href="#">
                            Điểm đặt ATM</a></li>
                
                        <li><a class="clorHex" href="#">
                            Điểm đặt POS</a></li>
                
                        <li><a class="clorHex" href="#">
                            Các Ngân hàng Đại lý</a></li>
                
                        <li><a class="clorHex" href="#">
                            Danh sách PGD/Chi nhánh</a></li>
                    </ul>
            </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
</asp:Content>
