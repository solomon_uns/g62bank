﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<List<G62BANK.ViewModels.HomeModel.News>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    News
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<style type="text/css">
    .title
    {
        font-size:12px;
    }
    .description
    {
        font-size:12px;
    }
    .publicdate
    {
        font-size:12px;
    }
</style>
<%for (int i = 0; i < Model.Count; i++)
  { %>
    <div class="title" ><a href="/Home/DetailsNews?Url=<%=Model[i].link%>"><%=Model[i].title%></a></div>
    <div class="description"><%=Model[i].description%></div>
    <div class="publicdate">Ngày đăng tin: <%=Model[i].pubdate%></div>
<%} %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
