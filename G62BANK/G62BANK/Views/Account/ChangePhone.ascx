﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<% Html.ValidationSummary(); %>
<% Html.EnableClientValidation();%>
<script type="text/javascript" src="../../Scripts/jquery-1.7.1.js" ></script>
<script type="text/javascript" src="../../Scripts/jquery-1.7.1.min.js" ></script>
<script type="text/javascript" src="../../Scripts/jquery.validate.min.js" ></script>
<script type="text/javascript" src="../../Scripts/jquery.validate.unobtrusive.min.js" ></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
  <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
  <% using (Html.BeginForm())
        { %>

    <div class="fbSettingsEditor uiBoxGray noborder">
            <div class="pbm fbSettingsEditorFields">
                <div class="ptm">
                    <table class="uiInfoTable uiInfoTableFixed noBorder">
                        <tbody>
                            <tr class="dataRow">
                                <th class="label">
                                </th>
                                <td class="data" id="phonemessage">
                                </td>
                            </tr>
                            <tr class="dataRow">
                                <th class="label">
                                    <label for="password_old">Điện Thoại :</label>
                                </th>
                                <td class="data">
                                    <input id="txtphone" type="text" value="">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="mtm topborder">
                    <div class="mtm">
                        <input value="SaveChange" type="button" id="btnsave"/>
                        <input value="Cancel" type="button" id="btncancel" />
                        <script type="text/javascript">
                            $(function () {
                                $("#btncancel").click(function () {
                                    $("#changephone").html("");
                                    $("#editphone").html("Edit");
                                });
                            });
                                
                        </script>
                        <script type="text/javascript">
                            $(function () {
                                $("#btnsave").click(function () {
                                    $.ajax({
                                        url: '<%= Url.Action("ChangePhone","Account") %>',
                                        type: "POST",
                                        data: { phone: $("#txtphone").val() },
                                        success: function (data) {
                                            if (data == false) {
                                                $("#phonemessage").html("<span style='color:Red'>Lỗi cập nhật địa chỉ mới!</span>");
                                            }
                                            else {
                                                $("#changephone").html("");
                                                $("#phone").html("<strong>" + data + "</strong>");
                                                $("#editphone").html("Edit");
                                            }
                                        }
                                    });
                                });
                            });
                                
                        </script>
                    </div>
                </div>
            </div>
    </div>
<div class="content"></div>

        <%} %>





