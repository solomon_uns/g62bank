﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.BankAccount>" %>

<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink clearfix" style="background-color:#3b5998; text-align:center; font-size:13px; font-family:Tahoma; font-weight:bold;">
    THÔNG TIN TÀI KHOẢN
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>CMND</strong></span>
        <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
            <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
            Edit
            </span>--%>
        <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.CustomerID) %></strong></span>
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink clearfix">
        <span class="pls fbSettingsListItemLabel"><strong>Họ Tên</strong></span>
        <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
            <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
            Edit</span>--%>
        <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.FullName) %></strong></span>
    </a>
    <div class="content">
    </div>
</li>

<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink clearfix">
        <span class="pls fbSettingsListItemLabel"><strong>Mật Khẩu</strong></span>
        <span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit" id="editpassword">
            Edit</span>
        <span class="fbSettingsListItemContent fcg"><strong id="textpassword">**********</strong></span>
    </a>
    <div class="content" id="password">
    </div>
    <script type="text/javascript">
        $(function () {
            $("#editpassword").click(function () {
                $.ajax({
                    url: '<%= Url.Action("ChangePasswordAccount","Account") %>',
                    type: "GET",
                    data: { code: $('#acc').text() },
                success: function (data) 
                {
                    $("#password").html(data);
                    $("#editpassword").html("");
                    $("#textpassword").html("");
                }
                });
            });
        });
</script>
</li>



