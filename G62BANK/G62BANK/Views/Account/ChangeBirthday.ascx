﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<html>
<head runat="server">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
                                    
</head>
<body>

  <% using (Html.BeginForm())
        { %>
        
    <div class="fbSettingsEditor uiBoxGray noborder">
            <div class="pbm fbSettingsEditorFields">
                <div class="ptm">
                    <table class="uiInfoTable uiInfoTableFixed noBorder">
                        <tbody>
                            <tr class="dataRow">
                                <th class="label">
                                </th>
                                <td class="data" id="birthdaymessage">
                                </td>
                            </tr>
                            <tr class="dataRow">
                                <th class="label">
                                    <label for="password_old">Ngày Sinh (mm/dd/yyy) :</label>
                                </th>
                                <td class="data">
                                    <input id="txtbirthday" type="text" class="date"/>
                                    
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="mtm topborder">
                    <div class="mtm">
                        <input value="SaveChange" type="button" id="btnsave"/>
                        <input value="Cancel" type="button" id="btncancel" />
                        <script type="text/javascript">
                            $(function () {
                                $("#btncancel").click(function () {
                                    $("#changebirthday").html("");
                                    $("#editbirthday").html("Edit");
                                });
                            });
                                
                        </script>
                        <script type="text/javascript">
                            $(function () {
                                $("#btnsave").click(function () {
                                    $.ajax({
                                        url: '<%= Url.Action("ChangeBirthday","Account") %>',
                                        type: "POST",
                                        data: { date: $("#txtbirthday").val() },
                                        success: function (data) {
                                            if (data == false) {
                                                $("#birthdaymessage").html("<span style='color:Red'>Lỗi cập nhật ngày sinh mới!</span>");
                                            }
                                            else {
                                                $("#changebirthday").html("");
                                                $("#birthday").html("<strong>" + data + "</strong>");
                                                $("#editbirthday").html("Edit");
                                            }
                                        }
                                    });
                                });
                            });
                                
                        </script>
                       <script type="text/javascript">
                           $(function () {
                               $(".date").datepicker();
                           });
                      </script> 
                    </div>
                </div>
            </div>
    </div>
<div class="content"></div>

        <%} %>

        </body>
</html>