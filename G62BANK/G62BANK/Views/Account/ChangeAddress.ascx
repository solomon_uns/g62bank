﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<% Html.ValidationSummary(); %>
<% Html.EnableClientValidation();%>
  <% using (Html.BeginForm())
        { %>

    <div class="fbSettingsEditor uiBoxGray noborder">
            <div class="pbm fbSettingsEditorFields">
                <div class="ptm">
                    <table class="uiInfoTable uiInfoTableFixed noBorder">
                        <tbody>
                            <tr class="dataRow">
                                <th class="label">
                                </th>
                                <td class="data" id="addressmessage">
                                </td>
                            </tr>
                            <tr class="dataRow">
                                <th class="label">
                                    <label for="password_old">Địa Chỉ :</label>
                                </th>
                                <td class="data">
                                    <input id="txtaddress" type="text" value="">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="mtm topborder">
                    <div class="mtm">
                        <input value="SaveChange" type="button" id="btnsave"/>
                        <input value="Cancel" type="button" id="btncancel" />
                        <script type="text/javascript">
                            $(function () {
                                $("#btncancel").click(function () {
                                    $("#changeaddress").html("");
                                    $("#editaddress").html("Edit");
                                });
                            });
                                
                        </script>
                        <script type="text/javascript">
                            $(function () {
                                $("#btnsave").click(function () {
                                    $.ajax({
                                        url: '<%= Url.Action("ChangeAddress","Account") %>',
                                        type: "POST",
                                        data: { address: $("#txtaddress").val() },
                                        success: function (data) {
                                            if (data == false) {
                                                $("#addressmessage").html("<span style='color:Red'>Lỗi cập nhật địa chỉ mới!</span>");
                                            }
                                            else {
                                                $("#changeaddress").html("");
                                                $("#address").html("<strong>" + data + "</strong>");
                                                $("#editaddress").html("Edit");
                                            }
                                        }
                                    });
                                });
                            });
                                
                        </script>
                    </div>
                </div>
            </div>
    </div>
<div class="content"></div>

        <%} %>



