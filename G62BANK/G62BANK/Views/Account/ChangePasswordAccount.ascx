﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.ViewModels.AccountModel.PassworModel>" %>
<% Html.ValidationSummary(); %>
<% Html.EnableClientValidation();%>
<script type="text/javascript" src="../../Scripts/jquery-1.7.1.js" ></script>
<script type="text/javascript" src="../../Scripts/jquery-1.7.1.min.js" ></script>
<script type="text/javascript" src="../../Scripts/jquery.validate.min.js" ></script>
<script type="text/javascript" src="../../Scripts/jquery.validate.unobtrusive.min.js" ></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
  <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
  <% using (Html.BeginForm())
        { %>

    <div class="fbSettingsEditor uiBoxGray noborder">
       
            <div class="pbm fbSettingsEditorFields">
                <div class="ptm">
                    <table class="uiInfoTable uiInfoTableFixed noBorder">
                        <tbody>
                            <tr class="dataRow">
                                <th class="label">
                                </th>
                                <td class="data" id="message">
                                </td>
                            </tr>
                            <tr class="dataRow">
                                <th class="label">
                                    <label for="password_old">Current :</label>
                                </th>
                                <td class="data">
                                    <%=Html.PasswordFor(p => p.CurrentPassword, new { id = "cur" })%>
                                </td>
                            </tr>
                            <tr>
                                <th class="label noLabel"></th>
                                <td class="data">
                                    <%=Html.ValidationMessageFor(p => p.CurrentPassword)%>
                                </td>
                            </tr>
                            <tr class="dataRow">
                                <th class="label">
                                    <label for="password_new">New :</label>
                                </th>
                                <td class="data">
                                    <%=Html.PasswordFor(p => p.Password, new { id = "pass" })%>
                            </tr>
                            <tr>
                                <th class="label noLabel"></th>
                                <td class="data">
                                    <%=Html.ValidationMessageFor(p=>p.Password) %>
                                </td>
                            </tr>
                            <tr class="dataRow">
                                <th class="label">
                                    <label for="password_confirm">Re-type new :</label>
                                </th>
                                <td class="data">
                                    <%=Html.PasswordFor(p => p.ConfirmPassword)%>
                                </td>
                            </tr>
                            <tr>
                                <th class="label noLabel"></th>
                                <td class="data">
                                    <%=Html.ValidationMessageFor(p=>p.ConfirmPassword) %>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="mtm topborder">
                    <div class="mtm">
                        <input value="SaveChange" type="button" id="btnsave"/>
                        <input value="Cancel" type="button" id="btncancel" />
                        <script type="text/javascript">
                            $(function () {
                                $("#btncancel").click(function () {
                                    $("#password").html("");
                                    $("#editpassword").html("Edit");
                                    $("#textpassword").html("**********");
                                });
                                });
                                
                        </script>
                        <script type="text/javascript">
                            $(function () {
                                $("#btnsave").click(function () {
                                    $.ajax({
                                        url: '<%= Url.Action("ChangePasswordAccount","Account") %>',
                                        type: "POST",
                                        data: { current: $("#cur").val(), pass: $("#pass").val() },
                                        success: function (data) {
                                            if (data == true) {
                                                $("#password").html("");
                                                $("#editpassword").html("Edit");
                                                $("#textpassword").html("**********");
                                            }
                                            else {
                                                $("#message").html("<span style='color:Red'>Lỗi cập nhật mật khẩu mới!</span>");
                                            }
                                        }
                                    });
                                });
                            });
                                
                        </script>
                    </div>
                </div>
            </div>
    </div>
<div class="content"></div>

        <%} %>