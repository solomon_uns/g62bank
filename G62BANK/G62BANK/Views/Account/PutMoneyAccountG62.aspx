﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<G62BANK.ViewModels.AccountModel.PutMoneyOnAccountModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    PutMoneyAccountG62
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="stylesheet" href="../../Content/Css/detailsAccount.css">
<div id="SettingsPage_Content">
    <ul class="uiList fbSettingsList _4kg  _4ks">
        <div class="content">
        </div>
            <% using (Html.BeginForm())
               { %>
               <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink clearfix" style="background-color:#3b5998; text-align:center; font-size:13px; font-family:Tahoma; font-weight:bold;">
                    THÔNG TIN TÀI KHOẢN
                    </a>
                    <div class="content">
                    </div>
                </li>
               <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink">
                        <span class="pls fbSettingsListItemLabel"><strong>Ngân Hàng :</strong></span>
                        <span class="fbSettingsListItemContent fcg" style="text-align:left"><strong>&nbsp;<%=Html.DropDownList("Foo", new SelectList(Model.listbank.Select(x => new { Value = x.BankCode, Text = x.BankName }), "Value","Text"))%></span>
                        <span class="pls fbSettingsListItemLabel"><strong>&nbsp;</strong></span>
                        <span class="fbSettingsListItemContent fcg" style="text-align:left"><strong>&nbsp;<%=Html.TextBoxFor(p=>p.bankcode) %></strong></span>
                        <span class="fbSettingsListItemContent fcg" style="text-align:center"><strong id="error-bank"><%=Html.ValidationMessageFor(p=>p.bankcode) %></strong></span>
                        <script type="text/javascript">
                            $(function () {
                                $("#Foo").change(function () {
                                    $('#bankcode').val($('#Foo').val());
                                });
                            });
                            $(function () {
                                $("#bankcode").change(function () {
                                    $.ajax({
                                        url: '<%= Url.Action("CheckBankCode","Account") %>',
                                        type: "GET",
                                        data: { code: $('#bankcode').val()},
                                        success: function (data) 
                                        {
                                            if(data.toString() == "false")
                                                $('#error-bank').html("<span style='color:Red'>Mã Ngân Hàng Không Tồn Tại!</span>");
                                            else
                                                $('#error-bank').html("");
                                        },
                                    });
                                });
                            });
                        </script>
                    </a>
                    <div class="content">
                    </div>
               </li>
               <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink">
                        <span class="pls fbSettingsListItemLabel"><strong>Số Tài Khoản :</strong></span>
                        <span class="fbSettingsListItemContent fcg" style="text-align:left"><strong>&nbsp;<%=Html.TextBoxFor(p=>p.accountcode)%></strong></span>
                        <span class="fbSettingsListItemContent fcg" style="text-align:center"><strong id="error-stk"><%=Html.ValidationMessageFor(p=>p.accountcode) %></strong></span>
                        <script type="text/javascript">
                            $(function () {
                                $("#accountcode").change(function () {
                                    $.ajax({
                                        url: '<%= Url.Action("ShowOwnerOfAccount","Account") %>',
                                        type: "GET",
                                        data: { code: $('#accountcode').val()},
                                        success: function (data) 
                                        {
                                            if(data.toString() == "false")
                                            {
                                                $('#error-stk').html("<span style='color:Red'>Tài khoản này không tồn tại!</span>");
                                                $('#error_des').html("&nbsp;");
                                            }
                                            else
                                            {
                                                $('#error-stk').html("&nbsp;");
                                                $('#error_des').html(data);
                                            }
                                        },
                                    });
                                });
                            });
                        </script>
                    </a>
                    <div class="content">
                    </div>
               </li>
               <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink">
                        <span class="pls fbSettingsListItemLabel"><strong id= "details"></strong></span>
                        <span class="fbSettingsListItemContent fcg" style="text-align:center"><strong id="error_des">&nbsp;</strong></span>
                    </a>
                    <div class="content">
                    </div>
                </li>
                <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink clearfix" style="background-color:#3b5998; text-align:center; font-size:13px; font-family:Tahoma; font-weight:bold;">
                    SỐ TIỀN & PHÍ DỊCH VỤ
                    </a>
                    <div class="content">
                    </div>
                </li>
                <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink">
                        <span class="pls fbSettingsListItemLabel"><strong>Số Tiền :</strong></span>
                        <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.TextBoxFor(p=>p.money) %> (VND)</strong></span>
                        <span class="fbSettingsListItemContent fcg" style="text-align:center" id="error-money"><strong><%=Html.ValidationMessageFor(p=>p.money) %></strong></span>
                        <script type="text/javascript">
                            $(function () {
                                $("#money").change(function () {
                                    $.ajax({
                                        url: '<%= Url.Action("SumMoneyPutMoneyAccountG62","Account") %>',
                                        type: "GET",
                                        data: { money: $('#money').val()},
                                        success: function (data) 
                                        {
                                            if(data.toString() == "false")
                                            {
                                                $('#error-money').html("<span style='color:Red'>Số tiền không hợp lệ!</span>");
                                                $('#sum-money').html("&nbsp;#NA");
                                            }
                                            else
                                            {
                                                $('#sum-money').html("&nbsp;"+data.toString());
                                                $('#error-money').html("");
                                            }
                                        },
                                    });
                                });
                            });
                        </script>
                    </a>
                    <div class="content">
                    </div>
                </li>
                <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink">
                        <span class="pls fbSettingsListItemLabel"><strong>Phí Dịch Vụ :</strong></span>
                        <span class="fbSettingsListItemContent fcg" ><strong>&nbsp;<%=Html.DisplayFor(p=>p.cost) %> %</strong></span>
                    </a>
                    <div class="content">
                    </div>
                </li>
                <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink">
                        <span class="pls fbSettingsListItemLabel"><strong>Tổng Số Tiền :</strong></span>
                        <span class="fbSettingsListItemContent fcg"><strong id="sum-money">&nbsp;#NA</strong></span>
                    </a>
                    <div class="content">
                    </div>
                </li>
                <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink clearfix" style="background-color:#3b5998; text-align:center; font-size:13px; font-family:Tahoma; font-weight:bold;">
                    NGƯỜI NỘP TIỀN
                    </a>
                    <div class="content">
                    </div>
                </li>
                <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink">
                        <span class="pls fbSettingsListItemLabel"><strong>Họ Tên :</strong></span>
                        <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.TextBoxFor(p=>p.customerName)%></strong></span>
                        <span class="fbSettingsListItemContent fcg" style="text-align:center"><strong><%=Html.ValidationMessageFor(p=>p.customerName) %></strong></span>
                    </a>
                    <div class="content">
                    </div>
                </li>
                <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink">
                        <span class="pls fbSettingsListItemLabel"><strong>CMND :</strong></span>
                        <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.TextBoxFor(p=>p.customerID)%></strong></span>
                        <span class="fbSettingsListItemContent fcg" style="text-align:center" ><strong><%=Html.ValidationMessageFor(p=>p.customerID) %></strong></span>
                    </a>
                    <div class="content">
                    </div>
                </li>
                <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink">
                        <span class="pls fbSettingsListItemLabel"><strong>Địa Chỉ :</strong></span>
                        <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.TextAreaFor(p => p.customerAddress)%></strong></span>
                        <span class="fbSettingsListItemContent fcg" style="text-align:center"><strong><%=Html.ValidationMessageFor(p=>p.customerAddress) %></strong></span>
                    </a>
                    <div class="content">
                    </div>
                </li>
                <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink">
                        <span class="pls fbSettingsListItemLabel"><strong>Nội Dung :</strong></span>
                        <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.TextAreaFor(p=>p.content)%></strong></span>
                        <span class="fbSettingsListItemContent fcg" style="text-align:center"><strong><%=Html.ValidationMessageFor(p=>p.content) %></strong></span>
                    </a>
                    <div class="content">
                    </div>
                </li>
                <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink clearfix" style="background-color:#3b5998; text-align:center; font-size:13px; font-family:Tahoma; font-weight:bold;">
                    CHỨNG THỰC
                    </a>
                    <div class="content">
                    </div>
                </li>
                <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink">
                        <span class="pls fbSettingsListItemLabel"><strong></strong></span>
                        <span class="fbSettingsListItemContent fcg" style="margin-left:32%;">&nbsp;<%=Html.Raw(Html.GenerateCaptcha()) %></span>
                        <span class="fbSettingsListItemContent fcg" style="text-align:center"><strong id ="error-captcha">&nbsp;</strong></span>
                    </a>
                    <div class="content">
                    </div>
                </li>
                <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink clearfix" style="background-color:#3b5998; text-align:center; font-size:13px; font-family:Tahoma; font-weight:bold;">
                    HOÀN TẤT NẠP TIỀN
                    </a>
                    <div class="content">
                    </div>
                </li>
                <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                    <a class="pvm phs fbSettingsListLink">
                        <span class="pls fbSettingsListItemLabel"><strong></strong></span>
                        <span class="fbSettingsListItemContent fcg" style="text-align:center"><strong>&nbsp;<input type="submit" class="btnsend" id="btnsend" value="Send"/></strong></span>
                        <script type="text/javascript">
                              
                        </script>
                    </a>
                    <div class="content">
                    </div>
                </li>
              
            <%} %>
    </ul>
</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
