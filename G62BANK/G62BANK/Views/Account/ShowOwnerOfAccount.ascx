﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.Account>" %>

<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel" style="text-align:center"><strong >Họ Tên :</strong></span>
        <span class="fbSettingsListItemContent fcg" style="text-align:left"><strong>&nbsp;<%=Html.DisplayFor(p=>p.FullName)%></strong></span>
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel" style="text-align:center"><strong>CMND :</strong></span>
        <span class="fbSettingsListItemContent fcg" style="text-align:left"><strong>&nbsp;<%=Html.DisplayFor(p => p.CustomerID)%></strong></span>
    </a>
    <div class="content">
    </div>
</li>