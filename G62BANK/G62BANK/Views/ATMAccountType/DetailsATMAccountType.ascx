﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.ATMAccountType>" %>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>Mã Loại Thẻ</strong></span>
        <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
            <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
            Edit
            </span>--%>
        <span class="fbSettingsListItemContent fcg"><strong id="mathe"><%=Html.DisplayFor(p => p.ATMAccountTypeCode)%></strong></span>
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>Tên Loại</strong></span>
        <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
            <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
            Edit
            </span>--%>
        <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.ATMAccountTypeName) %></strong></span>
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>Lãi Suất</strong></span>
        <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
            <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
            Edit
            </span>--%>
        <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.Rate) %> (%)</strong></span>
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>Số Ngày Hết Hạn</strong></span>
        <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
            <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
            Edit
            </span>--%>
        <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.NumberDayExpired) %></strong></span>
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>Mô tả</strong></span>
        <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
            <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
            Edit
            </span>--%>
        <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.Description) %></strong></span>
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <%--<span class="pls fbSettingsListItemLabel"><strong></strong></span>--%>
        <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
            <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
            Edit
            </span>--%>
        <span class="fbSettingsListItemContent fcg"><strong>&nbsp;
        <input value="Hủy Chọn" type="button" id="btnsave"/>
        <script type="text/javascript">
            $(function () {
                $("#btnsave").click(function () {
                    $("#details").html("");
                    $("#ajax").html("&nbsp;");
                });
            });
            </script></strong></span>
    </a>
    <div class="content">
    </div>
</li>

