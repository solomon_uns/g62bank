﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<G62BANK.Models.ATMAccountType>>" %>
<%using (Html.BeginForm()){ %>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink clearfix" style="background-color:#3b5998; text-align:center; font-size:13px; font-family:Tahoma; font-weight:bold;">
    THÔNG TIN THẺ
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong>Chọn Loại Thẻ</strong></span>
        <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DropDownList("Foo", new SelectList(Model.Select(x => new { Value = x.ATMAccountTypeCode, Text = x.ATMAccountTypeName }), "Value","Text"))%></strong></span>
        <script type="text/javascript">
            $(function () {
                $("#Foo").change(function () {
                    $("#details").html("Chi Tiết :");
                    $.ajax({
                        url: '<%= Url.Action("DetailsAnATMAccountType","ATMAccountType") %>',
                        type: "GET",
                        data: { code: document.getElementById("Foo").value },
                        success: function (data) {
                            $("#ajax").html(data);
                            $("#error_des").html("");
                        }
                    });

                });
            });
        </script>
    </a>
    <div class="content">
    </div>
</li>
<li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
    <a class="pvm phs fbSettingsListLink">
        <span class="pls fbSettingsListItemLabel"><strong id= "details"></strong></span>
        <span class="fbSettingsListItemContent fcg"><strong id="ajax">&nbsp;</strong></span>
        <span class="fbSettingsListItemContent fcg" style="text-align:center"><strong id="error_des"></strong></span>
    </a>
    <div class="content">
    </div>
</li>

<%} %>
