﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="accordion">
        <%  
            foreach (G62BANK.Models.ForumModel topic in ViewBag.Topics)
            {
        %>
        <h3>
            <% this.Response.Write(topic.Title); %></h3>
        <div>
            <% this.Response.Write(topic.Content); %>
            <br /><br />
            <div id="divComment_<% this.Response.Write(topic.Id.ToString()); %>">
                <b>Comments:</b>

                <%
                foreach (G62BANK.Models.ForumModel.Comment comment in topic.Comments)
                { 
                %>
                <div>
                    <% Response.Write(comment.Owner + ": " + comment.Content); %></div>
                <%}%>
            </div>
            <br />
            <div>
                <textarea type="text" id="txtComment_<% this.Response.Write(topic.Id.ToString()); %>" style="height: 50px; width:100%; overflow: hidden; 
                    padding-top: 0px; padding-bottom: 0px; color: rgb(159, 159, 159);"></textarea>
                    <br /><br />
                <%if (Request.IsAuthenticated)
                  { %>
                <input id="btnSendComment" type="button" value="Send" style="height: 25px; width: 75px; float:right;" title="<% this.Response.Write(topic.Id.ToString()); %>"/>
                <%} %>
            </div>
            
        </div>
        <% } %>
    </div>
    <br />
    <br />
    <%if (Request.IsAuthenticated)
    { %>
    <div>
        <div><h2>New Topic</h2></div>
        <div>
            <h3>
            <div>Tiêu đề:</div>
            <div><input type="text" id="txtNewTopicTitle" /></div>
            </h3>
        </div>
        <div>
            <h3>Nội dung:</h3>
            <textarea type="text" id="txtNewTopicContent" style="height: 100px; width: 100%; overflow: hidden;
                padding-top: 0px; padding-bottom: 0px; color: rgb(159, 159, 159);"></textarea>
        </div>
        <br />
        <br />
        <input id="btnNewTopic" type="button" value="New Topic" style="height: 25px; width: 125px; float: right;"/>
    </div>
    <% }%>
<script type="text/jscript">
    $(document).ready(function () {
        $('input[value|="Send"]').each(function () {
            $(this).click(function () {
                var id = $(this).attr("title");
                if ($("#txtComment_" + id).val() != "") {
                    $.ajax({
                        type: "POST",
                        url: '<%= Url.Action("AddComment","Forum") %>',
                        data: { TopicId: id, Owner: "<%:Page.User.Identity.Name%>", Content: $("#txtComment_" + id).val() }
                    }).done(function (msg) {
                        $("#divComment_" + id).append("<div><%:Page.User.Identity.Name%>: " + $("#txtComment_" + id).val() + "</div>");
                        $("#txtComment_" + id).val("");
                    });
                }

            });
        });

        $("#btnNewTopic").click(function () {
            if ($("#txtNewTopicTitle").val() != "" && $("#txtNewTopicContent").val() != "") {
                $.ajax({
                    type: "POST",
                    url: '<%= Url.Action("AddTopic","Forum") %>',
                    data: { Owner: "<%:Page.User.Identity.Name%>", Title: $("#txtNewTopicTitle").val(), Content: $("#txtNewTopicContent").val() }
                }).done(function (msg) {
                    //                    $("#txtNewTopicTitle").val("");
                    //                    $("#txtNewTopicContent").val("");
                    window.location.replace("<%= Url.Action("Index","Forum") %>");
                   
                });
            }
        });

        $("#accordion").accordion({
            //icons: null
        });
    });

    

</script>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>

