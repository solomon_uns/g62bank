﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.SavingBook>" %>

<script type="text/javascript">
    function Autorun() {
        $.ajax({
            url: '<%= Url.Action("DetailsSavingBook","BankManager") %>',
            type: "POST",
            data: {code : <%=Model.SavingBookCode %>},
            success: function (_data) {
                $("#det").html(_data);
            }
        });
        $.ajax({
            url: '<%= Url.Action("GetCostMaturity","BankManager") %>',
            type: "POST",
            success: function (_data) {
               var textbox1 = document.getElementById('phi');
                    textbox1.value = _data;
            }
        });
        var savcode = <%=Model.SavingBookCode %>;
        var money = <%=Model.Money %>; 
        $.ajax({
            url: '<%= Url.Action("CheckMaturitySavingBook","BankManager") %>',
            type: "POST",
            data: {code : savcode},
            success: function (data2) {
                if (data2.toString() != "false") {
                    var textbox2 = document.getElementById('lai');
                    textbox2.value = data2;

                    var textbox3 = document.getElementById("nhan");
                    textbox3.value = parseFloat(money) + parseFloat(data2);
                }
            }
        });
    }
    window.load = Autorun();
</script>
<div class="row-fluid">
    <div class="span12">
        <div class="page-header">
            <h4>Rút tiền tiết kiệm</h4>
        </div>

        <%using (Html.BeginForm("MaturitySavingBook", "BankManager", FormMethod.Post, new { @class = "form-horizontal seperator", @id = "frm" }))
            {%>
            <div class="form-row row-fluid">
                <div class="span12" id="det">
                </div>
            </div>
            <%-- Số tiền rút--%>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Số Tiền Rút:</label>
                            <%=Html.TextBoxFor(p=>p.Money, new {@class="span4 text", @readonly="readonly"})%>
                        </div>
                    </div>
                </div>
            </div>

            <%-- Số tiền lãi --%>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Số Tiền Lãi:</label>
                            <input type="text" class="span4 text" id="lai" readonly="readonly" />
                        </div>
                    </div>
                </div>
            </div>

            <%-- Phí dịch vụ--%>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Phí Dịch Vụ:</label>
                            <input type="text" class="span4 text" id="phi" readonly="readonly"/> (%)
                        </div>
                    </div>
                </div>
            </div>  

            <%-- Số tiền nhận--%>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Số Tiền Sẽ Nhận:</label>
                            <input type="text" class="span4 text" id="nhan" readonly="readonly" />
                        </div>
                    </div>
                </div>
            </div>

            <%-- button--%> 
            <div class="form-row row-fluid">        
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                        <div class="span3"></div>
                        <div class="span7 controls">
                            <img src="../../Content/Icon/help-3-24.ico" title="Xem công thức tính lãi suất" onclick="ShowMessages();"/>
                            <button type="button" class="btn btn-info marginR10" onclick="fnSave();">Save changes</button>
                        </div>
                        </div>
                    </div>
                </div>   
            </div>


        <%} %>
    </div><!-- End .span12 -->
</div>
<div id="NoteDialog14" title="" class="Hidden"></div>
<script type="text/javascript">
    $(function () {
        $("#NoteDialog14").dialog({
            autoOpen: false, width: 800, height: 500, modal: true,
            buttons: {
                Cancel: function () { $("#NoteDialog14").dialog("close"); }
            }
        });
    });
</script>

<script type="text/javascript">
    function ShowMessages() {
        var _code = <%=Model.SavingBookCode %>;
        $.ajax({
            url: '<%= Url.Action("GetStringMaturitySavingBook","BankManager") %>',
            type: "POST",
            data: { code: _code},
            success: function (_data) {
                if (_data.toString() == "false") {
                    $("#NoteDialog14").html("Không thể thực hiện thao tác").dialog("option", "title", "Cách Tính Lãi Suất");
                    $("#NoteDialog14").dialog("open");
                } else {
                    $.ajax({
                        url: '<%= Url.Action("Messages","BankManager") %>',
                        type: "POST",
                        data: _data,
                        success: function (data) {
                            $("#NoteDialog14").html(data).dialog("option", "title", "Cách Tính Lãi Suất");
                            $("#NoteDialog14").dialog("open");
                        }
                    });
                }
            }
        });
    }
</script>

<script type="text/javascript">
    function fnSave() {
        var savcode = <%=Model.SavingBookCode %>; 
        $.ajax({
            url: '<%= Url.Action("MaturitySavingBook_Submit","BankManager") %>',
            type: "POST",
            data: {code : savcode},
            success: function (_data) {
                if (_data.toString() == "true") {
                    alert("đã rút tiền thành công");
                } else {
                    $("#notegetmoney").html("Số tiền không hợp lệ");
                }
            }
        });
    }
</script>






