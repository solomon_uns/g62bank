﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<G62BANK.Models.SavingBookType>>" %>

<script src="../../Scripts/jquery.tablesorter.js" type="text/javascript"></script>
<script src="../../Scripts/jquery.tablesorter.pager.js" type="text/javascript"></script>


<script type="text/javascript">
    $(function () {
        $("table.tablesorter").tablesorter({ widthFixed: true, sortList: [[0, 0]] })
        .tablesorterPager({ container: $("#pager"), size: $(".pagesize option:selected").val() });
    });
</script>
<div class="row-fluid">
    <div class="span12">                    
        <div class="box">
            <div class="title">
                <h4>
                    <span class="icon16 icomoon-icon-equalizer-2"></span>
                    <span>SavingBook Type List</span>
                </h4>
            </div>
            <div class="content noPad">
                <table class="responsive table table-bordered tablesorter">
                    <thead>
                        <tr>
                        <th>#</th>
                        <th title="Mã Loại">Code</th>
                        <th title="Tên Loại">Name</th>
                        <th title="Thời Gian">Rate (%)</th>
                        <th title="LS Trước Hạn">RateBeforTerm (%)</th>
                        <th title="Ngày hết hạn">MinimumMoney</th>
                        <th title="Mô tả">MaximumMoney</th>
                        <th title="Mô tả">Payment Type</th>
                        <th title="" style="width:50px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                         <%for (int i = 0; i < Model.Count(); i++)
                           {%>
                        <tr">
                            <td><%=i+1%></td>
                            <td><%=Html.DisplayFor(p=>p[i].SavingBookTypeCode)%></td>
                            <td><%=Html.DisplayFor(p=>p[i].SavingBookTypeName)%></td>
                            <td><%=Html.DisplayFor(p=>p[i].Rate)%> (%)</td>
                            <td><%=Html.DisplayFor(p=>p[i].RateBeforeTerm)%> (%)</td>
                            <td><%=Html.DisplayFor(p=>p[i].MinimumMoney)%></td>
                            <td><%=Html.DisplayFor(p=>p[i].MaximumMoney)%></td>
                            <td><%=Html.DisplayFor(p=>p[i].PaymentType)%></td>
                            <td>
                                <div class="controls center">
                                    <a class="tip">
                                        <span>
                                            <img src="../../Content/Icon/pencil-8-24.ico" onclick="EditSaving('<%=Model[i].SavingBookTypeCode%>');" title="Edit" style="cursor:pointer;"/>
                                            <img src="../../Content/Icon/x-mark-4-24.ico" onclick="DeleteSaving('<%=Model[i].SavingBookTypeCode%>');" title="Delete" style="cursor:pointer;"/>
                                        </span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <%} %>
                    </tbody>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span6 -->
    <div id="pager" style="position: inherit;float:left;" >
        <img src="../../Content/Manager/img/first.png" class="first" />
        <img src="../../Content/Manager/img/prev.png" class="prev" />
        <input type="text" class="pagedisplay" />
        <img src="../../Content/Manager/img/next.png" class="next" />
        <img src="../../Content/Manager/img/last.png" class="last"/>
        <select class="pagesize" style="width: 150px;">
            <option selected="selected" value="5">5</option>
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="30">30</option>
            <option value="40">40</option>
        </select>
    </div>
    <div style="float:right; margin-right:5px;">
        <button type="button" class="btn btn-info marginR10" onclick="New();">New SavingBookType</button>
    </div>
</div>
<div id="NoteDialog20" title="" class="Hidden"></div>
<script type="text/javascript">
    $(function () {
        $("#NoteDialog20").dialog({
            autoOpen: false, width: 800, height: 600, modal: true,
            buttons: {
                Cancel: function () { $("#NoteDialog20").dialog("close"); }
            }
        });
    });
</script>
<script type="text/javascript">
    function EditSaving(_code) {
        $.ajax({
            url: '<%= Url.Action("EditRateSavingBookType_Get","BankManager") %>',
            type: "POST",
            data: { code: _code },
            success: function (_data) {
                if (_data.toString() != "false") {
                    $("#NoteDialog20").html(_data);
                    $("#NoteDialog20").dialog("option", "title", "Edit SavingBook Type");
                    $("#NoteDialog20").dialog("open");
                } else {
                    $("#NoteDialog20").html("Không thể thực hiện thao tác!");
                    $("#NoteDialog20").dialog("option", "title", "Error");
                    $("#NoteDialog20").dialog("open");
                }
            }
        });
    }
</script>
<script type="text/javascript">
    function DeleteSaving(_code) {
        $.ajax({
            url: '<%= Url.Action("DeleteRateSavingBookType","BankManager") %>',
            type: "POST",
            data: { code: _code },
            success: function (_data) {
                if (_data.toString() != "false") {
                    alert("Delete thành công!");
                    //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
                    $.ajax({
                        url: '<%= Url.Action("RateSavingBookType_Get","BankManager") %>',
                        type: "Post",
                        success: function (data) {
                            $("#main-content").html(data);
                        }
                    });
                } else {
                    $("#NoteDialog20").html("Không thể thực hiện thao tác!");
                    $("#NoteDialog20").dialog("option", "title", "Error");
                    $("#NoteDialog20").dialog("open");
                }
            }
        });
    }
</script>
<script type="text/javascript">
    function New() {
        $.ajax({
            url: '<%= Url.Action("CreateNewSavingBookType","BankManager") %>',
            type: "POST",
            success: function (_data) {
                if (_data.toString() != "false") {
                    $("#NoteDialog20").html(_data);
                    $("#NoteDialog20").dialog("option", "title", "Create New SavingBook Type");
                    $("#NoteDialog20").dialog("open");
                } else {
                    $("#NoteDialog20").html("Không thể thực hiện thao tác!");
                    $("#NoteDialog20").dialog("option", "title", "Error");
                    $("#NoteDialog20").dialog("open");
                }
            }
        });
    }
</script>
