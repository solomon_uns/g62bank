﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.TranferATMtoATM>" %>
<% Html.ValidationSummary(); %>
<% Html.EnableClientValidation();%>

<div class="row-fluid">
    <div class="span12">
        <div class="page-header">
            <h4>Chuyển tiền giữa các tài khoản DebitCard</h4>
        </div>
        <%using (Html.BeginForm("Tranfers_ATM_to_ATM", "BankManager", FormMethod.Post, new { @class = "form-horizontal seperator", @id = "frm" }))
            {%>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span4" style="color:Red; text-decoration:none;" id="mes"></label>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" >Số Tài Khoản Chuyển:</label>
                    <%=Html.TextBoxFor(p => p.FromAccountCode, new { @class = "span4 text", @OnChange = "CheckAccount1(this.value);" })%>
                </div>
                <div class="row-fluid" style="text-align:left;color:Red;">
                    <%=Html.ValidationMessageFor(p => p.FromAccountCode)%>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" >Chủ Tài Khoản Chuyển:</label>
                    <%=Html.TextBoxFor(p => p.FromCustomerName, new { @class = "span4 text", @readonly = "readonly" })%>
                </div>
                <div class="row-fluid" style="text-align:center;color:Red;">
                    <%=Html.ValidationMessageFor(p => p.FromCustomerName)%>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" >Số Tài Khoản Nhận:</label>
                    <%=Html.TextBoxFor(p => p.ToAccountCode, new { @class = "span4 text",@OnChange = "CheckAccount2(this.value);" })%>
                </div>
                <div class="row-fluid" style="text-align:center;color:Red;">
                    <%=Html.ValidationMessageFor(p => p.ToAccountCode)%>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" >Chủ Khoản Nhận:</label>
                    <%=Html.TextBoxFor(p => p.ToCustomerName, new { @class = "span4 text",@readonly = "readonly" })%>
                </div>
                <div class="row-fluid" style="text-align:center;color:Red;">
                    <%=Html.ValidationMessageFor(p => p.ToCustomerName)%>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <div class="row-fluid">
                        <label class="form-label span3" >Nội Dung:</label>
                        <%=Html.TextBoxFor(p => p.Description, new { @class = "span4 text", @Value = "Chuyển tiền cho tài khoản khác" })%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.Description)%>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <div class="row-fluid">
                        <label class="form-label span3" >Số Tiền Chuyển:</label>
                        <%=Html.TextBoxFor(p => p.Money, new { @class = "span4 text", @OnChange = "GetCost(this.value);" })%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.Money)%>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <div class="row-fluid">
                        <label class="form-label span3" >Phí Dịch Vụ:</label>
                        <%=Html.TextBoxFor(p => p.Cost, new { @class = "span4 text",@readonly="readonly" })%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.Cost)%>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <div class="row-fluid">
                        <label class="form-label span3" >Tổng Số Tiền Sẽ Phải Trừ:</label>
                        <%=Html.TextBoxFor(p => p.SumMoney, new { @class = "span4 text", @readonly = "readonly" })%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.SumMoney)%>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">        
            <div class="span12">
                <div class="row-fluid">
                    <div class="form-actions">
                    <div class="span3"></div>
                    <div class="span7 controls">
                        <img src="../../Content/Icon/help-3-24.ico" title="Xem công thức tính lãi suất" onclick="ShowMessages();"/>
                        <button type="button" class="btn btn-info marginR10" onclick="fnSave();">Save changes</button>
                    </div>
                    </div>
                </div>
            </div>   
        </div>
        <%} %>
    </div>
</div>
<div id="NoteDialog31" title="" class="Hidden"></div>
<script type="text/javascript">
    $(function () {
        $("#NoteDialog31").dialog({
            autoOpen: false, width: 800, height: 500, modal: true,
            buttons: {
                Cancel: function () { $("#NoteDialog31").dialog("close"); }
            }
        });
    });
</script>
<script type="text/javascript">
    function ShowMessages() {
        $.ajax({
            url: '<%= Url.Action("GetStringTranfersMoneyATMtoATM","BankManager") %>',
            type: "POST",
            data: $("#frm").serialize(),
            success: function (_data) {
                if (_data.toString() == "false") {
                    $("#NoteDialog31").html("Không thể thực hiện thao tác").dialog("option", "title", "Cách Tính Lãi Suất");
                    $("#NoteDialog31").dialog("open");
                } else {
                    $.ajax({
                        url: '<%= Url.Action("Messages","BankManager") %>',
                        type: "POST",
                        data: _data,
                        success: function (data) {
                            $("#NoteDialog31").html(data).dialog("option", "title", "Cách Tính Lãi Suất");
                            $("#NoteDialog31").dialog("open");
                        }
                    });
                }
            }
        });
    }
</script>
<script type="text/javascript">
    function CheckAccount1(_code) {
        $.ajax({
            url: '<%= Url.Action("CheckAccountExists","BankManager") %>',
            type: "GET",
            data: { code: _code },
            success: function (data) {
                if (data != "false") {
                    var textbox = document.getElementById('FromCustomerName');
                    textbox.value = data;
                }
            }
        });
    }
</script>
<script type="text/javascript">
    function CheckAccount2(_code) {
        $.ajax({
            url: '<%= Url.Action("CheckAccountExists","BankManager") %>',
            type: "GET",
            data: { code: _code },
            success: function (data) {
                if (data != "false") {
                    var textbox2 = document.getElementById('ToCustomerName');
                    textbox2.value = data;
                }
            }
        });
    }
</script>
<script type="text/javascript">
    function GetCost(_code) {
        $.ajax({
            url: '<%= Url.Action("GetCostTranfersATMtoATM","BankManager") %>',
            type: "GET",
            data: { code: _code },
            success: function (data) {
                if (data != "false") {
                    var textbox3 = document.getElementById('Cost');
                    textbox3.value = data;

                    var textbox4 = document.getElementById('SumMoney');
                    var textbox5 = document.getElementById('Money');
                    textbox4.value = parseInt(data) + parseInt(textbox5.value);
                }
            }
        });
    }
</script>
<script type="text/javascript">
    function fnSave() {
        $.ajax({
            url: '<%= Url.Action("Tranfers_ATM_to_ATM","BankManager") %>',
            type: "POST",
            data: $("#frm").serialize(),
            success: function (data) {
                if (data.toString() != "true") {
                    $("#mes").html(data);
                } else {
                    alert("Giao dịch thành công!");
                    var textbox6 = document.getElementById('FromAccountCode');
                    textbox6.value = "";

                    var textbox7 = document.getElementById('FromCustomerName');
                    textbox7.value = "";

                    var textbox6 = document.getElementById('ToAccountCode');
                    textbox6.value = "";

                    var textbox7 = document.getElementById('ToCustomerName');
                    textbox7.value = "";

                    var textbox8 = document.getElementById('Money');
                    textbox8.value = "";

                    var textbox9 = document.getElementById('Cost');
                    textbox9.value = "";

                    var textbox10 = document.getElementById('SumMoney');
                    textbox10.value = "";
                }
            }
        });
    }
</script>


