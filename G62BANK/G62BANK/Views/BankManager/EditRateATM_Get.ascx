﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.ATMAccountType>" %>

<% Html.ValidationSummary(); %>
<% Html.EnableClientValidation();%>

<div class="row-fluid">
    <div class="span12">
        <div class="page-header">
            <h4>Edit Rate ATM</h4>
        </div>

        <%using (Html.BeginForm("EditRateATM_Post", "BankManager", FormMethod.Post, new { @class = "form-horizontal seperator", @id = "frm" }))
            {%>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span4" for="username" style="color:Red; text-decoration:none;"><%=Html.ValidationSummary() %></label>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="username">Mã Loại:</label>
                        <%=Html.TextBoxFor(p => p.ATMAccountTypeCode, new { @class = "span4 text", @readonly = "reayonly" })%>
                        <%=Html.ValidationMessageFor(p => p.ATMAccountTypeCode)%>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="username">Tên Loại:</label>
                        <%=Html.TextBoxFor(p => p.ATMAccountTypeName, new { @class = "span4 text"})%>
                        <%=Html.ValidationMessageFor(p => p.ATMAccountTypeName)%>              
                    </div>
                    <div class="row-fluid" style="text-align:left;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.ATMAccountTypeName)%>
                    </div>

                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="username">Lãi Suất:</label>
                        <%=Html.TextBoxFor(p => p.Rate, new { @class = "span4 text"})%>
                        <%=Html.ValidationMessageFor(p=>p.Rate ) %>              
                    </div>
                    <div class="row-fluid" style="text-align:left;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.Rate)%>
                    </div>

                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="username">Ngày hết hạn:</label>
                        <%=Html.TextBoxFor(p => p.NumberDayExpired, new { @class = "span4 text"})%>
                        <%=Html.ValidationMessageFor(p => p.NumberDayExpired)%>              
                    </div>
                    <div class="row-fluid" style="text-align:left;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.NumberDayExpired)%>
                    </div>

                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <%=Html.LabelFor(p => p.Description, new { @class = "form-label span3" })%>
                        <%=Html.TextAreaFor(p => p.Description, new { @class = "span4 text" })%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.Description)%>
                    </div>
                </div>
            </div>       
            <div class="form-row row-fluid">        
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                        <div class="span3"></div>
                        <div class="span4 controls">
                            <button type="button" class="btn btn-info marginR10" onclick="fnSave();">Save changes</button>
                        </div>
                        </div>
                    </div>
                </div>   
            </div>


        <%} %>
    </div><!-- End .span12 -->
</div>
<script type="text/javascript">
    function fnSave() {
        $.ajax({
            url: '<%= Url.Action("EditRateATM_Post","BankManager") %>',
            type: "POST",
            data: $("#frm").serialize(),
            success: function (data) {
                if (data.toString() == "true") {
                    alert("Thay đổi thành công!");
                    //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
                    $.ajax({
                        url: '<%= Url.Action("RateATM_Get","BankManager") %>',
                        type: "Post",
                        success: function (data) {
                            $("#main-content").html(data);
                        }
                    });
                } else {

                }
            }
        });
    }
</script>