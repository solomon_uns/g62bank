﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.TranferATMtoPerson>" %>

<% Html.ValidationSummary(); %>
<% Html.EnableClientValidation();%>

<div class="row-fluid">
    <div class="span12">
        <div class="page-header">
            <h4>Nhận tiền chuyển từ tài khoản DebitCard</h4>
        </div>
        <%using (Html.BeginForm("PersonGetMoney_Tranfers_ATM_to_Person", "BankManager", FormMethod.Post, new { @class = "form-horizontal seperator", @id = "frm" }))
            {%>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span4" style="color:Red; text-decoration:none;" id="mes"></label>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" >Mã chuyển tiền:</label>
                    <%=Html.TextBoxFor(p => p.TransactionTranferCode, new { @class = "span4 text"})%>
                </div>
                <div class="row-fluid" style="text-align:left;color:Red;">
                    <%=Html.ValidationMessageFor(p => p.TransactionTranferCode)%>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" >Số Tài Khoản Chuyển:</label>
                    <%=Html.TextBoxFor(p => p.FromAccountCode, new { @class = "span4 text", @OnChange = "CheckAccount1(this.value);" })%>
                </div>
                <div class="row-fluid" style="text-align:left;color:Red;">
                    <%=Html.ValidationMessageFor(p => p.FromAccountCode)%>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" >Chủ Tài Khoản Chuyển:</label>
                    <%=Html.TextBoxFor(p => p.FromCustomerName, new { @class = "span4 text", @readonly = "readonly" })%>
                </div>
                <div class="row-fluid" style="text-align:center;color:Red;">
                    <%=Html.ValidationMessageFor(p => p.FromCustomerName)%>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" >CMND Người Nhận:</label>
                    <%=Html.TextBoxFor(p => p.ToCustomerID, new { @class = "span4 text"})%>
                </div>
                <div class="row-fluid" style="text-align:center;color:Red;">
                    <%=Html.ValidationMessageFor(p => p.ToCustomerID)%>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" >Họ Tên Người Nhận:</label>
                    <%=Html.TextBoxFor(p => p.ToCustomerName, new { @class = "span4 text"})%>
                </div>
                <div class="row-fluid" style="text-align:center;color:Red;">
                    <%=Html.ValidationMessageFor(p => p.ToCustomerName)%>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <div class="row-fluid">
                        <label class="form-label span3" >Nội Dung:</label>
                        <%=Html.TextBoxFor(p => p.Description, new { @class = "span4 text", @Value = "Nhận tiền chuyển từ tài khoản DebitCard" })%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.Description)%>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <div class="row-fluid">
                        <label class="form-label span3" >Số Tiền Chuyển:</label>
                        <%=Html.TextBoxFor(p => p.Money, new { @class = "span4 text", @OnChange = "GetCost(this.value);" })%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.Money)%>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">        
            <div class="span12">
                <div class="row-fluid">
                    <div class="form-actions">
                    <div class="span3"></div>
                    <div class="span7 controls">
                        <img src="../../Content/Icon/help-3-24.ico" title="Xem công thức tính lãi suất" onclick="ShowMessages();"/>
                        <button type="button" class="btn btn-info marginR10" onclick="fnSave();">Save changes</button>
                    </div>
                    </div>
                </div>
            </div>   
        </div>
        <%} %>
    </div>
</div>
<div id="NoteDialog16" title="" class="Hidden"></div>
<script type="text/javascript">
    $(function () {
        $("#NoteDialog16").dialog({
            autoOpen: false, width: 800, height: 500, modal: true,
            buttons: {
                Cancel: function () { $("#NoteDialog16").dialog("close"); }
            }
        });
    });
</script>
<script type="text/javascript">
    function CheckAccount1(_code) {
        $.ajax({
            url: '<%= Url.Action("CheckAccountExists","BankManager") %>',
            type: "GET",
            data: { code: _code },
            success: function (data) {
                if (data != "false") {
                    var textbox = document.getElementById('FromCustomerName');
                    textbox.value = data;
                }
            }
        });
    }
</script>
<script type="text/javascript">
    function fnSave() {
        $.ajax({
            url: '<%= Url.Action("PersonGetMoney_Tranfers_ATM_to_Person","BankManager") %>',
            type: "POST",
            data: $("#frm").serialize(),
            success: function (data) {
                if (data.toString() == "false") {
                    $("#mes").html("Thông Tin Không Chính Xác");
                } else {
                    $("#NoteDialog16").html("Nhận tiền xong!").dialog("option", "title","Thông báo");
                    $("#NoteDialog16").dialog("open");
                    $("#mes").html("");
                    var textbox6 = document.getElementById('FromAccountCode');
                    textbox6.value = "";

                    var textbox7 = document.getElementById('FromCustomerName');
                    textbox7.value = "";

                    var textbox6 = document.getElementById('ToCustomerID');
                    textbox6.value = "";

                    var textbox7 = document.getElementById('ToCustomerName');
                    textbox7.value = "";

                    var textbox8 = document.getElementById('TransactionTranferCode');
                    textbox8.value = "";

                    var textbox9 = document.getElementById('Money');
                    textbox9.value = "";
                }
            }
        });
    }
</script>