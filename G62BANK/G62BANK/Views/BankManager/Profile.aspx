﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Manager.Master" Inherits="System.Web.Mvc.ViewPage<G62BANK.Models.UserManager>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

<% Html.ValidationSummary(); %>
<% Html.EnableClientValidation();%>

<div class="row-fluid">
                    <div class="span12">
                        <div class="page-header">
                            <h4>User profile</h4>
                        </div>

                        <%using (Html.BeginForm("Profile", "BankManager", FormMethod.Post, new { @class = "form-horizontal seperator", enctype = "multipart/form-data" }))
                          {%>
                          <div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <label class="form-label span4" for="username" style="color:Red; text-decoration:none;"><%=Html.ValidationSummary() %></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <label class="form-label span3" for="username">Username:</label>
                                        <%=Html.TextBoxFor(p => p.UserName, new { @class = "span4 text", @disabled = "disabled" })%>
                                        <%=Html.ValidationMessageFor(p=>p.UserName ) %>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <label class="form-label span3" for="username">Avatar:</label> 
                                          <%if (Model.Avatar != null)
                                              { %>
                                                <img src="../../Content/Manager/UploadAvatar/<%=Model.Avatar %>" alt="" class="image marginR10" style="width:150px;height:150px;"/>
                                            <%}
                                              else
                                              { %>
                                            <img src="../../Content/Manager/img/noavatar.png" alt="" class="image marginR10" style="width:150px;height:150px;"/>
                                            <%} %>
                                            <div class="uploader" id="uniform-file">
                                                <%--<input type="file" name="fileinput" id="file"/>--%>
                                                <%=Html.TextBoxFor(p => p.Avatar, new { @id = "file", @name = "fileinput", @type = "file" })%>
                                                <span class="filename">No file selected</span>
                                                <span class="action">Choose File</span>
                                            </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <%=Html.LabelFor(p => p.FullName, new { @class = "form-label span3" })%>
                                        <%=Html.TextBoxFor(p => p.FullName, new { @class = "span4 text" })%>
                                        
                                    </div>
                                    <div class="row-fluid" style="text-align:left;color:Red;">
                                        <%=Html.ValidationMessageFor(p => p.FullName)%>
                                    </div>

                                </div>
                            </div>

                            <div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <%=Html.LabelFor(p => p.Email, new { @class = "form-label span3" })%>
                                        <%=Html.TextBoxFor(p => p.Email, new { @class = "span4 text" })%>
                                    </div>
                                    <div class="row-fluid" style="text-align:center;color:Red;">
                                        <%=Html.ValidationMessageFor(p => p.Email)%>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <div class="row-fluid">
                                            <%=Html.LabelFor(p => p.Birthday, new { @class = "form-label span3" })%>
                                            <%=Html.TextBoxFor(p => p.Birthday, "{0:MM/dd/yyyy}", new { @class = "span4 text" })%>
                                            <script type="text/javascript" src="../../Scripts/jquery-1.7.1.js" ></script>
                                            <script type="text/javascript" src="../../Scripts/jquery-1.7.1.min.js" ></script>
                                            <script type="text/javascript" src="../../Scripts/jquery.validate.min.js" ></script>
                                            <script type="text/javascript" src="../../Scripts/jquery.validate.unobtrusive.min.js" ></script>
                                            <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
                                            <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
                                            <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
                                            <script>
                                                $(function () {
                                                    $("#Birthday").datepicker();
                                                });
                                            </script>
                                        </div>
                                        <div class="row-fluid" style="text-align:center;color:Red;">
                                            <%=Html.ValidationMessageFor(p => p.Birthday)%>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <%=Html.LabelFor(p => p.Phone, new { @class = "form-label span3" })%>
                                        <%=Html.TextBoxFor(p => p.Phone, new { @class = "span4 text" })%>
                                    </div>
                                    <div class="row-fluid" style="text-align:center;color:Red;">
                                        <%=Html.ValidationMessageFor(p => p.Phone)%>
                                    </div>
                                </div>
                                </div>
  
                            <div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <%=Html.LabelFor(p => p.Address, new { @class = "form-label span3" })%>
                                        <%=Html.TextBoxFor(p => p.Address, new { @class = "span4 text" })%>
                                    </div>
                                    <div class="row-fluid" style="text-align:center;color:Red;">
                                        <%=Html.ValidationMessageFor(p => p.Address)%>
                                    </div>
                                </div>
                                </div>  
                            
                            <div class="form-row row-fluid">        
                                <div class="span12">
                                    <div class="row-fluid">
                                        <div class="form-actions">
                                        <div class="span3"></div>
                                        <div class="span4 controls">
                                            <button type="submit" class="btn btn-info marginR10">Save changes</button>
                                            <button class="btn btn-danger">Cancel</button>
                                        </div>
                                        </div>
                                    </div>
                                </div>   
                            </div>


                      <%} %>
                    </div><!-- End .span12 -->
                </div>
</asp:Content>
