﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<G62BANK.Models.Account>>" %>


<script src="../../Scripts/jquery.tablesorter.js" type="text/javascript"></script>
<script src="../../Scripts/jquery.tablesorter.pager.js" type="text/javascript"></script>

<link rel="stylesheet" href="../../Content/Css/detailsAccount.css">
<script type="text/javascript">
    $(function () {
        $("table.tablesorter").tablesorter({ widthFixed: true, sortList: [[0, 0]] })
        .tablesorterPager({ container: $("#pager"), size: $(".pagesize option:selected").val() });
    });
</script>
<div class="row-fluid">
    <div class="span12">                    
        <div class="box">
            <div class="title">
                <h4>
                    <span class="icon16 icomoon-icon-equalizer-2"></span>
                    <span>Kết Quả</span>
                </h4>
            </div>
            <div class="content noPad">
                <table class="responsive table table-bordered tablesorter">
                    <thead>
            <tr>
                <th>#</th>
                <th>AccountCode</th>
                <th>FullName</th>
                <th>CustomerID</th>
                <th>AccountTypeCode</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <%for(int i = 0 ; i < Model.Count(); i++) {%>      
                    <tr onclick="DetailsCreditCard('<%=Html.DisplayFor(p=>p[i].AccountCode)%>');">
                        <td><%=i+1%></td>
                        <td><%=Html.DisplayFor(p=>p[i].AccountCode)%></td>
                        <td><%=Html.DisplayFor(p=>p[i].FullName)%></td>
                        <td><%=Html.DisplayFor(p=>p[i].CustomerID)%></td>
                        <td><%=Html.DisplayFor(p=>p[i].AccountTypeCode)%></td>
                        <td><%=Html.DisplayFor(p=>p[i].Status)%></td>
                    </tr>
                   <% }
                %>
        </tbody>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span6 -->
    <div id="pager" style="position: inherit;float:left;" >
        <img src="../../Content/Manager/img/first.png" class="first" />
        <img src="../../Content/Manager/img/prev.png" class="prev" />
        <input type="text" class="pagedisplay" />
        <img src="../../Content/Manager/img/next.png" class="next" />
        <img src="../../Content/Manager/img/last.png" class="last"/>
        <select class="pagesize" style="width: 150px;">
            <option selected="selected" value="5">5</option>
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="30">30</option>
            <option value="40">40</option>
        </select>
    </div>
</div>
<div id="NoteDialog22" title="" class="Hidden"></div>
<script type="text/javascript">
    $(function () {
        $("#NoteDialog22").dialog({
            autoOpen: false, width: 800, height: 600, modal: true,
            buttons: {
                Cancel: function () { $("#NoteDialog22").dialog("close"); }
            }
        });
    });
</script>
<script type="text/javascript">
    function DetailsCreditCard(_code) {
        $.ajax({
            url: '<%= Url.Action("GetAtmAccount","BankManager") %>',
            type: "POST",
            data: { code: _code },
            success: function (_data) {
                if (_data.toString() != "false") {
                    $.ajax({
                        url: '<%= Url.Action("DetailsATMAccount","BankManager") %>',
                        type: "Get",
                        data: { code: _data },
                        success: function (data2) {
                            $("#NoteDialog22").html(data2.toString());
                            $("#NoteDialog22").dialog("option", "title", "Details CreditCard");
                            $("#NoteDialog22").dialog("open");
                        }
                    });
                } else {
                    $("#NoteDialog22").html("Không thể thực hiện thao tác!");
                    $("#NoteDialog22").dialog("option", "title", "Error");
                    $("#NoteDialog22").dialog("open");
                }
            }
        });
    }
</script>
