﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<G62BANK.Models.TransactionType>>" %>

<script src="../../Scripts/jquery.tablesorter.js" type="text/javascript"></script>
<script src="../../Scripts/jquery.tablesorter.pager.js" type="text/javascript"></script>


<script type="text/javascript">
    $(function () {
        $("table.tablesorter").tablesorter({ widthFixed: true, sortList: [[0, 0]] })
        .tablesorterPager({ container: $("#pager"), size: $(".pagesize option:selected").val() });
    });
</script>
<div class="row-fluid">
    <div class="span12">                    
        <div class="box">
            <div class="title">
                <h4>
                    <span class="icon16 icomoon-icon-equalizer-2"></span>
                    <span>Transaction Type List</span>
                </h4>
            </div>
            <div class="content noPad">
                <table class="responsive table table-bordered tablesorter">
                    <thead>
                        <tr>
                        <th>#</th>
                        <th title="Tên Loại">Name</th>
                        <th title="Phí">Cost (%)</th>
                        <th title="Số tiền tối thiểu">MinimumMoney</th>
                        <th title="Số tiền tối đa">MaximumMoney</th>
                        <th title="Ngày áp dụng">DayApply</th>
                        <th title="" style="width:50px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                         <%for (int i = 0; i < Model.Count(); i++)
                           {%>
                        <tr title="<%=Model[i].Description%>">
                            <td><%=i+1%></td>
                            <td><%=Html.DisplayFor(p=>p[i].TransactionTypeName)%></td>
                            <td><%=Html.DisplayFor(p=>p[i].Cost)%> (%)</td>
                            <td><%=Html.DisplayFor(p=>p[i].MinimumMoney)%></td>
                            <td><%=Html.DisplayFor(p=>p[i].MaximumMoney)%></td>
                            <td><%=Html.DisplayFor(p=>p[i].DayApply)%></td>
                            <td>
                                <div class="controls center">
                                    <a class="tip">
                                        <span>
                                            <img src="../../Content/Icon/pencil-8-24.ico" onclick="EditTransaction('<%=Model[i].TransactionTypeCode%>');" title="Edit" style="cursor:pointer;"/>
                                            <img src="../../Content/Icon/x-mark-4-24.ico" onclick="DeleteTransaction('<%=Model[i].TransactionTypeCode%>');" title="Delete" style="cursor:pointer;"/>
                                        </span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <%} %>
                    </tbody>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span6 -->
    <div id="pager" style="position: inherit;float:left;" >
        <img src="../../Content/Manager/img/first.png" class="first" />
        <img src="../../Content/Manager/img/prev.png" class="prev" />
        <input type="text" class="pagedisplay" />
        <img src="../../Content/Manager/img/next.png" class="next" />
        <img src="../../Content/Manager/img/last.png" class="last"/>
        <select class="pagesize" style="width: 150px;">
            <option selected="selected" value="5">5</option>
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="30">30</option>
            <option value="40">40</option>
        </select>
    </div>
</div>
<div id="NoteDialog" title="" class="Hidden"></div>
<script type="text/javascript">
    $(function () {
        $("#NoteDialog").dialog({
            autoOpen: false, width: 800, height: 600, modal: true,
            buttons: {
                Cancel: function () { $("#NoteDialog").dialog("close"); }
            }
        });
    });
</script>
<script type="text/javascript">
    function EditTransaction(_code) {
        $.ajax({
            url: '<%= Url.Action("EditTransaction_Get","BankManager") %>',
            type: "POST",
            data: { code: _code },
            success: function (_data) {
                if (_data.toString() != "false") {
                    $("#NoteDialog").html(_data);
                    $("#NoteDialog").dialog("option", "title", "Edit TransactionType");
                    $("#NoteDialog").dialog("open");
                } else {
                    $("#NoteDialog").html("Không thể thực hiện thao tác!");
                    $("#NoteDialog").dialog("option", "title", "Error");
                    $("#NoteDialog").dialog("open");
                }
            }
        });
    }
</script>
<script type="text/javascript">
    function DeleteTransaction(_code) {
        $("#NoteDialog").html("Không thể thực hiện thao tác!");
        $("#NoteDialog").dialog("option", "title", "Error");
        $("#NoteDialog").dialog("open");
    }
</script>