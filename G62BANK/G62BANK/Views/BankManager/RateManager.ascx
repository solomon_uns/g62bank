﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<div class="centerContent">         
    <ul class="bigBtnIcon">
        <li id= "atm">
            <a class="tipB" title="Lãi Suất Thẻ ATM">
                <span class="icon"><img src="../../Content/Icon/book-10-24.ico" style="margin-left:10px;margin-right:10px;"/></span>
                <span class="txt">ATM Rate</span>
            </a>
        </li>
        <li id= "saving">
            <a class="tipB" title="Lãi Suất Sổ Tiết Kiệm">
                <span class="icon"><img src="../../Content/Icon/book-10-24.ico" style="margin-left:10px;margin-right:10px;"/></span>
                <span class="txt">SavingBook Rate</span>
<%--                <span class="notification"><%=Html.DisplayFor(p=>p.NewCreditCard) %></span>--%>
            </a>
        </li>
    </ul>
</div>


<script type="text/javascript">
    $(function () {
        $("#atm").click(function () {
            //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
            $.ajax({
                url: '<%= Url.Action("RateATM_Get","BankManager") %>',
                type: "Post",
                success: function (data) {
                    $("#main-content").html(data);
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $("#saving").click(function () {
            //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
            $.ajax({
                url: '<%= Url.Action("RateSavingBookType_Get","BankManager") %>',
                type: "Post",
                success: function (data) {
                    $("#main-content").html(data);
                }
            });
        });
    });
</script>


