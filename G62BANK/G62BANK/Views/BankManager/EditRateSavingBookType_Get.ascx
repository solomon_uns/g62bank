﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.SavingBookType>" %>
<% Html.ValidationSummary(); %>
<% Html.EnableClientValidation();%>

<div class="row-fluid">
    <div class="span12">
        <div class="page-header">
            <h4>EditSavingBook Type </h4>
        </div>

        <%using (Html.BeginForm("EditRateSavingBookType_Post", "BankManager", FormMethod.Post, new { @class = "form-horizontal seperator", @id = "frm" }))
            {%>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span4" for="username" style="color:Red; text-decoration:none;"><%=Html.ValidationSummary() %></label>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="username">Mã Loại:</label>
                        <%=Html.TextBoxFor(p => p.SavingBookTypeCode, new { @class = "span4 text", @readonly = "reayonly" })%>
                    </div>
                    <div class="row-fluid" style="text-align:left;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.SavingBookTypeCode)%>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="username">Tên Loại:</label>
                        <%=Html.TextBoxFor(p => p.SavingBookTypeName, new { @class = "span4 text"})%>             
                    </div>
                    <div class="row-fluid" style="text-align:left;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.SavingBookTypeName)%>
                    </div>

                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="username">Thời Gian:</label>
                        <%=Html.TextBoxFor(p => p.SavingBookTime, new { @class = "span4 text"})%>            
                    </div>
                    <div class="row-fluid" style="text-align:left;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.Rate)%>
                    </div>

                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="username">Lãi Suất:</label>
                        <%=Html.TextBoxFor(p => p.Rate, new { @class = "span4 text"})%>            
                    </div>
                    <div class="row-fluid" style="text-align:left;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.Rate)%>
                    </div>

                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="username">Ngày áp dụng:</label>
                        <%=Html.TextBoxFor(p => p.DayApply, new { @class = "span4 text"})%>             
                    </div>
                    <div class="row-fluid" style="text-align:left;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.DayApply)%>
                    </div>

                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="username">Lãi Suất Trước hạn:</label>
                        <%=Html.TextBoxFor(p => p.RateBeforeTerm, new { @class = "span4 text"})%>             
                    </div>
                    <div class="row-fluid" style="text-align:left;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.RateBeforeTerm)%>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="username">Số tiền tối thiểu:</label>
                        <%=Html.TextBoxFor(p => p.MinimumMoney, new { @class = "span4 text"})%>             
                    </div>
                    <div class="row-fluid" style="text-align:left;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.MinimumMoney)%>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="username">Số tiền tối đa:</label>
                        <%=Html.TextBoxFor(p => p.MaximumMoney, new { @class = "span4 text"})%>             
                    </div>
                    <div class="row-fluid" style="text-align:left;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.MaximumMoney)%>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="username">PaymentType:</label>
                        <%=Html.TextBoxFor(p => p.PaymentType, new { @class = "span4 text"})%>             
                    </div>
                    <div class="row-fluid" style="text-align:left;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.PaymentType)%>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <%=Html.LabelFor(p => p.Other, new { @class = "form-label span3" })%>
                        <%=Html.TextAreaFor(p => p.Other, new { @class = "span4 text" })%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.Other)%>
                    </div>
                </div>
            </div>       
            <div class="form-row row-fluid">        
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                        <div class="span3"></div>
                        <div class="span4 controls">
                            <button type="button" class="btn btn-info marginR10" onclick="fnSave();">Save changes</button>
                        </div>
                        </div>
                    </div>
                </div>   
            </div>


        <%} %>
    </div><!-- End .span12 -->
</div>
<script type="text/javascript">
    function fnSave() {
        $.ajax({
            url: '<%= Url.Action("EditRateSavingBookType_Post","BankManager") %>',
            type: "POST",
            data: $("#frm").serialize(),
            success: function (data) {
                if (data.toString() == "true") {
                    alert("Thay đổi thành công!");
                    //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
                    $.ajax({
                        url: '<%= Url.Action("RateSavingBookType_Get","BankManager") %>',
                        type: "Post",
                        success: function (data) {
                            $("#main-content").html(data);
                        }
                    });
                } else {

                }
            }
        });
    }
</script>
