﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.TransactionType>" %>

<% Html.ValidationSummary(); %>
<% Html.EnableClientValidation();%>

<div class="row-fluid">
    <div class="span12">
        <div class="page-header">
            <h4>Edit TransactionType</h4>
        </div>

        <%using (Html.BeginForm("EditTransaction_Post", "BankManager", FormMethod.Post, new { @class = "form-horizontal seperator", @id = "frm" }))
            {%>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span4" for="username" style="color:Red; text-decoration:none;"><%=Html.ValidationSummary() %></label>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="username">Mã Loại:</label>
                        <%=Html.TextBoxFor(p => p.TransactionTypeCode, new { @class = "span4 text", @readonly = "reayonly" })%>
                        <%=Html.ValidationMessageFor(p=>p.TransactionTypeCode ) %>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="username">Tên Loại:</label>
                        <%=Html.TextBoxFor(p => p.TransactionTypeName, new { @class = "span4 text"})%>
                        <%=Html.ValidationMessageFor(p=>p.TransactionTypeName ) %>              
                    </div>
                    <div class="row-fluid" style="text-align:left;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.TransactionTypeName)%>
                    </div>

                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="username">Phí Dịch Vụ:</label>
                        <%=Html.TextBoxFor(p => p.Cost, new { @class = "span4 text"})%>
                        <%=Html.ValidationMessageFor(p=>p.Cost ) %>              
                    </div>
                    <div class="row-fluid" style="text-align:left;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.Cost)%>
                    </div>

                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <%=Html.LabelFor(p => p.Description, new { @class = "form-label span3" })%>
                        <%=Html.TextBoxFor(p => p.Description, new { @class = "span4 text" })%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.Description)%>
                    </div>
                </div>
            </div>
  
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <%=Html.LabelFor(p => p.MinimumMoney, new { @class = "form-label span3" })%>
                        <%=Html.TextBoxFor(p => p.MinimumMoney, new { @class = "span4 text" })%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.MinimumMoney)%>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <%=Html.LabelFor(p => p.MaximumMoney, new { @class = "form-label span3" })%>
                        <%=Html.TextBoxFor(p => p.MaximumMoney, new { @class = "span4 text" })%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.MaximumMoney)%>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <%=Html.LabelFor(p => p.DayApply, new { @class = "form-label span3" })%>
                        <%=Html.TextBoxFor(p => p.DayApply, new { @class = "span4 text" })%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.DayApply)%>
                    </div>
                </div>
            </div>  
                            
            <div class="form-row row-fluid">        
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                        <div class="span3"></div>
                        <div class="span4 controls">
                            <button type="button" class="btn btn-info marginR10" onclick="fnSave();">Save changes</button>
                        </div>
                        </div>
                    </div>
                </div>   
            </div>


        <%} %>
    </div><!-- End .span12 -->
</div>
<script type="text/javascript">
    function fnSave() {
        $.ajax({
            url: '<%= Url.Action("EditTransaction_Post","BankManager") %>',
            type: "POST",
            data: $("#frm").serialize(),
            success: function (data) {
                if (data.toString() == "true") {
                    alert("Đã thay đổi thông tin");
                    //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
                    $.ajax({

                        url: '<%= Url.Action("CostManager","BankManager") %>',
                        type: "GET",
                        success: function (data) {
                            $("#main-content").html(data);
                        }
                    });
                } else {
                    alert("Không thể thực hiện thao tác");
                }
            }
        });
    }
</script>