﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.SavingBook>" %>
<script type="text/javascript">
    function Autorun() {
        $.ajax({
            url: '<%= Url.Action("DetailsSavingBook","BankManager") %>',
            type: "POST",
            data: {code : <%=Model.SavingBookCode %>},
            success: function (_data) {
                $("#det").html(_data);
            }
        });
        $.ajax({
            url: '<%= Url.Action("GetCostGetMoney","BankManager") %>',
            type: "POST",
            success: function (_data) {
               var textbox1 = document.getElementById('cost');
                    textbox1.value = _data;
            }
        });
    }
    window.load = Autorun();
</script>
<div class="row-fluid">
    <div class="span12">
        <div class="page-header">
            <h4>Rút tiền tiết kiệm</h4>
        </div>

        <%using (Html.BeginForm("GetMoneySavingBook", "BankManager", FormMethod.Post, new { @class = "form-horizontal seperator", @id="frm"}))
            {%>
            <div class="form-row row-fluid">
                <div class="span12" id="det">
                </div>
            </div>
            <%-- Số tiền rút--%>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Số Tiền Rút:</label>
                            <input type="text" class="span4 text" id="getmoney" onchange="CheckMoney(this.value);"/>
                        </div>
                        <div class="row-fluid" style="text-align:center;color:Red;">
                            <label class="form-label span3" id="notegetmoney"></label>
                        </div>
                    </div>
                </div>
            </div>

            <%-- Phí dịch vụ--%>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Phí Dịch Vụ:</label>
                            <input type="text" class="span4 text" id="cost" readonly="readonly"/> (%)
                        </div>
                    </div>
                </div>
            </div>  

            <%-- Số tiền nhận--%>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Số Tiền Sẽ Nhận:</label>
                            <input type="text" class="span4 text" id="paymoney" readonly="readonly" />
                        </div>
                        <div class="row-fluid" style="text-align:center;color:Red;">
                            <label class="form-label span3" id="notepaymoney"></label>
                        </div>
                    </div>
                </div>
            </div>

            <%-- Số tiền còn lại--%>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Số Tiền Còn Lại:</label>
                            <input type="text" class="span4 text"id="reamimoney" readonly="readonly" />
                        </div>
                        <div class="row-fluid" style="text-align:center;color:Red;" id="notereamimoney">
                        </div>
                    </div>
                </div>
            </div>
            
            <%-- button--%> 
            <div class="form-row row-fluid">        
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                        <div class="span3"></div>
                        <div class="span7 controls">
                            <img src="../../Content/Icon/help-3-24.ico" title="Xem công thức tính lãi suất" onclick="ShowMessages();"/>
                            <button type="button" class="btn btn-info marginR10" onclick="fnSave();">Save changes</button>
                        </div>
                        </div>
                    </div>
                </div>   
            </div>


        <%} %>
    </div><!-- End .span12 -->
</div>
<div id="NoteDialog8" title="" class="Hidden"></div>
<div id="NoteDialog9" title="" class="Hidden"></div>
<script type="text/javascript">
    $(function () {
        $("#NoteDialog8").dialog({
            autoOpen: false, width: 800, height: 500, modal: true,
            buttons: {
                Cancel: function () { $("#NoteDialog8").dialog("close"); }
            }
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $("#NoteDialog9").dialog({
            autoOpen: false, width: 800, height: 500, modal: true,
            buttons: {
                Cancel: function () { $("#NoteDialog9").dialog("close"); }
            }
        });
    });
</script>
<script type="text/javascript">
    function ShowMessages() {
        var _code = <%=Model.SavingBookCode %>;
        $.ajax({
            url: '<%= Url.Action("GetStringGetMoneySavingBook","BankManager") %>',
            type: "POST",
            data: { code: _code, money: $("#getmoney").val() },
            success: function (_data) {
                if (_data.toString() == "false") {
                    $("#NoteDialog9").html("Không thể thực hiện thao tác").dialog("option", "title", "Cách Tính Lãi Suất");
                    $("#NoteDialog9").dialog("open");
                } else {
                    $.ajax({
                        url: '<%= Url.Action("Messages","BankManager") %>',
                        type: "POST",
                        data: _data,
                        success: function (data) {
                            $("#NoteDialog9").html(data).dialog("option", "title", "Cách Tính Lãi Suất");
                            $("#NoteDialog9").dialog("open");
                        }
                    });
                }
            }
        });
    }
        </script>

<script type="text/javascript">
    function CheckMoney(_moneyget) {
        var _code = <%=Model.SavingBookCode %>;
        var Money = <%=Model.Money %>;

        var cost = parseFloat($("#cost").val());
        $.ajax({
            url: '<%= Url.Action("CheckGetMoneySavingBook","BankManager") %>',
            type: "POST",
            data: {code: _code,  money: _moneyget },
            success: function (data) {
                if (data != "false") {
                    var MoneyGet = parseFloat(data);
                    var cost2 = (parseFloat(_moneyget) * cost)/100;
                    
                // payment
                    var textbox2 = document.getElementById('paymoney');
                    textbox2.value = MoneyGet;

                // số tiền còn lại
                     var textbox3 = document.getElementById('reamimoney');
                    textbox3.value = Money - parseFloat(_moneyget) - cost2; 
                }else{
                    $("#notegetmoney").html("Có lỗi! Có thể Số tiền rút không hợp lệ!");
                }
            }
        });
    }
</script>
<script type="text/javascript">
    function fnSave() {
        var savcode = <%=Model.SavingBookCode %>; 
        var mo = $("#getmoney").val();
        $.ajax({
            url: '<%= Url.Action("GetMoneySavingBook_Submit","BankManager") %>',
            type: "POST",
            data: {code : savcode, money : mo},
            success: function (_data) {
                if (_data.toString() == "true") {
                    alert("đã rút tiền thành công");
                } else {
                    $("#notegetmoney").html("Số tiền không hợp lệ");
                }
            }
        });
    }
</script>


