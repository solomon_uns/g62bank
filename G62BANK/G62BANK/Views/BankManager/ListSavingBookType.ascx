﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<G62BANK.Models.SavingBookType>>" %>

<%=Html.DropDownList("SavingBookTypeCode", new SelectList(Model.Select(x => new { Value = x.SavingBookTypeCode, Text = x.SavingBookTypeName }), "Value", "Text"), new { @class = "span4" })%>

