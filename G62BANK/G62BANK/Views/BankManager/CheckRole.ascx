﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<int>" %>
<%if (Model == 2)
  {%>
    <div class="sidenav">
        <div class="sidebar-widget" style="margin: -1px 0 0 0;">
            <h5 class="title" style="margin-bottom:0">Staff Link</h5>
        </div><!-- End .sidenav-widget -->

        <div class="mainnav">
            <ul>
                <li id="AccountManager" title="Quản Lý Tài Khoản Thẻ"><a><span><img src="../../Content/Icon/inbox-6-24.ico" style="margin-left:10px;margin-right:10px;"/></span>Accounts</a></li>
                <li id="SavingBookManager" title="Quản Lý Tài Khoản Sổ Tiết Kiệm"><a><span><img src="../../Content/Icon/book-10-24.ico" style="margin-left:10px;margin-right:10px;"/></span>SavingBooks</a></li>
                <li id="CustomerManager" title="Quản Lý Tài Khoản Khách Hàng"><a><span><img src="../../Content/Icon/user-14-24.ico" style="margin-left:10px;margin-right:10px;"/></span>Customers</a></li>
                <li title="Quản Lý Ngoại tệ"><a><span><img src="../../Content/Icon/bar-chart-4-24.ico" style="margin-left:10px;margin-right:10px;"/></span>Foreign Currency</a></li>
                <li title="Quy Định"><a><span><img src="../../Content/Icon/list-view-24.ico" style="margin-left:10px;margin-right:10px;"/></span>Regulations</a></li>
                <li title="Liên Ngân Hàng"><a><span><img src="../../Content/Icon/link-3-24.ico" style="margin-left:10px;margin-right:10px;"/></span>Interbank</a></li>
                <li title="Quản Lý Diễn Đàn"><a><span><img src="../../Content/Icon/home-2-24.ico" style="margin-left:10px;margin-right:10px;"/></span>Forum</a></li>
            </ul>
        </div>
    </div><!-- End sidenav -->
<%}
  else {%>
    <div class="sidenav">
        <div class="sidebar-widget" style="margin: -1px 0 0 0;">
            <h5 class="title" style="margin-bottom:0">Staff Link</h5>
        </div><!-- End .sidenav-widget -->

        <div class="mainnav">
            <ul>
                <li id="AccountManager" title="Quản Lý Tài Khoản Thẻ" style="cursor:pointer;"><a><span><img src="../../Content/Icon/inbox-6-24.ico" style="margin-left:10px;margin-right:10px;"/></span>Accounts</a></li>
                <li id="SavingBookManager" title="Quản Lý Tài Khoản Sổ Tiết Kiệm" style="cursor:pointer;"><a><span><img src="../../Content/Icon/book-10-24.ico" style="margin-left:10px;margin-right:10px;"/></span>SavingBooks</a></li>
                <li id="CustomerManager" title="Quản Lý Tài Khoản Khách Hàng" style="cursor:pointer;"><a><span><img src="../../Content/Icon/user-14-24.ico" style="margin-left:10px;margin-right:10px;"/></span>Customers</a></li>
                <li title="Quản Lý Ngoại tệ" style="cursor:pointer;"><a><span><img src="../../Content/Icon/bar-chart-4-24.ico" style="margin-left:10px;margin-right:10px;"/></span>Foreign Currency</a></li>
                <li title="Quy Định" style="cursor:pointer;"><a><span><img src="../../Content/Icon/list-view-24.ico" style="margin-left:10px;margin-right:10px;"/></span>Regulations</a></li>
                <li title="Liên Ngân Hàng" style="cursor:pointer;"><a><span><img src="../../Content/Icon/link-3-24.ico" style="margin-left:10px;margin-right:10px;"/></span>Interbank</a></li>
                <li title="Quản Lý Diễn Đàn" style="cursor:pointer;"><a><span><img src="../../Content/Icon/home-2-24.ico" style="margin-left:10px;margin-right:10px;"/></span>Forum</a></li>
            </ul>
        </div>
    </div><!-- End sidenav -->
      <div class="sidebar-widget">
                <h5 class="title">Admin Link</h5>
                <div class="mainnav">
                    <ul>
                        <li id="StaffManagerment" title="Quản Lý Nhân Viên" style="cursor:pointer;"><a><span><img src="../../Content/Icon/user-14-24.ico" style="margin-left:10px;margin-right:10px;"/></span>Staff Managerment</a></li>
                        <li id="RateManager" title="Lãi Suất" style="cursor:pointer;"><a><span><img src="../../Content/Icon/chess-33-24.ico" style="margin-left:10px;margin-right:10px;"/></span>Rate</a></li>
                        <li id="CostManager" title="Phí Dịch Vụ" style="cursor:pointer;"><a><span><img src="../../Content/Icon/money-8-24.ico" style="margin-left:10px;margin-right:10px;"/></span>Cost</a></li>
                    </ul>
                </div>
        </div><!-- End .sidenav-widget -->
  <%}%>
  


