﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<G62BANK.Models.SavingBook>>" %>
<script src="../../Scripts/jquery.tablesorter.js" type="text/javascript"></script>
<script src="../../Scripts/jquery.tablesorter.pager.js" type="text/javascript"></script>


<script type="text/javascript">
    $(function () {
        $("table.tablesorter").tablesorter({ widthFixed: true, sortList: [[0, 0]] })
        .tablesorterPager({ container: $("#pager"), size: $(".pagesize option:selected").val() });
    });
</script>
<div class="row-fluid">
    <div class="span12">                    
        <div class="box">
            <div class="title">
                <h4>
                    <span class="icon16 icomoon-icon-equalizer-2"></span>
                    <span>Kết Quả</span>
                </h4>
            </div>
            <div class="content noPad">
                <table class="responsive table table-bordered tablesorter">
                    <thead>
                        <tr>
                        <th>#</th>
                        <th title="Mã sổ">SavingBookCode</th>
                        <%--<th title="Mã TK">AccountCode</th>--%>
                        <th title="Họ Tên">CustomerName</th>
                        <th title="Loại Tiết Kiệm">Type</th>
                        <th title="Số Tiền TK">Money</th>
                        <th title="Ngày Mở">DayCreated</th>
                        <%--<th title="Ngày Đáo Hạn">DayExpired</th>--%>
                        <th title="">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                         <%for (int i = 0; i < Model.Count(); i++)
                           {%>
                        <tr>
                            <td><%=i+1%></td>
                            <td onclick="Details('<%=Model[i].SavingBookCode%>');" style="cursor:pointer;"><%=Html.DisplayFor(p=>p[i].SavingBookCode)%></td>
                            <%--<td><%=Html.DisplayFor(p=>p[i].AccountCode)%></td>--%>
                            <td><%=Html.DisplayFor(p=>p[i].CustomerName)%></td>
                            <td><%=Html.DisplayFor(p=>p[i].SavingBookTypeCode)%></td>
                            <td><%=Html.DisplayFor(p=>p[i].Money)%></td>
                            <td><%=Html.DisplayFor(p=>p[i].DayCreated)%></td>
                            <%--<td><%=Html.DisplayFor(p=>p[i].DayExpired)%></td>--%>
                            <td>
                                <div class="controls center">
                                    <a class="tip">
                                        <span>
                                            <%if (Model[i].DayExpired.Value.Date.Subtract(DateTime.Now.Date).Days > 0 || Model[i].SavingBookTypeCode == 1)
                                            {%>
                                                <img src="../../Content/Icon/download-13-24.ico" onclick="GetMoney('<%=Model[i].SavingBookCode%>');" title="Rút tiền trước hạn" style="cursor:pointer;"/>
                                            <%} %>
                                        </span>
                                    </a>
                                    <a class="tip">
                                        <span>
                                            <%if (Model[i].DayExpired.Value.Date.Subtract(DateTime.Now.Date).Days == 0 || Model[i].SavingBookTypeCode == 1)
                                            {%>
                                                <img src="../../Content/Icon/xing-5-24.ico" onclick="Maturity('<%=Model[i].SavingBookCode%>');" title="Đáo hạn" style="cursor:pointer;" />
                                            <%} %>
                                        </span>
                                    </a>
                                    <a class="tip">
                                        <span>
                                            <%if (Model[i].DayExpired.Value.Date.Subtract(DateTime.Now.Date).Days < 0)
                                            {%>
                                                <img src="../../Content/Icon/alarm-clock-9-24.ico" onclick="TimeOut('<%=Model[i].SavingBookCode%>');" title="Quá hạn" style="cursor:pointer;"/>
                                            <%} %>
                                        </span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <%} %>
                    </tbody>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span6 -->
    <div id="pager" style="position: inherit;float:left;" >
        <img src="../../Content/Manager/img/first.png" class="first" />
        <img src="../../Content/Manager/img/prev.png" class="prev" />
        <input type="text" class="pagedisplay" />
        <img src="../../Content/Manager/img/next.png" class="next" />
        <img src="../../Content/Manager/img/last.png" class="last"/>
        <select class="pagesize" style="width: 150px;">
            <option selected="selected" value="5">5</option>
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="30">30</option>
            <option value="40">40</option>
        </select>
    </div>
</div>
<div id="NoteDialog25" title="" class="Hidden"></div>
<script type="text/javascript">
    $(function () {
        $("#NoteDialog25").dialog({
            autoOpen: false, width: 800, height: 600, modal: true,
            buttons: {
                Cancel: function () { $("#NoteDialog25").dialog("close"); }
            }
        });
    });
</script>
<script type="text/javascript">
    function Details(_code) {
        $.ajax({
            url: '<%= Url.Action("DetailsSavingBook","BankManager") %>',
            type: "POST",
            data: { code: _code },
            success: function (_data) {
                if (_data.toString() != "false") {
                    $("#NoteDialog25").html(_data);
                    $("#NoteDialog25").dialog("option", "title", "Details SavingBook");
                    $("#NoteDialog25").dialog("open");
                } else {
                    $("#NoteDialog25").html("Không thể thực hiện thao tác!");
                    $("#NoteDialog25").dialog("option", "title", "Error");
                    $("#NoteDialog25").dialog("open");
                }
            }
        });
    }
</script>
<script type="text/javascript">
    function GetMoney(_code) {
        $.ajax({
            url: '<%= Url.Action("GetMoneySavingBook","BankManager") %>',
            type: "POST",
            data: { code: _code },
            success: function (_data) {
                if (_data.toString() != "false") {
                    $("#NoteDialog25").html(_data);
                    $("#NoteDialog25").dialog("option", "title", "GetMoney SavingBook");
                    $("#NoteDialog25").dialog("open");
                } else {
                    $("#NoteDialog25").html("Không thể thực hiện thao tác!");
                    $("#NoteDialog25").dialog("option", "title", "Error");
                    $("#NoteDialog25").dialog("open");
                }
            }
        });
    }
</script>
<script type="text/javascript">
    function Maturity(_code) {
        $.ajax({
            url: '<%= Url.Action("MaturitySavingBook","BankManager") %>',
            type: "POST",
            data: { code: _code },
            success: function (_data) {
                if (_data.toString() != "false") {
                    $("#NoteDialog25").html(_data);
                    $("#NoteDialog25").dialog("option", "title", "Maturity SavingBook");
                    $("#NoteDialog25").dialog("open");
                } else {
                    $("#NoteDialog25").html("Không thể thực hiện thao tác!");
                    $("#NoteDialog25").dialog("option", "title", "Error");
                    $("#NoteDialog25").dialog("open");
                }
            }
        });
    }
</script>
<script type="text/javascript">
    function TimeOut(_code) {
        $.ajax({
            url: '<%= Url.Action("TimeOutSavingBook","BankManager") %>',
            type: "POST",
            data: { code: _code },
            success: function (_data) {
                if (_data.toString() != "false") {
                    $("#NoteDialog25").html(_data);
                    $("#NoteDialog25").dialog("option", "title", "TimeOut SavingBook");
                    $("#NoteDialog25").dialog("open");
                } else {
                    $("#NoteDialog25").html("Không thể thực hiện thao tác!");
                    $("#NoteDialog25").dialog("option", "title", "Error");
                    $("#NoteDialog25").dialog("open");
                }
            }
        });
    }
</script>