﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<G62BANK.Models.CreditCardLoan>>" %>
<script src="../../Scripts/jquery.tablesorter.js" type="text/javascript"></script>
<script src="../../Scripts/jquery.tablesorter.pager.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function () {
        $("table.tablesorter").tablesorter({ widthFixed: true, sortList: [[0, 0]] })
        .tablesorterPager({ container: $("#pager"), size: $(".pagesize option:selected").val() });
    });
</script>
<div class="row-fluid">
    <div class="span12">                    
        <div class="box">
            <div class="title">
                <h4>
                    <span class="icon16 icomoon-icon-equalizer-2"></span>
                    <span>Kết Quả</span>
                </h4>
            </div>
            <div class="content noPad">
                <table class="responsive table table-bordered tablesorter">
                    <thead>
                        <tr>
                        <th>#</th>
                        <th title="Mã Vay">LoanCode</th>
                        <th title="Ngày">Day</th>
                        <th title="Số tiền">Money</th>
                        <th title="Số ngày không tính lãi">NumberDayLimit</th>
                        <th title="Số ngày còn lại không tính lãi">NumberDayRemaining</th>
                        <th title="Số tiền đã trả">Pay</th>
                        <th title="Số tiền còn lại">PayRemaining</th>
                        <th title="Ngày hết hạn không tính lại">DayLimit</th>
                        <th title="Lãi suất áp dụng nếu quá hạn tính lãi">Rate</th>
                        <th title="">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                         <%for (int i = 0; i < Model.Count(); i++)
                           {%>
                        <tr>
                            <td><%=i+1%></td>
                            <td><%=Html.DisplayFor(p=>p[i].LoanCode)%></td>
                            <td><%=Html.DisplayFor(p=>p[i].Day)%></td>
                            <td><%=Html.DisplayFor(p=>p[i].Money)%></td>
                            <td><%=Html.DisplayFor(p=>p[i].NumberDayLimt)%></td>
                            <td><%=Html.DisplayFor(p=>p[i].NumberDayRemaining)%></td>
                            <td><%=Html.DisplayFor(p=>p[i].Pay)%></td>
                            <td><%=Html.DisplayFor(p=>p[i].PayRemaining)%></td>
                            <td><%=Html.DisplayFor(p=>p[i].DayLimit)%></td>
                            <td><%=Html.DisplayFor(p=>p[i].Rate)%></td>
                            <td>
                                <div class="controls center">
                                    <a class="tip">
                                        <span>
                                            <img src="../../Content/Icon/money-8-24.ico" onclick="ReturnRate('<%=Model[i].LoanCode%>');" title="Trả lãi" style="cursor:pointer;"/>
                                        </span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <%} %>
                    </tbody>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span6 -->
    <div id="pager" style="position: inherit;float:left;" >
        <img src="../../Content/Manager/img/first.png" class="first" />
        <img src="../../Content/Manager/img/prev.png" class="prev" />
        <input type="text" class="pagedisplay" />
        <img src="../../Content/Manager/img/next.png" class="next" />
        <img src="../../Content/Manager/img/last.png" class="last"/>
        <select class="pagesize" style="width: 150px;">
            <option selected="selected" value="5">5</option>
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="30">30</option>
            <option value="40">40</option>
        </select>
    </div>
</div>
<div id="NoteDialog24" title="" class="Hidden"></div>
<script type="text/javascript">
    $(function () {
        $("#NoteDialog24").dialog({
            autoOpen: false, width: 800, height: 600, modal: true,
            buttons: {
                Cancel: function () { $("#NoteDialog24").dialog("close"); }
            }
        });
    });
</script>

<script type="text/javascript">
    function ReturnRate(_code) {
        $.ajax({
            url: '<%= Url.Action("ReturnRateCreditCard_Post","BankManager") %>',
            type: "POST",
            data: { code: _code },
            success: function (_data) {
                if (_data.toString() != "false") {
                    $("#NoteDialog24").html(_data);
                    $("#NoteDialog24").dialog("option", "title", "ReturnRate CreditCard");
                    $("#NoteDialog24").dialog("open");
                } else {
                    $("#NoteDialog24").html("Không thể thực hiện thao tác!");
                    $("#NoteDialog24").dialog("option", "title", "Error");
                    $("#NoteDialog24").dialog("open");
                }
            }
        });
    }
</script>
