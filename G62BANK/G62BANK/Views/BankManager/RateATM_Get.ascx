﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<G62BANK.Models.ATMAccountType>>" %>
<script src="../../Scripts/jquery.tablesorter.js" type="text/javascript"></script>
<script src="../../Scripts/jquery.tablesorter.pager.js" type="text/javascript"></script>


<script type="text/javascript">
    $(function () {
        $("table.tablesorter").tablesorter({ widthFixed: true, sortList: [[0, 0]] })
        .tablesorterPager({ container: $("#pager"), size: $(".pagesize option:selected").val() });
    });
</script>
<div class="row-fluid">
    <div class="span12">                    
        <div class="box">
            <div class="title">
                <h4>
                    <span class="icon16 icomoon-icon-equalizer-2"></span>
                    <span>ATM Account Type List</span>
                </h4>
            </div>
            <div class="content noPad">
                <table class="responsive table table-bordered tablesorter">
                    <thead>
                        <tr>
                        <th>#</th>
                        <th title="Mã Loại">Code</th>
                        <th title="Tên Loại">Name</th>
                        <th title="Lãi Suất">Rate (%)</th>
                        <th title="Số tiền nhớ nhất">MinimumMoney</th>
                        <th title="Số tiền lớn nhất">MaximumMoney</th>
                        <th title="" style="width:50px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                         <%for (int i = 0; i < Model.Count(); i++)
                           {%>
                        <tr title="<%=Model[i].Description%>">
                            <td><%=i+1%></td>
                            <td><%=Html.DisplayFor(p=>p[i].ATMAccountTypeCode)%></td>
                            <td><%=Html.DisplayFor(p=>p[i].ATMAccountTypeName)%></td>
                            <td><%=Html.DisplayFor(p=>p[i].Rate)%> (%)</td>
                            <td><%=Html.DisplayFor(p=>p[i].NumberDayExpired)%></td>
                            <td><%=Html.DisplayFor(p=>p[i].Description)%></td>
                            <td>
                                <div class="controls center">
                                    <a class="tip">
                                        <span>
                                            <img src="../../Content/Icon/pencil-8-24.ico" onclick="EditRate('<%=Model[i].ATMAccountTypeCode%>');" title="Edit" style="cursor:pointer;"/>
                                            <img src="../../Content/Icon/x-mark-4-24.ico" onclick="DeleteRate('<%=Model[i].ATMAccountTypeCode%>');" title="Delete" style="cursor:pointer;"/>
                                        </span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <%} %>
                    </tbody>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span6 -->
    <div id="pager" style="position: inherit;float:left;" >
        <img src="../../Content/Manager/img/first.png" class="first" />
        <img src="../../Content/Manager/img/prev.png" class="prev" />
        <input type="text" class="pagedisplay" />
        <img src="../../Content/Manager/img/next.png" class="next" />
        <img src="../../Content/Manager/img/last.png" class="last"/>
        <select class="pagesize" style="width: 150px;">
            <option selected="selected" value="5">5</option>
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="30">30</option>
            <option value="40">40</option>
        </select>
    </div>
</div>
<div id="NoteDialog19" title="" class="Hidden"></div>
<script type="text/javascript">
    $(function () {
        $("#NoteDialog19").dialog({
            autoOpen: false, width: 800, height: 600, modal: true,
            buttons: {
                Cancel: function () { $("#NoteDialog19").dialog("close"); }
            }
        });
    });
</script>
<script type="text/javascript">
    function EditRate(_code) {
        $.ajax({
            url: '<%= Url.Action("EditRateATM_Get","BankManager") %>',
            type: "POST",
            data: { code: _code },
            success: function (_data) {
                if (_data.toString() != "false") {
                    $("#NoteDialog19").html(_data);
                    $("#NoteDialog19").dialog("option", "title", "Edit Rate ATM");
                    $("#NoteDialog19").dialog("open");
                } else {
                    $("#NoteDialog19").html("Không thể thực hiện thao tác!");
                    $("#NoteDialog19").dialog("option", "title", "Error");
                    $("#NoteDialog19").dialog("open");
                }
            }
        });
    }
</script>
<script type="text/javascript">
    function DeleteRate(_code) {
        $("#NoteDialog19").html("Không thể thực hiện thao tác!");
        $("#NoteDialog19").dialog("option", "title", "Error");
        $("#NoteDialog19").dialog("open");
    }
</script>
