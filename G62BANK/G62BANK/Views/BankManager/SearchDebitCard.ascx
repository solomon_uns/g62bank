﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.SearchDebitCard>" %>

<div class="contentwrapper"><!--Content wrapper-->
<%using (Html.BeginForm("SearchDebitCard", "BankManager", FormMethod.Post, new { @class = "form-horizontal seperator", @id = "frm" }))
  {%>
    <div class="row-fluid">                      
        <div class="span12">
            <div class="box">
                <div class="title">
                    <h4>
                        <span class="icon16 icomoon-icon-equalizer-2"></span>
                        <span title="Mã Tài Khoản">AccountCode</span>
                    </h4>
                </div>
                <div class="content">
                    <p><%=Html.TextBoxFor(p => p.AccountCode)%></p>
                </div>
            </div><!-- End .box -->
        </div><!-- End .span12 -->
    </div><!-- End .row-fluid -->
    <div class="row-fluid">                      
        <div class="span12">
            <div class="box">
                <div class="title">
                    <h4>
                        <span class="icon16 icomoon-icon-equalizer-2"></span>
                        <span title="Mã thẻ">DebitCardCode</span>
                    </h4>
                </div>
                <div class="content">
                    <p><%=Html.TextBoxFor(p => p.DebitCardCode)%></p>
                </div>
            </div><!-- End .box -->
        </div><!-- End .span12 -->
    </div><!-- End .row-fluid -->
    <div class="row-fluid">
        <div class="span4">
            <div class="box">
                <div class="title">
                    <h4>
                        <span class="icon16 icomoon-icon-equalizer-2"></span>
                        <span>CustomerID</span>
                    </h4>
                </div>
                <div class="content">
                    <p><%=Html.TextBoxFor(p => p.CustomerID)%></p>
                </div>
            </div><!-- End .box -->
        </div><!-- End .span4 -->

        <div class="span4">
            <div class="box">
                <div class="title">
                    <h4>
                        <span class="icon16 icomoon-icon-equalizer-2"></span>
                        <span>CustomerName</span>
                    </h4>
                </div>
                <div class="content">
                    <p><%=Html.TextBoxFor(p => p.CustomerName)%></p>
                </div>
            </div><!-- End .box -->
        </div><!-- End .span4 -->

        <div class="span4">
            <div class="box">
                <div class="title">
                    <h4>
                        <span class="icon16 icomoon-icon-equalizer-2"></span>
                        <span>Customer Address</span>
                    </h4>
                </div>
                <div class="content">
                    <p><%=Html.TextBoxFor(p => p.CustomerAddress)%></p>
                </div>
            </div><!-- End .box -->
        </div><!-- End .span4 -->
    </div><!-- End .row-fluid -->
    <div class="row-fluid">                      
        <div class="span12">
            <div class="box">
                <div class="title">
                    <h4>
                        <span class="icon16 icomoon-icon-equalizer-2"></span>
                        <span>DebitCard Search</span>
                    </h4>
                </div>
                <div class="content">
                    <p><button type="button" class="btn btn-info marginR10" onclick="Search();">Search <img src="../../Content/Icon/magnifier-4-24.ico"/></button></p>
                </div>
            </div><!-- End .box -->
        </div><!-- End .span12 -->
    </div><!-- End .row-fluid -->
    <%} %>
    <div id="searchresult">                      
    </div><!-- End .row-fluid -->   
<!-- Page end here -->
</div>
<script type="text/javascript">
    function Search() {
        $.ajax({
            url: '<%= Url.Action("SearchDebitCardResult","BankManager") %>',
            type: "Get",
            data: $("#frm").serialize(),
            success: function (data) {
                $("#searchresult").html(data);
            }
        });
    }
</script>
