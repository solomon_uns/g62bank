﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<G62BANK.Models.Account>>" %>

<script src="../../Scripts/jquery.tablesorter.js" type="text/javascript"></script>
<script src="../../Scripts/jquery.tablesorter.pager.js" type="text/javascript"></script>


<script type="text/javascript">
    $('#checkboxhead').click(function () {
        if (this.checked == false) {
            $('.checkboxcell:checked').attr('checked', false);
        }
        else {
            $('.checkboxcell:not(:checked)').attr('checked', true);
        }
    });
</script>
<script type="text/javascript">
    function GetSelected() {
        var str = "";
        $("#table_debit_regis tbody tr td input:checkbox").each(function () {
            if (this.checked == true) {
                str += "-" + this.name;
            }
        });
        return str;
    }
</script>
<div>
    <table class="tablesorter" id="table_debit_regis">
        <thead>
            <tr>
                <th><input type="checkbox" id= "checkboxhead"/></th>
                <th>AccountCode</th>
                <th>FullName</th>
                <th>CustomerID</th>
                <th>AccountTypeCode</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <%for(int i = 0 ; i < Model.Count(); i++) {%>      
                    <tr>
                        <td><input type="checkbox" class="checkboxcell" name="<%=Html.DisplayFor(p=>p[i].AccountCode)%>"/></td>
                        <td><%=Html.DisplayFor(p=>p[i].AccountCode)%></td>
                        <td onclick="CustomerReview('<%=Html.DisplayFor(p=>p[i].CustomerID)%>');"><%=Html.DisplayFor(p=>p[i].FullName)%></td>
                        <td onclick="CustomerReview('<%=Html.DisplayFor(p=>p[i].CustomerID)%>');"><%=Html.DisplayFor(p=>p[i].CustomerID)%></td>
                        <td><%=Html.DisplayFor(p=>p[i].AccountTypeCode)%></td>
                        <td><%=Html.DisplayFor(p=>p[i].Status)%></td>
                    </tr>
                   <% }
                %>
        </tbody>
    </table>
    <p></p>
    <div id="pager" style="position: inherit;">
        <img src="../../Content/Manager/img/first.png" class="first" />
        <img src="../../Content/Manager/img/prev.png" class="prev" />
        <input type="text" class="pagedisplay" />
        <img src="../../Content/Manager/img/next.png" class="next" />
        <img src="../../Content/Manager/img/last.png" class="last"/>
        <select class="pagesize" style="width: 150px;">
            <option selected="selected" value="5">5</option>
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="30">30</option>
            <option value="40">40</option>
        </select>
    </div>
    <div style="float:right; margin-right:5px;">
        <button type="button" class="btn btn-danger marginR10" onclick="DeleteSelected();">Delete</button>
    </div>
    <div style="float:right; margin-right:5px;">
        <button type="button" class="btn btn-info marginR10" onclick="AcceptSelected();">Accept</button>
    </div>
    <div id="add-edit-user"></div>  
</div>
<div id="NoteDialog5" title="" class="Hidden"></div>
<div id="NoteDialog6" title="" class="Hidden"></div>
<div id="NoteDialog7" title="" class="Hidden"></div>
<script type="text/javascript">
    $(function () {
        $("#NoteDialog5").dialog({
            autoOpen: false, width: 400, height: 250, modal: true,
            buttons: {
                "Save": function () {
                    // goi thực hiện
                    $se = "Accept" + GetSelected();
                    $.ajax({
                        url: '<%= Url.Action("CheckDebitCardRegister","BankManager") %>',
                        type: "POST",
                        data: { data: $se },
                        success: function (data) {
                            if (data.toString() == "true") {
                                $("#NoteDialog5").dialog("close");
                                $.ajax({
                                    url: '<%= Url.Action("CheckDebitCardRegister","BankManager") %>',
                                    type: "GET",
                                    success: function (data) {
                                        $("#main-content").html(data);
                                    }
                                });
                            }
                            else
                            { alert('Error !'); }
                        }
                    });
                },
                Cancel: function () { $(this).dialog("close"); }
            }
        });
    });
</script>
<script type="text/javascript">
    function AcceptSelected() {
        $("#NoteDialog5").html("Chấp nhận kích hoạt các tài khoản thẻ DebitCard này?").dialog("option", "title", "Accept Selected");
        $("#NoteDialog5").dialog("open");
    }
        </script>

<script type="text/javascript">
    $(function () {
        $("#NoteDialog6").dialog({
            autoOpen: false, width: 400, height: 250, modal: true,
            buttons: {
                "Save": function () {
                    // goi thực hiện
                    $se2 = "Delete" + GetSelected();
                    $.ajax({
                        url: '<%= Url.Action("CheckDebitCardRegister","BankManager") %>',
                        type: "POST",
                        data: { data: $se2 },
                        success: function (data) {
                            if (data.toString() == "true") {
                                $("#NoteDialog6").dialog("close");
                                $.ajax({
                                    url: '<%= Url.Action("CheckDebitCardRegister","BankManager") %>',
                                    type: "GET",
                                    success: function (data) {
                                        $("#main-content").html(data);
                                    }
                                });
                            }
                            else
                            { alert('Error !'); }
                        }
                    });
                },
                Cancel: function () { $(this).dialog("close"); }
            }
        });
    });
        </script>
<script type="text/javascript">
    function DeleteSelected() {
        $("#NoteDialog6").html("Hủy các tài khoản thẻ DebitCard này?").dialog("option", "title", "Delete Selected")
        $("#NoteDialog6").dialog("open");
    }
        </script>

<script type="text/javascript">
    $(function () {
        $("#NoteDialog7").dialog({
            autoOpen: false, width: 800, height: 600, modal: true,
            buttons: {
                "Save": function () {
                    // goi thực hiện
                    $se2 = "Delete" + GetSelected();
                    $.ajax({
                        url: '<%= Url.Action("CheckDebitCardRegister","BankManager") %>',
                        type: "POST",
                        data: { data: $se2 },
                        success: function (data) {
                            if (data.toString() == "true") {
                                $("#NoteDialog7").dialog("close");
                                $.ajax({
                                    url: '<%= Url.Action("CheckDebitCardRegister","BankManager") %>',
                                    type: "GET",
                                    success: function (data) {
                                        $("#main-content").html(data);
                                    }
                                });
                            }
                            else
                            { alert('Error !'); }
                        }
                    });
                },
                Cancel: function () { $(this).dialog("close"); }
            }
        });
    });
        </script>
<script type="text/javascript">
    function CustomerReview(id) {
        $("#NoteDialog7").html("")
        .dialog("option", "title", "CustomerReview")
        .load("/BankManager/CustomerReview?CustomerID=" + id, function () { $("#NoteDialog7").dialog("open"); });
    }
    </script>
<style type="text/css">
        body {
    font-size: 75%;
    font-family: Verdana, Tahoma, Arial, "Helvetica Neue", Helvetica, Sans-Serif;
    color: #232323;
    background-color: #fff;
}
table {width:100%; border-spacing:0; border:1px solid gray;}
table.tablesorter thead tr .header {
	background-image: url(images/bg.png);
	background-repeat: no-repeat;
	background-position: center right;
	cursor: pointer;
}
table.tablesorter tbody td {
	color: #3D3D3D;
	padding: 4px;
	background-color: #FFF;
	vertical-align: middle;
}
table.tablesorter tbody tr.odd td {
	background-color:#F0F0F6;
}
table.tablesorter thead tr .headerSortUp {
	background-image: url(images/asc.png);
}
table.tablesorter thead tr .headerSortDown {
	background-image: url(images/desc.png);
}
table th { width:150px; 
           border:1px outset gray; 
           background-color:#3C78B5; 
           color:White; 
           cursor:pointer;
}
table thead th:hover { background-color:Yellow; color:Black;}
table td {border:1px solid gray; text-align:center;}
#pager{float:left;}
    </style>