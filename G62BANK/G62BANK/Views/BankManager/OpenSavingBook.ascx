﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.CreateSavingBook>" %>
<div class="row-fluid">
    <div class="span12">
        <div class="page-header">
            <h4>Mở Sổ Tiết Kiệm</h4>
        </div>

        <%using (Html.BeginForm("OpenSavingBook", "BankManager", FormMethod.Post, new { @class = "form-horizontal seperator", @id= "frm", enctype = "multipart/form-data" }))
            {%>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span4" for="username" style="color:Red; text-decoration:none;" id="mes"><%=Html.ValidationSummary() %></label>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3">CustomerID:</label>
                        <input class="span4 text valid" id="CustomerID" name="CustomerID" style="width: 27.5%;" type="text" value="" onchange="CheckCustomer(this.value);">
                        <img src="../../Content/Icon/user-12-24.ico" style="margin-left:10px;margin-right:10px;" title="Tạo Customer" onclick="CreateCustomer();"/>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3">FullName:</label>
                        <input class="span4 text valid" id="FullName" name="FullName" type="text" value="" readonly="readonly">
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3">SavingBook Type:</label>
                        <%Html.RenderAction("ListSavingBookType", "BankManager");%>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3">Money:</label>
                        <%=Html.TextBoxFor(p => p.Savingbook.Money, new { @class = "span4 text"})%>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">        
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                        <div class="span3"></div>
                        <div class="span4 controls">
                            <button type="button" class="btn btn-info marginR10" onclick="fnSave();">Save changes</button>
                            <button type="button" class="btn btn-danger">Cancel</button>
                        </div>
                        </div>
                    </div>
                </div>   
            </div>
        <%} %>
    </div>
</div>
<div id="NoteDialog15" title="" class="Hidden"></div>
<script type="text/javascript">
    $(function () {
        $("#NoteDialog15").dialog({
            autoOpen: false, width: 800, height: 600, modal: true,
            buttons: {
                Cancel: function () { $("#NoteDialog15").dialog("close"); }
            }
        });
    });
</script>

<script type="text/javascript">
    function CheckCustomer(_code) {
        $.ajax({
            url: '<%= Url.Action("CheckUserName2","BankCustomer") %>',
            type: "GET",
            data: { id: _code },
            success: function (data) {
                if (data.toString() != "false") {
                    var textbox1 = document.getElementById('FullName');
                    textbox1.value = data;
                }
                else {
                    var textbox2 = document.getElementById('FullName');
                    textbox2.value = "";
                }
            }
        });
    }
</script>
<script type="text/javascript">
    function fnSave() {
        $.ajax({
            url: '<%= Url.Action("OpenSavingBook","BankManager") %>',
            type: "POST",
            data: $("#frm").serialize(),
            success: function (_data) {
                if (_data.toString() != "false") {
                    $.ajax({
                        url: '<%= Url.Action("DetailsSavingBook","BankManager") %>',
                        type: "POST",
                        data: {code:_data},
                        success: function (data_) {
                            if (data_.toString() != "false") {
                                $("#NoteDialog15").html(data_);
                                $("#NoteDialog15").dialog("option", "title", "Details SavingBook");
                                $("#NoteDialog15").dialog("open");
                            } else {
                                $("#NoteDialog15").html("Không thể thực hiện thao tác!");
                                $("#NoteDialog15").dialog("option", "title", "Error");
                                $("#NoteDialog15").dialog("open");
                            }
                        }
                    });
                } else {
                    $("#NoteDialog15").html("Không thể thực hiện thao tác!");
                    $("#NoteDialog15").dialog("option", "title", "Error");
                    $("#NoteDialog15").dialog("open");
                }
            }
        });
    }
</script>



