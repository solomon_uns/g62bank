﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Manager.Master" Inherits="System.Web.Mvc.ViewPage<string>" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<div id = "main-content" style="width:100%; height:100%;text-align:center;">
</div>
<script type="text/javascript">
    function checklink() {
        if (document.URL.indexOf("StaffManager") > 0) {
            $.ajax({
                url: '<%= Url.Action("StaffManager", "BankManager")%>',
                type: "GET",
                success: function (data) {
                    $("#main-content").html(data);
                }
            });
        }
        else {
            if (document.URL.indexOf("AccountManager") > 0) {
                $.ajax({
                    url: '<%= Url.Action("AccountManager", "BankManager")%>',
                    type: "GET",
                    success: function (data) {
                        $("#main-content").html(data);
                    }
                });
            }
            else {
                if (document.URL.indexOf("CustomerManager") > 0) {
                    $.ajax({
                        url: '<%= Url.Action("CustomerManager", "BankManager")%>',
                        type: "GET",
                        success: function (data) {
                            $("#main-content").html(data);
                        }
                    });
                }
            }
        }
    }
    window.onload = checklink;
    </script>
<!-- Le javascript
    ================================================== -->
    <!-- Important plugins put in all pages -->
        <script  type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script src="../../Content/Manager/js/bootstrap.js" type="text/javascript"></script>
        <script src="../../Content/Manager/js/jquery.cookie.js" type="text/javascript"></script>
        <script src="../../Content/Manager/js/jquery.mousewheel.js" type="text/javascript"></script>

    <!-- Charts plugins -->
        <script src="../../Content/Manager/js/jquery.flot.js" type="text/javascript"></script>
        <script src="../../Content/Manager/js/jquery.flot.grow.js" type="text/javascript"></script>
        <script src="../../Content/Manager/js/jquery.flot.pie.js" type="text/javascript"></script>
        <script src="../../Content/Manager/js/jquery.flot.resize.js" type="text/javascript"></script>
        <script src="../../Content/Manager/js/jquery.ui.totop.min.js" type="text/javascript"></script>
        <script src="../../Content/Manager/js/jquery.flot.orderBars.js" type="text/javascript"></script>
        <script src="../../Content/Manager/js/jquery.sparkline.min.js" type="text/javascript"></script>
        <script src="../../Content/Manager/js/jquery.knob.js" type="text/javascript"></script>

    <!-- Misc plugins -->
        <script src="../../Content/Manager/js/fullcalendar.min.js" type="text/javascript"></script>
        <script src="../../Content/Manager/js/jquery.qtip.min.js" type="text/javascript"></script>
        <script src="../../Content/Manager/js/jquery.ui.totop.min.js" type="text/javascript"></script>
    
    <!-- Search plugin -->
        <script src="../../Content/Manager/js/tipuesearch_set.js" type="text/javascript"></script>
        <script src="../../Content/Manager/js/tipuesearch_data.js" type="text/javascript"></script>
        <script src="../../Content/Manager/js/tipuesearch.js" type="text/javascript"></script>

    <!-- Form plugins -->
        <script src="../../Content/Manager/js/jquery.watermark.min.js" type="text/javascript"></script>
        <script src="../../Content/Manager/js/jquery.uniform.min.js" type="text/javascript"></script>
    
    <!-- Fix plugins -->
        <script src="../../Content/Manager/js/ios-orientationchange-fix.js" type="text/javascript"></script>

    <!-- Important Place before main.js  -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
      <script src="../../Content/Manager/js/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
    
    <!-- Init plugins -->
        <script src="../../Content/Manager/js/main.js" type="text/javascript"></script>
        <script src="../../Content/Manager/js/dashboard.js" type="text/javascript"></script>
        
        <script type="text/javascript">
            $(function () {
                $("#StaffManagerment").click(function () {
                    //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
                    $.ajax({
                        url: '<%= Url.Action("StaffManager","BankManager") %>',
                        type: "GET",
                        success: function (data) {
                            $("#main-content").html(data);
                        }
                    });
                });
            });
        </script>
        <script type="text/javascript">
            $(function () {
                $("#AccountManager").click(function () {
                    //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
                    $.ajax({
                        url: '<%= Url.Action("AccountManager","BankManager") %>',
                        type: "GET",
                        success: function (data) {
                            $("#main-content").html(data);
                        }
                    });
                });
            });
        </script>
        <script type="text/javascript">
            $(function () {
                $("#CustomerManager").click(function () {
                    //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
                    $.ajax({
                        url: '<%= Url.Action("CustomerManager","BankManager") %>',
                        type: "GET",
                        success: function (data) {
                            $("#main-content").html(data);
                        }
                    });
                });
            });
        </script>
        <script type="text/javascript">
            $(function () {
                $("#SavingBookManager").click(function () {
                    //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
                    $.ajax({
                        url: '<%= Url.Action("SavingBookManager","BankManager") %>',
                        type: "GET",
                        success: function (data) {
                            $("#main-content").html(data);
                        }
                    });
                });
            });
        </script>
        <script type="text/javascript">
            $(function () {
                $("#CostManager").click(function () {
                    //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
                    $.ajax({
                        url: '<%= Url.Action("CostManager","BankManager") %>',
                        type: "GET",
                        success: function (data) {
                            $("#main-content").html(data);
                        }
                    });
                });
            });
        </script>
        <script type="text/javascript">
            $(function () {
                $("#RateManager").click(function () {
                    //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
                    $.ajax({
                        url: '<%= Url.Action("RateManager","BankManager") %>',
                        type: "GET",
                        success: function (data) {
                            $("#main-content").html(data);
                        }
                    });
                });
            });
        </script>
                <script type="text/javascript">
                    $(function () {
                        $("#Statistics").click(function () {
                            $("#main-content").html("<img src='/BankManager/ChartProfit' alt=''/><img src='/BankManager/ChartTotalProceeds' alt=''/>");
                        });
                    });
        </script>
        
</asp:Content>
