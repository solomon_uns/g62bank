﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.PlusMoneyDebitCardIn>" %>

<% Html.ValidationSummary(); %>
<% Html.EnableClientValidation();%>

<div class="row-fluid">
    <div class="span12">
        <div class="page-header">
            <h4>Nộp Tiền Vào Tài Khoản DebitCard</h4>
        </div>

        <%using (Html.BeginForm("PlusMoneyDebitCardIn", "BankManager", FormMethod.Post, new { @class = "form-horizontal seperator", @id="frm"}))
            {%>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span4" style="color:Red; text-decoration:none;" id="mes"></label>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" >Số Tài Khoản:</label>
                        <%=Html.TextBoxFor(p => p.AccountCode, new { @class = "span4 text", @OnChange = "CheckAccount(this.value);" })%>
                    </div>
                    <div class="row-fluid" style="text-align:left;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.AccountCode)%>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" >Chủ Tài Khoản:</label>
                        <%=Html.TextBoxFor(p => p.CustomerName, new { @class = "span4 text", @readonly = "readonly" })%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.CustomerName)%>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" >Họ Tên Người Nộp:</label>
                        <%=Html.TextBoxFor(p => p.Action_FullName, new { @class = "span4 text"})%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.Action_FullName)%>
                    </div>
                </div>
                </div>
  
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" >CMND Người Nộp:</label>
                        <%=Html.TextBoxFor(p => p.Action_ID, new { @class = "span4 text" })%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.Action_ID)%>
                    </div>
                </div>
            </div> 
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Nội Dung:</label>
                            <%=Html.TextBoxFor(p => p.Description, new { @class = "span4 text", @Value = "Nộp tiền vào tài khoản" })%>
                        </div>
                        <div class="row-fluid" style="text-align:center;color:Red;">
                            <%=Html.ValidationMessageFor(p => p.Description)%>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Số Tiền Nộp:</label>
                            <%=Html.TextBoxFor(p => p.Money, new { @class = "span4 text" })%>
                        </div>
                        <div class="row-fluid" style="text-align:center;color:Red;">
                            <%=Html.ValidationMessageFor(p => p.Money)%>
                        </div>
                    </div>
                </div>
            </div>               
            <div class="form-row row-fluid">        
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                        <div class="span3"></div>
                        <div class="span7 controls">
                            <img src="../../Content/Icon/help-3-24.ico" title="Xem công thức tính lãi suất" onclick="ShowMessages();"/>
                            <button type="button" class="btn btn-info marginR10" onclick="fnSave();">Save changes</button>
                        </div>
                        </div>
                    </div>
                </div>   
            </div>


        <%} %>
    </div><!-- End .span12 -->
</div>
<div id="NoteDialog18" title="" class="Hidden"></div>
<script type="text/javascript">
    $(function () {
        $("#NoteDialog18").dialog({
            autoOpen: false, width: 800, height: 500, modal: true,
            buttons: {
                Cancel: function () { $("#NoteDialog18").dialog("close"); }
            }
        });
    });
</script>
<script type="text/javascript">
    function ShowMessages() {
        $.ajax({
            url: '<%= Url.Action("GetStringPlusMoneyDebitCardIn","BankManager") %>',
            type: "POST",
            data: $("#frm").serialize(),
            success: function (_data) {
                if (_data.toString() == "false") {
                    $("#NoteDialog18").html("Không thể thực hiện thao tác").dialog("option", "title", "Cách Tính Lãi Suất");
                    $("#NoteDialog18").dialog("open");
                } else {
                    $.ajax({
                        url: '<%= Url.Action("Messages","BankManager") %>',
                        type: "POST",
                        data: _data,
                        success: function (data) {
                            $("#NoteDialog18").html(data).dialog("option", "title", "Cách Tính Lãi Suất");
                            $("#NoteDialog18").dialog("open");
                        }
                    });
                }
            }
        });
    }
        </script>
<script type="text/javascript">
    function CheckAccount(_code) {
        $.ajax({
            url: '<%= Url.Action("CheckAccountExists","BankManager") %>',
            type: "GET",
            data: { code: _code },
            success: function (data) {
                if (data != "false") {
                    var textbox = document.getElementById('CustomerName');
                    textbox.value = data;
                }
            }
        });
    }
</script>
<script type="text/javascript">
    function fnSave() {
        $.ajax({
            url: '<%= Url.Action("PlusMoneyDebitCardIn","BankManager") %>',
            type: "POST",
            data: $("#frm").serialize(),
            success: function (data) {
                if (data.toString() != "true") {
                    $("#mes").html(data);
                } else {
                    alert("Giao dịch thành công!");
                    var textbox5 = document.getElementById('AccountCode');
                    textbox5.value = "";

                    var textbox1 = document.getElementById('CustomerName');
                    textbox1.value = "";

                    var textbox2 = document.getElementById('Action_ID');
                    textbox2.value = "";

                    var textbox3 = document.getElementById('Action_FullName');
                    textbox3.value = "";

                    var textbox4 = document.getElementById('Money');
                    textbox4.value = "";
                }
            }
        });
    }
</script>
