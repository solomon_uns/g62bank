﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.ViewModels.ATMModel.CreditCardModel>" %>

<link rel="stylesheet" href="../../Content/Css/detailsAccount.css">
<div id="SettingsPage_Content">
    <ul class="uiList fbSettingsList _4kg  _4ks">
        <div class="content">
        </div>
            <%using(Html.BeginForm()){ %>
                <%Html.RenderAction("DetailsAccount", "Account", Model.account); %>
            <%} %>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink clearfix">
                    <span class="pls fbSettingsListItemLabel"><strong>Mã Tài Khoản</strong></span>
                    <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
                        <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
                        Edit</span>--%>
                    <span class="fbSettingsListItemContent fcg"><strong id="Strong1">&nbsp;<%=Html.DisplayFor(p=>p.account.AccountCode) %></strong></span>
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink clearfix">
                    <span class="pls fbSettingsListItemLabel"><strong>Số Dư Tài Khoản</strong></span>
                    <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
                        <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
                        Edit</span>--%>
                    <span class="fbSettingsListItemContent fcg"><strong id="acc">&nbsp;<%=Html.DisplayFor(p=>p.atmaccount.Balance) %></strong></span>
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink clearfix" style="background-color:#3b5998; text-align:center; font-size:13px; font-family:Tahoma; font-weight:bold;">
                THÔNG TIN THẺ ATM
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink">
                    <span class="pls fbSettingsListItemLabel"><strong>Mã Thẻ</strong></span>
                    <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
                        <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
                        Edit
                        </span>--%>
                    <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.creditcard.CreditCardCode) %></strong></span>
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink">
                    <span class="pls fbSettingsListItemLabel"><strong>Ngày Mở</strong></span>
                    <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
                        <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
                        Edit
                        </span>--%>
                    <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.creditcard.DayCreated) %></strong></span>
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink">
                    <span class="pls fbSettingsListItemLabel"><strong>Ngày Hết Hạn</strong></span>
                    <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
                        <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
                        Edit
                        </span>--%>
                    <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.creditcard.DayExpired) %></strong></span>
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink">
                    <span class="pls fbSettingsListItemLabel"><strong>Số Tiền Đang Vay (nếu có)</strong></span>
                    <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
                        <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
                        Edit
                        </span>--%>
                    <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.creditcard.Loan) %></strong></span>
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink clearfix" style="background-color:#3b5998; text-align:center; font-size:13px; font-family:Tahoma; font-weight:bold;">
                THÔNG TIN LOẠI THẺ
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink">
                    <span class="pls fbSettingsListItemLabel"><strong>Mã Loại</strong></span>
                    <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
                        <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
                        Edit
                        </span>--%>
                    <span class="fbSettingsListItemContent fcg"><strong><%=Html.DisplayFor(p=>p.atmaccounttype.ATMAccountTypeCode) %></strong></span>
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink">
                    <span class="pls fbSettingsListItemLabel"><strong>Tên Loại</strong></span>
                    <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
                        <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
                        Edit
                        </span>--%>
                    <span class="fbSettingsListItemContent fcg"><strong><%=Html.DisplayFor(p=>p.atmaccounttype.ATMAccountTypeName) %></strong></span>
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink">
                    <span class="pls fbSettingsListItemLabel"><strong>Lãi Suất</strong></span>
                    <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
                        <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
                        Edit
                        </span>--%>
                    <span class="fbSettingsListItemContent fcg"><strong><%=Html.DisplayFor(p=>p.atmaccounttype.Rate)%> (%)</strong></span>
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink">
                    <span class="pls fbSettingsListItemLabel"><strong>Số Ngày Hết Hạn Từ Ngày Mở</strong></span>
                    <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
                        <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
                        Edit
                        </span>--%>
                    <span class="fbSettingsListItemContent fcg"><strong><%=Html.DisplayFor(p=>p.atmaccounttype.NumberDayExpired)%></strong></span>
                </a>
                <div class="content">
                </div>
            </li>
            <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
                <a class="pvm phs fbSettingsListLink">
                    <span class="pls fbSettingsListItemLabel"><strong>Mô Tả</strong></span>
                    <%--<span style="padding-left: 23px;" class="uiIconText fbSettingsListItemEdit">
                        <i class="img sp_bqmn96 sx_3ef030" style="top: -2px;"></i>
                        Edit
                        </span>--%>
                    <span class="fbSettingsListItemContent fcg"><strong><%=Html.DisplayFor(p=>p.atmaccounttype.Description)%></strong></span>
                </a>
                <div class="content">
                </div>
            </li>

    </ul>
</div>