﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<G62BANK.Models.LoginManager>" %>

<% Html.ValidationSummary(); %>
<% Html.EnableClientValidation();%>
<html>
    <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Đăng Nhập Hệ Thống Quản lý Ngân Hàng G62Bank</title>
        <script src="../../Content/Manager/selector/imagepreview.js" type="text/javascript"></script>
        <script src="../../Content/Manager/selector/jquery-1.4.2.min.js" type="text/javascript"></script>
        <script src="../../Content/Manager/selector/jquery.framerate.js" type="text/javascript"></script>
        <link href="../../Content/Manager/selector/styles.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../Scripts/jquery.validate.min.js" ></script>
    <script type="text/javascript" src="../../Scripts/jquery.validate.unobtrusive.min.js" ></script>
    
    </head>

    <body>
    
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>G62Bank</title>
    <meta name="author" content="SuggeElson" />
    <meta name="description" content="Supr admin template - new premium responsive admin template. This template is designed to help you build the site administration without losing valuable time.Template contains all the important functions which must have one backend system.Build on great twitter boostrap framework" />
    <meta name="keywords" content="admin, admin template, admin theme, responsive, responsive admin, responsive admin template, responsive theme, themeforest, 960 grid system, grid, grid theme, liquid, masonry, jquery, administration, administration template, administration theme, mobile, touch , responsive layout, boostrap, twitter boostrap" />
    <meta name="application-name" content="Supr admin template" />

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <link href="../../Content/Manager/css/bootstrap-responsive.css" rel="stylesheet"
            type="text/css" />
        <link href="../../Content/Manager/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="../../Content/Manager/css/bootstrap.css" rel="stylesheet" type="text/css" />

        <link href="../../Content/Manager/css/jquery.ui.supr.css" rel="stylesheet" type="text/css" />
        <link href="../../Content/Manager/css/main.css" rel="stylesheet" type="text/css" />
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->

    </head>
      
    <body class="loginPage">

    <div class="container-fluid">

        <div class="loginContainer">
            <%using(Html.BeginForm())
{%>
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span12" for="username">
                                Username:
                                </label>
                            <%=Html.TextBoxFor(p => p.UserName, new { @class = "span12" })%>
                            
                            <span style="color:Red;"><%=Html.ValidationMessageFor(p=>p.UserName) %></span>
                        </div>
                    </div>
                </div>
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span12" for="password">
                                Password:
                            </label>
                            <%=Html.PasswordFor(p => p.Password, new { @class = "span12" })%>
                            <span style="color:Red;"><%=Html.ValidationMessageFor(p=>p.Password) %></span>
                        </div>
                    </div>
                </div>
                
                <div class="form-row row-fluid">                       
                    <div class="span12">
                        <div class="row-fluid">
                            <div class="form-actions">
                            <div class="span12 controls">
                                <span style="color:Red;font-style:italic;" id="login-error"></span><button type="submit" class="btn btn-info right" id="loginBtn">Login</button>
                            </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <%} %>
        </div>

    </div><!-- End .container-fluid -->
    </body>
</html>
