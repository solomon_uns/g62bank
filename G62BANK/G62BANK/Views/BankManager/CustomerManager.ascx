﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<div class="centerContent">            
    <ul class="bigBtnIcon">
        <li id= "createcustomer">
            <a class="tipB" title="Đăng ký khách hàng">
                <span class="icon"><img src="../../Content/Icon/add-customer.ico" style="margin-left:10px;margin-right:10px; width:20px;height:20px;"/></span>
                <span class="txt">Create Customer</span>
                <%--<span class="notification"><%=Html.DisplayFor(p=>p.NewDebitCard) %></span>--%>
            </a>
        </li>
        <li id= "listcustomer">
            <a class="tipB" title="Danh sách khách hàng">
                <span class="icon"><img src="../../Content/Icon/user-6-16.ico" style="margin-left:10px;margin-right:10px; width:20px;height:20px;"/></span>
                <span class="txt">List Customer</span>
                <%--<span class="notification"><%=Html.DisplayFor(p=>p.NewDebitCard) %></span>--%>
            </a>
        </li>
    </ul>
</div>
<script type="text/javascript">
    $(function () {
        $("#createcustomer").click(function () {
            //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
            $.ajax({
                url: '<%= Url.Action("CreateCustomer","BankManager") %>',
                type: "GET",
                success: function (data) {
                    $("#main-content").html(data);
                }
            });
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#listcustomer").click(function () {
            //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
            $.ajax({
                url: '<%= Url.Action("ListCustomer","BankManager") %>',
                type: "GET",
                success: function (data) {
                    $("#main-content").html(data);
                }
            });
        });
    });
</script>