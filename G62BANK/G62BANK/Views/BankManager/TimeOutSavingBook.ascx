﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.SavingBook>" %>

<script type="text/javascript">
    function Autorun() {
        $.ajax({
            url: '<%= Url.Action("DetailsSavingBook","BankManager") %>',
            type: "POST",
            data: {code : <%=Model.SavingBookCode %>},
            success: function (_data) {
                $("#det").html(_data);
            }
        });
        $.ajax({
            url: '<%= Url.Action("GetCostTimeOut","BankManager") %>',
            type: "POST",
            success: function (_data) {
               var textbox1 = document.getElementById('phi');
                    textbox1.value = _data;
            }
        });
        var savcode = <%=Model.SavingBookCode %>;
        var money = <%=Model.Money %>; 
        $.ajax({
            url: '<%= Url.Action("CheckTimeOutSavingBook","BankManager") %>',
            type: "POST",
            data: {code : savcode},
            success: function (data2) {
                if (data2.toString() != "false") {

                    var text = data2.toString().split('|');
                    var LDH = parseFloat(text[0]);
                    var STDH = parseFloat(text[1]);
                    var LQH = parseFloat(text[2]);


                    var textbox2 = document.getElementById('laidunghan');
                    textbox2.value = LDH;

                    var textbox3 = document.getElementById('tiendunghan');
                    textbox3.value = STDH;

                    var textbox3 = document.getElementById('laiquahan');
                    textbox3.value = LQH;

                    var textbox4 = document.getElementById('nhan');
                    textbox4.value = STDH + LQH;
                }
            }
        });
    }
    window.load = Autorun();
</script>
<div class="row-fluid">
    <div class="span12">
        <div class="page-header">
            <h4>Rút tiền tiết kiệm</h4>
        </div>

        <%using (Html.BeginForm("MaturitySavingBook", "BankManager", FormMethod.Post, new { @class = "form-horizontal seperator", @id = "frm" }))
            {%>
            <div class="form-row row-fluid">
                <div class="span12" id="det">
                </div>
            </div>
            <%-- Số tiền rút--%>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Số Tiền Rút:</label>
                            <%=Html.TextBoxFor(p=>p.Money, new {@class="span4 text", @readonly="readonly"})%>
                        </div>
                    </div>
                </div>
            </div>

            <%-- Số tiền lãi đúng hạn --%>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Lãi Đúng Hạn:</label>
                            <input type="text" class="span4 text" id="laidunghan" readonly="readonly" />
                        </div>
                    </div>
                </div>
            </div>
            <%-- Số tiền đúng hạn được nhận --%>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Tổng Tiền Đúng Hạn:</label>
                            <input type="text" class="span4 text" id="tiendunghan" readonly="readonly" />
                        </div>
                    </div>
                </div>
            </div>
            <%-- Lãi quá hạn --%>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Lãi Quá Hạn:</label>
                            <input type="text" class="span4 text" id="laiquahan" readonly="readonly" />
                        </div>
                    </div>
                </div>
            </div>
            <%-- Phí dịch vụ--%>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Phí Dịch Vụ:</label>
                            <input type="text" class="span4 text" id="phi" readonly="readonly"/> (%)
                        </div>
                    </div>
                </div>
            </div>  

            <%-- Số tiền nhận--%>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Số Tiền Sẽ Nhận:</label>
                            <input type="text" class="span4 text" id="nhan" readonly="readonly" />
                        </div>
                    </div>
                </div>
            </div>

            <%-- button--%> 
            <div class="form-row row-fluid">        
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                        <div class="span3"></div>
                        <div class="span7 controls">
                            <img src="../../Content/Icon/help-3-24.ico" title="Xem công thức tính lãi suất" onclick="ShowMessages();"/>
                            <button type="button" class="btn btn-info marginR10" onclick="fnSave();">Save changes</button>
                        </div>
                        </div>
                    </div>
                </div>   
            </div>


        <%} %>
    </div><!-- End .span12 -->
</div>
<div id="NoteDialog30" title="" class="Hidden"></div>
<script type="text/javascript">
    $(function () {
        $("#NoteDialog30").dialog({
            autoOpen: false, width: 800, height: 500, modal: true,
            buttons: {
                Cancel: function () { $("#NoteDialog30").dialog("close"); }
            }
        });
    });
</script>

<script type="text/javascript">
    function ShowMessages() {
        var _code = <%=Model.SavingBookCode %>;
        $.ajax({
            url: '<%= Url.Action("GetStringTimeOutSavingBook","BankManager") %>',
            type: "POST",
            data: { code: _code},
            success: function (_data) {
                if (_data.toString() == "false") {
                    $("#NoteDialog30").html("Không thể thực hiện thao tác").dialog("option", "title", "Cách Tính Lãi Suất");
                    $("#NoteDialog30").dialog("open");
                } else {
                    $.ajax({
                        url: '<%= Url.Action("Messages","BankManager") %>',
                        type: "POST",
                        data: _data,
                        success: function (data) {
                            $("#NoteDialog30").html(data).dialog("option", "title", "Cách Tính Lãi Suất");
                            $("#NoteDialog30").dialog("open");
                        }
                    });
                }
            }
        });
    }
</script>

<script type="text/javascript">
    function fnSave() {
        var savcode = <%=Model.SavingBookCode %>; 
        $.ajax({
            url: '<%= Url.Action("TimeOutSavingBook_Submit","BankManager") %>',
            type: "POST",
            data: {code : savcode},
            success: function (_data) {
                if (_data.toString() == "true") {
                    alert("đã rút tiền thành công");
                } else {
                    $("#notegetmoney").html("Số tiền không hợp lệ");
                }
            }
        });
    }
</script>






