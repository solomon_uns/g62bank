﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.TranferPersontoPerson>" %>

<% Html.ValidationSummary(); %>
<% Html.EnableClientValidation();%>

<div class="row-fluid">
    <div class="span12">
        <div class="page-header">
            <h4>Chuyển tiền giữa các cá nhân</h4>
        </div>
        <%using (Html.BeginForm("Tranfers_Person_to_Person", "BankManager", FormMethod.Post, new { @class = "form-horizontal seperator", @id = "frm" }))
            {%>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span4" style="color:Red; text-decoration:none;" id="mes"></label>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" >CMND Người Chuyển:</label>
                    <%=Html.TextBoxFor(p => p.FromCustomerID, new { @class = "span4 text"})%>
                </div>
                <div class="row-fluid" style="text-align:left;color:Red;">
                    <%=Html.ValidationMessageFor(p => p.FromCustomerID)%>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" >Họ Tên Người Chuyển:</label>
                    <%=Html.TextBoxFor(p => p.FromCustomerName, new { @class = "span4 text"})%>
                </div>
                <div class="row-fluid" style="text-align:center;color:Red;">
                    <%=Html.ValidationMessageFor(p => p.FromCustomerName)%>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" >CMND Người Nhận:</label>
                    <%=Html.TextBoxFor(p => p.ToCustomerID, new { @class = "span4 text"})%>
                </div>
                <div class="row-fluid" style="text-align:center;color:Red;">
                    <%=Html.ValidationMessageFor(p => p.ToCustomerID)%>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" >Họ Tên Người Nhận:</label>
                    <%=Html.TextBoxFor(p => p.ToCustomerName, new { @class = "span4 text"})%>
                </div>
                <div class="row-fluid" style="text-align:center;color:Red;">
                    <%=Html.ValidationMessageFor(p => p.ToCustomerName)%>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <div class="row-fluid">
                        <label class="form-label span3" >Nội Dung:</label>
                        <%=Html.TextBoxFor(p => p.Description, new { @class = "span4 text", @Value = "Chuyển tiền giữa các cá nhân" })%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.Description)%>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <div class="row-fluid">
                        <label class="form-label span3" >Số Tiền Chuyển:</label>
                        <%=Html.TextBoxFor(p => p.Money, new { @class = "span4 text", @OnChange = "GetCost(this.value);" })%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.Money)%>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <div class="row-fluid">
                        <label class="form-label span3" >Phí Dịch Vụ:</label>
                        <%=Html.TextBoxFor(p => p.Cost, new { @class = "span4 text",@readonly="readonly" })%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.Cost)%>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <div class="row-fluid">
                        <label class="form-label span3" >Tổng Số Tiền Sẽ Phải Thanh Toán:</label>
                        <%=Html.TextBoxFor(p => p.SumMoney, new { @class = "span4 text", @readonly = "readonly" })%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.SumMoney)%>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">        
            <div class="span12">
                <div class="row-fluid">
                    <div class="form-actions">
                    <div class="span3"></div>
                    <div class="span7 controls">
                        <img src="../../Content/Icon/help-3-24.ico" title="Xem công thức tính lãi suất" onclick="ShowMessages();"/>
                        <button type="button" class="btn btn-info marginR10" onclick="fnSave();">Save changes</button>
                    </div>
                    </div>
                </div>
            </div>   
        </div>
        <%} %>
    </div>
</div>
<div id="NoteDialog33" title="" class="Hidden"></div>
<script type="text/javascript">
    $(function () {
        $("#NoteDialog33").dialog({
            autoOpen: false, width: 800, height: 500, modal: true,
            buttons: {
                Cancel: function () { $("#NoteDialog33").dialog("close"); }
            }
        });
    });
</script>
<script type="text/javascript">
    function ShowMessages() {
        $.ajax({
            url: '<%= Url.Action("GetStringTranfersMoneyPersontoPerson","BankManager") %>',
            type: "POST",
            data: $("#frm").serialize(),
            success: function (_data) {
                if (_data.toString() == "false") {
                    $("#NoteDialog33").html("Không thể thực hiện thao tác").dialog("option", "title", "Cách Tính Lãi Suất");
                    $("#NoteDialog33").dialog("open");
                } else {
                    $.ajax({
                        url: '<%= Url.Action("Messages","BankManager") %>',
                        type: "POST",
                        data: _data,
                        success: function (data) {
                            $("#NoteDialog33").html(data).dialog("option", "title", "Cách Tính Lãi Suất");
                            $("#NoteDialog33").dialog("open");
                        }
                    });
                }
            }
        });
    }
</script>

<script type="text/javascript">
    function GetCost(_code) {
        $.ajax({
            url: '<%= Url.Action("GetCostTranfersPersontoPerson","BankManager") %>',
            type: "GET",
            data: { code: _code },
            success: function (data) {
                if (data != "false") {
                    var textbox3 = document.getElementById('Cost');
                    textbox3.value = data;

                    var textbox4 = document.getElementById('SumMoney');
                    var textbox5 = document.getElementById('Money');
                    textbox4.value = parseInt(data) + parseInt(textbox5.value);
                }
            }
        });
    }
</script>
<script type="text/javascript">
    function fnSave() {
        $.ajax({
            url: '<%= Url.Action("Tranfers_Person_to_Person","BankManager") %>',
            type: "POST",
            data: $("#frm").serialize(),
            success: function (data) {
                if (data.toString() == "false") {
                    $("#mes").html("Thông Tin Không Chính Xác");
                } else {
                    $("#NoteDialog33").html("Mã giao dịch chuyển tiền của bạn là :" + data + " \n Lưu ý: Không để lộ mã này").dialog("option", "title", "Mã giao dịch chuyền tiền");
                    $("#NoteDialog33").dialog("open");
                    var textbox6 = document.getElementById('FromCustomerID');
                    textbox6.value = "";

                    var textbox7 = document.getElementById('FromCustomerName');
                    textbox7.value = "";

                    var textbox6 = document.getElementById('ToCustomerID');
                    textbox6.value = "";

                    var textbox7 = document.getElementById('ToCustomerName');
                    textbox7.value = "";

                    var textbox8 = document.getElementById('Money');
                    textbox8.value = "";

                    var textbox9 = document.getElementById('Cost');
                    textbox9.value = "";

                    var textbox10 = document.getElementById('SumMoney');
                    textbox10.value = "";
                }
            }
        });
    }
</script>