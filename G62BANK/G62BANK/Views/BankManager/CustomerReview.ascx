﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.Customer>" %>

<% Html.ValidationSummary(); %>
<% Html.EnableClientValidation();%>

<div class="row-fluid">
    <div class="span12">
        <%using (Html.BeginForm("CustomerReview", "BankManager", FormMethod.Post, new { @class = "form-horizontal seperator", enctype = "multipart/form-data" }))
            {%>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="username">Username:</label>
                        <%=Html.TextBoxFor(p => p.UserName, new { @class = "span4 text", @readonly = "readonly" })%>
                        <%=Html.ValidationMessageFor(p=>p.UserName ) %>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <%=Html.LabelFor(p => p.CustomerID, new { @class = "form-label span3" })%>
                        <%=Html.TextBoxFor(p => p.CustomerID, new { @class = "span4 text" })%>
                                        
                    </div>
                    <div class="row-fluid" style="text-align:left;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.CustomerID)%>
                    </div>

                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <%=Html.LabelFor(p => p.FullName, new { @class = "form-label span3" })%>
                        <%=Html.TextBoxFor(p => p.FullName, new { @class = "span4 text" })%>
                                        
                    </div>
                    <div class="row-fluid" style="text-align:left;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.FullName)%>
                    </div>

                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <%=Html.LabelFor(p => p.Address, new { @class = "form-label span3" })%>
                        <%=Html.TextAreaFor(p => p.Address, new { @class = "span4 text" })%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.Address)%>
                    </div>
                </div>
            </div> 
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <%=Html.LabelFor(p => p.Email, new { @class = "form-label span3" })%>
                        <%=Html.TextBoxFor(p => p.Email, new { @class = "span4 text" })%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.Email)%>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <%=Html.LabelFor(p => p.Birthday, new { @class = "form-label span3" })%>
                            <%=Html.TextBoxFor(p => p.Birthday, "{0:MM/dd/yyyy}", new { @class = "span4 text" })%>
                        </div>
                        <div class="row-fluid" style="text-align:center;color:Red;">
                            <%=Html.ValidationMessageFor(p => p.Birthday)%>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <%=Html.LabelFor(p => p.Phone, new { @class = "form-label span3" })%>
                        <%=Html.TextBoxFor(p => p.Phone, new { @class = "span4 text" })%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.Phone)%>
                    </div>
                </div>
                </div>

            <div class="form-row row-fluid">        
                <div class="span12">
                    <div class="row-fluid">
                    </div>
                    <div class="row-fluid" style="text-align:center;">
                        <button type="submit" class="btn btn-info marginR10">Save changes</button>
                    </div>
                </div>   
            </div>


        <%} %>
    </div>
</div>

