﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.UserManager>" %>

<div class="span6">
    <div class="box hover">
        <div class="title">
            <h4> 
                <span>Avatar</span>
            </h4>                  
        </div>
        <div class="content">             
            <% using (Ajax.BeginForm("ChangeAvatar", "BankManager", new AjaxOptions { UpdateTargetId = "columnMappings" }, new { id = "UpdateDataset", enctype = "multipart/form-data" }))
                {
                %>
                <fieldset>                  
                <div class="row-fluid" style="text-align:center;">
                <%if (Model != null)
                    { %>
                    <img src="../../Content/Manager/UploadAvatar/<%=Model.Avatar %>" alt="" class="image marginR10" style="width:150px;height:150px;"/>
                    <%}
                    else
                    { %>
                    <img src="../../Content/Manager/img/noavatar.png" alt="" class="image marginR10" style="width:150px;height:150px;"/>
                    <%} %>
                    <div class="uploader" id="uniform-file">
                        <input type="file" name="fileinput" id="file" size="20" style="opacity: 0;">
                        <span class="filename">No file selected</span>
                        <span class="action">Choose File</span>
                    </div>
                    <div class="form-actions" style="text-align:center;">
                        <button type="submit" class="btn btn-info" id= "btn-changeavatar">Save changes</button>
                        <%--<script type="text/javascript">
                            $(function () {
                                var fu1 = document.getElementById("file");
                                $("#btn-changeavatar").click(function () {

                                    $.ajax({
                                        url: '<%= Url.Action("ChangeAvatar","BankManager") %>',
                                        type: "POST",
//                                        data: {file: fu1.value },
                                        success: function (data) {
                                        }
                                    });
                                });
                            });
                    </script>--%>
                    </div>
                    <//div>    
                </fieldset>
            <%} %>      
            
        </div>
    </div><!-- End .box -->
    </div>