﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.ViewModels.SavingBookModel.DetailsSavingBookModel>" %>

<link rel="stylesheet" href="../../Content/Css/detailsAccount.css">
<div id="SettingsPage_Content">
    <ul class="uiList fbSettingsList _4kg  _4ks">
        <div class="content">
        </div>
        <%Html.RenderAction("DetailsAccount", "Account", Model.account); %>
        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink clearfix">
                <span class="pls fbSettingsListItemLabel"><strong>Mã Tài Khoản :</strong></span>
                <span class="fbSettingsListItemContent fcg"><strong id="Strong1">&nbsp;<%=Html.DisplayFor(p=>p.account.AccountCode) %></strong></span>
            </a>
            <div class="content">
            </div>
        </li>
        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink clearfix">
                <span class="pls fbSettingsListItemLabel"><strong>Số Dư Tài Khoản :</strong></span>
                <span class="fbSettingsListItemContent fcg"><strong id="acc">&nbsp;<%=Html.DisplayFor(p=>p.savingbook.Money) %></strong></span>
            </a>
            <div class="content">
            </div>
        </li>
        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink clearfix" style="background-color:#3b5998; text-align:center; font-size:13px; font-family:Tahoma; font-weight:bold;">
            SỔ TIẾT KIỆM
            </a>
            <div class="content">
            </div>
        </li>
        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink">
                <span class="pls fbSettingsListItemLabel"><strong>Lãi Suất Đang Áp Dụng :</strong></span>
                <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.savingbook.RateNow) %> (%)</strong></span>
            </a>
            <div class="content">
            </div>
        </li>
        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink">
                <span class="pls fbSettingsListItemLabel"><strong>Lãi Suất Trước Thời Gian Đáo Hạn :</strong></span>
                <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.savingbook.RateBeforeTerm) %> (%)</strong></span>
            </a>
            <div class="content">
            </div>
        </li>
        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink">
                <span class="pls fbSettingsListItemLabel"><strong>Ngày Đáo Hạn</strong></span>
                <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.savingbook.DayExpired) %></strong></span>
            </a>
            <div class="content">
            </div>
        </li>
        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink">
                <span class="pls fbSettingsListItemLabel"><strong>Ngày Mở</strong></span>
                <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p => p.savingbook.DayCreated)%></strong></span>
            </a>
            <div class="content">
            </div>
        </li>
        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink">
                <span class="pls fbSettingsListItemLabel"><strong>Ngày Đáo Hạn</strong></span>
                <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.savingbook.DayExpired) %></strong></span>
            </a>
            <div class="content">
            </div>
        </li>

        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink clearfix" style="background-color:#3b5998; text-align:center; font-size:13px; font-family:Tahoma; font-weight:bold;">
            THÔNG TIN LOẠI TIẾT KIỆM ĐANG ÁP DỤNG
            </a>
            <div class="content">
            </div>
        </li>
        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink">
                <span class="pls fbSettingsListItemLabel"><strong>Mã Loại :</strong></span>
                <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.savingbooktype.SavingBookTypeCode) %></strong></span>
            </a>
            <div class="content">
            </div>
        </li>
        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink">
                <span class="pls fbSettingsListItemLabel"><strong>Tên Loại :</strong></span>
                <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.savingbooktype.SavingBookTypeName) %></strong></span>
            </a>
            <div class="content">
            </div>
        </li>
        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink">
                <span class="pls fbSettingsListItemLabel"><strong>Số Ngày Đáo Hạn Từ Ngày Mở :</strong></span>
                <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.savingbooktype.SavingBookTime)%> (Ngày)</strong></span>
            </a>
            <div class="content">
            </div>
        </li>
        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink">
                <span class="pls fbSettingsListItemLabel"><strong>Lãi Suất :</strong></span>
                <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.savingbooktype.Rate)%> (%)</strong></span>
            </a>
            <div class="content">
            </div>
        </li>
        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink">
                <span class="pls fbSettingsListItemLabel"><strong>Lãi Suất Trước Thời Gian Đáo Hạn :</strong></span>
                <span class="fbSettingsListItemContent fcg"><strong> &nbsp;<%=Html.DisplayFor(p=>p.savingbooktype.RateBeforeTerm)%></strong></span>
            </a>
            <div class="content">
            </div>
        </li>
        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink">
                <span class="pls fbSettingsListItemLabel"><strong>Ngày Áp Dụng :</strong></span>
                <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.savingbooktype.DayApply)%></strong></span>
            </a>
            <div class="content">
            </div>
        </li>
        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink">
                <span class="pls fbSettingsListItemLabel"><strong>Loại Tiền Gửi :</strong></span>
                <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.savingbooktype.TypeOfDeposit)%></strong></span>
            </a>
            <div class="content">
            </div>
        </li>
        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink">
                <span class="pls fbSettingsListItemLabel"><strong>Số Tiền Gửi Tối Thiểu :</strong></span>
                <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.savingbooktype.MinimumMoney)%></strong></span>
            </a>
            <div class="content">
            </div>
        </li>
        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink">
                <span class="pls fbSettingsListItemLabel"><strong>Số Tiền Gửi Tối Đa :</strong></span>
                <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.savingbooktype.MaximumMoney)%></strong></span>
            </a>
            <div class="content">
            </div>
        </li>
        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink">
                <span class="pls fbSettingsListItemLabel"><strong>Phương Thức Trả Lãi :</strong></span>
                <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.savingbooktype.PaymentType)%></strong></span>
            </a>
            <div class="content">
            </div>
        </li>
        <li class="fbSettingsListItem clearfix uiListItem fbSettingsListItemLabeled">
            <a class="pvm phs fbSettingsListLink">
                <span class="pls fbSettingsListItemLabel"><strong>Khác :</strong></span>
                <span class="fbSettingsListItemContent fcg"><strong>&nbsp;<%=Html.DisplayFor(p=>p.savingbooktype.Other)%></strong></span>
            </a>
            <div class="content">
            </div>
        </li>
    </ul>
</div>