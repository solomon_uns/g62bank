﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<div class="row-fluid">
    <div class="span12">
        <div class="page-header">
            <h4>Vay tiền CreditCard</h4>
        </div>

        <%using (Html.BeginForm("OpenSavingBook", "BankManager", FormMethod.Post, new { @class = "form-horizontal seperator", @id= "frm", enctype = "multipart/form-data" }))
            {%>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span4" for="username" style="color:Red; text-decoration:none;" id="mes"><%=Html.ValidationSummary() %></label>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3">Account:</label>
                        <input class="span4 text valid" id="AccountCode" name="AccountCode" type="text" value="" style="width: 27.5%;" onchange="CheckAccount(this.value);">
                        <img src="../../Content/Icon/user-12-24.ico" style="margin-left:10px;margin-right:10px;" title="Tạo Customer" onclick="ShowCreditCard();"/>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3">FullName:</label>
                        <input class="span4 text valid" id="FullName" name="FullName" type="text" value="" readonly="readonly">
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3">Money:</label>
                        <input class="span4 text valid" id="Money" name="FullName" type="text" value="">
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">        
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                        <div class="span3"></div>
                        <div class="span4 controls">
                            <button type="button" class="btn btn-info marginR10" onclick="fnSave();">Save changes</button>
                        </div>
                        </div>
                    </div>
                </div>   
            </div>
        <%} %>
    </div>
</div>
<div id="NoteDialog13" title="" class="Hidden"></div>
<script type="text/javascript">
    $(function () {
        $("#NoteDialog13").dialog({
            autoOpen: false, width: 800, height: 600, modal: true,
            buttons: {
                Cancel: function () { $("#NoteDialog13").dialog("close"); }
            }
        });
    });
</script>

<script type="text/javascript">
    function CheckAccount(_code) {
        $.ajax({
            url: '<%= Url.Action("CheckAccountExists","BankManager") %>',
            type: "GET",
            data: { code: _code },
            success: function (data) {
                if (data != "false") {
                    var textbox = document.getElementById('FullName');
                    textbox.value = data;
                }
            }
        });
    }
</script>
<script type="text/javascript">
    function ShowCreditCard() {
//        $.ajax({
//            url: '<%= Url.Action("DetailsATMAccount_CreditCard_2","ATMAccount") %>',
//            type: "GET",
//            data: { code: $("#AccountCode").val() },
//            success: function (_data) {
//                if (_data != "false") {
//                    $("#NoteDialog").html(_data);
//                    $("#NoteDialog").dialog("option", "title", "Details CreditCard");
//                    $("#NoteDialog").dialog("open");
//                }
//            }
//        });
    }
</script>
<script type="text/javascript">
    function fnSave() {
        $.ajax({
            url: '<%= Url.Action("LoanCreditCard_Post","BankManager") %>',
            type: "Post",
            data: { code: $("#AccountCode").val(), money: $("#Money").val() },
            success: function (_data) {
                if (_data.toString() != "false") {
                    alert("Đã vay thành công!");
                } else {
                    alert("Không thể thực hiện thao tác");
                }
            }
        });
    }
</script>
