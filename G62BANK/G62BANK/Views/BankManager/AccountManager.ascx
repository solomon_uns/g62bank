﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.AccountManager>" %>

<div class="centerContent">            
    <ul class="bigBtnIcon">
        <li id= "searchdebitcard">
            <a class="tipB" title="Tìm kiếm DebitCard">
                <span class="icon"><img src="../../Content/Icon/book-10-24.ico" style="margin-left:10px;margin-right:10px;"/></span>
                <span class="txt">Search DebitCard</span>
            </a>
        </li>
        <li id= "searchcreditcard">
            <a class="tipB" title="Tìm kiếm CrebitCard">
                <span class="icon"><img src="../../Content/Icon/book-10-24.ico" style="margin-left:10px;margin-right:10px;"/></span>
                <span class="txt">Search CrebitCard</span>
            </a>
        </li>
        <li id= "newdebitcard">
            <a class="tipB" title="Duyệt đăng ký mới tk thẻ DebitCard">
                <span class="icon"><img src="../../Content/Icon/book-10-24.ico" style="margin-left:10px;margin-right:10px;"/></span>
                <span class="txt">Check DBCard</span>
                <span class="notification"><%=Html.DisplayFor(p=>p.NewDebitCard) %></span>
            </a>
        </li>
        <li id= "newcreditcard">
            <a class="tipB" title="Duyệt đăng ký mới tk thẻ CreditCard">
                <span class="icon"><img src="../../Content/Icon/book-10-24.ico" style="margin-left:10px;margin-right:10px;"/></span>
                <span class="txt">Check CDCard</span>
                <span class="notification"><%=Html.DisplayFor(p=>p.NewCreditCard) %></span>
            </a>
        </li>
        <li id= "plusmoneydebitcardin"">
            <a class="tipB" title="Nạp tiền vào tài khoản thẻ">
                <span class="icon"><img src="../../Content/Icon/add-money-24.ico" style="margin-left:10px;margin-right:10px;"/></span>
                <span class="txt">Plus Money DebitCard</span>
                <span class="notification">in</span>
            </a>
        </li>
        <li id= "Li2">
            <a class="tipB" title="Nạp tiền vào tài khoản của ngân hàng khác">
                <span class="icon"><img src="../../Content/Icon/add-money-24.ico" style="margin-left:10px;margin-right:10px;"/></span>
                <span class="txt">Plus Money DebitCard</span>
                <span class="notification">Bank Systems</span>
            </a>
        </li>
        <li id= "Li3">
            <a class="tipB" title="Gia hạn sử dụng thẻ">
                <span class="icon"><img src="../../Content/Icon/alarm-clock-9-24.ico" style="margin-left:10px;margin-right:10px;"/></span>
                <span class="txt">Extension</span>
                <%--<span class="notification">Bank Systems</span>--%>
            </a>
        </li>
        <li id= "tranfersmoneyatmtoatm">
            <a class="tipB" title="Chuyển tiền giữa các tài khoản thẻ">
                <span class="icon"><img src="../../Content/Icon/arrow-32-24.ico" style="margin-left:10px;margin-right:10px;"/></span>
                <span class="txt">Tranfers Money</span>
                <span class="notification">in</span>
            </a>
        </li>
        <li id= "Li5">
            <a class="tipB" title="Chuyển tiền từ các tài khoản thẻ G62Bank sang tài khoàn ngân hàng khác">
                <span class="icon"><img src="../../Content/Icon/arrow-32-24.ico" style="margin-left:10px;margin-right:10px;"/></span>
                <span class="txt">Tranfers Money</span>
                <span class="notification">Bank Systems</span>
            </a>
        </li>
        <li id= "tranfersmoneyatmtoperson">
            <a class="tipB" title="Chuyển tiền từ tài khoản thẻ cho các nhân">
                <span class="icon"><img src="../../Content/Icon/arrow-32-24.ico" style="margin-left:10px;margin-right:10px;"/></span>
                <span class="txt">Tranfers Money</span>
                <span class="notification">ATMAccount - Person</span>
            </a>
        </li>
        <li id= "tranfersmoneypersontoperson">
            <a class="tipB" title="Chuyển tiền từ giữa các cá nhân">
                <span class="icon"><img src="../../Content/Icon/arrow-32-24.ico" style="margin-left:10px;margin-right:10px;"/></span>
                <span class="txt">Tranfers Money</span>
                <span class="notification">Person - Person</span>
            </a>
        </li>
        <li id= "PersonGetMoney_Tranfers_ATM_to_Person">
            <a class="tipB" title="Nhận tiền chuyển từ tài khoản DebitCard tới Cá nhân">
                <span class="icon"><img src="../../Content/Icon/download-13-24.ico" style="margin-left:10px;margin-right:10px;"/></span>
                <span class="txt">Get Money Tranfer</span>
                <span class="notification">From ATMAccount</span>
            </a>
        </li>
        <li id= "PersonGetMoney_Tranfers_Person_to_Person">
            <a class="tipB" title="Nhận tiền chuyển tới Cá nhân">
                <span class="icon"><img src="../../Content/Icon/download-13-24.ico" style="margin-left:10px;margin-right:10px;"/></span>
                <span class="txt">Get Money Tranfer</span>
                <span class="notification">From Person</span>
            </a>
        </li>
        <li id= "loadcreditcard">
            <a class="tipB" title="Vay tiền vào thẻ CreditCard">
                <span class="icon"><img src="../../Content/Icon/download-13-24.ico" style="margin-left:10px;margin-right:10px;"/></span>
                <span class="txt">Loan CreditCard</span>
            </a>
        </li>
        <li id= "returnloan">
            <a class="tipB" title="Trả lãi CreditCard">
                <span class="icon"><img src="../../Content/Icon/download-13-24.ico" style="margin-left:10px;margin-right:10px;"/></span>
                <span class="txt">Return Rate CreditCard</span>
            </a>
        </li>
    </ul>
</div>
<script type="text/javascript">
    $(function () {
        $("#searchdebitcard").click(function () {
            //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
            $.ajax({
                url: '<%= Url.Action("SearchDebitCard","BankManager") %>',
                type: "GET",
                success: function (data) {
                    $("#main-content").html(data);
                }
            });
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#searchcreditcard").click(function () {
            //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
            $.ajax({
                url: '<%= Url.Action("SearchCreditCard","BankManager") %>',
                type: "GET",
                success: function (data) {
                    $("#main-content").html(data);
                }
            });
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#newdebitcard").click(function () {
            //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
            $.ajax({
                url: '<%= Url.Action("CheckDebitCardRegister","BankManager") %>',
                type: "GET",
                success: function (data) {
                    $("#main-content").html(data);
                }
            });
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#newcreditcard").click(function () {
            //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
            $.ajax({
                url: '<%= Url.Action("CheckCreditCardRegister","BankManager") %>',
                type: "GET",
                success: function (data) {
                    $("#main-content").html(data);
                }
            });
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#plusmoneydebitcardin").click(function () {
            //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
            $.ajax({
                url: '<%= Url.Action("PlusMoneyDebitCardIn","BankManager") %>',
                type: "GET",
                success: function (data) {
                    $("#main-content").html(data);
                }
            });
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#tranfersmoneyatmtoatm").click(function () {
            //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
            $.ajax({
                url: '<%= Url.Action("Tranfers_ATM_to_ATM","BankManager") %>',
                type: "GET",
                success: function (data) {
                    $("#main-content").html(data);
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $("#tranfersmoneyatmtoperson").click(function () {
            //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
            $.ajax({
                url: '<%= Url.Action("Tranfers_ATM_to_Person","BankManager") %>',
                type: "GET",
                success: function (data) {
                    $("#main-content").html(data);
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $("#tranfersmoneypersontoperson").click(function () {
            //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
            $.ajax({
                url: '<%= Url.Action("Tranfers_Person_to_Person","BankManager") %>',
                type: "GET",
                success: function (data) {
                    $("#main-content").html(data);
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $("#PersonGetMoney_Tranfers_ATM_to_Person").click(function () {
            //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
            $.ajax({
                url: '<%= Url.Action("PersonGetMoney_Tranfers_ATM_to_Person","BankManager") %>',
                type: "GET",
                success: function (data) {
                    $("#main-content").html(data);
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $("#PersonGetMoney_Tranfers_Person_to_Person").click(function () {
            //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
            $.ajax({
                url: '<%= Url.Action("PersonGetMoney_Tranfers_Person_to_Person","BankManager") %>',
                type: "GET",
                success: function (data) {
                    $("#main-content").html(data);
                }
            });
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#loadcreditcard").click(function () {
            $("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
            $.ajax({
                url: '<%= Url.Action("LoanCreditCard","BankManager") %>',
                type: "GET",
                success: function (data) {
                    $("#main-content").html(data);
                }
            });
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#returnloan").click(function () {
            $("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
            $.ajax({
                url: '<%= Url.Action("ReturnRateCreditCard","BankManager") %>',
                type: "Post",
                success: function (data) {
                    $("#main-content").html(data);
                }
            });
        });
    });
</script>