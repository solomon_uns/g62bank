﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.CreditCardLoan>" %>

<% Html.ValidationSummary(); %>
<% Html.EnableClientValidation();%>

<script type"text/javascript">
    function fnLoad() {
        $.ajax({
            url: '<%= Url.Action("CheckOverCreditCard","BankManager") %>',
            type: "POST",
            data: { code: $("#LoanCode").val() },
            success: function (_data) {
                if (_data.toString() != "false" && _data.toString() != "0") {
                    var text = document.getElementById('PayReamining');
                    text.value = _data;
                } else {
                }
            }
        });
    }
    window.load = fnLoad();
</script>
<div class="row-fluid">
    <div class="span12">
        <div class="page-header">
            <h4>Trả lãi vay CreditCard</h4>
        </div>

        <%using (Html.BeginForm("PlusMoneyDebitCardIn", "BankManager", FormMethod.Post, new { @class = "form-horizontal seperator", @id="frm"}))
            {%>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span4" style="color:Red; text-decoration:none;" id="mes"></label>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" >Mã vay:</label>
                        <%=Html.TextBoxFor(p => p.LoanCode, new { @class = "span4 text", @readonly = "readonly" })%>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" >Ngày vay:</label>
                        <%=Html.TextBoxFor(p => p.Day, new { @class = "span4 text", @readonly = "readonly" })%>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" >Số tiền vay:</label>
                        <%=Html.TextBoxFor(p => p.Money, new { @class = "span4 text", @readonly = "readonly" })%>
                    </div>
                </div>
                </div>
  
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" >Số ngày duy trì không tính lãi:</label>
                        <%=Html.TextBoxFor(p => p.NumberDayLimt, new { @class = "span4 text", @readonly = "readonly" })%>
                    </div>                </div>
            </div> 
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Số ngày còn lại:</label>
                            <%=Html.TextBoxFor(p => p.NumberDayRemaining, new { @class = "span4 text", @readonly = "readonly" })%>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Số Tiền Đã Trả:</label>
                            <%=Html.TextBoxFor(p => p.Pay, new { @class = "span4 text", @readonly = "readonly" })%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Số Tiền còn lại:</label>
                            <input class="span4 text" data-val="true" data-val-date="The field DayLimit must be a date." id="PayReamining" name="PayReamining" readonly="readonly" type="text">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Ngày hết hạn Không lãi:</label>
                            <%=Html.TextBoxFor(p => p.DayLimit , new { @class = "span4 text", @readonly = "readonly" })%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Lãi suất nếu tính lãi:</label>
                            <%=Html.TextBoxFor(p => p.Rate, new { @class = "span4 text", @readonly = "readonly" })%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Trạng thái:</label>
                            <%if (Model.Status == 0)
                              {%>
                                <input type="text" class="span4 text" value="Chưa thanh toán" />
                            <% }
                              else
                              {
                                  if (Model.Status == 1)
                                  {%>
                                    <input type="text" class="span4 text" value="Đã thanh toán 1 phần" />
                                <%}
                                  else
                                  { %>
                                    <input type="text" class="span4 text" value="Đã thanh toáns" />
                                    <%} %>
                            <%} %>

                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Cách trả lãi :</label>
                            <span><button type="button" class="btn btn-info marginR10" onclick="Return5();">Trả 5%</button></span><span><button type="button" class="btn btn-info marginR10" onclick="ReturnFull();">Trả Hết</button></span><span><button type="button" class="btn btn-info marginR10" onclick="ReturnOther();">Khác</button></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Số tiền :</label>
                            <input type="text" class="span4 text" value="" id="moneyreturn" readonly="readonly"/>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">        
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                        <div class="span3"></div>
                        <div class="span7 controls">
                            <img src="../../Content/Icon/help-3-24.ico" title="Xem công thức tính lãi suất" onclick="ShowMessages();"/>
                            <button type="button" class="btn btn-info marginR10" onclick="fnSave();">Save changes</button>
                        </div>
                        </div>
                    </div>
                </div>   
            </div>


        <%} %>
    </div><!-- End .span12 -->
</div>
<div id="NoteDialog21" title="" class="Hidden"></div>
<script type="text/javascript">
    $(function () {
        $("#NoteDialog21").dialog({
            autoOpen: false, width: 400, height: 250, modal: true,
            buttons: {
                Cancel: function () { $("#NoteDialog21").dialog("close"); }
            }
        });
    });
</script>
<script type="text/javascript">
    function ShowMessages() {
        $("#NoteDialog21").html("Không thể thực hiện thao tác").dialog("option", "title", "Cách Tính Lãi Suất");
        $("#NoteDialog21").dialog("open"); 
    }
        </script>
<script type="text/javascript">
    function Return5() {
        var money = parseFloat($("#PayReamining").val());
        var mg = (5 * money)/100;
        var textbox1 = document.getElementById('moneyreturn');
        textbox1.value = mg;
        document.getElementById("moneyreturn").readOnly = true;
    }
</script>
<script type="text/javascript">
    function ReturnFull() {
        var money = parseFloat($("#PayReamining").val());
        var textbox2 = document.getElementById('moneyreturn');
        textbox2.value = money;
        document.getElementById("moneyreturn").readOnly = true;
    }
</script>
<script type="text/javascript">
    function ReturnOther() {
        var money = <%=Model.PayRemaining %>;
        var mg = (5 * money)/100;
        var textbox3 = document.getElementById('moneyreturn');
        textbox3.value = mg;
        document.getElementById("moneyreturn").readOnly = false;
    }
</script>
<script type="text/javascript">
    function fnSave() {
        $.ajax({
            url: '<%= Url.Action("ReturnRateCreditCard_Path_Over","BankManager") %>',
            type: "POST",
            data: { code: $("#LoanCode").val(), money: $("#moneyreturn").val() },
            success: function (data) {
                if (data.toString() != "true") {
                    $("#NoteDialog21").html("Không thể thực hiện thao tác").dialog("option", "title", "Cách Tính Lãi Suất");
                    $("#NoteDialog21").dialog("open");
                } else {
                    $("#NoteDialog21").html("Đã trả lại xong!").dialog("option", "title", "Cách Tính Lãi Suất");
                    $("#NoteDialog21").dialog("open");
                }
            }
        });
    }
</script>
