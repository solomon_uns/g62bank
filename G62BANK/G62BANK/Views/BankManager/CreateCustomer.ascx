﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.Customer>" %>

<div class="row-fluid">
    <div class="span12">
        <div class="page-header">
            <h4>Đăng ký mới khách hàng</h4>
        </div>

        <%using (Html.BeginForm("CreateCustomer", "BankManager", FormMethod.Post, new { @class = "form-horizontal seperator", @id="frm"}))
            {%>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span4" style="color:Red; text-decoration:none;" id="mes"></label>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" >Tên đăng nhập:</label>
                        <%=Html.TextBoxFor(p => p.UserName, new { @class = "span4 text", @OnChange = "CheckCustomer(this.value);" })%>
                    </div>
                    <div class="row-fluid" style="text-align:left;color:Red;" id="mes-username">
                        <%=Html.ValidationMessageFor(p => p.UserName)%>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" >Họ Tên:</label>
                        <%=Html.TextBoxFor(p => p.FullName, new { @class = "span4 text"})%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.FullName)%>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" >Email:</label>
                        <%=Html.TextBoxFor(p => p.Email, new { @class = "span4 text"})%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.Email)%>
                    </div>
                </div>
                </div>
  
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3">Điện Thoại:</label>
                        <%=Html.TextBoxFor(p => p.Phone, new { @class = "span4 text" })%>
                    </div>
                    <div class="row-fluid" style="text-align:center;color:Red;">
                        <%=Html.ValidationMessageFor(p => p.Phone)%>
                    </div>
                </div>
            </div> 
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Ngày Sinh (mm/dd/yyyy):</label>
                            <%=Html.TextBoxFor(p => p.Birthday, new { @class = "span4 text"})%>
                        </div>
                        <div class="row-fluid" style="text-align:center;color:Red;">
                            <%=Html.ValidationMessageFor(p => p.Birthday)%>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >CMND:</label>
                            <%=Html.TextBoxFor(p => p.CustomerID, new { @class = "span4 text" })%>
                        </div>
                        <div class="row-fluid" style="text-align:center;color:Red;">
                            <%=Html.ValidationMessageFor(p => p.CustomerID)%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <label class="form-label span3" >Địa Chỉ:</label>
                            <%=Html.TextBoxFor(p => p.Address, new { @class = "span4 text" })%>
                        </div>
                        <div class="row-fluid" style="text-align:center;color:Red;">
                            <%=Html.ValidationMessageFor(p => p.Address)%>
                        </div>
                    </div>
                </div>
            </div>               
            <div class="form-row row-fluid">        
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                        <div class="span3"></div>
                        <div class="span7 controls">
                            <img src="../../Content/Icon/help-3-24.ico" title="hướng dẫn" onclick="ShowMessages();"/>
                            <button type="button" class="btn btn-info marginR10" onclick="fnSave();">Save changes</button>
                        </div>
                        </div>
                    </div>
                </div>   
            </div>


        <%} %>
    </div><!-- End .span12 -->
</div>
<div id="NoteDialog1" title="" class="Hidden"></div>
<script type="text/javascript">
    $(function () {
        $("#NoteDialog1").dialog({
            autoOpen: false, width: 800, height: 500, modal: true,
            buttons: {
                Cancel: function () { $("#NoteDialog1").dialog("close"); }
            }
        });
    });
</script>
<script type="text/javascript">
    function CheckCustomer(_code) {
        $.ajax({
            url: '<%= Url.Action("CheckUserName","BankCustomer") %>',
            type: "GET",
            data: { username: _code },
            success: function (data) {
                if (data.toString() != "true") {
                    $("#mes-username").html(data);
                }
                else {
                    $("#mes-username").html("");
                }
            }
        });
    }
</script>
<script type="text/javascript">
    function fnSave() {
        $.ajax({
            url: '<%= Url.Action("CreateCustomer","BankManager") %>',
            type: "POST",
            data: $("#frm").serialize(),
            success: function (data) {
                if (data.toString() == "false") {
                    $("#mes").html("Thông Tin Không Hợp Lệ");
                } else {
                    $("#NoteDialog1").html("Tạo mới khách hàng thành công").dialog("option", "title", "Thông Báo");
                    $("#NoteDialog1").dialog("open");
                    $("#mes").html("");
                    $("#mes-username").html("");
                }
            }
        });
    }
</script>