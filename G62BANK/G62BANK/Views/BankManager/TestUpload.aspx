﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>TestUpload</title>
    <link href="../../Content/Manager/css/changeavatar.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-asyncUpload-0.1.js" type="text/javascript"></script>
    <script src="../../Scripts/swfupload.js" type="text/javascript"></script>
</head>
<body>
    <div>
        <input type="file" id="yourID" name="yourID" />
<script>
    $(function () {
        $("#yourID").makeAsyncUploader({
            upload_url: "/BankManager/AsyncUpload", // Important! This isn't a directory, it's a HANDLER such as an ASP.NET MVC action method, or a PHP file, or a Classic ASP file, or an ASP.NET .ASHX handler. The handler should save the file to disk (or database).
            flash_url: '/Scripts/swfupload.swf',
            button_image_url: '/Scripts/blankButton.png'
        });
    });
</script>
    </div>
</body>
</html>
