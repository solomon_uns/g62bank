﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<G62BANK.Models.Customer>>" %>

<script src="../../Scripts/jquery.tablesorter.js" type="text/javascript"></script>
<script src="../../Scripts/jquery.tablesorter.pager.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        $("table.tablesorter").tablesorter({ widthFixed: true, sortList: [[0, 0]] })
        .tablesorterPager({ container: $("#pager"), size: $(".pagesize option:selected").val() });
    });
</script>
<div>
    <table class="tablesorter">
        <thead>
            <tr>
                <th>UserName</th>
                <th>FullName</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Address</th>
                <th>Birthday</th>
                <th>Delete</th>
                <th>Reset Password</th>
            </tr>
        </thead>
        <tbody>
            <%for(int i = 0 ; i < Model.Count(); i++) {%>      
                <tr>
                    <td style="cursor:pointer;" onclick="sm_username('<%=Model[i].CustomerID%>');" ><%=Html.DisplayFor(p=>p[i].UserName)%></td>
                    <td><%=Html.DisplayFor(p=>p[i].FullName)%></td>
                    <td><%=Html.DisplayFor(p=>p[i].Phone)%></td>
                    <td><%=Html.DisplayFor(p=>p[i].Email)%></td>
                    <td><%=Html.DisplayFor(p=>p[i].Address)%></td>
                    <td><%=Html.DisplayFor(p=>p[i].Birthday)%></td>
                    <td style="cursor:pointer;" onclick="DeleteUser('<%=Model[i].UserName%>');"><img src="../../Content/Icon/user-12-24.ico"/></td>
                    <td style="cursor:pointer;" onclick="ResetPasswordUser('<%=Model[i].UserName%>');"><img src="../../Content/Icon/refresh-4-24.ico"/></td>
                </tr>
                <% }
            %>
        </tbody>
    </table>
    <p></p>
    <div id="pager" style="position: inherit;">
        <img src="../../Content/Manager/img/first.png" class="first" />
        <img src="../../Content/Manager/img/prev.png" class="prev" />
        <input type="text" class="pagedisplay" />
        <img src="../../Content/Manager/img/next.png" class="next" />
        <img src="../../Content/Manager/img/last.png" class="last"/>
        <select class="pagesize" style="width: 150px;">
            <option selected="selected" value="5">5</option>
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="30">30</option>
            <option value="40">40</option>
        </select>
    </div>
<%--
    <div style="float:right; margin-right:5px;">
        <button type="button" class="btn btn-info marginR10" onclick="add_customer();">Create Customer</button>
    </div>--%>
    <div id="add-edit-user"></div>  
</div>
<div id="NoteDialog10" title="" class="Hidden"></div>
<div id="NoteDialog11" title="" class="Hidden"></div>
<div id="NoteDialog12" title="" class="Hidden"></div>
<script type="text/javascript">
    $(function () {
        $("#NoteDialog10").dialog({
            autoOpen: false, width: 800, height: 600, modal: true,
            buttons: {
                Cancel: function () { $("#NoteDialog10").dialog("close"); }
            }
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#NoteDialog11").dialog({
            autoOpen: false, width: 400, height: 250, modal: true,
            buttons: {
                "Save": function () {
                        $.ajax({
                        url: '<%= Url.Action("AcceptDeleteCustomer","BankManager") %>',
                        type: "GET",
                        data: { username: $("#us").val() },
                        success: function (data) {
                            if (data.toString() == "true") {
                                $("#NoteDialog11").dialog("close");
                                window.location.href = "BankManager?link=CustomerManager";
                            }
                            else
                            { $("#mes").html("Không thể xóa User Này!"); }
                        }
                    });
                },
                Cancel: function () { $("#NoteDialog11").dialog("close"); }
            }
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#NoteDialog12").dialog({
            autoOpen: false, width: 400, height: 250, modal: true,
            buttons: {
                "Save": function () {
                    $.ajax({
                        url: '<%= Url.Action("AcceptResetPasswordCustomer","BankManager") %>',
                        type: "GET",
                        data: { username: $("#us").val() },
                        success: function (data) {
                            if (data.toString() == "true") {
                                $("#NoteDialog12").dialog("close");
                                alert("Rest Thành Công!");
                            }
                            else
                            { $("#mes").html("Không thể Reset Password User Này!"); }
                        }
                    });
                },
                Cancel: function () { $("#NoteDialog12").dialog("close"); }
            }
        });
    });
</script>

<script type="text/javascript">
    function sm_username(theUrl) {
        $.ajax({
            url: '<%= Url.Action("CustomerReview","BankManager") %>',
            type: "Get",
            data: { CustomerID: theUrl },
            success: function (_data) {
                $("#NoteDialog10").html(_data.toString());
                $("#NoteDialog10").dialog("option", "title", "Details Customer");
                $("#NoteDialog10").dialog("open");
            }
        });
    }
</script> 
<script type="text/javascript">
    function DeleteUser(username) {
        $("#NoteDialog11").html("")
                    .dialog("option", "title", "Delete User")
                    .load("/BankManager/BeforeDelete?username=" + username, function () { $("#NoteDialog11").dialog("open"); });
    }
        </script>
<script type="text/javascript">
    function ResetPasswordUser(username) {
        $("#NoteDialog12").html("")
                    .dialog("option", "title", "Reset Password User")
                    .load("/BankManager/BeforeResetPassword?username=" + username, function () { $("#NoteDialog12").dialog("open"); });
    }
        </script>

<style type="text/css">
        body {
    font-size: 75%;
    font-family: Verdana, Tahoma, Arial, "Helvetica Neue", Helvetica, Sans-Serif;
    color: #232323;
    background-color: #fff;
}
table {width:100%; border-spacing:0; border:1px solid gray;}
table.tablesorter thead tr .header {
	background-image: url(images/bg.png);
	background-repeat: no-repeat;
	background-position: center right;
	cursor: pointer;
}
table.tablesorter tbody td {
	color: #3D3D3D;
	padding: 4px;
	background-color: #FFF;
	vertical-align: middle;
}
table.tablesorter tbody tr.odd td {
	background-color:#F0F0F6;
}
table.tablesorter thead tr .headerSortUp {
	background-image: url(images/asc.png);
}
table.tablesorter thead tr .headerSortDown {
	background-image: url(images/desc.png);
}
table th { width:150px; 
           border:1px outset gray; 
           background-color:#3C78B5; 
           color:White; 
           cursor:pointer;
}
table thead th:hover { background-color:Yellow; color:Black;}
table td {border:1px solid gray; text-align:center;}
#pager{float:left;}
    </style>