﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

  
<div class="centerContent">         
    <ul class="bigBtnIcon">
        <li id= "new">
            <a class="tipB" title="Mở Số Tiết Kiệm">
                <span class="icon"><img src="../../Content/Icon/book-10-24.ico" style="margin-left:10px;margin-right:10px;"/></span>
                <span class="txt">Open SavingBook</span>
                <%--<span class="notification"><%=Html.DisplayFor(p=>p.NewDebitCard) %></span>--%>
            </a>
        </li>
        <li id= "search">
            <a class="tipB" title="Quản Lý Sổ Tiết Kiệm">
                <span class="icon"><img src="../../Content/Icon/book-10-24.ico" style="margin-left:10px;margin-right:10px;"/></span>
                <span class="txt">SavingBook Action</span>
<%--                <span class="notification"><%=Html.DisplayFor(p=>p.NewCreditCard) %></span>--%>
            </a>
        </li>
    </ul>
</div>


<script type="text/javascript">
    $(function () {
        $("#new").click(function () {
            //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
            $.ajax({
                url: '<%= Url.Action("OpenSavingBook","BankManager") %>',
                type: "GET",
                success: function (data) {
                    $("#main-content").html(data);
                }
            });
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#search").click(function () {
            //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
            $.ajax({
                url: '<%= Url.Action("SearchSavingBook","BankManager") %>',
                type: "GET",
                success: function (data) {
                    $("#main-content").html(data);
                }
            });
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#getmoney").click(function () {
            //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
            $.ajax({
                url: '<%= Url.Action("GetMoneySavingBook","BankManager") %>',
                type: "GET",
                success: function (data) {
                    $("#main-content").html(data);
                }
            });
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#settle").click(function () {
            //$("#main-content").html("<img src='../../Content/images/loading.gif' style='width=100%;margin:auto;'/>");
            $.ajax({
                url: '<%= Url.Action("SettleSavingBook","BankManager") %>',
                type: "GET",
                success: function (data) {
                    $("#main-content").html(data);
                }
            });
        });
    });
</script>
