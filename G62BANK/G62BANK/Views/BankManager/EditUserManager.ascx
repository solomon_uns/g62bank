﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.UserManager>" %>

<% Html.ValidationSummary(); %>
<% Html.EnableClientValidation();%>
<script type="text/javascript">
    function codeAddress() {
        $('#RoleCode').val('<%=Model.RoleCode%>');  //try this
    }
    window.onload = codeAddress();
    </script>
<div class="row-fluid">
                    <div class="span12">
                        <%using (Html.BeginForm("AddUserManager", "BankManager", FormMethod.Post, new { @class = "form-horizontal seperator", @id="frm", enctype = "multipart/form-data" }))
                          {%>
                          <div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <label class="form-label span4" for="username" style="color:Red; text-decoration:none;" id= "mes"><%=Html.ValidationSummary() %></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <label class="form-label span3" for="username">Username:</label>
                                        <%=Html.TextBoxFor(p => p.UserName, new { @class = "span4 text",@readonly="readonly"})%>
                                        <%=Html.ValidationMessageFor(p=>p.UserName ) %>
                                    </div>
                                </div>
                            </div>
                            <%--<div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <label class="form-label span3" for="username">Avatar:</label> 
                                            <img src="../../Content/Manager/img/noavatar.png" alt="" class="image marginR10" style="width:150px;height:150px;"/>
                                            <div class="uploader" id="uniform-file">
                                                <%--<input type="file" name="fileinput" id="file"/>
                                                <%=Html.TextBoxFor(p => p.Avatar, new { @id = "Avatar", @name = "Avatar", @type = "file" })%>
                                                <span class="filename"></span>
                                                <span class="action">Choose File</span>
                                            </div>
                                    </div>
                                </div>
                            </div>--%>

                            <div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <%=Html.LabelFor(p => p.FullName, new { @class = "form-label span3" })%>
                                        <%=Html.TextBoxFor(p => p.FullName, new { @class = "span4 text" })%>
                                        
                                    </div>
                                    <div class="row-fluid" style="text-align:left;color:Red;">
                                        <%=Html.ValidationMessageFor(p => p.FullName)%>
                                    </div>

                                </div>
                            </div>

                            <div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <%=Html.LabelFor(p => p.Email, new { @class = "form-label span3" })%>
                                        <%=Html.TextBoxFor(p => p.Email, new { @class = "span4 text" })%>
                                    </div>
                                    <div class="row-fluid" style="text-align:center;color:Red;">
                                        <%=Html.ValidationMessageFor(p => p.Email)%>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <div class="row-fluid">
                                            <%=Html.LabelFor(p => p.Birthday, new { @class = "form-label span3" })%>
                                            <%=Html.TextBoxFor(p => p.Birthday, "{0:MM/dd/yyyy}", new { @class = "span4 text" })%>
                                        </div>
                                        <div class="row-fluid" style="text-align:center;color:Red;">
                                            <%=Html.ValidationMessageFor(p => p.Birthday)%>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <%=Html.LabelFor(p => p.Phone, new { @class = "form-label span3" })%>
                                        <%=Html.TextBoxFor(p => p.Phone, new { @class = "span4 text" })%>
                                    </div>
                                    <div class="row-fluid" style="text-align:center;color:Red;">
                                        <%=Html.ValidationMessageFor(p => p.Phone)%>
                                    </div>
                                </div>
                                </div>
  
                            <div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <%=Html.LabelFor(p => p.Address, new { @class = "form-label span3" })%>
                                        <%=Html.TextBoxFor(p => p.Address, new { @class = "span4 text" })%>
                                    </div>
                                    <div class="row-fluid" style="text-align:center;color:Red;">
                                        <%=Html.ValidationMessageFor(p => p.Address)%>
                                    </div>
                                </div>
                            </div>  
                            <div class="form-row row-fluid">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <label class="form-label span3" for="Role">Role</label>
                                        <%Html.RenderAction("ListRole", "BankManager");%>
                                    </div>
                                </div>
                            </div>
                            <%--<div class="form-row row-fluid">        
                                <div class="span12">
                                    <div class="row-fluid">
                                        <div class="form-actions">
                                        <div class="span3"></div>
                                        <div class="span4 controls">
                                            <button type="button" class="btn btn-info marginR10" onclick ="Save();">Save changes</button>
                                            <button type="button" class="btn btn-danger" onclick ="Cancel();">Cancel</button>
                                        </div>
                                        </div>
                                    </div>
                                </div>   
                            </div>--%>


                      <%} %>
                    </div><!-- End .span12 -->
                </div>

<%--<script type="text/javascript">
    function Save() {
        $.ajax({
            url: '<%= Url.Action("AddUserManager","BankManager") %>',
            type: "POST",
            data: $("#frm").serialize(),
            success: function (data) {
                if (data.toString() == "true") {
                    $.ajax({
                        url: '<%= Url.Action("StaffManager","BankManager") %>',
                        type: "GET",
                        success: function (_data) {
                            $("#main-content").html(_data);
                        }
                    });
                }
                else
                { $("#mes").html("Thông tin không hợp lệ!"); }
            }
        });
    }
</script>
<script type="text/javascript">
    function Cancel() {
        $("#add-edit-user").html("");
    }
</script>--%>