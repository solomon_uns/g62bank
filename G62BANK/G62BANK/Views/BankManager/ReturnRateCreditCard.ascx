﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<G62BANK.Models.SearchReturnRateCreditCard>" %>


<div class="contentwrapper"><!--Content wrapper-->
<%using (Html.BeginForm("SearchSavingBookResult", "BankManager", FormMethod.Post, new { @class = "form-horizontal seperator", @id = "frm" }))
  {%>
    
    <div class="row-fluid">
        <div class="span4">
            <div class="box">
                <div class="title">
                    <h4>
                        <span class="icon16 icomoon-icon-equalizer-2"></span>
                        <span>AccountCode</span>
                    </h4>
                </div>
                <div class="content">
                    <p><%=Html.TextBoxFor(p => p.AccountCode)%></p>
                </div>
            </div><!-- End .box -->
        </div><!-- End .span4 -->

        <div class="span4">
            <div class="box">
                <div class="title">
                    <h4>
                        <span class="icon16 icomoon-icon-equalizer-2"></span>
                        <span>CreditCardCode</span>
                    </h4>
                </div>
                <div class="content">
                    <p><%=Html.TextBoxFor(p => p.CreditCardCode)%></p>
                </div>
            </div><!-- End .box -->
        </div><!-- End .span4 -->

        <div class="span4">
            <div class="box">
                <div class="title">
                    <h4>
                        <span class="icon16 icomoon-icon-equalizer-2"></span>
                        <span>ATMAccountCode</span>
                    </h4>
                </div>
                <div class="content">
                    <p><%=Html.TextBoxFor(p => p.ATMAccountCode)%></p>
                </div>
            </div><!-- End .box -->
        </div><!-- End .span4 -->
    </div><!-- End .row-fluid -->

    <div class="row-fluid">                      
        <div class="span12">
            <div class="box">
                <div class="title">
                    <h4>
                        <span class="icon16 icomoon-icon-equalizer-2"></span>
                        <span>ReturnRate CreditCard</span>
                    </h4>
                </div>
                <div class="content">
                    <p><button type="button" class="btn btn-info marginR10" onclick="Search();">Search <img src="../../Content/Icon/magnifier-4-24.ico"/></button></p>
                </div>
            </div><!-- End .box -->
        </div><!-- End .span12 -->
    </div><!-- End .row-fluid -->
    <%} %>
    <div id="searchresult">                      
    </div><!-- End .row-fluid -->   
<!-- Page end here -->
</div>
<script type="text/javascript">
    function Search() {
        $.ajax({
            url: '<%= Url.Action("SearchReturnRateCreditCard_Result","BankManager") %>',
            type: "POST",
            data: $("#frm").serialize(),
            success: function (data) {
                $("#searchresult").html(data);
            }
        });
    }
</script>
