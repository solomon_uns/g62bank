﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<string>>" %>
<script type="text/javascript" src="../../Scripts/jquery-1.7.1.min.js"></script>    
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.cycle/2.99/jquery.cycle.all.js"></script>    
<script type="text/javascript">
    $(document).ready(function () { $('.slideshow').cycle({ metaAttr: 'cycle', delay: 0, fx: 'fade' }); });    </script>
<style>
    #wrapper
    {
      height:240px;
      width:100%;
      overflow:hidden;
      position:relative;
    }
    #wrapper ul
    {
      margin:0;
      padding:0;
    }
    #wrapper ul li
    {
      float:left;
      list-style-type:none;
    }
    #wrapper img
    {
    	width:99%;
    	height:96%;
    	margin: 5 5 5 5;
    	-moz-border-radius: 10px; 
        -webkit-border-radius: 10px; 
        -khtml-border-radius: 10px;
    }
</style>     
<div id="wrapper" class="slideshow">

    <%for (int i=0; i < Model.Count; i++)
      {
          string str = Model[i].ToString();%>
      <img src="<%=str.ToString()%>"/>
    <% }%>

</div>

