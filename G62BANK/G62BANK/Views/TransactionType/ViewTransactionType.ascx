﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<G62BANK.Models.TransactionType>>" %>

<table summary="Submitted table designs" cellspacing="1" cellpadding="1" border="0" width="238px" style="border-collapse: collapse;
                margin:0 auto; clear:left;">
    <thead>
        <tr>
            <th scope="col" style="border: 1px solid #C9D9F3; border-collapse: collapse;">Tên Loại</th>
            <th scope="col" style="border: 1px solid #C9D9F3; border-collapse: collapse;">Phí dịch vụ</th>
            <th scope="col" style="border: 1px solid #C9D9F3; border-collapse: collapse;">Mô tả</th>
        </tr>
    </thead>

    <tbody>
        <%for(int i = 0 ; i < Model.Count(); i++) 
        {%>
            <tr>
                <td style="border: 1px solid #C9D9F3; border-collapse: collapse;"><%=Html.DisplayFor(p=>p[i].TransactionTypeName)%></td>
                <td style="width:30%;border: 1px solid #C9D9F3; border-collapse: collapse;"><%=Html.DisplayFor(p=>p[i].Cost)%></td>
                <td style="width:30%;border: 1px solid #C9D9F3; border-collapse: collapse;"><%=Html.DisplayFor(p=>p[i].Description)%></td>
            </tr>
        <%}%>
        <tr>
            <td style="border: 0px solid #C9D9F3; border-collapse: collapse;" colspan="3"><a href="#" style="cursor:pointer;">Xem chi tiết</a></td>
        </tr>
    </tbody>
</table>