﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace G62Service
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service1 : System.Web.Services.WebService
    {
        public G62DBDataContext ServerLinq = new G62DBDataContext();
        public string CreateCode()
        {
            return DateTime.Now.ToString("yyyyddMMHHmmssff");
        }
        [WebMethod]
        public ATMAccount ViewAccountATM(string code, string pass)
        {
            try
            {
                ServerLinq = new G62DBDataContext();
                Account acc = ServerLinq.Accounts.SingleOrDefault(p => p.AccountCode == code && p.PinCode == pass && p.AccountTypeCode == 1);
                ATMAccount atm = ServerLinq.ATMAccounts.SingleOrDefault(p => p.AccountCode == acc.AccountCode);
                return atm;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        [WebMethod]
        public bool TranfersMoneyToAccount(string code1, string pass, string code2, double money)
        {
            try
            {
                if (money >= 50000)
                {
                    ServerLinq = new G62DBDataContext();
                    Account acc1 = ServerLinq.Accounts.SingleOrDefault(p => p.AccountCode == code1 && p.PinCode == pass && p.AccountTypeCode == 1);
                    ATMAccount atm = ServerLinq.ATMAccounts.SingleOrDefault(p => p.AccountCode == acc1.AccountCode && p.ATMAccountTypeCode == 1);
                    Account acc2 = ServerLinq.Accounts.SingleOrDefault(p => p.AccountCode == code2 && p.AccountTypeCode == 1);
                    ATMAccount atm2 = ServerLinq.ATMAccounts.SingleOrDefault(p => p.AccountCode == acc2.AccountCode && p.ATMAccountTypeCode == 1);
                    // tính phí
                    TransactionType tran = ServerLinq.TransactionTypes.SingleOrDefault(p => p.TransactionTypeCode == 12);

                    double cost = (tran.Cost * money) / 100;

                    double enmoney = money + cost;

                    if (atm.Balance - 50000 >= enmoney)
                    {
                        // cộng phí ngân hàng
                        Bank bank = ServerLinq.Banks.SingleOrDefault(p => p.BankCode == "62");
                        bank.Profit_ATM_DebitCard += cost;
                        bank.SumProfit += cost;
                        // chuyển tiền các tài khoản
                        atm.Balance -= enmoney;
                        atm2.Balance += money;

                        // lưu nhật ký
                        DebitCardTransaction de = new DebitCardTransaction();
                        de.Balance = atm.Balance;
                        de.Day = DateTime.Now;
                        de.DebitCardTransactionCode = CreateCode();
                        de.Description = "Chuyển Tiền Vào Tài Khoản Thẻ Khác";
                        de.FromAccountCode = acc1.AccountCode;
                        de.FromATMAccountCode = atm.ATMAcountCode;
                        de.FromDebitCardCode = ServerLinq.DebitCards.SingleOrDefault(p => p.ATMAccountCode == atm.ATMAcountCode).DebitCardCode;
                        de.Money = money;
                        de.Status = 1;
                        de.SumCost = cost;
                        de.SumMoney = cost + money;
                        de.ToAccountCode = acc2.AccountCode;
                        de.ToATMAccountCode = atm2.ATMAcountCode;
                        de.TransactionTypeCode = tran.TransactionTypeCode;
                        de.TransactionTypeName = tran.TransactionTypeName;

                        ServerLinq.DebitCardTransactions.InsertOnSubmit(de);
                        ServerLinq.SubmitChanges();
                        return true;
                    }
                    return false;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [WebMethod]
        public string TranfersMoneyToCustomer(string code1, string pass, string CustomerID, string CustomerName, double money)
        {
            try
            {
                if (money >= 50000)
                {
                    ServerLinq = new G62DBDataContext();
                    Account acc1 = ServerLinq.Accounts.SingleOrDefault(p => p.AccountCode == code1 && p.PinCode == pass && p.AccountTypeCode == 1);
                    ATMAccount atm = ServerLinq.ATMAccounts.SingleOrDefault(p => p.AccountCode == acc1.AccountCode && p.ATMAccountTypeCode == 1);

                    // tính phí
                    TransactionType tran = ServerLinq.TransactionTypes.SingleOrDefault(p => p.TransactionTypeCode == 13);

                    double cost = (tran.Cost * money) / 100;

                    double enmoney = money + cost;

                    if (atm.Balance - 50000 >= enmoney)
                    {
                        // cộng phí ngân hàng
                        Bank bank = ServerLinq.Banks.SingleOrDefault(p => p.BankCode == "62");
                        bank.Profit_ATM_DebitCard += cost;
                        bank.SumProfit += cost;
                        // chuyển tiền các tài khoản
                        atm.Balance -= enmoney;

                        // lưu nhật ký
                        DebitCardTransaction de = new DebitCardTransaction();
                        de.Balance = atm.Balance;
                        de.Day = DateTime.Now;
                        de.DebitCardTransactionCode = CreateCode();
                        de.Description = "Chuyển Tiền Từ Tài Khoản Thẻ Cho Cá Nhân";
                        de.FromAccountCode = acc1.AccountCode;
                        de.FromATMAccountCode = atm.ATMAcountCode;
                        de.FromDebitCardCode = ServerLinq.DebitCards.SingleOrDefault(p => p.ATMAccountCode == atm.ATMAcountCode).DebitCardCode;
                        de.Money = money;
                        de.Status = 0;
                        de.SumCost = cost;
                        de.SumMoney = cost + money;
                        de.ToCustomerID = CustomerID;
                        de.ToCustomerName = CustomerName;
                        de.TransactionTypeCode = tran.TransactionTypeCode;
                        de.TransactionTypeName = tran.TransactionTypeName;

                        ServerLinq.DebitCardTransactions.InsertOnSubmit(de);

                        ServerLinq.SubmitChanges();
                        return de.DebitCardTransactionCode.ToString();
                    }
                    return "false";
                }
                return "false";
            }
            catch (Exception)
            {
                return "false";
            }
        }
    
    }

}